<?php
define('main_gen', 'General Preferences');
define('main_block', 'Blocks Management');
define('main_coverage', 'Blocks Coverage');
define('main_video', 'Blocks video');
define('main_html', 'Block HTML');
define('main_article', 'Block League');
define('main_other', 'Block TopMatch');
define('main_mess', 'Back to the site');

define('mess_err01', 'Error : Warning, can\'t write in <br/><br/><b>kg_adm/preferences.php</b><br/> <b>kg_adm/cfg</b><br/> <b>kg_adm/cfg/pref.txt</b><br/><br/> Check file access permissions (CHMOD) !');
define('mess_err02', 'Error : Warning, can\'t write in  <br/><br/><b>kg_adm/blocks.php</b><br/> <b>kg_adm/cfg</b><br/> <b>kg_adm/cfg/blocks.txt</b> <br/><br/> Check file access permissions (CHMOD) !');
define('mess_err03', 'Error : Warning, can\'t write in  <br/><br/><b>kg_adm/video.php</b><br/> <b>kg_adm/cfg</b><br/> <b>kg_adm/cfg/video.txt</b> <br/><br/> Check file access permissions (CHMOD) !');
define('mess_err04', 'Error : Warning, can\'t write in  <br/><br/><b>kg_adm/html.php</b><br/> <b>kg_adm/cfg</b><br/> <b>kg_adm/cfg/html.txt</b> <br/><br/> Check file access permissions (CHMOD) !');
define('mess_err05', 'Error : Warning, can\'t write in  <br/><br/><b>kg_adm/article.php</b><br/> <b>kg_adm/cfg</b><br/> <b>kg_adm/cfg/article.txt</b> <br/><br/> Check file access permissions (CHMOD) !');
define('mess_err06', 'Error : Warning, can\'t write in  <br/><br/><b>kg_adm/topmatch.php</b><br/> <b>kg_adm/cfg</b><br/> <b>kg_adm/cfg/topmatch.txt</b> <br/><br/> Check file access permissions (CHMOD) !');
define('mess_err07', 'Error : Warning, can\'t write in  <br/><br/><b>kg_adm/slider.php</b><br/> <b>kg_adm/cfg</b><br/> <b>kg_adm/cfg/slider.txt</b><br/><br/><br/> Check file access permissions (CHMOD) !');

define('comm_title', 'Community');
define('comm_fb', 'Link Facebook :');
define('comm_steam', 'Link Steam :');
define('comm_tw', 'Link Twitter :');
define('comm_help', 'If the input is empty, the logo will not appear');

define('league', 'Location of the picture ');
define('league2', 'Website league :');

define('mess_ok', 'Configuration saved.');

define('page_mess', 'Back');
define('lang', 'Language');

define("pref_color","(STYLES)");
define("pref_color2", "Import website colors of new Style :");
define("pref_install", "Install");
define("pref_tag","Meta Tags");
define('pref_key', 'Key Words');
define("pref_desc","Website's description");
define("block_show","Display Navigation");
define("block_hide","Hide");
define('block_ac', 'Currently');
define("block_title", "Display, hide the following blocks");
define('block_display', 'Info : <b>Display</b> block left when navigation');

define("video_title", "Choose Your Player");
define('video_compa', 'Compatibility :');
define('video_for', 'For');
define('video_youtube', 'The link page');
define('video_flv', 'Location of the file to play');
define('video_daily', '');


define('html_title', 'Block HTML management');
define('html_title2', 'Block title :');
define('html_source', 'HTML Source :');

define('article_title', 'Choose Your block type');
define('article_tuto1', '<center><h1>Add an image to the latest article block</h1>(click to show)</center>');
define('article_tuto', '
<br/><br/>
<table width="80%" align="center">
<tr>
<td>
To add an image to your article, click on " Upload images "  :<br/><br/>
<img src="images/kg_admin/tuto/article1.jpg" alt="article" /><br/><br/>
The first image will be automaticaly placed in the block, the others will not appear. <br/><br/>
<img src="images/kg_admin/tuto/article2.jpg" alt="article" /><br/><br/>
Information : The image does not have to be placed in the textarea to appear.
</td>
</tr></table>
');

define('match_title', 'Informations regarding the block "Topmatch"');
define('match_titre', 'Select the clanwar which will appear :');
define('match_logo', ' Your team\'s name :');
define('match_myname', 'Enter your logo\'s link');


define('match_tuto1', '<center><h4>Add an image to the topmatch block</h4></a>(click to show)</center>');
define('match_tuto', '
<br/><br/>
<table width="80%" align="center">
<tr>
<td>
To add an image to your topmatch block, click on " Upload screens "  :<br/><br/>
<img src="images/kg_admin/tuto/clanwar1.jpg" alt="clanwar" /><br/><br/>
The first image will be automaticaly placed in the block, the others will appear on details. <br/><br/>
<img src="images/kg_admin/tuto/clanwar2.jpg" alt="clanwar" /><br/><br/>
Informations: <br/>- The image must have a white background.<br/>- The image\'s resolution has to be set at : 78px x 72px
</td>
</tr></table>
');

define('slide_title', 'Informations regarding the block "Coverage"');
define('slide_url', 'Location of the picture ');
define('slide_titre', 'Title :');

define('lang1', 'French');
define('lang2', 'English');

