<?php

// *****************************************************************//
// ** Theme evilGames  version 1.0           									   
// **													          
// ** Template provided by Kit-gaming.org               	    	              
//             				           							    
// ** http://www.kit-gaming.org  	     
// ** http://webspell.org                      						 
// *****************************************************************//


include("_mysql.php");
//require_once('_mysqli.php');
include("_settings.php");
include("_functions.php");

$_language->read_module('index');
$index_language = $_language->module;

include_once("kg_adm/cfg/blocks.txt");
include_once("kg_adm/cfg/pref.txt");

if($site != '') $type_affichage = '2';
if($_SESSION['language'] === 'fr') { $var_l = 'fr';  } 
else $var_l = 'en';

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title><?php echo PAGETITLE; ?></title>
<meta name="keywords" content="<?php echo $key; ?>" />
<meta name="description" content="<?php echo  stripslashes($desc); ?>" />
<link rel="stylesheet" media="screen" type="text/css" title="templates_kg" href="css/index.css" />
<link rel="stylesheet" media="screen" type="text/css" title="templates_kg" href="css/style_color.css" />
<link rel="stylesheet" media="screen" type="text/css" title="templates_kg" href="css/glide.css" />
<!--[if lt IE 8]>
<link rel="stylesheet" media="screen" type="text/css" title="templates_kg" href="css/ie.css" />
<![endif]-->

<script src="modules/video/swfobject.js" type="text/javascript"></script>
<script src="modules/js/jquery.js" type="text/javascript"></script>
<script src="modules/js/featuredcontentglider.js"type="text/javascript"></script>
<script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
<script src="js/bbcode.js" language="jscript" type="text/javascript"></script>
<script src="modules/js/contentslider.js" type="text/javascript"></script>
<script src="modules/js/jquery_notification_v.1.js" type="text/javascript"></script>
<link rel="stylesheet" media="screen" type="text/css" href="modules/js/jquery_notification.css" />

</head>

<body>



 
<!--<script type="text/javascript">
        showNotification({
            message: "Salut la meute, un message requiert toute votre attention sur <a href=\"http://www.meute-apax.fr/site/index.php?site=forum_topic&topic=108\">cette page</a>. Merci d'avance, Ben_ftwc",
            type: "success",
            autoClose: true,
            duration: 5                                        
        });
</script>      
 -->
<div id="main">

	<!-- HEADER -->
	<div id="header">
		<div class="fire"></div>
		<div id="language"><?php include_once('./blocks/language.php'); ?></div>
		<div id="community">
			<ul id="social">
			<?php 	if($tw != '') { ?><li><a href="<?php echo stripslashes($tw); ?>" id="twitter">Twitter</a></li><?php } 
			if($fb != '') { ?><li><a href="<?php echo stripslashes($fb); ?>" id="facebook">Facebook</a></li><?php } 
			if($steam != '') { ?><li><a href="<?php echo stripslashes($steam); ?>" id="steam">Twitter</a></li><?php } ?>
			</ul>
		</div><img  src="images/kg/header.png" alt="header" />
	</div>	

	<!-- LOGIN AREA -->	
	<div id="login">
		<?php include("blocks/kg_login.php"); ?>
		<?php include("blocks/kg_quicksearch.php"); ?>
	</div>
	
	<!-- MENU -->
	<div id="menu" <?php if($_SESSION['language'] == 'es') echo 'style="word-spacing: 10px;"'; ?> >
		<div class="fire"></div><?php include("blocks/kg_navigation.php"); ?>
	</div>

	
	<!-- COVERAGE -->
	<div id="coverage"><div class="fire"></div>
		<div  id="p-select2">
			<div class="cov_next"><a href="#" onfocus="this.blur();" class="next"><img class="next_cov" src="images/kg/next.png" width="49" height="39" alt="next" /></a></div>
			<div class="cov_preview" ><a href="#" onfocus="this.blur();" class="prev"><img  class="prev_cov" src="images/kg/prev.png" width="49" height="39" alt="prev" /></a></div>
			<div id="canadaprovinces2" class="glidecontentwrapper2">
			<?php include_once("kg_adm/cfg/slide.txt"); 
			echo stripslashes($cov1.$cov2.$cov3.$cov4.$cov5); 
			?> </div>
		</div>
		<script type="text/javascript">
		featuredcontentglider.init({gliderid: "canadaprovinces2",contentclass: "glidecontent2", togglerid: "p-select2", remotecontent: "", selected: 0, persiststate: true, speed: 400, direction: "rightleft", autorotate: true, autorotateconfig: [20000, 10] })
		</script>	
	</div>
	
	<!-- HEADLINES -->
	<div id="headlines">
		<div class="titlex"><img src="images/kg/<?php echo $var_l; ?>/headlines.png" alt="headlines" /></div>
		<div  id="p-select3">
		<div class="up"><a href="#" onfocus="this.blur();" class="next"><img  src="images/kg/bank.gif" width="85" height="17" alt="next" /></a></div>
		<div class="body">
			<div id="canadaprovinces3" class="glidecontentwrapper3">
				<?php include("blocks/kg_headlines.php"); ?>
			</div>
		</div>
		<div class="down"><a href="#" onfocus="this.blur();" class="prev"><img   src="images/kg/bank.gif" width="85" height="17" alt="prev" /></a></div>
		</div>
		<script type="text/javascript">
		featuredcontentglider.init({gliderid: "canadaprovinces3",contentclass: "glidecontent3",	togglerid: "p-select3",	remotecontent: "", 	selected: 0, persiststate: true, 	speed:800, 	direction: "downup", autorotate: true, 	autorotateconfig: [20000, 10] 	})
		</script>
	</div>	
<?php 
	 if($type_affichage === '2')
	  include_once('./blocks/navigation_full.php');
	 else
	  include_once('./blocks/navigation_block.php');
?>	
	<!-- FOOTER -->
	<div id="copyright">		
	<?php include_once('blocks/ll2.php'); ?>		
	<div id="footer"></div>		
	<div id="index_r11_c1"></div></div>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-39605671-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

</body>
</html>
