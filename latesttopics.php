<?php
/*
##########################################################################
#                                                                        #
#           Version 4       /                        /   /               #
#          -----------__---/__---__------__----__---/---/-               #
#           | /| /  /___) /   ) (_ `   /   ) /___) /   /                 #
#          _|/_|/__(___ _(___/_(__)___/___/_(___ _/___/___               #
#                       Free Content / Management System                 #
#                                   /                                    #
#                                                                        #
#                                                                        #
#   Copyright 2005-2009 by webspell.org                                  #
#                                                                        #
#   visit webSPELL.org, webspell.info to get webSPELL for free           #
#   - Script runs under the GNU GENERAL PUBLIC LICENSE                   #
#   - It's NOT allowed to remove this copyright-tag                      #
#   -- http://www.fsf.org/licensing/licenses/gpl.html                    #
#                                                                        #
#   Code based on WebSPELL Clanpackage (Michael Gruber - webspell.at),   #
#   Far Development by Development Team - webspell.org                   #
#                                                                        #
#   visit webspell.org                                                   #
#                                                                        #
##########################################################################
*/
if (isset($site)) $_language->read_module('latesttopics');

$ergebnis=safe_query("SELECT t.*, u.nickname, b.name
						FROM ".PREFIX."forum_topics t 
				   LEFT JOIN ".PREFIX."user u ON u.userID = t.lastposter
				   LEFT JOIN ".PREFIX."forum_boards b ON b.boardID = t.boardID
					   WHERE t.moveID = '0' 
					ORDER BY t.lastdate DESC 
					   LIMIT 0,7");
$anz=mysql_num_rows($ergebnis);
if($anz) {
	eval ("\$latesttopics_head = \"".gettemplate("latesttopics_head")."\";");
	echo $latesttopics_head;
	$n=1;
	while($ds=mysql_fetch_array($ergebnis)) {
		if($ds['readgrps'] != "") {
			$usergrps = explode(";", $ds['readgrps']);
			$usergrp = 0;
			foreach($usergrps as $value) {
				if(isinusergrp($value, $userID)) {
					$usergrp = 1;
					break;
				}
			}
			if(!$usergrp and !ismoderator($userID, $ds['boardID'])) continue;
		}
		if($n%2) {
			$bg1=BG_1;
			$bg2=BG_2;
		}
		else {
			$bg1=BG_3;
			$bg2=BG_4;
		}
    
		$topictitle	= clearfromtags($ds['topic']);
		if(mb_strlen($topictitle)>$maxlatesttopicchars) {
			$topictitle=mb_substr($topictitle, 0, $maxlatesttopicchars);
			$topictitle.='...';
		}
    
		$last_poster = $ds['nickname'];
		$board = $ds['name'];
		$date = date('d.m.Y - H:i', $ds['lastdate']);
		$small_date	= date('d.m H:i', $ds['lastdate']);

		$latesticon	=	'<img src="images/icons/'.$ds['icon'].'" width="15" height="15" alt="" />';
		$boardlink	=	'<a href="index.php?site=forum&amp;board='.$ds['boardID'].'">'.$board.'</a>';
		$topiclink	=	'<a href="index.php?site=forum_topic&amp;topic='.$ds['topicID'].'&amp;type=ASC&amp;page='.ceil(($ds['replys']+1)/$maxposts).'" onmouseover="showWMTT(\'latesttopics_'.$n.'\')" onmouseout="hideWMTT()">'.$topictitle.'</a>';
		$replys			=	$ds['replys'];

		eval ("\$latesttopics_content = \"".gettemplate("latesttopics_content")."\";");
		echo $latesttopics_content;
		$n++;
	}
	eval ("\$latesttopics_foot = \"".gettemplate("latesttopics_foot")."\";");
	echo $latesttopics_foot;
}

unset($board);

?>