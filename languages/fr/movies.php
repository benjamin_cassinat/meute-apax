<?php

//************************************
//* Movie-Addon 2.0 by FIRSTBORN e.V.*
//************************************

$language_array = Array(

/* do not edit above this line */

	'myvideos'=>'Mes videos',
	'video'=>'Video',
	'movies'=>'Videos',
	'movie'=>'Video',
	'sort'=>'Ordre',
	'realywanttodelete'=>'Voulez vous vraiment supprimer cette video',
	'votes'=>'votes',
	'no_votes'=>'Pas de vote',
	'allready_voted'=>'Vous avez deja vote pour cette video!',
	'poor'=>'Mauvais',
	'perfect'=>'Parfait',
	'unlogged_vote'=>'Pour evaluer cette video, vous devez etre inscrit et connecte!',
	'hits'=>'hits',
	'category'=>'Categorie',
	'videos'=>'Videos',
	'catselect'=>'Categorie',
	'screen'=>'Screenshot',
	'description'=>'Description',
	'path_url'=>'Path-Url',
	'o_no_embed'=>'Permalien ou URL',
	'add_video'=>'Ajouter video',
	'edit_video'=>'Editer video',
	'pscreen'=>'Present Screenshot',
	'img_up'=>'Image Upload',
	'vid_created'=>'Video creer',
	'screen_error'=>'Screenshot n a pas ete creer',
	'screen_error1'=>'Le format de l image est incorrecte. S il vous plait telecharger Captures d ecran seulement en *.gif, *.jpg et *.png format.',
	'formerror'=>'S il vous plait remplir le formulaire correctement.',
	'mov_updated'=>'Video mis a jour',
	'no_mov_del'=>'Video non supprimez',
	'mov_del'=>'Video suprimmez',
	'no_perm'=>'Pas la permission requise',
	'new_vid'=>'Nouvelle video',
	'headline'=>'Nom',
	'uploader'=>'Uploader',
	'actions'=>'Actions',
	'discliamer'=>'Videos qui offensent les droits reserves ou afficher n importe quel type de Les actes de violence � caractere pornographique, ou aucun autre contenu moral sera supprime sans aucun commentaire',
	'edit'=>'Editer',
	'del'=>'Supprimer',
	'no_entries'=>'Pas d entree',
	'unact_vids'=>'Videos inactive',
	'page_link'=>'Lien de la page',
	'uploaded_by'=>'Uploader par',
	'times'=>'fois',
	'watched'=>'Regarder',
	'rating'=>'Rating',
	'comments'=>'Commentaire',
	'play'=>'Play',
	'latest_movie'=>'Latest Movie',
	'embed'=>'Embed',
	'movhead'=>'Nom video',
	'mov_added'=>'Ajouter video',
);
?>