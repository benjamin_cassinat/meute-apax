<?php
/*
##########################################################################
#                                                                        #
#           Version 4       /                        /   /               #
#          -----------__---/__---__------__----__---/---/-               #
#           | /| /  /___) /   ) (_ `   /   ) /___) /   /                 #
#          _|/_|/__(___ _(___/_(__)___/___/_(___ _/___/___               #
#                       Free Content / Management System                 #
#                                   /                                    #
#                                                                        #
#                                                                        #
#   Copyright 2005-2009 by webspell.org                                  #
#                                                                        #
#   visit webSPELL.org, webspell.info to get webSPELL for free           #
#   - Script runs under the GNU GENERAL PUBLIC LICENSE                   #
#   - It's NOT allowed to remove this copyright-tag                      #
#   -- http://www.fsf.org/licensing/licenses/gpl.html                    #
#                                                                        #
#   Code based on WebSPELL Clanpackage (Michael Gruber - webspell.at),   #
#   Far Development by Development Team - webspell.org                   #
#                                                                        #
#   visit webspell.org                                                   #
#                                                                        #
##########################################################################
# Byte-Order-Mark Check  #
# Find and Fix files     #
# webSPELL Version 4.02* #
# Copyright              #
#       Philipp Rehs     #
##########################
*/
if(mb_substr(basename($_SERVER['REQUEST_URI']),0,15) != "admincenter.php" OR !ispageadmin($userID)) die();
$_language->read_module('bom');
function checkBom($dir){
	global $col;
	$BOM = chr(239).chr(187).chr(191);
	$dh  = opendir($dir);
	$return = array();
	while (false !== ($filename = readdir($dh))) {
		if($filename != "." && $filename != ".."){
			if(is_dir($dir.$filename)){
				$return = array_merge($return, checkBom($dir.$filename."/"));
			}
			else{
				if(substr(file_get_contents($dir.$filename),0,strlen($BOM)) == $BOM){
					$return[] = $dir.$filename;
				}
			}
		}
	}
	return $return;
}
function fixBom($file){
	$BOM = chr(239).chr(187).chr(191);
	if(file_exists($file) && is_readable($file) && is_writeable($file)){
		$source = file_get_contents($file);
		if(substr($source,0,strlen($BOM)) == $BOM){
			$new_source = substr($source,strlen($BOM));
			$fh = fopen($file,"w+");
			$do = fwrite($fh,$new_source);
			fclose($fh);
			return $do;
		}
	}
	return false;
}
if(isset($_POST['sfix'])){
	$CAPCLASS = new Captcha;
	if($CAPCLASS->check_captcha(0, $_POST['captcha_hash'])) {
		if(isset($_POST['fix'])) $files = $_POST['fix'];
		else $files = array();
		foreach($files as $file){
			fixBom($file);
		}
	} else echo $_language->module['transaction_invalid'];
}
$col = 'td1';
echo'<h1>&curren; '.$_language->module['bom'].'</h1>';
echo'<form method="post" name="form" action="admincenter.php?site=bomcheck">
<table width="100%" border="0" cellspacing="1" cellpadding="3" bgcolor="#DDDDDD">
    <tr>
      <td width="85%" class="title"><b>'.$_language->module['file'].'</b></td>
      <td width="15%" class="title" align="center"><b>'.$_language->module['fix'].'</b></td>
    </tr>';
if(count($files = checkBom("../"))){
	$CAPCLASS = new Captcha;
    $CAPCLASS->create_transaction();
    $hash = $CAPCLASS->get_hash();
	foreach($files as $file){
		if( $col == 'td1') { $col='td2'; }
		else { $col='td1'; }
		echo '<tr>
	        <td class="'.$col.'" align="left">'.$file.'</td>
	        <td class="'.$col.'" align="center"><input type="checkbox" value="'.$file.'" name="fix[]" /></td>
	    </tr>';
	}
	if( $col == 'td1') { $col='td2'; }
	else { $col='td1'; }
	echo '<tr>
      <td class="'.$col.'" align="right"><input type="hidden" name="captcha_hash" value="'.$hash.'" /><input type="checkbox" onclick="SelectAll();" value="ALL" name="ALL" /> '.$_language->module['select_all'].'</td>
      <td class="'.$col.'" align="center"><input type="submit" name="sfix" value="'.$_language->module['fix'].'" /></td>
    </tr>';
}
else{
	echo '<tr>
	        <td class="'.$col.'" colspan="2" align="left">'.$_language->module['no_bom'].'</td>
	    </tr>';
}
echo "</table></form>";
?>