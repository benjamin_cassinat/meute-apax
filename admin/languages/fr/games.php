<?php
/*
##########################################################################
#                                                                        #
#           Version 4       /                        /   /               #
#          -----------__---/__---__------__----__---/---/-               #
#           | /| /  /___) /   ) (_ `   /   ) /___) /   /                 #
#          _|/_|/__(___ _(___/_(__)___/___/_(___ _/___/___               #
#                       Free Content / Management System                 #
#                                   /                                    #
#                                                                        #
#                                                                        #
#   Copyright 2005-2011 by webspell.org                                  #
#                                                                        #
#   visit webSPELL.org, webspell.info to get webSPELL for free           #
#   - Script runs under the GNU GENERAL PUBLIC LICENSE                   #
#   - It's NOT allowed to remove this copyright-tag                      #
#   -- http://www.fsf.org/licensing/licenses/gpl.html                    #
#                                                                        #
#   Code based on WebSPELL Clanpackage (Michael Gruber - webspell.at),   #
#   Far Development by Development Team - webspell.org                   #
#                                                                        #
#   visit webspell.org                                                   #
#                                                                        #
##########################################################################
*/

$language_array = Array(

/* do not edit above this line */

  'access_denied'=>'Accès refusé',
  'actions'=>'Actions',
  'add_game'=>'Enregistrer',
  'back'=>'Retour',
  'delete'=>'Supprimer',
  'edit'=>'Editer',
  'edit_game'=>'Enregistrer',
  'fill_correctly'=>'Veuillez remplir le formulaire correctement.',
  'format_incorrect'=>'Le format de l\'icône est incorrect. Veuillez envoyer seulement des icônes en format *.gif.',
  'game_icon'=>'Icône du jeu',
  'game_name'=>'Nom du jeu',
  'game_tag'=>'Raccourci',
  'games'=>'Jeux',
  'icons'=>'Icônes',
  'new_game'=>'Nouveau jeu',
  'no_entries'=>'Aucun jeu',
  'present_icon'=>'Icône actuel',
  'really_delete'=>'Voulez-vous vraiment supprimer ce jeu?',
  'transaction_invalid'=>'Transaction de l\'ID invalide'
);
?>