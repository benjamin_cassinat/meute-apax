<?php
/*
##########################################################################
#                                                                        #
#           Version 4       /                        /   /               #
#          -----------__---/__---__------__----__---/---/-               #
#           | /| /  /___) /   ) (_ `   /   ) /___) /   /                 #
#          _|/_|/__(___ _(___/_(__)___/___/_(___ _/___/___               #
#                       Free Content / Management System                 #
#                                   /                                    #
#                                                                        #
#                                                                        #
#   Copyright 2005-2011 by webspell.org                                  #
#                                                                        #
#   visit webSPELL.org, webspell.info to get webSPELL for free           #
#   - Script runs under the GNU GENERAL PUBLIC LICENSE                   #
#   - It's NOT allowed to remove this copyright-tag                      #
#   -- http://www.fsf.org/licensing/licenses/gpl.html                    #
#                                                                        #
#   Code based on WebSPELL Clanpackage (Michael Gruber - webspell.at),   #
#   Far Development by Development Team - webspell.org                   #
#                                                                        #
#   visit webspell.org                                                   #
#                                                                        #
##########################################################################
*/

$language_array = Array(

/* do not edit above this line */

  'about_myself'=>'À propos de moi',
  'access_denied'=>'Accès refusé',
  'actions'=>'Actions',
  'activate'=>'Activer',
  'active'=>'Actif',
  'activity'=>'Activité',
  'add_new_user'=>'Ajouter un nouvel utilisateur',
  'add_to_clan'=>'Enregistrer',
  'admin'=>'Administrateur',
  'avatar'=>'Avatar',
  'back'=>'Retour',
  'banish'=>'Bannir',
  'banned'=>'Banni',
  'ban_for'=>'Banni depuis',
  'ban_status'=>'Ban',
  'ban_until'=>'Bannir jusqu\'à',
  'ban_user'=>'Bannir cet utilisateur',
  'birthday'=>'Date de naissance',
  'clan_history'=>'Anciennes teams',
  'clan_homepage'=>'Site web',
  'can_not_copy_file'=>'ERREUR: impossible de copier ce fichier sur le serveur',
  'clan_irc'=>'Irc',
  'clanmember'=>'Membre du clan',
  'clanname'=>'Nom',
  'clantag'=>'Tag',
  'connection'=>'Connexion internet',
  'country'=>'Pays',
  'cpu'=>'Processeur',
  'days'=>'Jours',
  'del'=>'Supprimer',
  'delete_avatar'=>'Supprimer l\'avatar',
  'delete_picture'=>'Supprimer la photo',
  'edit_ban'=>'Enregistrer',
  'edit_profile'=>'Enregistrer',
  'email'=>'E-Mail',
  'error_avatar'=>'ERREUR: l\'avatar est trop grand (max. 90x90)',
  'error_picture'=>'ERREUR: la photo est trop grande (max. 230x210)',
  'female'=>'Féminin',
  'firstname'=>'Prénom',
  'gender'=>'Sexe',
  'general'=>'Général',
  'graphiccard'=>'Carte graphique',
  'homepage'=>'Site web',
  'icq'=>'ICQ',
  'inactive'=>'Inactif',
  'invalid_format'=>'ERREUR: le format de la photo est invalide (autorisés: *.gif, *.jpg ou *.png)',
  'keyboard'=>'Clavier',
  'lastname'=>'Nom',
  'mainboard'=>'Carte mère',
  'male'=>'Masculin',
  'max_90x90'=>'(max. 90x90)',
  'max_230x210'=>'(max. 230x210)',
  'moderator'=>'Modérateur du forum',
  'month'=>'Mois',
  'monitor'=>'Ecran',
  'mouse'=>'Souris',
  'mousepad'=>'Tapis de souris',
  'nickname'=>'Pseudo',
  'not_available'=>'Non indiqué',
  'no_users'=>'Aucun utilisateur trouvé',
  'password'=>'Mot de passe',
  'permanently'=>'Ban permanent ?',
  'personal'=>'Personnel',
  'picture'=>'Image',
  'pictures'=>'Images',
  'position'=>'Position',
  'profile'=>'Profil',
  'ram'=>'Mémoire',
  'really_ban'=>'Vraiment bannir',
  'really_delete'=>'Voulez-vous vraiment supprimer cet utilisateur?',
  'really_unban'=>'Vraiment débannir',
  'reason'=>'Raison',
  'registered_since'=>'Enregistré depuis le',
  'remove_ban'=> 'Retirer le ban ?',
  'rights'=>'Droits',
  'signatur'=>'Signature',
  'sort'=>'Ordre',
  'soundcard'=>'Carte son',
  'squad'=>'Section',
  'status'=>'Statut',
  'superadmin'=>'Super-Admin',
  'town'=>'Ville',
  'transaction_invalid'=>'Transaction de l\'ID invalide',
  'to_clan'=>'Section',
  'to_sort'=>'Ordre',
  'undo_ban'=>'Débannir',
  'upload_failed'=>'ERREUR: l\'envoi a échoué',
  'user'=>'Utilisateur',
  'user_exists'=>'Utilisateur déjà existant',
  'user_id'=>'ID utilisateur',
  'username'=>'Nom d\'utilisateur',
  'users'=>'Utilisateurs',
  'users_available'=>'utilisateurs',
  'usersearch'=>'Recherche d\'utilisateur',
  'exactsearch'=>'Exacte',
  'various'=>'Divers',
  'weeks'=>'Semaines',
  'you_cant_ban'=>'Vous ne pouvez pas bannir cet utilisateur ! (Super-Admin)',
  'you_cant_ban_yourself'=>'Vous ne pouvez pas vous bannir vous-même'
);
?>