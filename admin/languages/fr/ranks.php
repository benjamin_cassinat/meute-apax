<?php
/*
##########################################################################
#                                                                        #
#           Version 4       /                        /   /               #
#          -----------__---/__---__------__----__---/---/-               #
#           | /| /  /___) /   ) (_ `   /   ) /___) /   /                 #
#          _|/_|/__(___ _(___/_(__)___/___/_(___ _/___/___               #
#                       Free Content / Management System                 #
#                                   /                                    #
#                                                                        #
#                                                                        #
#   Copyright 2005-2011 by webspell.org                                  #
#                                                                        #
#   visit webSPELL.org, webspell.info to get webSPELL for free           #
#   - Script runs under the GNU GENERAL PUBLIC LICENSE                   #
#   - It's NOT allowed to remove this copyright-tag                      #
#   -- http://www.fsf.org/licensing/licenses/gpl.html                    #
#                                                                        #
#   Code based on WebSPELL Clanpackage (Michael Gruber - webspell.at),   #
#   Far Development by Development Team - webspell.org                   #
#                                                                        #
#   visit webspell.org                                                   #
#                                                                        #
##########################################################################
*/

$language_array = Array(

/* do not edit above this line */

  'access_denied'=>'Accès refusé',
  'add_rank'=>'Enregistrer',
  'actions'=>'Actions',
  'delete'=>'Supprimer',
  'edit_rank'=>'Enregistrer',
  'information_incomplete'=>'Quelques informations sont manquantes.',
  'max_posts'=>'Messages max.',
  'min_posts'=>'Messages min.',
  'new_rank'=>'Nouveau rang',
  'rank_icon'=>'Icône du rang',
  'rank_name'=>'Nom du rang',
  'really_delete'=>'Voulez-vous vraiment supprimer ce rang?',
  'transaction_invalid'=>'Transaction de l\'ID invalide',
  'update'=>'Enregistrer',
  'user_ranks'=>'Rangs des utilisateurs'
);
?>