﻿<?php
/*
##########################################################################
#                                                                        #
#           Version 4       /                        /   /               #
#          -----------__---/__---__------__----__---/---/-               #
#           | /| /  /___) /   ) (_ `   /   ) /___) /   /                 #
#          _|/_|/__(___ _(___/_(__)___/___/_(___ _/___/___               #
#                       Free Content / Management System                 #
#                                   /                                    #
#                                                                        #
#                                                                        #
#   Copyright 2005-2009 by webspell.org                                  #
#                                                                        #
#   visit webSPELL.org, webspell.info to get webSPELL for free           #
#   - Script runs under the GNU GENERAL PUBLIC LICENSE                   #
#   - It's NOT allowed to remove this copyright-tag                      #
#   -- http://www.fsf.org/licensing/licenses/gpl.html                    #
#                                                                        #
#   Code based on WebSPELL Clanpackage (Michael Gruber - webspell.at),   #
#   Far Development by Development Team - webspell.org                   #
#                                                                        #
#   visit webspell.org                                                   #
#                                                                        #
##########################################################################
*/

$language_array = Array(

/* do not edit above this line */

  'about_us'=>'À propos de nous',
  'access_denied'=>'Accès refusé',
  'bannerrotation'=>'Rotation de bannière',
  'boards'=>'Forums',
  'clanmembers'=>'Membres du clan',
  'contact'=>'Contacts',
  'content'=>'Contenu',
  'countries'=>'Pays',
  'database'=>'Base de données',
  'faq'=>'FAQ',
  'faq_categories'=>'Catégories de la FAQ',
  'file_categories'=>'Catégories des fichiers',
  'forum'=>'Forum',
  'gallery'=>'Galerie',
  'games'=>'Jeux',
  'history'=>'Historique',
  'icons'=>'Icônes',
  'imprint'=>'Description',
  'link_categories'=>'Catégories des liens',
  'log_out'=>'Se déconnecter',
  'main_panel'=>'Panneau principal',
  'manage_galleries'=>'Gérer les galeries',
  'manage_groups'=>'Gérer les groupes/catégories',
  'manage_group_users'=>'Gérer les membres des groupes',
  'manage_user_groups'=>'Gérer les groupes d\'utilisateurs',
  'newsletter'=>'Newsletter',
  'news_languages'=>'Langues des news',
  'news_rubrics'=>'Rubriques des news',
  'not_logged_in'=>'Vous n\'êtes pas identifié',
  'overview'=>'Accueil / Info. du serveur',
  'page_statistics'=>'Statistiques',
  'partners'=>'Partenaires',
  'registered_users'=>'Utilisateurs enregistrés',
  'rubrics'=>'Rubriques/Catégories',
  'scrolltext'=>'Texte défilant',
  'select_icons'=>'Veuillez sélectionner des icônes',
  'servers'=>'Serveurs',
  'settings'=>'Configurations',
  'smilies'=>'Smileys',
  'sponsors'=>'Sponsors',
  'squads'=>'Sections',
  'static_pages'=>'Pages statiques',
  'styles'=>'Styles',
  'update_webspell'=>'Mise à jour de webSPELL',
  'user_administration'=>'Gestion des utilisateurs',
  'user_ranks'=>'Rangs des utilisateurs',
  'visitor_statistics'=>'Statistiques des visiteurs',
  'add_video'=>'Ajouter une video',
  'add_video_category'=>'Ajouter une categorie',
  'activate_video'=>'Activer video',
  'video'=>'video',
);
?>