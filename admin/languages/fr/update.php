<?php
/*
##########################################################################
#                                                                        #
#           Version 4       /                        /   /               #
#          -----------__---/__---__------__----__---/---/-               #
#           | /| /  /___) /   ) (_ `   /   ) /___) /   /                 #
#          _|/_|/__(___ _(___/_(__)___/___/_(___ _/___/___               #
#                       Free Content / Management System                 #
#                                   /                                    #
#                                                                        #
#                                                                        #
#   Copyright 2005-2011 by webspell.org                                  #
#                                                                        #
#   visit webSPELL.org, webspell.info to get webSPELL for free           #
#   - Script runs under the GNU GENERAL PUBLIC LICENSE                   #
#   - It's NOT allowed to remove this copyright-tag                      #
#   -- http://www.fsf.org/licensing/licenses/gpl.html                    #
#                                                                        #
#   Code based on WebSPELL Clanpackage (Michael Gruber - webspell.at),   #
#   Far Development by Development Team - webspell.org                   #
#                                                                        #
#   visit webspell.org                                                   #
#                                                                        #
##########################################################################
*/

$language_array = Array(

/* do not edit above this line */

  'access_denied'=>'Accès refusé',
  'error'=>'Le serveur n\'a pas de mise à jour compatible ou fichier non compatible à',
  'filename'=>'Nom du fichier',
  'get_new_version'=>'Prenez la nouvelle version de webSPELL ici !',
  'information'=>'Information',
  'new_functions'=>'Nouvelles fonctionnalités pour webSPELL disponibles',
  'new_updates'=>'Nouvelles mises à jour de webSPELL disponibles',
  'new_version'=>'Nouvelle version de webSPELL disponible',
  'no_updates'=>'Aucune mise à jour de disponible !',
  'version'=>'Version',
  'webspell_update'=>'Mise à jour de webSPELL'
);
?>