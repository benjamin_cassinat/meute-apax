<?php
/*
##########################################################################
#                                                                        #
#           Version 4       /                        /   /               #
#          -----------__---/__---__------__----__---/---/-               #
#           | /| /  /___) /   ) (_ `   /   ) /___) /   /                 #
#          _|/_|/__(___ _(___/_(__)___/___/_(___ _/___/___               #
#                       Free Content / Management System                 #
#                                   /                                    #
#                                                                        #
#                                                                        #
#   Copyright 2005-2011 by webspell.org                                  #
#                                                                        #
#   visit webSPELL.org, webspell.info to get webSPELL for free           #
#   - Script runs under the GNU GENERAL PUBLIC LICENSE                   #
#   - It's NOT allowed to remove this copyright-tag                      #
#   -- http://www.fsf.org/licensing/licenses/gpl.html                    #
#                                                                        #
#   Code based on WebSPELL Clanpackage (Michael Gruber - webspell.at),   #
#   Far Development by Development Team - webspell.org                   #
#                                                                        #
#   visit webspell.org                                                   #
#                                                                        #
##########################################################################
*/

$language_array = Array(

/* do not edit above this line */

  'access_denied'=>'Accès refusé',
  'articles'=>'Articles',
  'awards'=>'Prix',
  'banner'=>'Bannières',
  'bannerrotation'=>'Rotation de bannière',
  'challenge'=>'Défis',
  'clanwars'=>'Matchs',
  'comments'=>'Commentaires',
  'contacts'=>'Contacts',
  'countries'=>'Pays',
  'database'=>'Base de données',
  'demos'=>'Démos',
  'faq'=>'FAQs',
  'faq_categories'=>'Catégories des FAQs',
  'files'=>'Fichiers',
  'files_categorys'=>'Catégories des fichiers',
  'forum_announcements'=>'Annonces du forum',
  'forum_boards'=>'Forums',
  'forum_categories'=>'Catégories du forum',
  'forum_groups'=>'Groupes d\'utilisateurs du forum',
  'forum_moderators'=>'Modérateurs du forum',
  'forum_posts'=>'Messages du forum',
  'forum_ranks'=>'Rangs du forum',
  'forum_topics'=>'Sujets du forum',
  'gallery'=>'Galeries',
  'gallery_groups'=>'Groupes de la galerie',
  'gallery_pictures'=>'Images de la galerie',
  'games'=>'Jeux',
  'guestbook'=>'Entrées du livre d\'or',
  'links'=>'Liens',
  'links_categorys'=>'Catégories des liens',
  'linkus'=>'Bannières nous recommander',
  'messenger'=>'Messages envoyés',
  'mysql_version'=>'Version MySQL',
  'news'=>'News',
  'news_languages'=>'Langues des news',
  'news_rubrics'=>'Rubriques des news',
  'optimize'=>'Optimiser maintenant !',
  'overhead'=>'Au-dessus',
  'page_stats'=>'Statistiques',
  'partners'=>'Partenaires',
  'poll'=>'Sondages',
  'servers'=>'Serveurs',
  'shoutbox'=>'Entrées de la tribune libre',
  'size'=>'Taille',
  'smileys'=>'Smileys',
  'sponsors'=>'Sponsors',
  'squads'=>'Sections',
  'static'=>'Pages statiques',
  'tables'=>'Tables',
  'user'=>'Utilisateurs enregistrés',
  'user_gbook'=>'Entrées du livre d\'or utilisateur'
);
?>