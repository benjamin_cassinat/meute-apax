<?php
/*
##########################################################################
#                                                                        #
#           Version 4       /                        /   /               #
#          -----------__---/__---__------__----__---/---/-               #
#           | /| /  /___) /   ) (_ `   /   ) /___) /   /                 #
#          _|/_|/__(___ _(___/_(__)___/___/_(___ _/___/___               #
#                       Free Content / Management System                 #
#                                   /                                    #
#                                                                        #
#                                                                        #
#   Copyright 2005-2011 by webspell.org                                  #
#                                                                        #
#   visit webSPELL.org, webspell.info to get webSPELL for free           #
#   - Script runs under the GNU GENERAL PUBLIC LICENSE                   #
#   - It's NOT allowed to remove this copyright-tag                      #
#   -- http://www.fsf.org/licensing/licenses/gpl.html                    #
#                                                                        #
#   Code based on WebSPELL Clanpackage (Michael Gruber - webspell.at),   #
#   Far Development by Development Team - webspell.org                   #
#                                                                        #
#   visit webspell.org                                                   #
#                                                                        #
##########################################################################
*/

$language_array = Array(

/* do not edit above this line */

  'access_denied'=>'Accès refusé',
  'lock'=>'Verrouiller',
  'locked_since'=>'La page est verrouillée depuis',
  'pagelock'=>'Page verrouillée',
  'page_locked'=>'<b>Page verrouillée !</b><br />Seulement les admins peuvent actuellement accéder à cette page.',
  'page_unlocked'=>'<b>Page déverrouillée !</b>',
  'settings'=>'Configurations',
  'transaction_invalid'=>'Transaction de l\'ID invalide',
  'unlock'=>'Déverrouiler',
  'unlock_page'=>'Voulez-vous déverrouiller la page?',
  'you_can_use_html'=>'Vous pouvez utiliser les balises HTML'
);
?>