<?php
/*
##########################################################################
#                                                                        #
#           Version 4       /                        /   /               #
#          -----------__---/__---__------__----__---/---/-               #
#           | /| /  /___) /   ) (_ `   /   ) /___) /   /                 #
#          _|/_|/__(___ _(___/_(__)___/___/_(___ _/___/___               #
#                       Free Content / Management System                 #
#                                   /                                    #
#                                                                        #
#                                                                        #
#   Copyright 2005-2011 by webspell.org                                  #
#                                                                        #
#   visit webSPELL.org, webspell.info to get webSPELL for free           #
#   - Script runs under the GNU GENERAL PUBLIC LICENSE                   #
#   - It's NOT allowed to remove this copyright-tag                      #
#   -- http://www.fsf.org/licensing/licenses/gpl.html                    #
#                                                                        #
#   Code based on WebSPELL Clanpackage (Michael Gruber - webspell.at),   #
#   Far Development by Development Team - webspell.org                   #
#                                                                        #
#   visit webspell.org                                                   #
#                                                                        #
##########################################################################
*/

$language_array = Array(

/* do not edit above this line */

  '1_slow'=>'1 (lent)',
  '2_normal'=>'2 (normal)',
  '3_fast'=>'3 (rapide)',
  'access_denied'=>'Accès refusé',
  'color'=>'Couleur',
  'delay'=>'Vitesse',
  'delete'=>'Supprimer',
  'direction'=>'Direction',
  'example'=>'(ex.: #FFFFFF)',
  'left_to_right'=>'de gauche à droite',
  'right_to_left'=>'de droite à gauche',
  'scrolltext'=>'Texte défilant',
  'transaction_invalid'=>'Transaction de l\'ID invalide',
  'update'=>'Enregistrer',
  'you_can_use_html'=>'Vous pouvez utiliser les balises HTML'
);
?>