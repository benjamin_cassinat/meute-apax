<?php
/*
##########################################################################
#                                                                        #
#           Version 4       /                        /   /               #
#          -----------__---/__---__------__----__---/---/-               #
#           | /| /  /___) /   ) (_ `   /   ) /___) /   /                 #
#          _|/_|/__(___ _(___/_(__)___/___/_(___ _/___/___               #
#                       Free Content / Management System                 #
#                                   /                                    #
#                                                                        #
#                                                                        #
#   Copyright 2005-2009 by webspell.org                                  #
#                                                                        #
#   visit webSPELL.org, webspell.info to get webSPELL for free           #
#   - Script runs under the GNU GENERAL PUBLIC LICENSE                   #
#   - It's NOT allowed to remove this copyright-tag                      #
#   -- http://www.fsf.org/licensing/licenses/gpl.html                    #
#                                                                        #
#   Code based on WebSPELL Clanpackage (Michael Gruber - webspell.at),   #
#   Far Development by Development Team - webspell.org                   #
#                                                                        #
#   visit webspell.org                                                   #
#                                                                        #
##########################################################################
*/

$language_array = Array(

/* do not edit above this line */
  'add_topmatch'=>'Ajouter topmatch',
  'access_denied'=>'Acces non autoriser',
  'actions'=>'Actions',
		'country'=>'country',
  'back'=>'back',
		'date'=>'date',
  'delete'=>'Supprimez',
		'des_settings'=>'Ce sont des r�glages pour votre �quipe',
  'edit'=>'Editer',
		'edit_topmatch'=>'Editer Topmatch',
  'format_incorrect'=>'Le format de l image est incorrecte.Les format disponible sont *.gif, *.jpg and *.png',
		'fill_correctly'=>'Sil vous plait remplir le formulaire correctement.',
		'how'=>'comment?',
		'height'=>'# hauteur',
		'hp1'=>'Site team 1',
  'hp2'=>'Site team 2',
  'is_displayed'=>'Est affich�?',
		'league'=>'league',
		'logo1'=>'logo team 1',
		'logo2'=>'logo team 2',
		'matchlink'=>'matchlink',
		'maps'=>'maps',
  'new_topmatch'=>'Nouveau topmatch',
  'no'=>'Non',
  'really_delete'=>'Supprimer le match?',
		'server'=>'SrcTV/HLTV',
		'headline'=>'Headline',
		'statement'=>'D�claration',
  'sort'=>'Sort',
		'team1'=>'nom team 1',
		'team2'=>'nom team 2',
		'topmatch_displayed'=>'Est affichee?',
		'settings'=>'Configuraiton',
		'team1_our'=>'Team 1 is Clan',
  'transaction_invalid'=>'Transaction ID invalid',
  'to_sort'=>'sort',
		'width'=>'Largeur',
  'yes'=>'Yes'
);
?>