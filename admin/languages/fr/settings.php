<?php
/*
##########################################################################
#                                                                        #
#           Version 4       /                        /   /               #
#          -----------__---/__---__------__----__---/---/-               #
#           | /| /  /___) /   ) (_ `   /   ) /___) /   /                 #
#          _|/_|/__(___ _(___/_(__)___/___/_(___ _/___/___               #
#                       Free Content / Management System                 #
#                                   /                                    #
#                                                                        #
#                                                                        #
#   Copyright 2005-2011 by webspell.org                                  #
#                                                                        #
#   visit webSPELL.org, webspell.info to get webSPELL for free           #
#   - Script runs under the GNU GENERAL PUBLIC LICENSE                   #
#   - It's NOT allowed to remove this copyright-tag                      #
#   -- http://www.fsf.org/licensing/licenses/gpl.html                    #
#                                                                        #
#   Code based on WebSPELL Clanpackage (Michael Gruber - webspell.at),   #
#   Far Development by Development Team - webspell.org                   #
#                                                                        #
#   visit webspell.org                                                   #
#                                                                        #
##########################################################################
*/

$language_array = Array(

/* do not edit above this line */

  'access_denied'=>'Accès refusé',
  'additional_options'=>'Options',
  'admin_email'=>'E-mail de l\'admin',
  'admin_name'=>'Nom de l\'admin',
  'allow_usergalleries'=>'Autoriser les galeries d\'utilisateurs',
  'archive'=>'Archives',
  'articles'=>'Articles',
  'bomcheck'=>'Bomcheck',
  'autoresize'=>'Fonction de redimension d\'image',
  'autoresize_js'=>'Par JavaScript',
  'autoresize_off'=>'Désactivé',
  'autoresize_php'=>'Par PHP',
  'awards'=>'Prix',
  'captcha'=>'Captcha',
  'captcha_autodetect'=>'Automatique',
  'captcha_bgcol'=>'Couleur de fond',
  'captcha_both'=>'Chemin',
  'captcha_fontcol'=>'Couleur de la police',
  'captcha_image'=>'Image',
  'captcha_linenoise'=>'Lignes sonores',
  'captcha_noise'=>'Son',
  'captcha_only_math'=>'Maths seulement',
  'captcha_only_text'=>'Texte seulement',
  'captcha_text'=>'Texte',
  'captcha_type'=>'Type de captcha',
  'captcha_style'=>'Style de captcha',
  'clan_name'=>'Nom du clan',
  'clan_tag'=>'Tag du clan',
  'clanwars'=>'Matchs',
  'comments'=>'Commentaires',
  'content_size'=>'Taille max. du contenu',
  'default_language'=>'Langue par défaut',
  'demos'=>'Démos',
  'forum'=>'Forum',
  'forum_posts'=>'Messages du forum',
  'forum_topics'=>'Sujets du forum',
  'gallery'=>'Galerie',
  'guestbook'=>'Livre d\'or',
  'headlines'=>'Dernières news',
  'insert_links'=>'Insérer des liens aux membres',
  'latest_articles'=>'Derniers articles',
  'latest_results'=>'Derniers résultats',
  'latest_topics'=>'Derniers sujets',
  'login_duration'=>'Temps de connexion',
  'max_length_headlines'=>'Taille max. des dernières news',
  'max_length_latest_articles'=>'Taille max. des derniers articles',
  'max_length_latest_topics'=>'Taille max. des derniers sujets',
  'max_length_topnews'=>'Taille max. des topnews',
  'max_wrong_pw'=>'Nombre max. mauvais mots de passe',
  'messenger'=>'Messagerie',
  'msg_on_gb_entry'=>'Message lors d\'un nouveau commentaire du livre d\'or',
  'news'=>'News',
  'other'=>'Autres',
  'page_title'=>'Titre du site',
  'page_url'=>'Adresse du site',
  'pagelock'=>'Page verouillée',
  'pictures'=>'Images',
  'profile_last_posts'=>'Derniers sujets dans le profil',
  'public_admin'=>'Admin de l\'espace public',
  'registered_users'=>'Utilisateurs enregistrés',
  'search_min_length'=>'Taille min. de la recherche',
  'settings'=>'Configurations',
  'shoutbox'=>'Tribune libre',
  'shoutbox_all_messages'=>'Nombre de messages de la tribune libre par page',
  'shoutbox_refresh'=>'Rafraîchissement de la tribune libre',
  'space_user'=>'Espace par utilisateur (Mo)',
  'thumb_width'=>'Taille des miniatures',
  'tooltip_1'=>'Ceci est l\'adresse de votre site, ex. (votredomaine.com/chemin/webspell).<br />Sans http:// au début et ne pas finir avec un slash!<br />Devrait ressembler à',
  'tooltip_2'=>'Ceci est le titre de votre site, montré comme titre de votre fenêtre windows',
  'tooltip_3'=>'Le nom de votre clan',
  'tooltip_4'=>'Le tag de votre clan',
  'tooltip_5'=>'Le nom du webmaster = votre nom',
  'tooltip_6'=>'L\'adresse E-Mail du webmaster',
  'tooltip_7'=>'Nombre de news qui sont affichées entièrement',
  'tooltip_8'=>'Sujets du forum par page',
  'tooltip_9'=>'Images par page',
  'tooltip_10'=>'Nombre de news qui sont affichées dans les archives par page',
  'tooltip_11'=>'Nombre de messages du forum par page',
  'tooltip_12'=>'Taille (largeur) des miniatures de la galerie',
  'tooltip_13'=>'Headlines listed by sc_headlines',
  'tooltip_14'=>'Topics listed by latesttopics',
  'tooltip_15'=>'Webspace for usergalleries per user in MByte',
  'tooltip_16'=>'Longueur maximale des titres des dernières news du bloc sc_headlines',
  'tooltip_17'=>'Longueur minimale des termes de la recherche',
  'tooltip_18'=>'Voulez-vous autoriser les galeries d\'utilisateurs ries pour chaque utilisateur?',
  'tooltip_19'=>'Voulez-vous administrer les images de la galerie diretement sur votre page? (préférable de laisser cocher)',
  'tooltip_20'=>'Articles par page',
  'tooltip_21'=>'Prix par page',
  'tooltip_22'=>'Nombre d\'articles qui sont affichés dans le bloc sc_articles',
  'tooltip_23'=>'Démos par page',
  'tooltip_24'=>'Longueur maximale des titres des articles du bloc sc_articles',
  'tooltip_25'=>'Nombre de messages du livre d\'or par page',
  'tooltip_26'=>'Commentaires par page',
  'tooltip_27'=>'Messages par page',
  'tooltip_28'=>'Matchs par page',
  'tooltip_29'=>'Nombre d\'utilisateurs enregistrés par page',
  'tooltip_30'=>'Nombre de résultats qui sont affichés dans le bloc sc_results',
  'tooltip_31'=>'Nombres de derniers sujets qui sont affichés dans le profil',
  'tooltip_32'=>'Nombre d\'entrées qui sont affichées par le bloc sc_upcoming',
  'tooltip_33'=>'Temps pour rester connecter [en heures] (0 = 20 minutes)',
  'tooltip_34'=>'Taille maximale (largeur) du contenu (images, textes, etc...) (0 = désactivé)',
  'tooltip_35'=>'Taille maximale (hauteur) du contenu (images) (0 = désactivé)',
  'tooltip_36'=>'Envoyer un message aux admins lors d\'un nouveau commentaire sur le livre d\'or?',
  'tooltip_37'=>'Nombre de messages de la tribune libre qui sont affichés dans la tribune libre',
  'tooltip_38'=>'Nombre de messages de la tribune libre par page',
  'tooltip_39'=>'Temps (en secondes) de rafraîchissement de la tribune libre',
  'tooltip_40'=>'Langue par défaut du site',
  'tooltip_41'=>'Insérer des liens automatiquement au profil des membres?',
  'tooltip_42'=>'Longueur maximale des titres des sujets du bloc latesttopics',
  'tooltip_43'=>'Nombre maximal de tentatives incorrectes du mot de passe avant un ban de l\'IP',
  'tooltip_44'=>'Type de captcha',
  'tooltip_45'=>'Couleur de fond du captcha',
  'tooltip_46'=>'Couleur de la police du captcha',
  'tooltip_47'=>'Contenu du type/style de captcha',
  'tooltip_48'=>'Nombre de pixels sonores',
  'tooltip_49'=>'Nombre de lignes sonores',
  'tooltip_50'=>'Sélection de la fonction automatique de redimension d\'image',
  'tooltip_51'=>'Longueur maximale des topnews dans le bloc sc_topnews',
  'transaction_invalid'=>'Transaction de l\'ID invalide',
  'upcoming_actions'=>'Prochains matchs (événements)',
  'update'=>'Enregistrer'
);
?>