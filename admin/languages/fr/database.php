<?php
/*
##########################################################################
#                                                                        #
#           Version 4       /                        /   /               #
#          -----------__---/__---__------__----__---/---/-               #
#           | /| /  /___) /   ) (_ `   /   ) /___) /   /                 #
#          _|/_|/__(___ _(___/_(__)___/___/_(___ _/___/___               #
#                       Free Content / Management System                 #
#                                   /                                    #
#                                                                        #
#                                                                        #
#   Copyright 2005-2011 by webspell.org                                  #
#                                                                        #
#   visit webSPELL.org, webspell.info to get webSPELL for free           #
#   - Script runs under the GNU GENERAL PUBLIC LICENSE                   #
#   - It's NOT allowed to remove this copyright-tag                      #
#   -- http://www.fsf.org/licensing/licenses/gpl.html                    #
#                                                                        #
#   Code based on WebSPELL Clanpackage (Michael Gruber - webspell.at),   #
#   Far Development by Development Team - webspell.org                   #
#                                                                        #
#   visit webspell.org                                                   #
#                                                                        #
##########################################################################
*/

$language_array = Array(

/* do not edit above this line */

  'access_denied'=>'Accès refusé',
  'allowed_commands'=>'<b>Commandes autorisées:</b> insert, alter table, select',
  'backup_file'=>'Fichier de sauvegarde',
  'database'=>'Base de données',
  'export'=>'Exporter/Télécharger une sauvegarde',
  'import_info'=>'Sélectionner votre fichier contenant les requêtes SQL.<br /><br /><b>Important:</b> Créez une sauvegarde avant une importation !',
  'optimize'=>'Optimiser la base de données',
  'select_option'=>'Veuillez sélectionner une option',
  'submit'=>'Soumettre',
  'sql_query'=>'SQL Query',
  'syntax_not_allowed'=>'Syntaxe non autorisée !',
  'upload'=>'Importer',
  'transaction_invalid'=>'Transaction de l\'ID invalide'
);
?>