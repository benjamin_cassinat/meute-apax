<?php
/*
##########################################################################
#                                                                        #
#           Version 4       /                        /   /               #
#          -----------__---/__---__------__----__---/---/-               #
#           | /| /  /___) /   ) (_ `   /   ) /___) /   /                 #
#          _|/_|/__(___ _(___/_(__)___/___/_(___ _/___/___               #
#                       Free Content / Management System                 #
#                                   /                                    #
#                                                                        #
#                                                                        #
#   Copyright 2005-2011 by webspell.org                                  #
#                                                                        #
#   visit webSPELL.org, webspell.info to get webSPELL for free           #
#   - Script runs under the GNU GENERAL PUBLIC LICENSE                   #
#   - It's NOT allowed to remove this copyright-tag                      #
#   -- http://www.fsf.org/licensing/licenses/gpl.html                    #
#                                                                        #
#   Code based on WebSPELL Clanpackage (Michael Gruber - webspell.at),   #
#   Far Development by Development Team - webspell.org                   #
#                                                                        #
#   visit webspell.org                                                   #
#                                                                        #
##########################################################################
*/

$language_array = Array(

/* do not edit above this line */

  'access_denied'=>'Accès refusé',
  'actions'=>'Actions',
  'add_sponsor'=>'Enregistrer',
  'back'=>'Retour',
  'banner'=>'Bannière',
  'banner_to_big'=>'La bannière est trop grande',
  'banner_upload'=>'Upload (bannière)',
  'banner_upload_info'=>'pour le sc_sponsors include',
  'banner_upload_small'=>'Upload (petite bannière)',
  'clicks'=>'Clics (par jour)',
  'current_banner'=>'Bannière actuelle',
  'current_banner_small'=>'Bannière actuelle (petite)',
  'delete'=>'Supprimer',
  'description'=>'Description',
  'edit'=>'Editer',
  'edit_sponsor'=>'Enregistrer',
  'format_incorrect'=>'Le format de l\'icône est incorrect. Veuillez envoyer seulement une bannière en format *.gif, *.jpg or *.png.',
  'is_displayed'=>'Est affichée ?',
  'mainsponsor'=>'Sponsor principal',
  'new_sponsor'=>'Nouveau sponsor',
  'no'=>'Non',
  'no_upload'=>'Aucune image envoyée',
  'no_entries'=>'Aucun sponsor',
  'really_delete'=>'Voulez-vous vraiment supprimer cette bannière?',
  'sort'=>'Ordre',
  'sponsor'=>'Sponsor',
  'sponsor_name'=>'Nom du sponsor',
  'sponsor_url'=>'Adresse du sponsor',
  'sponsors'=>'Sponsors',
  'transaction_invalid'=>'Transaction de l\'ID invalide',
  'to_sort'=>'Ordre',
  'yes'=>'Oui'
);
?>