<?php
/*
##########################################################################
#                                                                        #
#           Version 4       /                        /   /               #
#          -----------__---/__---__------__----__---/---/-               #
#           | /| /  /___) /   ) (_ `   /   ) /___) /   /                 #
#          _|/_|/__(___ _(___/_(__)___/___/_(___ _/___/___               #
#                       Free Content / Management System                 #
#                                   /                                    #
#                                                                        #
#                                                                        #
#   Copyright 2005-2011 by webspell.org                                  #
#                                                                        #
#   visit webSPELL.org, webspell.info to get webSPELL for free           #
#   - Script runs under the GNU GENERAL PUBLIC LICENSE                   #
#   - It's NOT allowed to remove this copyright-tag                      #
#   -- http://www.fsf.org/licensing/licenses/gpl.html                    #
#                                                                        #
#   Code based on WebSPELL Clanpackage (Michael Gruber - webspell.at),   #
#   Far Development by Development Team - webspell.org                   #
#                                                                        #
#   visit webspell.org                                                   #
#                                                                        #
##########################################################################
*/

$language_array = Array(

/* do not edit above this line */

  'access_denied'=>'Accès refusé',
  'actions'=>'Actions',
  'add_banner'=>'Enregistrer',
  'back'=>'Retour',
  'banner'=>'Bannière',
  'bannerrotation'=>'Rotation de bannière',
  'banner_name'=>'Nom de la bannière',
  'banner_upload'=>'Upload',
  'banner_url'=>'Adresse de la bannière',
  'clicks'=>'Clics (par jour)',
  'delete'=>'Supprimer',
  'edit'=>'Editer',
  'edit_banner'=>'Enregistrer',
  'fill_correctly'=>'Veuillez remplir le formulaire correctement.',
  'format_incorrect'=>'Le format de la bannière est incorrect. Veuillez envoyer seulement une bannière en format *.gif, *.jpg et *.png.',
  'is_displayed'=>'Est affichée ?',
  'new_banner'=>'Nouvelle bannière',
  'no'=>'Non',
  'no_upload'=>'Aucune image envoyée',
  'no_entries'=>'Aucune bannière',
  'present_banner'=>'Bannière actuelle',
  'really_delete'=>'Voulez-vous vraiment supprimer ce fichier?',
  'transaction_invalid'=>'Transaction de l\'ID invalide',
  'yes'=>'Oui'
);
?>