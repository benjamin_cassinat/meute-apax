<?php
/*
##########################################################################
#                                                                        #
#           Version 4       /                        /   /               #
#          -----------__---/__---__------__----__---/---/-               #
#           | /| /  /___) /   ) (_ `   /   ) /___) /   /                 #
#          _|/_|/__(___ _(___/_(__)___/___/_(___ _/___/___               #
#                       Free Content / Management System                 #
#                                   /                                    #
#                                                                        #
#                                                                        #
#   Copyright 2005-2011 by webspell.org                                  #
#                                                                        #
#   visit webSPELL.org, webspell.info to get webSPELL for free           #
#   - Script runs under the GNU GENERAL PUBLIC LICENSE                   #
#   - It's NOT allowed to remove this copyright-tag                      #
#   -- http://www.fsf.org/licensing/licenses/gpl.html                    #
#                                                                        #
#   Code based on WebSPELL Clanpackage (Michael Gruber - webspell.at),   #
#   Far Development by Development Team - webspell.org                   #
#                                                                        #
#   visit webspell.org                                                   #
#                                                                        #
##########################################################################
*/

$language_array = Array(

/* do not edit above this line */

  'access_denied'=>'Accès refusé',
  'actions'=>'Actions',
  'add_squad'=>'Enregistrer',
  'back'=>'Retour',
  'current_icon'=>'Icône actuel',
  'current_icon_small'=>'Icône actuel (petit)',
  'delete'=>'Supprimer',
  'edit'=>'Editer',
  'edit_squad'=>'Enregistrer',
  'format_incorrect'=>'Le format de l\'icône est incorrect. Veuillez envoyer seulement une bannière en format *.gif, *.jpg or *.png.',
  'game'=>'Jeu',
  'gaming_squad'=>'Section de joueurs',
  'icon'=>'Icône',
  'icon_upload'=>'Upload (icône)',
  'icon_upload_info'=>'pour le sc_squads include',
  'icon_upload_small'=>'Upload (petit icône)',
  'information_incomplete'=>'Quelques informations sont manquantes.',
  'new_squad'=>'Nouvelle équipe',
  'no_icon'=>'Aucun icône disponible',
  'non_gaming_squad'=>'Section de non joueurs',
  'really_delete'=>'Voulez-vous vraiment supprimer cette équipe?',
  'sort'=>'Ordre',
  'squad_info'=>'Info de l\'équipe',
  'squad_name'=>'Nom de l\'équipe',
  'squad_type'=>'Type d\'équipe',
  'squads'=>'Sections',
  'transaction_invalid'=>'Transaction de l\'ID invalide',
  'to_sort'=>'Ordre'
);
?>