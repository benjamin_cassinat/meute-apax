<?php
/*
##########################################################################
#                                                                        #
#           Version 4       /                        /   /               #
#          -----------__---/__---__------__----__---/---/-               #
#           | /| /  /___) /   ) (_ `   /   ) /___) /   /                 #
#          _|/_|/__(___ _(___/_(__)___/___/_(___ _/___/___               #
#                       Free Content / Management System                 #
#                                   /                                    #
#                                                                        #
#                                                                        #
#   Copyright 2005-2011 by webspell.org                                  #
#                                                                        #
#   visit webSPELL.org, webspell.info to get webSPELL for free           #
#   - Script runs under the GNU GENERAL PUBLIC LICENSE                   #
#   - It's NOT allowed to remove this copyright-tag                      #
#   -- http://www.fsf.org/licensing/licenses/gpl.html                    #
#                                                                        #
#   Code based on WebSPELL Clanpackage (Michael Gruber - webspell.at),   #
#   Far Development by Development Team - webspell.org                   #
#                                                                        #
#   visit webspell.org                                                   #
#                                                                        #
##########################################################################
*/

$language_array = Array(

/* do not edit above this line */

  'access_denied'=>'Accès refusé',
  'actions'=>'Actions',
  'add_gallery'=>'Enregistrer',
  'add_group'=>'Enregistrer',
  'add_img'=>'Enregistrer',
  'comment'=>'Commentaire',
  'delete'=>'Supprimer',
  'disable_comments'=>'Désactiver les commentaires',
  'edit_gallery'=>'Enregistrer',
  'edit_group'=>'Enregistrer',
  'edit'=>'Editer',
  'enable_user_comments'=>'Activer les commentaires pour les membres',
  'enable_visitor_comments'=>'Activer les commentaires',
  'filename'=>'Nom du fichier',
  'formular'=>'Formulaire',
  'ftp'=>'FTP',
  'ftp_info'=>'Pour ajouter des fichiers, envoyez vos images par FTP',
  'galleries'=>'Galeries',
  'galleries_available'=>'Des galeries existent encore dans ce groupe. Si vous voulez supprimer ce groupe vous devez tout d\'abord déplacer ou supprimer ces galeries.',
  'gallery'=>'Galerie',
  'gallery_name'=>'Nom de la galerie',
  'group'=>'Groupe',
  'groups'=>'Groupes/Catégories',
  'group_name'=>'Nom du groupe',
  'information_incomplete'=>'Quelques informations sont manquantes.',
  'name'=>'Nom',
  'need_group'=>'Vous devez créer au moins un groupe de galerie avant de créer des galeries.',
  'new_gallery'=>'Nouvelle galerie',
  'new_group'=>'Nouveau groupe',
  'per_form'=>'par formulaire',
  'per_ftp'=>'par FTP',
  'picture'=>'Image',
  'pic_upload'=>'Upload',
  'really_delete_group'=>'Voulez-vous vraiment supprimer ce groupe?',
  'really_delete_gallery'=>'Voulez-vous vraiment supprimer cette galerie?',
  'sort'=>'Ordre',
  'transaction_invalid'=>'Transaction de l\'ID invalide',
  'to_sort'=>'Ordre',
  'upload'=>'Upload',
  'usergalleries'=>'Galeries d\'utilisateurs',
  'usergallery_of'=>'Galerie utilisateur de',
  'visitor_comments'=>'Activer les commentaires',
  'no_thumb'=>'No Thumb'
);
?>