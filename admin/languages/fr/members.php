<?php
/*
##########################################################################
#                                                                        #
#           Version 4       /                        /   /               #
#          -----------__---/__---__------__----__---/---/-               #
#           | /| /  /___) /   ) (_ `   /   ) /___) /   /                 #
#          _|/_|/__(___ _(___/_(__)___/___/_(___ _/___/___               #
#                       Free Content / Management System                 #
#                                   /                                    #
#                                                                        #
#                                                                        #
#   Copyright 2005-2011 by webspell.org                                  #
#                                                                        #
#   visit webSPELL.org, webspell.info to get webSPELL for free           #
#   - Script runs under the GNU GENERAL PUBLIC LICENSE                   #
#   - It's NOT allowed to remove this copyright-tag                      #
#   -- http://www.fsf.org/licensing/licenses/gpl.html                    #
#                                                                        #
#   Code based on WebSPELL Clanpackage (Michael Gruber - webspell.at),   #
#   Far Development by Development Team - webspell.org                   #
#                                                                        #
#   visit webspell.org                                                   #
#                                                                        #
##########################################################################
*/

$language_array = Array(

/* do not edit above this line */

  'access_denied'=>'Accès refusé',
  'access_rights'=>'Droits d\'accès',
  'actions'=>'Actions',
  'active'=>'Actif',
  'activity'=>'Activité',
  'cash_admin'=>'Caisse (Admin)',
  'clanwar_admin'=>'Matchs (Admin)',
  'country_nickname'=>'Pays / Pseudo',
  'delete'=>'Supprimer',
  'description'=>'Description',
  'edit'=>'Editer',
  'edit_member'=>'Enregistrer',
  'error_own_rights'=>'ERREUR: impossible de changer ses propres droits',
  'feedback_admin'=>'Retour d\'info. (Admin)',
  'fightus_admin'=>'Défis (Admin)',
  'file_admin'=>'Fichiers (Admin)',
  'gallery_admin'=>'Galerie (Admin)',
  'group_access'=>'Accès aux groupes du forum',
  'inactive'=>'Inactif',
  'joinus_admin'=>'Recrutement (Admin)',
  'members'=>'Membres',
  'messageboard_admin'=>'Forum (Admin)',
  'messageboard_moderator'=>'Forum (Modérateur)',
  'news_admin'=>'News (Admin)',
  'news_writer'=>'News (Rédacteur)',
  'nickname'=>'Pseudo',
  'no'=>'Non',
  'page_admin'=>'Page (Admin)',
  'polls_admin'=>'Sondages (Admin)',
  'position'=>'Position',
  'really_delete'=>'Voulez-vous vraiment supprimer ce membre?',
  'sort'=>'Ordre',
  'squad'=>'Section',
  'super_admin'=>'Super-Admin',
  'to_sort'=>'Ordre',
  'tooltip_1'=>'<b>Accès:</b><br />- Rubriques des news<br />- Langues des news<br />- News<br />- Articles<br />- Prix<br />- Liens',
  'tooltip_2'=>'<b>Accès:</b><br />- Rédaction de news',
  'tooltip_3'=>'<b>Accès:</b><br />- Sondages',
  'tooltip_4'=>'<b>Accès:</b><br />- Commentaires<br />- Livre d\'or<br />- Livre d\'or des utilisateurs<br />- Tribune Libre',
  'tooltip_5'=>'<b>Accès:</b><br />- Utilisateurs<br />- Droits des utilisateurs<br />- Sections<br />- Membres<br />- Contacts<br />- Newsletter',
  'tooltip_6'=>'<b>Accès:</b><br />- Matchs<br />- Prix<br />- Défis<br />- Calendrier',
  'tooltip_7'=>'<b>Accès:</b><br />- Catégories des forums<br />- Forums<br />- Sujets/Messages (Editer/Supprimer)<br />- Groupes d\'utilisateurs<br />- Membres des groupes<br />- Rangs des utilisateurs',
  'tooltip_8'=>'<b>Accès:</b><br />- Sujets/Messages (Editer/Supprimer)<br />(Le forum doit être attribué)',
  'tooltip_9'=>'<b>Accès:</b><br />- À propos de nous<br />- Rotation de bannière<br />- Icônes<br />- Base de données<br />- Catégories des FAQs<br />- FAQ<br />- Historique<br />- Description<br />- Catégories des liens<br />- Liens<br />- Page verrouilée<br />- Partenaires<br />- Texte défilant<br />- Serveurs<br />- Configurations<br />- Pages statiques<br />- Styles<br />- Mise à jour de webSPELL<br />- Nous recommander<br />- Sponsors',
  'tooltip_10'=>'<b>Accès:</b><br />- Catégories des fichiers<br />- Fichiers<br />- Démos',
  'tooltip_11'=>'<b>Accès:</b><br />- Caisse',
  'tooltip_12'=>'<b>Accès:</b><br />- Catégories des galeries<br />- Galeries<br />- Images',
  'tooltip_13'=>'<b>Accès:</b><br />- Accès à toute l\'administration',
  'transaction_invalid'=>'Transaction de l\'ID invalide',
  'user_admin'=>'Admin',
  'yes'=>'Oui'
);
?>