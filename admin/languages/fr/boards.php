<?php
/*
##########################################################################
#                                                                        #
#           Version 4       /                        /   /               #
#          -----------__---/__---__------__----__---/---/-               #
#           | /| /  /___) /   ) (_ `   /   ) /___) /   /                 #
#          _|/_|/__(___ _(___/_(__)___/___/_(___ _/___/___               #
#                       Free Content / Management System                 #
#                                   /                                    #
#                                                                        #
#                                                                        #
#   Copyright 2005-2011 by webspell.org                                  #
#                                                                        #
#   visit webSPELL.org, webspell.info to get webSPELL for free           #
#   - Script runs under the GNU GENERAL PUBLIC LICENSE                   #
#   - It's NOT allowed to remove this copyright-tag                      #
#   -- http://www.fsf.org/licensing/licenses/gpl.html                    #
#                                                                        #
#   Code based on WebSPELL Clanpackage (Michael Gruber - webspell.at),   #
#   Far Development by Development Team - webspell.org                   #
#                                                                        #
#   visit webspell.org                                                   #
#                                                                        #
##########################################################################
*/

$language_array = Array(

/* do not edit above this line */

  'access_denied'=>'Accès refusé',
  'actions'=>'Actions',
  'add_board'=>'Enregistrer',
  'add_category'=>'Enregistrer',
  'boardinfo'=>'Info. du forum',
  'boardname'=>'Nom du forum',
  'boards'=>'Forums',
  'category'=>'Catégorie',
  'category_info'=>'Info. de la catégorie',
  'category_name'=>'Nom de la catégorie',
  'choose_moderators'=>'Choisissez les modérateurs du forum:',
  'delete'=>'Supprimer',
  'edit'=>'Editer',
  'edit_board'=>'Enregistrer',
  'edit_category'=>'Enregistrer',
  'moderators'=>'Modérateurs',
  'mods'=>'Modérateurs',
  'new_board'=>'Nouveau forum',
  'new_category'=>'Nouvelle catégorie',
  'read_right'=>'Droit de lecture',
  'read_right_info_board'=>'Ce/Ces groupe(s) d\'utilisateurs est/sont autorisé(s) à voir le forum et lire dans le forum.<br />Si aucun groupe d\'utilisateur n\'est sélectionné, tout le monde peut voir et lire ce forum.<br />Utilisez la touche CTRL pour une multi-sélection ou déselection.',
  'really_delete_board'=>'Voulez-vous vraiment supprimer ce forum?',
  'really_delete_category'=>'Voulez-vous vraiment supprimer cette catégorie?',
  'registered_users'=>'Utilisateurs enregistrés',
  'right_info_category'=>'Ce/Ces groupe(s) d\'utilisateurs est/sont autorisé(s) à voir cette catégorie.<br />Si aucun groupe d\'utilisateur n\'est sélectionné, tout le monde peut voir la catégorie.<br />Utilisez la touche CTRL pour une multi-sélection ou déselection.',
  'select_moderators'=>'Enregistrer',
  'sort'=>'Ordre',
  'transaction_invalid'=>'Transaction de l\'ID invalide',
  'to_sort'=>'Ordre',
  'unselect_all'=>'Tout déselectionner',
  'write_right'=>'Droit d\'écriture',
  'write_right_info_board'=>'Ce/Ces groupe(s) d\'utilisateurs est/sont autorisé(s) à écrire sur le forum.<br />Si aucun groupe d\'utilisateur n\'est sélectionné, tous les utilisateurs enregistrés sont autorisés à écrirer sur ce forum.<br />Utilisez la touche CTRL pour une multi-sélection ou déselection.'
);
?>