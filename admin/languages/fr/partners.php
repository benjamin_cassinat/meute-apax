﻿<?php
/*
##########################################################################
#                                                                        #
#           Version 4       /                        /   /               #
#          -----------__---/__---__------__----__---/---/-               #
#           | /| /  /___) /   ) (_ `   /   ) /___) /   /                 #
#          _|/_|/__(___ _(___/_(__)___/___/_(___ _/___/___               #
#                       Free Content / Management System                 #
#                                   /                                    #
#                                                                        #
#                                                                        #
#   Copyright 2005-2009 by webspell.org                                  #
#                                                                        #
#   visit webSPELL.org, webspell.info to get webSPELL for free           #
#   - Script runs under the GNU GENERAL PUBLIC LICENSE                   #
#   - It's NOT allowed to remove this copyright-tag                      #
#   -- http://www.fsf.org/licensing/licenses/gpl.html                    #
#                                                                        #
#   Code based on WebSPELL Clanpackage (Michael Gruber - webspell.at),   #
#   Far Development by Development Team - webspell.org                   #
#                                                                        #
#   visit webspell.org                                                   #
#                                                                        #
##########################################################################
*/

$language_array = Array(

/* do not edit above this line */

  'access_denied'=>'Accès refusé',
  'actions'=>'Actions',
  'add_partner'=>'Enregistrer',
  'back'=>'Retour',
  'banner'=>'Bannière',
  'banner_to_big'=>'Bannière trop grande',
  'clicks'=>'Clics (par jour)',
  'current_banner'=>'Bannière actuelle',
  'delete'=>'Supprimer',
  'edit'=>'Editer',
  'edit_partner'=>'Enregistrer',
  'format_incorrect'=>'Le format de la bannière est incorrect. Veuillez envoyer seulement une bannière en format *.gif, *.jpg et *.png.',
  'homepage_url'=>'Adresse de la bannière',
  'is_displayed'=>'Est affichée ?',
  'max_88x31'=>'(max. 171x51)',
  'new_partner'=>'Nouveau partenaire',
  'no'=>'Non',
  'partner_name'=>'Nom du partenaire',
  'partners'=>'Partenaires',
  'really_delete'=>'Voulez-vous vraiment supprimer ce partenaire?',
  'sort'=>'Ordre',
  'transaction_invalid'=>'Transaction de l\'ID invalide',
  'to_sort'=>'Ordre',
  'yes'=>'Oui'
);
?>