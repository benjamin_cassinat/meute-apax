<?php
/*
##########################################################################
#                                                                        #
#           Version 4       /                        /   /               #
#          -----------__---/__---__------__----__---/---/-               #
#           | /| /  /___) /   ) (_ `   /   ) /___) /   /                 #
#          _|/_|/__(___ _(___/_(__)___/___/_(___ _/___/___               #
#                       Free Content / Management System                 #
#                                   /                                    #
#                                                                        #
#                                                                        #
#   Copyright 2005-2011 by webspell.org                                  #
#                                                                        #
#   visit webSPELL.org, webspell.info to get webSPELL for free           #
#   - Script runs under the GNU GENERAL PUBLIC LICENSE                   #
#   - It's NOT allowed to remove this copyright-tag                      #
#   -- http://www.fsf.org/licensing/licenses/gpl.html                    #
#                                                                        #
#   Code based on WebSPELL Clanpackage (Michael Gruber - webspell.at),   #
#   Far Development by Development Team - webspell.org                   #
#                                                                        #
#   visit webspell.org                                                   #
#                                                                        #
##########################################################################
*/

$language_array = Array(

/* do not edit above this line */

  'access_denied'=>'Accès refusé',
  'change_size'=>'Changer la taille',
  'days'=>'Jours',
  'days_online'=>'Jours en ligne',
  'guest'=>'Invité',
  'guests'=>'Invités',
  'last'=>'Derniers',
  'max_day'=>'Jour record',
  'months'=>'Mois',
  'now_online'=>'Actuellement en ligne',
  'settings'=>'Configurations',
  'show'=>'Voir',
  'show_year_month'=>'Voir une année ou un mois personnalisé',
  'stats'=>'Statistiques',
  'this_month'=>'Ce mois',
  'today'=>'Aujourd\'hui',
  'total'=>'Total',
  'user'=>'Utilisateur',
  'users'=>'Utilisateurs',
  'visitor'=>'Visiteur',
  'visitor_stats_graphics'=>'Statistiques des visiteurs (Graphiques)',
  'visitor_stats_overall'=>'Statistiques des visiteurs (Général)',
  'visits_day'=>'Visites par jour',
  'visits_hour'=>'Visites par heure',
  'visits_month'=>'Visites par mois',
  'visits_total'=>'Visites totales',
  'width_height'=>'(largeur x hauteur)',
  'yesterday'=>'Hier',
  'yyyy'=>'(YYYY)',
  'yyyy_mm'=>'(YYYY.MM)'
);
?>