<?php
/*
##########################################################################
#                                                                        #
#           Version 4       /                        /   /               #
#          -----------__---/__---__------__----__---/---/-               #
#           | /| /  /___) /   ) (_ `   /   ) /___) /   /                 #
#          _|/_|/__(___ _(___/_(__)___/___/_(___ _/___/___               #
#                       Free Content / Management System                 #
#                                   /                                    #
#                                                                        #
#                                                                        #
#   Copyright 2005-2011 by webspell.org                                  #
#                                                                        #
#   visit webSPELL.org, webspell.info to get webSPELL for free           #
#   - Script runs under the GNU GENERAL PUBLIC LICENSE                   #
#   - It's NOT allowed to remove this copyright-tag                      #
#   -- http://www.fsf.org/licensing/licenses/gpl.html                    #
#                                                                        #
#   Code based on WebSPELL Clanpackage (Michael Gruber - webspell.at),   #
#   Far Development by Development Team - webspell.org                   #
#                                                                        #
#   visit webspell.org                                                   #
#                                                                        #
##########################################################################
*/

$language_array = Array(

/* do not edit above this line */

  'access_denied'=>'Accès refusé',
  'back'=>'Retour',
  'bordercolor'=>'Couleur des bordures des tableaux',
  'category_bg'=>'Couleur de fond des catégories',
  'cell_bg1'=>'Cellule 1',
  'cell_bg2'=>'Cellule 2',
  'cell_bg3'=>'Cellule 3',
  'cell_bg4'=>'Cellule 4',
  'draw_color'=>'Couleur des matchs nuls',
  'error_bordercolor'=>'La valeur de la couleur des "bordures des tableaux" est invalide',
  'error_category_bg'=>'La valeur de la couleur de "fond des catégories" est invalide',
  'error_cell_bg1'=>'La valeur de la couleur de la "Cellule 1" est invalide',
  'error_cell_bg2'=>'La valeur de la couleur de la "Cellule 2" est invalide',
  'error_cell_bg3'=>'La valeur de la couleur de la "Cellule 3" est invalide',
  'error_cell_bg4'=>'La valeur de la couleur de la "Cellule 4" est invalide',
  'error_draw_color'=>'La valeur de la couleur des "matchs nuls" est invalide',
  'error_head_bg'=>'La valeur de la couleur de "fond du header" est invalide',
  'error_loose_color'=>'La valeur de la couleur des "matchs perdus" est invalide',
  'error_page_bg'=>'La valeur de la couleur de "fond de la page" est invalide',
  'error_win_color'=>'La valeur de la couleur des "matchs gagnés" est invalide',
  'errors'=>'Il y a des erreurs',
  'head_bg'=>'Couleur de fond du header',
  'loose_color'=>'Couleur des matchs perdus',
  'page_bg'=>'Couleur de fond de la page',
  'page_title'=>'Titre du site',
  'styles'=>'Styles',
  'stylesheet'=>'Feuille de style',
  'stylesheet_info'=>'Ne supprimez pas de classes si vous n\'êtes pas sûr à 100% de ce que vous faîtes !',
  'transaction_invalid'=>'Transaction de l\'ID invalide',
  'update'=>'Enregistrer',
  'win_color'=>'Couleur des matchs gagnés'
);
?>