<?php
/*
##########################################################################
#                                                                        #
#           Version 4       /                        /   /               #
#          -----------__---/__---__------__----__---/---/-               #
#           | /| /  /___) /   ) (_ `   /   ) /___) /   /                 #
#          _|/_|/__(___ _(___/_(__)___/___/_(___ _/___/___               #
#                       Free Content / Management System                 #
#                                   /                                    #
#                                                                        #
#                                                                        #
#   Copyright 2005-2011 by webspell.org                                  #
#                                                                        #
#   visit webSPELL.org, webspell.info to get webSPELL for free           #
#   - Script runs under the GNU GENERAL PUBLIC LICENSE                   #
#   - It's NOT allowed to remove this copyright-tag                      #
#   -- http://www.fsf.org/licensing/licenses/gpl.html                    #
#                                                                        #
#   Code based on WebSPELL Clanpackage (Michael Gruber - webspell.at),   #
#   Far Development by Development Team - webspell.org                   #
#                                                                        #
#   visit webspell.org                                                   #
#                                                                        #
##########################################################################
*/

$language_array = Array(

/* do not edit above this line */

  'access_denied'=>'Accès refusé',
  'error_send'=>'ERREUR: la newsletter ne peut pas être envoyé (la fonction mail du serveur est-elle disponible ?)',
  'html_mail'=>'E-Mail (HTML)',
  'newsletter'=>'Newsletter',
  'no_htmlmail'=>'Bonjour !
Votre système ne supporte pas les E-Mail HTML.
Vous avez reçu le message suivant:',
  'profile'=>'Profil',
  'receptionists'=>'La newsletter a été envoyé !<br /><br />Réceptionniste(s):',
  'remove'=>'Vous pouvez vous retirer de la liste de diffusion',
  'send'=>'Envoyer',
  'send_to'=>'Envoyer à',
  'test'=>'Tester',
  'test_newsletter'=>'Test de la newsletter',
  'title'=>'Titre',
  'transaction_invalid'=>'Transaction de l\'ID invalide',
  'user_clanmembers'=>'Membres du clan',
  'user_newsletter'=>'Abonnés à la newsletter',
  'user_registered'=>'Utilisateurs enregistrés (inclus Membres du clan) ',
  'users'=>'Utilisateurs'
);
?>