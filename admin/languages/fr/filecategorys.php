<?php
/*
##########################################################################
#                                                                        #
#           Version 4       /                        /   /               #
#          -----------__---/__---__------__----__---/---/-               #
#           | /| /  /___) /   ) (_ `   /   ) /___) /   /                 #
#          _|/_|/__(___ _(___/_(__)___/___/_(___ _/___/___               #
#                       Free Content / Management System                 #
#                                   /                                    #
#                                                                        #
#                                                                        #
#   Copyright 2005-2011 by webspell.org                                  #
#                                                                        #
#   visit webSPELL.org, webspell.info to get webSPELL for free           #
#   - Script runs under the GNU GENERAL PUBLIC LICENSE                   #
#   - It's NOT allowed to remove this copyright-tag                      #
#   -- http://www.fsf.org/licensing/licenses/gpl.html                    #
#                                                                        #
#   Code based on WebSPELL Clanpackage (Michael Gruber - webspell.at),   #
#   Far Development by Development Team - webspell.org                   #
#                                                                        #
#   visit webspell.org                                                   #
#                                                                        #
##########################################################################
*/

$language_array = Array(

/* do not edit above this line */

  'access_denied'=>'Accès refusé',
  'actions'=>'Actions',
  'add_category'=>'Enregistrer',
  'category'=>'Catégorie',
  'category_name'=>'Nom de la catégorie',
  'delete'=>'Supprimer',
  'enter_name'=>'Vous devez entrer un nom',
  'edit'=>'Editer',
  'edit_category'=>'Enregistrer',
  'file_categories'=>'Catégories des fichiers',
  'main'=>'Principal',
  'new_category'=>'Nouvelle catégorie',
  'really_delete'=>'Voulez-vous vraiment supprimer cette catégorie?',
  'sub_category'=>'Sous-catégorie de',
  'transaction_invalid'=>'Transaction de l\'ID invalide'
);
?>