<?php
/*
##########################################################################
#                                                                        #
#           Version 4       /                        /   /               #
#          -----------__---/__---__------__----__---/---/-               #
#           | /| /  /___) /   ) (_ `   /   ) /___) /   /                 #
#          _|/_|/__(___ _(___/_(__)___/___/_(___ _/___/___               #
#                       Free Content / Management System                 #
#                                   /                                    #
#                                                                        #
#                                                                        #
#   Copyright 2005-2011 by webspell.org                                  #
#                                                                        #
#   visit webSPELL.org, webspell.info to get webSPELL for free           #
#   - Script runs under the GNU GENERAL PUBLIC LICENSE                   #
#   - It's NOT allowed to remove this copyright-tag                      #
#   -- http://www.fsf.org/licensing/licenses/gpl.html                    #
#                                                                        #
#   Code based on WebSPELL Clanpackage (Michael Gruber - webspell.at),   #
#   Far Development by Development Team - webspell.org                   #
#                                                                        #
#   visit webspell.org                                                   #
#                                                                        #
##########################################################################
*/

$language_array = Array(

/* do not edit above this line */

  'access_denied'=>'Accès refusé',
  'back'=>'Retour',
  'edit_group_users'=>'Enregistrer',
  'error_group'=>'ERREUR: groupe d\'utilisateur inexistant',
  'filter'=>'Filtre',
  'filter_anyadmin'=>'Admins',
  'filter_clanmember'=>'Membres du clan',
  'filter_registered'=>'Utilisateurs enregistrés',
  'filter_superadmin'=>'Super Admin',
  'go'=>'Go',
  'group_users'=>'Membres des groupes',
  'groups'=>'Groupes d\'utilisateurs',
  'or_just'=>'ou juste',
  'save'=>'Enregistrer',
  'save_and_jump'=>'Sauvegarder et aller à la page',
  'select_all'=>'Tout sélectionner',
  'show'=>'Voir',
  'user_filter'=>'Filtre d\'utilisateur',
  'users_from_group'=>'Membres du groupe',
  'transaction_invalid'=>'Transaction de l\'ID invalide'
);
?>