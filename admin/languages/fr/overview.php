<?php
/*
##########################################################################
#                                                                        #
#           Version 4       /                        /   /               #
#          -----------__---/__---__------__----__---/---/-               #
#           | /| /  /___) /   ) (_ `   /   ) /___) /   /                 #
#          _|/_|/__(___ _(___/_(__)___/___/_(___ _/___/___               #
#                       Free Content / Management System                 #
#                                   /                                    #
#                                                                        #
#                                                                        #
#   Copyright 2005-2011 by webspell.org                                  #
#                                                                        #
#   visit webSPELL.org, webspell.info to get webSPELL for free           #
#   - Script runs under the GNU GENERAL PUBLIC LICENSE                   #
#   - It's NOT allowed to remove this copyright-tag                      #
#   -- http://www.fsf.org/licensing/licenses/gpl.html                    #
#                                                                        #
#   Code based on WebSPELL Clanpackage (Michael Gruber - webspell.at),   #
#   Far Development by Development Team - webspell.org                   #
#                                                                        #
#   visit webspell.org                                                   #
#                                                                        #
##########################################################################
*/

$language_array = Array(

/* do not edit above this line */

  'access_denied'=>'Accès refusé',
  'apache'=>'Apache',
  'apache_modules'=>'Modules Apache',
  'create'=>'Créer',
  'current'=>'Courant',
  'databasename'=>'Nom de la base de données',
  'disable'=>'Désactivé',
  'enable'=>'Activé',
  'green'=>'Vert',
  'hello'=>'Bonjour',
  'interface'=>'Interface',
  'last_login'=>'votre dernière connexion date du',
  'legend'=>'Légende',
  'mysql_version'=>'Version MySQL',
  'notice1'=>'Avis:',
  'notice2'=>'Valeur haute:',
  'off'=>'Arrêté',
  'on'=>'Démarré',
  'orange'=>'Orange',
  'path'=>'Chemin',
  'php_settings'=>'Configurations PHP',
  'php_version'=>'Version PHP',
  'read'=>'Lire',
  'red'=>'Rouge',
  'server_api'=>'API',
  'server_host'=>'Hôte',
  'server_machine'=>'Machine',
  'server_os'=>'Système d\'exploitation',
  'server_release'=>'Sortie',
  'server_version'=>'Version',
  'serverinfo'=>'Informations du serveur',
  'setting_error'=>'Configuration non recommandée',
  'setting_notice'=>'Configuration avec avis',
  'setting_ok'=>'Configuration ok',
  'supported_types'=>'Types supportés',
  'version'=>'Version',
  'webspell_version'=>'Version webSPELL',
  'welcome'=>'Bienvenue dans votre panel d\'administration webSPELL',
  'welcome_message'=>'Ce système d\'administration vous permets de gérer votre site, utilisez le menu sur la gauche.<br />Si vous avez des questions, veuillez utiliser notre <a href="http://www.webspell.fr/index.php?site=forum" target="_blank">Support</a>.<br /><br />Merci d\'avoir choisi webSPELL.<br /><br />Votre <a href="http://www.webspell.org" target="_blank">équipe de développement webSPELL</a>',
  'zend_version'=>'Version Zend',
  'na'=>'Information non disponible'
);
?>