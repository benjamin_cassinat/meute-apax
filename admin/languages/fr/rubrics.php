<?php
/*
##########################################################################
#                                                                        #
#           Version 4       /                        /   /               #
#          -----------__---/__---__------__----__---/---/-               #
#           | /| /  /___) /   ) (_ `   /   ) /___) /   /                 #
#          _|/_|/__(___ _(___/_(__)___/___/_(___ _/___/___               #
#                       Free Content / Management System                 #
#                                   /                                    #
#                                                                        #
#                                                                        #
#   Copyright 2005-2011 by webspell.org                                  #
#                                                                        #
#   visit webSPELL.org, webspell.info to get webSPELL for free           #
#   - Script runs under the GNU GENERAL PUBLIC LICENSE                   #
#   - It's NOT allowed to remove this copyright-tag                      #
#   -- http://www.fsf.org/licensing/licenses/gpl.html                    #
#                                                                        #
#   Code based on WebSPELL Clanpackage (Michael Gruber - webspell.at),   #
#   Far Development by Development Team - webspell.org                   #
#                                                                        #
#   visit webspell.org                                                   #
#                                                                        #
##########################################################################
*/

$language_array = Array(

/* do not edit above this line */

  'access_denied'=>'Accès refusé',
  'actions'=>'Actions',
  'add_rubric'=>'Enregistrer',
  'back'=>'Retour',
  'delete'=>'Supprimer',
  'edit'=>'Editer',
  'edit_rubric'=>'Enregistrer',
  'format_incorrect'=>'Le format de la bannière est incorrect. Veuillez envoyer seulement une bannière en format *.gif, *.jpg et *.png.',
  'information_incomplete'=>'Quelques informations sont manquantes.',
  'new_rubric'=>'Nouvelle rubrique',
  'news_rubrics'=>'Rubrique des news',
  'picture'=>'Image',
  'picture_upload'=>'Upload',
  'really_delete'=>'Voulez-vous vraiment supprimer cette rubrique?',
  'rubric_name'=>'Nom de la rubrique',
  'transaction_invalid'=>'Transaction de l\'ID invalide'
);
?>