-- phpMyAdmin SQL Dump
-- version 3.3.2deb1ubuntu1
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- Généré le : Dim 19 Mai 2013 à 21:49
-- Version du serveur: 5.1.69
-- Version de PHP: 5.3.2-1ubuntu4.19

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `admin_apax_webspell`
--
-- CREATE DATABASE `admin_apax_webspell` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `admin_apax_webspell`;

-- --------------------------------------------------------

--
-- Structure de la table `about`
--

CREATE TABLE IF NOT EXISTS `about` (
  `about` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `about`
--

INSERT INTO `about` (`about`) VALUES
('Meneur :  WAZA-PsY-4-x \r\n \r\nCo-leadeur : WAZA-gp_one, gyom-88 \r\n \r\nRecruteur : Yuri, Pignouf-30, Ankoleloup \r\n\r\nMembres : Dartoch, LaPsYkOsE, jujube-basque , ItadakimasTB, lucas-74_44, kenouzZ-28, Bio-gamer , lord-beckett59, clems-206, ousou, aulyro, Mvgusta3, sweelfor , Rakoto, Lebetz, lil-blakstar, Misteur-reveur, faveval, fisher ');

-- --------------------------------------------------------

--
-- Structure de la table `articles`
--

CREATE TABLE IF NOT EXISTS `articles` (
  `articlesID` int(11) NOT NULL AUTO_INCREMENT,
  `date` int(14) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `content` text NOT NULL,
  `screens` text NOT NULL,
  `poster` int(11) NOT NULL DEFAULT '0',
  `link1` varchar(255) NOT NULL DEFAULT '',
  `url1` varchar(255) NOT NULL DEFAULT '',
  `window1` int(1) NOT NULL DEFAULT '0',
  `link2` varchar(255) NOT NULL DEFAULT '',
  `url2` varchar(255) NOT NULL DEFAULT '',
  `window2` int(1) NOT NULL DEFAULT '0',
  `link3` varchar(255) NOT NULL DEFAULT '',
  `url3` varchar(255) NOT NULL DEFAULT '',
  `window3` int(1) NOT NULL DEFAULT '0',
  `link4` varchar(255) NOT NULL DEFAULT '',
  `url4` varchar(255) NOT NULL DEFAULT '',
  `window4` int(1) NOT NULL DEFAULT '0',
  `votes` int(11) NOT NULL DEFAULT '0',
  `points` int(11) NOT NULL DEFAULT '0',
  `rating` int(11) NOT NULL DEFAULT '0',
  `saved` int(1) NOT NULL DEFAULT '0',
  `viewed` int(11) NOT NULL DEFAULT '0',
  `comments` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`articlesID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `articles`
--

INSERT INTO `articles` (`articlesID`, `date`, `title`, `content`, `screens`, `poster`, `link1`, `url1`, `window1`, `link2`, `url2`, `window2`, `link3`, `url3`, `window3`, `link4`, `url4`, `window4`, `votes`, `points`, `rating`, `saved`, `viewed`, `comments`) VALUES
(1, 1361715533, '26 FÉVRIER 2013 - MAINTENANCE SUR PC, PS3, 360', '', '', 1, 'Forum Battlelog', 'http://battlelog.battlefield.com/bf3/fr/forum/threadview/2832654625451245143/last/', 1, '', 'http://', 1, '', 'http://', 1, '', 'http://', 1, 1, 9, 9, 1, 103, 2);

-- --------------------------------------------------------

--
-- Structure de la table `articles_contents`
--

CREATE TABLE IF NOT EXISTS `articles_contents` (
  `articlesID` int(11) NOT NULL,
  `content` text NOT NULL,
  `page` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `articles_contents`
--

INSERT INTO `articles_contents` (`articlesID`, `content`, `page`) VALUES
(1, 'Bonjour, \r\n\r\nAfin de préparer l''arrivée de la prochaine extension Battlefield 3 : End Game [battlefield.com], une maintenance de Battlelog et des bases de données aura lieu mardi 26 février 2013 [timeanddate.com] aux alentours de 08h00 / 10h00 heure française sur une durée estimée de 6 heures. \r\n\r\n[ALIGN=center][IMG]http://uppix.net/d/c/4/b4f0ea98ed003dd168038349e98bd.png[/IMG]\r\n[/ALIGN]\r\n#BF3 will be intermittently unavailable across all platforms in preparation for #BF3EndGame on Feb 26 from 0800 to 1400 GMT\r\n\r\nSource Twitter @Battlefield : https://twitter.com/Battlefield/status/305591318959251456 [twitter.com] \r\n\r\nJe vous invite à surveiller les annonces et les échanges sur les différentes zones de news FR et US ainsi que sur les médias sociaux. \r\nSi vous voyez l''indisponibilité s''affiner, n''hésitez pas à tenir au courant les autres joueurs. ;) \r\n\r\nVotre capacité à accéder au multijoueur de Battlefield 3 sur PC, PS3, 360 en sera certainement affectée mardi 26/02/2013 dans la journée. \r\n\r\nSachez que les liens suivants restent toujours utiles si vous cherchez une information rapide : \r\nhttp://www.ea.com/servers/battlefield3 [ea.com] \r\nhttp://www.origin.com/fr/status [origin.com] \r\nhttps://twitter.com/OriginStatus [twitter.com] \r\nhttps://twitter.com/Battlefield [twitter.com] \r\n\r\nMerci de votre patience. \r\n\r\nBonne journée', 0);

-- --------------------------------------------------------

--
-- Structure de la table `awards`
--

CREATE TABLE IF NOT EXISTS `awards` (
  `awardID` int(11) NOT NULL AUTO_INCREMENT,
  `date` int(14) NOT NULL DEFAULT '0',
  `squadID` int(11) NOT NULL DEFAULT '0',
  `award` varchar(255) NOT NULL DEFAULT '',
  `homepage` varchar(255) NOT NULL DEFAULT '',
  `rang` int(11) NOT NULL DEFAULT '0',
  `info` text NOT NULL,
  PRIMARY KEY (`awardID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `awards`
--

INSERT INTO `awards` (`awardID`, `date`, `squadID`, `award`, `homepage`, `rang`, `info`) VALUES
(1, 1361401200, 1, 'Glandeurs suprêmes ', 'http://www.glandeur.fr/', 1, 'héhé');

-- --------------------------------------------------------

--
-- Structure de la table `banned_ips`
--

CREATE TABLE IF NOT EXISTS `banned_ips` (
  `banID` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(255) NOT NULL,
  `deltime` int(15) NOT NULL,
  `reason` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`banID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `banned_ips`
--


-- --------------------------------------------------------

--
-- Structure de la table `banner`
--

CREATE TABLE IF NOT EXISTS `banner` (
  `bannerID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `url` varchar(255) NOT NULL DEFAULT '',
  `banner` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`bannerID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `banner`
--


-- --------------------------------------------------------

--
-- Structure de la table `bannerrotation`
--

CREATE TABLE IF NOT EXISTS `bannerrotation` (
  `bannerID` int(11) NOT NULL AUTO_INCREMENT,
  `banner` varchar(255) NOT NULL DEFAULT '',
  `bannername` varchar(255) NOT NULL DEFAULT '',
  `bannerurl` varchar(255) NOT NULL DEFAULT '',
  `displayed` varchar(255) NOT NULL DEFAULT '',
  `hits` int(11) DEFAULT '0',
  `date` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`bannerID`),
  UNIQUE KEY `banner` (`banner`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `bannerrotation`
--


-- --------------------------------------------------------

--
-- Structure de la table `buddys`
--

CREATE TABLE IF NOT EXISTS `buddys` (
  `buddyID` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL DEFAULT '0',
  `buddy` int(11) NOT NULL DEFAULT '0',
  `banned` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`buddyID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=66 ;

--
-- Contenu de la table `buddys`
--

INSERT INTO `buddys` (`buddyID`, `userID`, `buddy`, `banned`) VALUES
(1, 5, 1, 0),
(2, 5, 3, 0),
(3, 5, 4, 0),
(4, 5, 2, 0),
(5, 5, 6, 0),
(6, 5, 7, 0),
(7, 5, 8, 0),
(8, 5, 9, 0),
(9, 5, 10, 0),
(10, 5, 11, 0),
(11, 2, 5, 0),
(12, 5, 12, 0),
(13, 5, 13, 0),
(14, 1, 6, 0),
(15, 1, 8, 0),
(16, 1, 5, 0),
(17, 5, 14, 0),
(18, 5, 15, 0),
(19, 5, 17, 0),
(20, 5, 18, 0),
(22, 5, 22, 0),
(23, 5, 24, 0),
(24, 2, 25, 0),
(25, 5, 25, 0),
(26, 5, 28, 0),
(27, 5, 31, 0),
(28, 5, 16, 0),
(29, 5, 27, 0),
(30, 5, 26, 0),
(31, 5, 29, 0),
(33, 10, 16, 0),
(34, 5, 36, 0),
(35, 37, 5, 0),
(36, 37, 10, 0),
(37, 37, 8, 0),
(38, 37, 1, 0),
(40, 5, 30, 0),
(41, 5, 34, 0),
(42, 5, 37, 0),
(43, 5, 46, 0),
(44, 30, 27, 0),
(45, 37, 30, 0),
(46, 34, 30, 0),
(47, 34, 5, 0),
(48, 34, 27, 0),
(49, 34, 11, 0),
(50, 34, 6, 0),
(51, 34, 4, 0),
(52, 34, 16, 0),
(53, 34, 1, 0),
(54, 34, 3, 0),
(55, 34, 28, 0),
(56, 34, 8, 0),
(57, 34, 2, 0),
(58, 34, 10, 0),
(59, 34, 21, 0),
(60, 89, 5, 0),
(61, 106, 1, 0),
(62, 89, 1, 0),
(63, 5, 89, 0),
(64, 5, 35, 0),
(65, 110, 5, 0);

-- --------------------------------------------------------

--
-- Structure de la table `captcha`
--

CREATE TABLE IF NOT EXISTS `captcha` (
  `hash` varchar(255) NOT NULL DEFAULT '',
  `captcha` int(11) NOT NULL DEFAULT '0',
  `deltime` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`hash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `captcha`
--

INSERT INTO `captcha` (`hash`, `captcha`, `deltime`) VALUES
('00502c1344a7eaa56007a039202bf108', 59765, 1369027839),
('0055a565e72aa86aca83a49ff96f06e4', 0, 1369027204),
('0137d00248ebf96f821f682b0886b40d', 13, 1369026701),
('0161339fa59e367d77f36fd64cc08c9a', 37177, 1368996508),
('022bfe817d7fbd79fe357f3b5b12a328', 27, 1369021151),
('032d7726dbb43fd2c372978d13301ee0', 84413, 1369051230),
('040dcf0098392ff405ed0512f6c9ac4e', 38, 1369027202),
('046e70eddfb909eaddb59b792f944f73', 45, 1369000951),
('084598a43126f713781ebaac16088b07', 30, 1369031624),
('08e51fe07278465128b36e13a73c6af2', 92701, 1369010712),
('0d6ec0c4846f7244ca107942e22174ca', 30, 1369051256),
('0f763a7ef16cb1ff96665ad2d9628f01', 18, 1369073655),
('11d54374de8140db6f3745554ff5342b', 24, 1369027615),
('132c884562e9e684b78eda16b48af79a', 65491, 1369027698),
('13e20665e9b6217776b934259143d4fd', 35371, 1369027644),
('17657409461a5364dc9cfc94908a4545', 13, 1369027666),
('1abbadabaa1508bcb30de3de9104fb3c', 53537, 1369027696),
('1ada23dd09a0ad88f8ab9c7fb0a6ae8f', 52, 1369027681),
('1c51ba7b9cc248ef51ed3264679421ee', 39002, 1369051243),
('1ca2e30b2a97454af20e44bbb711e2b1', 17049, 1369031708),
('1d266d687eabec934487cde5852844dd', 82922, 1369027635),
('1e9d8849cfb8598e4872d68b108294c4', 18, 1369017507),
('1f3e28f74f7d110f21ab13d7128f95d9', 21326, 1369027589),
('1fff1fe6c4c91cc2be670ccaa4edee0a', 0, 1369070147),
('22d19b773b94bad88c61c492606ed9eb', 65967, 1369026673),
('23f28a9db033ed1ad1c5fcb0bf14217e', 93461, 1369051206),
('25c5400669d20e8e4930d68db4a8f9c7', 34350, 1369027707),
('2727b327dcf3ab2aa8996a78e45c9cff', 94616, 1369027531),
('28d2e6f7f640d7e36ca3c39120480f78', 13206, 1369051848),
('28f704c295920638f3fa0514da6537d8', 3436, 1369056636),
('299d5de074af64f29ccf4cb89e45fb59', 2342, 1369027741),
('2e071e56092be4d501bc29a9ec8e410a', 43, 1369027690),
('3112ce91779565e4765ca0ee4de8f8fa', 22591, 1369051204),
('32e14585ab016f0e75dae70cd795321b', 83067, 1369062932),
('35959c9c1c091805e600ca0baa576195', 79507, 1369027513),
('36082973be8d4607a97f5710c3fd23e1', 43042, 1369051202),
('36bab1d187b4c01ca6f21410b9b50391', 22141, 1369070323),
('381f6fd332d4134194d5c9ee0144c6fd', 32750, 1369030163),
('3956052364671aa7191e2eac3e0f48ad', 9005, 1369027591),
('3aadaac90d1b0d0b6b0433b3b45130be', 2, 1369027678),
('3c054ae3a9093e87213678a09eebc3d4', 15, 1369027664),
('3e6ac1893da384d883329c295bbe0704', 2, 1369027792),
('3ef589bd5a308eb40f3e4409bd0230b2', 47783, 1369058044),
('3f69457287b32be5661b46481fa0c47a', 30, 1369027676),
('4132d0e9e2856db15c545f46d4a4e0be', 27, 1369027723),
('449a22187b6322d51514a4b74e2b96d6', 73261, 1369027505),
('4578f29e56d55753fc54d210f1068429', 88116, 1369014007),
('46190c4bd3a4e82f418114014dca29df', 10597, 1369032549),
('46268720397664ee978a90d1ef5a7074', 39401, 1369027503),
('4a6392565bdbd82efe6bb544b74b027f', 4, 1369007008),
('4bd387e70aa9edbf5f2cbbe5d6dc527a', 1339, 1369051268),
('4d6d771116f449833116228ed983df99', 58263, 1369051239),
('4eff32a38bb375d531778c47d729a9a5', 90310, 1369056636),
('51e422f51c7d7db7704b34e870dad4e8', 0, 1369070157),
('5398f7a5da6d8ac05152d84c2d04388c', 19, 1369055074),
('548a91e123ad100272cfdd06f6797e02', 81277, 1369051192),
('549f638c7191e25b76be9bf075ed54b6', 0, 1368995028),
('559a656b118b6b127ca3c823a71a206a', 51864, 1369033201),
('583816004cb7be5557e30c4cf4d8b10e', 10, 1369027206),
('598304c795e00e5dde094f19567d22a6', 34, 1369027539),
('5c71eca5c494d079fb1ca352a7556d29', 36, 1369051200),
('5face0008df5963073c4a0ab898110f6', 0, 1369070138),
('5fc40ff16bc49bfffde9d2e3176fde94', 97041, 1369057978),
('605debe733bacf3772d7beb8a2c2f7a8', 30, 1369011781),
('68eb84a935919ae7a38938a529dc1314', 23, 1369027609),
('69ac19cf495d85fc3b54106379ddd672', 6, 1369026666),
('6b9e751fca0c2fd2778bbb8cd9fbc84c', 29, 1369027671),
('6c681201d5e1b4697482e0bcd1125145', 74993, 1369008870),
('6d6d8caa14131ef56f152104eb2b4dee', 31, 1369056152),
('6dc0b50505579aef66cec4bbaa0a3c12', 75597, 1369076715),
('6f050ea5806ec6dcc28e4815af5f166f', 12, 1369027687),
('6f4db16e642464b601e2e4ee701be750', 18, 1369051208),
('71d31077d3d22700e0ea621b8a767f38', 25, 1369076802),
('753096b6bbfa43936c62a682e8a4a04b', 35103, 1369009499),
('76b2f929cbe80bde5eaff64ef966198d', 5, 1369055435),
('7c6cd435e6e77cb755876e028e797c70', 5, 1369027208),
('7c7f98fcd63275182c1c820415cddf45', 57843, 1369027546),
('7ccfb504c30b21b900415676c6360aff', 20785, 1369027656),
('7f926a91b829f71e114028bf856dd7f8', 0, 1369070130),
('818a26677d2dea7b6beff4a75a30be75', 13, 1369027627),
('821f1ad72e529b4d555a1e2613cbd8b2', 2, 1369011098),
('8277176184163eb15c85d44a991e6c55', 45, 1369021109),
('831dcced9f63ebb3267b752e085598c6', 18114, 1369051263),
('84d468e1ba08f8b67fb333f0d484e8cc', 24, 1369027674),
('8735d486c1d643745e1f633763b24c07', 0, 1369049937),
('89891a1cd9a1c8b43fbf80ac705716d3', 7, 1369027516),
('89b49d5c8c98225be76445ac2f6f36c9', 99589, 1369026684),
('8ab52cee8e56db9943946c5c31eb1b91', 14825, 1369051215),
('8b2bb63d6674525a04e5330f1addfa3f', 4, 1369027649),
('8bdcf52fc4b8875b7cc2fe2511091215', 0, 1368994988),
('8c3b0ba568999eaa1e1f7aad630aef59', 67545, 1369027774),
('900666c89197ab6aa6e6a2b3d2858801', 50416, 1369077819),
('9179cd2b19665c09f1c8e762f4403ece', 37650, 1369027715),
('92c1c302d920cc346108591c4c827828', 95853, 1369027811),
('92e8e6c5a7c313fa8be788c588643b6b', 8, 1369051194),
('961fdecd9243c7bed719b8fb61846a19', 31, 1369026739),
('967150158ad3d9f44ec0e5dd0dfcdcb4', 26683, 1369027557),
('9cc630ac2ca67f8f7ce1052c2f691877', 20, 1369027766),
('9d536906495c421c25126e0a8e76df60', 0, 1369049964),
('a01659ac732b8505f9f3fa14ffb1cf77', 24, 1369051219),
('a0385cfdc31ffab5c8c6dfd4eef70602', 2363, 1369030803),
('a10971844fe6b7994f2fa5a63525ab9e', 10264, 1369027669),
('a80ebb8cc07fd2aebef71244ea15924a', 5, 1369051252),
('a9b198f97060309b12284621387f6a62', 0, 1369070154),
('aa0ebcc781e422a4ca70db11a26c9aad', 89059, 1369054503),
('abf5648fbea75b05e3f98405b9240a8b', 71762, 1369064947),
('ad13ed97d19ef6045806122509610452', 35314, 1369027575),
('ad7a9c252b8c6c9efdec67056c3954d1', 89036, 1369027607),
('aed8c53bb6313711412f42deef98cd9a', 68482, 1369040916),
('b06963d92df4c3aa7b7b84dc0479b1d5', 83719, 1369023410),
('b116d05cd1d99b1c750460621add0e0b', 87251, 1369007771),
('b1b6a392c9aaa4c6593d4dcc613a9242', 50, 1369027646),
('b230956e74c73d341aa40bf786397035', 40, 1369027154),
('b24fcaabe172f57fc1757497dcdf43ca', 87805, 1369027520),
('b2de5ad16d2130de44dcc957e468a9d2', 65067, 1369027527),
('b34eab389a390db9103812571b01bf42', 0, 1369070170),
('b3e7995fefcc4bf48a30aee33655294a', 97093, 1369027831),
('b468f85503d388df69dd6ef712b5268b', 17, 1369060475),
('b515f408516b21be77616d91e105a53a', 57518, 1369027683),
('b6cdda6f109d685e403668eb86c876e3', 34, 1369027565),
('b73d2bc8a80e28c4216b0b0f5f16212a', 94902, 1369027661),
('ba2ae4c2308110620e9c84fee8a2e386', 16, 1369027508),
('ba6a396da7254fdbec4a44fd14ace21f', 0, 1369070135),
('bb8b1e43b7b28e392f16358c283ae30d', 80138, 1368992226),
('c55f8afc6d2fbad1d07c0ba6b304c40a', 56800, 1369027685),
('c6d30abe297ffb3692eaf2036fd9d6fa', 12, 1369051227),
('cca33c5e4a21c036ebec75b2a11ecef7', 6, 1369025033),
('d2f446849f06d76ecc1484b1289e5e35', 0, 1369037442),
('d4b6eaf78005b05f7599f0114fc2d337', 75601, 1369027658),
('d6148322ebbe5a01f1e5a591e28edb0a', 32, 1369005829),
('d86e03e67eaa813f6d5667401dd918b9', 30357, 1369056519),
('da1db3381d7736a3c875f24a021340a5', 2, 1369027611),
('dad238a006c67e458afa07e4fb7c9e35', 91549, 1369004315),
('db1ad1eca0c3f1433977e1c52d1d948e', 0, 1368995008),
('dc20e550b7df20f8f79f2ff1b9a82dc6', 16, 1369051221),
('dc4e910e78451b804fa692ccd2caa48d', 34895, 1369027584),
('de7cfc8ea9f1339a9874533b100a8b4e', 6, 1369011553),
('dfd6cfa065f2e91454838645ac8d676f', 1456, 1369054506),
('dfe1f4ad4ecf144a29eb901479abd53c', 0, 1368995017),
('e03853f2a4e6865748018cd1d1fae788', 2, 1369009098),
('e13cef6bdbc69d968e0a4d044cc6f713', 51, 1369027653),
('e2db7e7c9da98b64b7a236cff0f75c90', 77102, 1369027651),
('e577a4fdf00906755324f3c42300bb2c', 88689, 1369027783),
('e5f62500d2ea71bd049dcfd8b65f94ee', 57046, 1369027587),
('eaca816bdc735b32cbfbe2e674d6e78b', 59125, 1369051223),
('f1d6475d9eca4e34c386b5a817f81ea0', 56716, 1369051246),
('f816bd24be6bec16305e448221ea82b7', 1, 1369027580),
('f892867119e73195e10a1e35ab749dff', 19, 1369051196),
('f92f64643cf0a48125d31b24204f07c7', 32, 1369027803),
('f9e64d9f61a9051387e3ba0f96903931', 28, 1369027582),
('fba9e89dea19b73d1f8aa8a0dfc64743', 56, 1369027825),
('fe0f6ec28bcf75cfb0c656855fe72b88', 59722, 1369027750);

-- --------------------------------------------------------

--
-- Structure de la table `cash_box`
--

CREATE TABLE IF NOT EXISTS `cash_box` (
  `cashID` int(11) NOT NULL AUTO_INCREMENT,
  `date` int(14) NOT NULL DEFAULT '0',
  `paydate` int(14) NOT NULL DEFAULT '0',
  `usedfor` text NOT NULL,
  `info` text NOT NULL,
  `totalcosts` double(8,2) NOT NULL DEFAULT '0.00',
  `usercosts` double(8,2) NOT NULL DEFAULT '0.00',
  `squad` int(11) NOT NULL DEFAULT '0',
  `konto` text NOT NULL,
  PRIMARY KEY (`cashID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `cash_box`
--


-- --------------------------------------------------------

--
-- Structure de la table `cash_box_payed`
--

CREATE TABLE IF NOT EXISTS `cash_box_payed` (
  `payedID` int(11) NOT NULL AUTO_INCREMENT,
  `cashID` int(11) NOT NULL DEFAULT '0',
  `userID` int(11) NOT NULL DEFAULT '0',
  `costs` double(8,2) NOT NULL DEFAULT '0.00',
  `date` int(14) NOT NULL DEFAULT '0',
  `payed` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`payedID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `cash_box_payed`
--


-- --------------------------------------------------------

--
-- Structure de la table `challenge`
--

CREATE TABLE IF NOT EXISTS `challenge` (
  `chID` int(11) NOT NULL AUTO_INCREMENT,
  `date` int(14) NOT NULL DEFAULT '0',
  `cwdate` int(14) NOT NULL DEFAULT '0',
  `squadID` varchar(255) NOT NULL DEFAULT '',
  `opponent` varchar(255) NOT NULL DEFAULT '',
  `opphp` varchar(255) NOT NULL DEFAULT '',
  `oppcountry` char(2) NOT NULL DEFAULT '',
  `league` varchar(255) NOT NULL DEFAULT '',
  `map` varchar(255) NOT NULL DEFAULT '',
  `server` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `info` text NOT NULL,
  PRIMARY KEY (`chID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `challenge`
--

INSERT INTO `challenge` (`chID`, `date`, `cwdate`, `squadID`, `opponent`, `opphp`, `oppcountry`, `league`, `map`, `server`, `email`, `info`) VALUES
(1, 1361966562, 1361997000, '1', 'A voir', 'http://www.google.fr', 'fr', 'Entrainement', 'Metro', 'APAX', 'benftwc@gmail.com', 'Yep, mettez moi sur le forum si vous n''êtes pas dispo, il faut également trouver un adversaire pour gamer :D\r\n\r\nConcernant l''heure, 21.30, dites moi si ça vous va pour les prochaines sessions ^^');

-- --------------------------------------------------------

--
-- Structure de la table `clanwars`
--

CREATE TABLE IF NOT EXISTS `clanwars` (
  `cwID` int(11) NOT NULL AUTO_INCREMENT,
  `date` int(14) NOT NULL DEFAULT '0',
  `squad` int(11) NOT NULL DEFAULT '0',
  `game` varchar(5) NOT NULL DEFAULT '',
  `league` varchar(255) NOT NULL DEFAULT '',
  `leaguehp` varchar(255) NOT NULL DEFAULT '',
  `opponent` varchar(255) NOT NULL DEFAULT '',
  `opptag` varchar(255) NOT NULL DEFAULT '',
  `oppcountry` char(2) NOT NULL DEFAULT '',
  `opphp` varchar(255) NOT NULL DEFAULT '',
  `maps` varchar(255) NOT NULL DEFAULT '',
  `hometeam` varchar(255) NOT NULL DEFAULT '',
  `oppteam` varchar(255) NOT NULL DEFAULT '',
  `server` varchar(255) NOT NULL DEFAULT '',
  `hltv` varchar(255) NOT NULL,
  `homescore` text,
  `oppscore` text,
  `screens` text NOT NULL,
  `report` text NOT NULL,
  `comments` int(1) NOT NULL DEFAULT '0',
  `linkpage` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`cwID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Contenu de la table `clanwars`
--

INSERT INTO `clanwars` (`cwID`, `date`, `squad`, `game`, `league`, `leaguehp`, `opponent`, `opptag`, `oppcountry`, `opphp`, `maps`, `hometeam`, `oppteam`, `server`, `hltv`, `homescore`, `oppscore`, `screens`, `report`, `comments`, `linkpage`) VALUES
(1, 1361401200, 1, 'BF3', 'Tournois RdP', 'http://', 'Killer Special Squad', 'KSS', 'fr', 'http://www.google.fr/', 'a:2:{i:0;s:5:"bazar";i:1;s:5:"seine";}', 'a:5:{i:0;s:1:"1";i:1;s:2:"19";i:2;s:1:"5";i:3;s:1:"4";i:4;s:1:"3";}', '', '', '', 'a:2:{i:0;s:1:"2";i:1;s:1:"2";}', 'a:2:{i:0;s:1:"0";i:1;s:1:"0";}', '', 'Victoire par forfait au 2ème round', 2, 'http://battlelog.battlefield.com/bf3/fr/battlereport/show/78887448/4/210612024/ '),
(2, 1361574000, 1, 'BF3', 'Amical', 'http://', 'SBS', 'SBS', 'fr', 'http://', 'a:2:{i:0;s:4:"Karg";i:1;s:6:"Basard";}', 'a:5:{i:0;s:2:"15";i:1;s:1:"8";i:2;s:1:"7";i:3;s:1:"4";i:4;s:1:"3";}', '', '', '', 'a:2:{i:0;s:1:"2";i:1;s:1:"2";}', 'a:2:{i:0;s:1:"0";i:1;s:1:"0";}', '', 'Très bon travail d''équipe en attaque.\r\nTrès bonne défense.\r\n\r\n\r\nDonc je propose à travailler un peu de Squad rush, et de préparer d''autres matchs dans ce mode de jeu.\r\nJe trouve que c''est une tactique plus facile à travailler. Et des matchs plus rapide et moins stressant.', 2, 'http://'),
(4, 1365890400, 3, 'BF3', 'Match T12', 'http://', '2099', '2099', 'fr', 'http://2099.teamplay.fr/', 'a:3:{i:0;s:20:"Tempête de feu (CQ)";i:1;s:22:"Pic de Damavand (RUEE)";i:2;s:36:"calcule des points suite a égalité";}', 'a:14:{i:0;s:1:"1";i:1;s:1:"6";i:2;s:2:"27";i:3;s:2:"28";i:4;s:2:"16";i:5;s:2:"34";i:6;s:2:"30";i:7;s:2:"22";i:8;s:1:"3";i:9;s:1:"4";i:10;s:1:"2";i:11;s:1:"5";i:12;s:2:"10";i:13;s:1:"8";}', '', '', '', 'a:3:{i:0;s:1:"1";i:1;s:1:"1";i:2;s:1:"1";}', 'a:3:{i:0;s:1:"1";i:1;s:1:"1";i:2;s:1:"0";}', '', 'GG à tous !', 2, 'http://'),
(8, 1367964000, 3, 'BF3', 'Match T12', 'http://', '2099', '[2099]', 'fr', 'http://', 'a:4:{i:0;s:4:"Karg";i:1;s:4:"Karg";i:2;s:5:"Metro";i:3;s:5:"Metro";}', 'a:0:{}', '', 'Apax', '', 'a:4:{i:0;s:1:"1";i:1;s:1:"0";i:2;s:1:"1";i:3;s:1:"1";}', 'a:4:{i:0;s:1:"0";i:1;s:1:"1";i:2;s:1:"0";i:3;s:1:"0";}', '', 'FELICITATION !!!!!!! Oui , Voici un trés beau match , voila 2 semaines que je boss dur sur ce match , et vous me l''avez bien rendu !! Certe sur métro ca etait un peu le boxon dans les micros , mais qu''est ce que tu veux faire face a 12 loups qui veulent de la viande fraiche !!\r\nKharg nous avons un peu de boulot a effectuer , la difficultés primaire rencontré , une equipe d''igla sur pied en face !! Donc difficile de mener dans les airs mais on se débrouillez bien au sol avec 2 trés bon tankiste ,\r\nMetro : 2 trés belles manches ac une premier manche enorme de la part des 12 apax avec 217 tickets gagné,\r\nLa deuxieme etait mené par les 2099 , avec 50 tickets de retards nous avons rattrapé a la fin pour les depasser et gagner la manche !!\r\nAlors si on me demande si je suis fiere d''etre le meneur de la Meute APAX , je répondrais que je suis meneur grace a vous , et fierté n''est pas encore assez fort pour eprouver le kiff que j''ai eu !\r\nMerci a vous !!\r\n\r\nPsy\r\n\r\n\r\n              Apax       2099\r\n\r\nkarg            43/0\r\nkarg             0/47\r\nmetro        217/0\r\nmetro          63/0', 1, 'http://');

-- --------------------------------------------------------

--
-- Structure de la table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `commentID` int(11) NOT NULL AUTO_INCREMENT,
  `parentID` int(11) NOT NULL DEFAULT '0',
  `type` char(2) NOT NULL DEFAULT '',
  `userID` int(11) NOT NULL DEFAULT '0',
  `nickname` varchar(255) NOT NULL DEFAULT '',
  `date` int(14) NOT NULL DEFAULT '0',
  `comment` text NOT NULL,
  `url` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `ip` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`commentID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=56 ;

--
-- Contenu de la table `comments`
--

INSERT INTO `comments` (`commentID`, `parentID`, `type`, `userID`, `nickname`, `date`, `comment`, `url`, `email`, `ip`) VALUES
(1, 1, 'ne', 2, '', 1361820054, 'pas mal ! \r\nje te met une video certainement une des plus belle que j''ai jamais vu sur un bf c''est du grandiose !\r\nhttp://www.youtube.com/watch?v=XVjIcSSTyrI', '', '', ''),
(4, 6, 'ne', 1, '', 1363548300, 'Bonne nouvelle ça. J''attends la nouvelle plaque si la rumeur s''avère ne pas être qu''un bruit de couloir ;)', '', '', ''),
(5, 6, 'ne', 5, '', 1363611651, 'j’espère aussi surtout pour voir le teaser et savoir se que nous réserve bf4, plusieurs possibilité totalement différente....', '', '', ''),
(6, 1, 'de', 0, 'JTJ', 1364003041, 'Ce droit, mais avec [url=http://www.wowofwow.fr]wow po[/url] un placement macro sur votre Bar de Vente aux enchères vous pouvez voyager autour d''entre tous les deux les deux professions. On est cela vous devriez pour créer une banque alt, cela aussi le fondamental du rasant qui vous aidera avec votre Meilleur les Professions de Réalisation Hou la D''or sont critiques à ce stade du jeu comme vous ne voulez pas gaspiller le temps de rasant précieux rembobinant à la Ville pour vendre votre truc sur la Maison de Vente  [url=http://www.wowofwow.fr]guide po wow[/url]  aux enchères. Un autre les choses cela simple que vous devriez lever votre caractère pour raser 5.Si ce n''est pas nécessaire, je le conseil vous pour ne pas passer or sur n''importe quoi comme la nourriture ou la boisson, car vous en aurez besoin pour poster des choses sur l''AH. Pour un nouveau joueur la voie la plus rapide de complètement apprécier le Monde de Wacraft recueille le Meilleur les Professions de réalisation Hou la d''or. Le Meilleur les Professions de Réalisation [url=http://www.wowofwow.fr]wow boutique[/url] Hou la D''or Extrait et Herbalism. ', 'http://', '', '85.195.87.66'),
(7, 1, 'po', 5, '', 1364144695, 'pour l''instant on est pas assez dans les match pour organisé un tournoi, enfin c''est mon avis ;)', '', '', ''),
(8, 4, 'ne', 0, 'vcxd', 1364179021, 'Ces professions [url=http://www.raidgold.fr/guild-wars-2-gold.html]GW2 Gold[/url] incluent blacksmithing, jewelcrafting, le fait d''adapter et leatherworking. Aux niveaux supérieurs vous pouvez créer des articles pour vendre avec de hauts prix à la maison de vente aux enchères. L''armure, les Pierres précieuses, les Armes et les sacs peuvent être d''énormes brasseurs d''affaires pour vous mais ils sont trop difficiles pour apprendre. Vous pouvez utiliser l''exploitation minière et blacksmithing pour niveler, mais la combinaison d''eux avec  une profession se [url=http://www.raidgold.fr/guild-wars-2-gold.html]gold guild wars[/url] rassemblant peut vous aider à gagner plus d''argent. Avec cette approche vous serez en mesure de gagner de l''or tant aux plus bas que niveaux supérieurs. Vous pouvez enchanter des articles pour d''autres joueurs pour utiliser pour faire de l''argent et vous pouvez aussi désillusionner des articles pour vendre les composantes pour un profit décent. Si vous êtes un alchimiste, vous pouvez vendre les potions, les élixirs et le transmuez vous créez avec vos adresses.  Les potions guérissantes sont très demandées toujours. Si vous choisissez enchanteur, il y a  [url=http://www.raidgold.fr/guild-wars-2-gold.html]guild wars factions[/url] deux voies pour vous pour gagner de l''or.', 'http://', '', '85.195.87.66'),
(9, 1, 'ne', 0, 'android tablets', 1364189099, 'Thanks for sharing.I really appreciate it that you shared with us such informative post,great tips and very easy to understand.[URL=http://www.newfreeshipping.com/notebook-tablet-pc-mid-c438/android-os-c306.html]android tablets[/URL]', 'http://', '', '204.74.211.113'),
(10, 1, 'po', 1, '', 1364207365, 'Complètement, je considère plus ce sondage comme un test qu''autres chose, j''étais à cours d''idée, et "Aimez vous le site" entre nous ... ^^', '', '', ''),
(12, 2, 'ne', 0, 'cxvd', 1364356255, 'Ces professions [url=http://www.raidgold.fr/guild-wars-2-gold.html]GW2 Gold[/url] incluent blacksmithing, jewelcrafting, le fait d''adapter et leatherworking. Aux niveaux supérieurs vous pouvez créer des articles pour vendre avec de hauts prix à la maison de vente aux enchères. L''armure, les Pierres précieuses, les Armes et les sacs peuvent être d''énormes brasseurs d''affaires pour vous mais ils sont trop difficiles pour apprendre. Vous pouvez utiliser l''exploitation minière et blacksmithing pour niveler, mais la combinaison d''eux avec  une profession se [url=http://www.raidgold.fr/guild-wars-2-gold.html]gold guild wars[/url] rassemblant peut vous aider à gagner plus d''argent. Avec cette approche vous serez en mesure de gagner de l''or tant aux plus bas que niveaux supérieurs. Vous pouvez enchanter des articles pour d''autres joueurs pour utiliser pour faire de l''argent et vous pouvez aussi désillusionner des articles pour vendre les composantes pour un profit décent. Si vous êtes un alchimiste, vous pouvez vendre les potions, les élixirs et le transmuez vous créez avec vos adresses.  Les potions guérissantes sont très demandées toujours. Si vous choisissez enchanteur, il y a  [url=http://www.raidgold.fr/guild-wars-2-gold.html]guild wars factions[/url] deux voies pour vous pour gagner de l''or.', 'http://', '', '85.195.87.66'),
(13, 4, 'ne', 0, 'longmaobaobao', 1364367426, 'Anthony is merely twenty six years, men and women love to get your ex along with Tracy McGrady throughout 2004 in comparison with. The two get negatives, Anthony Taking pictures outside the house while Tracy McGrady, occupation three-point taking pictures merely 40. 8%, along with they doesn''t need your eye-sight along with impression involving Tracy McGrady''s moving past, the helps along with turnover rate of just one.[url=http://www.nbaleagueshop.us/kyrie-irving-jersey/]Kyrie Irving Jersey[/url]\r\n[url=http://www.nbaleagueshop.us/chris-paul-jersey/]chris paul jersey[/url]\r\n[url=http://www.nbaleagueshop.us/james-harden-jersey/]harden jersey[/url]', 'http://', 'qilanlongmao@hotmail.com', '120.40.90.74'),
(15, 2, 'ne', 0, 'newfreeshipping', 1364523942, 'K To a friend who recommended a great e-commerce website, everything inside the phone Tablet PC, the address is	[url=http://www.newfreeshipping.com/android-5.0]	android 5.0 	[/url]\r\n', 'http://', '', '58.250.54.33'),
(16, 4, 'ne', 0, 'asdaghgq', 1364523970, 'K To a friend who recommended a great e-commerce website, everything inside the phone Tablet PC, the address is	[url=http://www.newfreeshipping.com/notebook-tablet-pc-mid-c438/android-os-c306.html]	android tablets	[/url]\r\n', 'http://', '', '58.250.54.33'),
(17, 1, 'ne', 0, 'adgsdfh', 1364523982, 'K To a friend who recommended a great e-commerce website, everything inside the phone Tablet PC, the address is	[url=http://www.newfreeshipping.com/car-electronics-c50/car-lights-c408.html]	led car lights	[/url]\r\n', 'http://', '', '58.250.54.33'),
(18, 1, 'ar', 0, 'cxvd', 1364525283, 'Ces professions [url=http://www.raidgold.fr/guild-wars-2-gold.html]GW2 Gold[/url] incluent blacksmithing, jewelcrafting, le fait d''adapter et leatherworking. Aux niveaux supérieurs vous pouvez créer des articles pour vendre avec de hauts prix à la maison de vente aux enchères. L''armure, les Pierres précieuses, les Armes et les sacs peuvent être d''énormes brasseurs d''affaires pour vous mais ils sont trop difficiles pour apprendre. Vous pouvez utiliser l''exploitation minière et blacksmithing pour niveler, mais la combinaison d''eux avec  une profession se [url=http://www.raidgold.fr/guild-wars-2-gold.html]gold guild wars[/url] rassemblant peut vous aider à gagner plus d''argent. Avec cette approche vous serez en mesure de gagner de l''or tant aux plus bas que niveaux supérieurs. Vous pouvez enchanter des articles pour d''autres joueurs pour utiliser pour faire de l''argent et vous pouvez aussi désillusionner des articles pour vendre les composantes pour un profit décent. Si vous êtes un alchimiste, vous pouvez vendre les potions, les élixirs et le transmuez vous créez avec vos adresses.  Les potions guérissantes sont très demandées toujours. Si vous choisissez enchanteur, il y a  [url=http://www.raidgold.fr/guild-wars-2-gold.html]guild wars factions[/url] deux voies pour vous pour gagner de l''or.', 'http://', '', '85.195.87.66'),
(19, 1, 'ne', 0, 'freeshipping', 1364534825, 'Y  Online sale, Cheap China Electronics --China Electronics Wholesale at  pandawill[url=http://www.pandawill.com/lenovo-lephone-k800-le-magic-30-os-80mp-camera-45-inch-ips-screen-3g-gps-black-p66954.html]Lenovo K800[/url]\r\n', '', 'kindwewe5@hotmaio.com', '58.250.54.33'),
(20, 4, 'ne', 0, 'freeshipping', 1364534867, 'Y  Online sale, Cheap China Electronics --China Electronics Wholesale at  pandawill[url=http://www.pandawill.com/802-smart-phone-android-41-os-bcm21654-10ghz-40-inch-50mp-camera-yellow-p72611.html]820 Smart Phone[/url]\r\n', '', 'kindwewe5@hotmaio.com', '58.250.54.33'),
(21, 2, 'ne', 0, 'freeshipping', 1364534873, 'Y  Online sale, Cheap China Electronics --China Electronics Wholesale at  pandawill[url=http://www.pandawill.com/onda-v813-quad-core-16gb-tablet-pc-8-inch-ips-screen-android-41-2g-ram-4k-video-ultra-thin-white-p72710.html]ONDA V813[/url]\r\n', '', 'kindwewe5@hotmaio.com', '58.250.54.33'),
(22, 1, 'ne', 0, 'China Wholesale', 1364540854, ' g Glad to visit your website, we provide a large number of electronic products, low prices.we have two sites，specialized sales of tablet  mobilephones and electronic products, you can  buy sth!!	[url=http://www.pandawill.com/ultrafire-h008-q3-flexible-zoom-expansion-flashlight-camping-lamp-p68508.html]	Ultrafire Q3	[/url]\r\n', 'http://', '', '58.250.54.33'),
(23, 2, 'ne', 0, 'China Wholesale', 1364540915, ' g Glad to visit your website, we provide a large number of electronic products, low prices.we have two sites，specialized sales of tablet  mobilephones and electronic products, you can  buy sth!!	[url=http://www.pandawill.com/ultrafire-501b-q5-portable-led-lantern-alternative-torch-head-flashlight-camp-light-p68463.html]	Ultrafire 501B 	[/url]\r\n', 'http://', '', '58.250.54.33'),
(24, 4, 'ne', 0, 'China Wholesale', 1364540930, ' g Glad to visit your website, we provide a large number of electronic products, low prices.we have two sites，specialized sales of tablet  mobilephones and electronic products, you can  buy sth!!	[url=http://www.pandawill.com/thl-w7-quad-core-smart-phone-android-42-mtk6589-57-inch-hd-ips-screen-3g-gps-32mp-front-camera-p72830.html]	ThL W7+	[/url]\r\n', 'http://', '', '58.250.54.33'),
(25, 5, 'ga', 8, '', 1364584789, 'ce gars qui fait de la pub pour la compagnie du sur saut c ''est beau', '', '', ''),
(26, 4, 'ne', 0, 'buy wow gold news', 1364636907, 'Wow, more ret paladin nerfs: /  <a href="http://www.isleofwightfacebook.co.uk/index.php?do=/blog/141507/my-parents-obtained-me-these-http-guy4game-com-au-5-years-ago-and-theyre-ne/" title="buy wow gold news">buy wow gold news</a>', 'http://www.isleofwightfacebook.co.u', 'uuiidhmt@gmail.com', '58.20.98.93'),
(27, 1, 'de', 0, 'waegerg', 1364976297, '\r\nLes trompeurs et les développeurs ressembleront toujours au chat [url=http://www.diablo3usa.fr]Diablo 3 Gold[/url]  et à la souris. Taillade dans le jeu sont toujours rapiécés pour prévenir n''importe quelle exploitation de l''économie du jeu ou de ses joueurs, donc il est important de recevoir votre Diablo 3 tricheurs d''un site Internet. Cela peut être un [url=http://www.diablo3usa.fr]Gold pour diablo 3[/url] avertissement pour les joueurs quand la Sécurité de guerres de Guilde a commencé à attraper la tricherie de gens.\r\n', 'http://', '', '85.195.87.66'),
(28, 1, 'ne', 0, 'waegreg', 1364976322, '\r\nLes trompeurs et les développeurs ressembleront toujours au chat [url=http://www.diablo3usa.fr]Diablo 3 Gold[/url]  et à la souris. Taillade dans le jeu sont toujours rapiécés pour prévenir n''importe quelle exploitation de l''économie du jeu ou de ses joueurs, donc il est important de recevoir votre Diablo 3 tricheurs d''un site Internet. Cela peut être un [url=http://www.diablo3usa.fr]Gold pour diablo 3[/url] avertissement pour les joueurs quand la Sécurité de guerres de Guilde a commencé à attraper la tricherie de gens.\r\n', 'http://', '', '85.195.87.66'),
(29, 1, 'ne', 0, 'China free shipping', 1365301693, 's At 7.9-inches, with a lower resolution and slower processor than the retina iPads, and at a non-competitive starting price point of $330, does this current iPad have any relevance in this market? Lets find out.[url=http://www.pandawill.com/cube-u30gt-2-quad-core-rk3188-101-inch-tablet-pc-hd1080p-ips-screen-android-41-2g-32g-bluetooth-white-p72311.html]cube u30gt2 quad core[/url]\r\n', 'http://', 'pandawill22@gmail.com', '72.52.116.242'),
(30, 4, 'ne', 0, 'China free shipping', 1365301718, 's At 7.9-inches, with a lower resolution and slower processor than the retina iPads, and at a non-competitive starting price point of $330, does this current iPad have any relevance in this market? Lets find out.[url=http://www.pandawill.com/ultrafire-wf503b-cree-r5-350-lumen-led-flashlight-torch-5-mode-1x18650-battery-titanium-color-p52480.html]UltraFire WF-503B [/url]\r\n', 'http://', 'pandawill22@gmail.com', '72.52.116.242'),
(32, 1, 'cw', 0, 'China free shipping', 1365301747, 's At 7.9-inches, with a lower resolution and slower processor than the retina iPads, and at a non-competitive starting price point of $330, does this current iPad have any relevance in this market? Lets find out.[url=http://www.pandawill.com/ultrafire-wf501a-cree-r5-280-lumen-led-flashlight-torch-5-mode-1x16340-battery-p52350.html]UltraFire WF-501A[/url]\r\n', 'http://', 'pandawill22@gmail.com', '72.52.116.242'),
(33, 2, 'ne', 0, 'China free shipping', 1365301762, 's At 7.9-inches, with a lower resolution and slower processor than the retina iPads, and at a non-competitive starting price point of $330, does this current iPad have any relevance in this market? Lets find out.[url=http://www.pandawill.com/new-ultrafire-c8-cree-q5-5mode-tactical-flashlight-yellow-light-p51229.html]UltraFire C8[/url]\r\n', 'http://', 'pandawill22@gmail.com', '72.52.116.242'),
(34, 2, 'cw', 0, 'China free shipping', 1365301776, 's At 7.9-inches, with a lower resolution and slower processor than the retina iPads, and at a non-competitive starting price point of $330, does this current iPad have any relevance in this market? Lets find out.[url=http://www.pandawill.com/ultrafire-c10-cree-q5-5mode-waterproof-led-flashlight-with-strap-p51937.html]UltraFire C10 [/url]\r\n', 'http://', 'pandawill22@gmail.com', '72.52.116.242'),
(35, 1, 'de', 0, 'China free shipping', 1365301789, 's At 7.9-inches, with a lower resolution and slower processor than the retina iPads, and at a non-competitive starting price point of $330, does this current iPad have any relevance in this market? Lets find out.[url=http://www.pandawill.com/somic-g923-professional-35mm-game-headset-headphone-with-microphone-p72593.html]SOMiC G923[/url]\r\n', 'http://', 'pandawill22@gmail.com', '72.52.116.242'),
(36, 1, 'ar', 0, 'China free shipping', 1365301804, 's At 7.9-inches, with a lower resolution and slower processor than the retina iPads, and at a non-competitive starting price point of $330, does this current iPad have any relevance in this market? Lets find out.[url=http://www.pandawill.com/m2-smart-phone-android-23-os-sc6820-10ghz-40-inch-30mp-camera-orange-p71236.html]M2 Smart Phone[/url]\r\n', 'http://', 'pandawill22@gmail.com', '72.52.116.242'),
(37, 4, 'cw', 5, '', 1365960694, '[COLOR=#87CEEB]Bien joué a tous !!!!!\r\nCela n''a pas été simple car nous avion une bonne team en face (bon teamplay) la preuve veut qu''il a fallu joué les point sur la dernière manche[/COLOR]\r\n\r\n[COLOR=#FFA500]Sur pic de damavan, un bon jeux de notre part même si notre strat demande a être pofiné en attaque[/COLOR]\r\n[COLOR=#DEB887]attaque: 5 relais détruit\r\ndéfense: 2 relais perdu \r\nrésultat: apax "5" / 2099 "2"[/COLOR]\r\n\r\n[COLOR=#FFA500]Sur tempete de feu, vu qu''il n''y a pas eu d''entrainement, c''était un peut le bordel ;) mais on a su se reprendre sur la 2eme partie et du coup remporté le match :p[/COLOR]\r\n[COLOR=#DEB887]allé:   apax "0" / 2099 "12"\r\nretour: apax "31" / 2099 "0"\r\nrésultat: apax "31" / 2099 "12"\r\n[/COLOR]\r\n[COLOR=#87CEEB]Encore un gros GG a tous !!!!![/COLOR]\r\n\r\n', '', '', ''),
(38, 1, 'cw', 0, 'measponge', 1366079773, 'World of Warcraft est un jeu en ligne internationalement reconnu qui a de nombreux [url=http://www.wowofwow.fr]wow po[/url] admirateurs et de ventilateurs. Chaque semaine, des milliers de personnes entrent dans le monde étonnant de Warcraft jeu en ligne. Wow hante le joueur à la suite des grandes lignes, sous réserve de nombreux types de caractères et des fonctionnalités incroyables. Mais pour vous d''atteindre un stade particulier,[url=http://www.wowofwow.fr]wow gold fr[/url] d''obtenir d''excellents résultats et le pouvoir sur la communauté Warcraft, vous aurez certainement besoin de prendre beaucoup de tentatives ainsi que le temps. Par la suite débutants Warcraft devrait se doter,[url=http://www.wowofwow.fr]wow gold[/url] dans la sérénité. Si vous souhaitez déplacer les gammes Warcraft, vous devez obtenir Selver wow.\r\n', 'http://www.wowofwow.fr', 'gamelove@hotmail.com', '85.195.109.130'),
(39, 1, 'po', 71, '', 1366651826, 'je viendrais faire un tours quand y'' auras des journées speciale, surtout si c'' est only snip verrous :D', '', '', ''),
(40, 2, 'po', 71, '', 1366651887, 'j'' y serais si c'' est only snip a verrous :D', '', '', ''),
(41, 4, 'ne', 0, 'og jordans for sale', 1366778963, 'I think this is<a href="http://www.cjordans.com/jordan-4-retro-c-7.html">jordan4 cavs</a> a real great blog.Thanks Again. <a href="http://www.cjordans.com/jordan-12-retro-c-14.html">jordan 12 for sale</a>Fantastic.', 'http://shop2.jumpman23s.com/', 'tddhnarae@gmail.com', '199.193.70.230'),
(42, 4, 'ne', 0, 'jordan 11 concords', 1367025308, 'Say, you got a nice <a href="tp://www.nesiankicks.com/">retro jordan for sale</a>blog.Really looking forward to read more. Keep writing.', 'http://www.nesiankicks.com/36-Jordan-Retro-11', 'haalsam@gmail.com', '199.193.70.230'),
(43, 1, 'cw', 0, 'StoneWilliam', 1367025481, 'Welcome game users to our site to [url=http://www.3zoom.com/swtor+cdkey-44.html][B]Buy SWTOR CD Key[/B][/url], our website offers the cheapest cheap game gold. Diablo 3 Gold, Guild Wars 2 gold fiery game gold game gold being sold on our site. Game users to [url=http://www.3zoom.com/guild+wars+2+gold-35.html][B]buy Guild Wars 2 Gold[/B][/url], very grateful to you to visit us on our website. Of course, our website does not sell the game gold, our website also sell game account and cdkey game items such as the BNS account; The Elder Scrolls the CD Key. Also, please note that when you [url=http://www.3zoom.com/guild+wars+2+gold-35.html][B]buy GW2 Gold[/B][/url] or other items on our website, if there is a problem, please contact us immediately, our website 24x7 online service.', 'http://www.3zoom.com/', 'format412@yahoo.com', '126.114.225.194'),
(44, 8, 'ne', 1, '', 1367064115, 'Plaque rare ?!?', '', '', ''),
(45, 8, 'ne', 5, '', 1367101149, 'ouaip ^^ en fait tu joue comme une partie normal sauf que tout les joueurs doive avoir des plaque rare pour que les autre est une chance de pouvoir les piqués ^^ pour ma part je métrait la plaque de la team, celle avec les loups ;)', '', '', ''),
(46, 8, 'ne', 94, '', 1367236450, 'Only Défibrilateur XD', '', '', ''),
(47, 8, 'ne', 5, '', 1367257080, '^^ oui sur une carte comme métro sa peut être fandar :p', '', '', ''),
(48, 4, 'ne', 0, 'jordan 11 for sale', 1367426446, 'Awesome <a href="http://www.louisvuitton6outlet.com/lv-epi-leather-c-1.html">real louis vuitton sale</a>post.Much thanks again. Want more.', 'http://www.specialsjordans.com/air-jordan-11-c-11.html', 'asabzuktyb@gmail.com', '199.193.70.230'),
(50, 8, 'ne', 34, '', 1367708418, 'toch tu la eu ou la plaque avec le loup ? :O\r\n', '', '', ''),
(51, 8, 'ne', 11, '', 1367778578, 'C''est a dire spécial VIP ???', '', '', ''),
(52, 8, 'ne', 5, '', 1367950627, 'Vip c''est qu''il faut que le chef d''escouade reste en vie par tout les moyen possible et imaginable en sachan qu''il n''aura que le pistolé pour se défendre, on vera bien si on arive a le metre en place sur une journée ou s''il faut le garder pour une soirée\r\n', '', '', ''),
(53, 8, 'ne', 5, '', 1367950771, 'Demain, journée spécial plaque rare !!!\r\n\r\nLa règle est simple, les règles du serv ne change pas, le seul critaire c''est d''avoir une plaque rare pour venir sur le serv,toute personne venant avec une plaque banale se verra kick!!! après a vous de joué du couteau !!!! ;)', '', '', ''),
(54, 8, 'cw', 1, '', 1368448537, 'Très beau match en effet.\r\n\r\nMerci au 2099 et puisse cette partie se re-offrir à nous ;)', '', '', ''),
(55, 8, 'ne', 11, '', 1368477525, 'Ok euh j''avait pas vue l''évenement Plaque rare (aucun coup de cut ou autre)', '', '', '');

-- --------------------------------------------------------

--
-- Structure de la table `contact`
--

CREATE TABLE IF NOT EXISTS `contact` (
  `contactID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `email` varchar(200) NOT NULL,
  `sort` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`contactID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `contact`
--

INSERT INTO `contact` (`contactID`, `name`, `email`, `sort`) VALUES
(1, 'Administrator', 'benftwc@gmail.com', 1);

-- --------------------------------------------------------

--
-- Structure de la table `counter`
--

CREATE TABLE IF NOT EXISTS `counter` (
  `hits` int(20) NOT NULL DEFAULT '0',
  `online` int(14) NOT NULL DEFAULT '0',
  `maxonline` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `counter`
--

INSERT INTO `counter` (`hits`, `online`, `maxonline`) VALUES
(3349, 1361454910, 27);

-- --------------------------------------------------------

--
-- Structure de la table `counter_iplist`
--

CREATE TABLE IF NOT EXISTS `counter_iplist` (
  `dates` varchar(255) NOT NULL DEFAULT '',
  `del` int(20) NOT NULL DEFAULT '0',
  `ip` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `counter_iplist`
--

INSERT INTO `counter_iplist` (`dates`, `del`, `ip`) VALUES
('18.05.2013', 1368906695, '119.63.196.32'),
('18.05.2013', 1368908227, '109.23.236.23'),
('18.05.2013', 1368910108, '95.108.241.252'),
('18.05.2013', 1368910844, '89.3.201.170'),
('18.05.2013', 1368913705, '199.30.24.61'),
('19.05.2013', 1368920518, '208.80.194.159'),
('19.05.2013', 1368934637, '89.83.161.182'),
('19.05.2013', 1368937008, '92.149.194.49'),
('19.05.2013', 1368938431, '220.161.151.222'),
('19.05.2013', 1368940263, '173.208.165.194'),
('19.05.2013', 1368940703, '69.197.129.42'),
('19.05.2013', 1368944331, '66.249.74.211'),
('19.05.2013', 1368948027, '90.84.144.18'),
('19.05.2013', 1368956829, '88.173.218.29'),
('19.05.2013', 1368957085, '66.249.75.217'),
('19.05.2013', 1368957134, '88.167.3.244'),
('19.05.2013', 1368957227, '66.249.75.136'),
('19.05.2013', 1368958786, '120.43.29.30'),
('19.05.2013', 1368961860, '66.249.75.76'),
('19.05.2013', 1368963422, '157.55.32.80'),
('19.05.2013', 1368963472, '93.182.199.77'),
('19.05.2013', 1368964482, '204.12.226.2'),
('19.05.2013', 1368964873, '88.162.192.51'),
('19.05.2013', 1368968102, '157.55.32.110'),
('19.05.2013', 1368968103, '157.55.33.47'),
('19.05.2013', 1368968103, '157.55.33.47'),
('19.05.2013', 1368968222, '65.55.24.221'),
('19.05.2013', 1368968284, '157.55.33.14'),
('19.05.2013', 1368968284, '157.55.33.14'),
('19.05.2013', 1368968673, '157.56.92.151'),
('19.05.2013', 1368968673, '157.56.92.151'),
('19.05.2013', 1368968673, '157.56.92.151'),
('19.05.2013', 1368969035, '157.55.32.93'),
('19.05.2013', 1368969035, '157.55.32.93'),
('19.05.2013', 1368969035, '157.55.32.93'),
('19.05.2013', 1368970236, '157.55.32.107'),
('19.05.2013', 1368970236, '157.55.32.107'),
('19.05.2013', 1368970236, '157.55.32.107'),
('19.05.2013', 1368970236, '157.55.32.107'),
('19.05.2013', 1368970665, '92.146.94.253'),
('19.05.2013', 1368976531, '198.20.69.74'),
('19.05.2013', 1368976532, '117.26.225.170'),
('19.05.2013', 1368978536, '88.190.254.18'),
('19.05.2013', 1368982941, '37.160.6.169'),
('19.05.2013', 1368983636, '79.84.90.137'),
('19.05.2013', 1368984568, '119.63.196.93'),
('19.05.2013', 1368989792, '88.176.242.208'),
('19.05.2013', 1368990147, '90.84.144.243'),
('19.05.2013', 1368990290, '92.141.198.14');

-- --------------------------------------------------------

--
-- Structure de la table `counter_stats`
--

CREATE TABLE IF NOT EXISTS `counter_stats` (
  `dates` varchar(255) NOT NULL DEFAULT '',
  `count` int(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `counter_stats`
--

INSERT INTO `counter_stats` (`dates`, `count`) VALUES
('21.02.2013', 8),
('22.02.2013', 4),
('23.02.2013', 7),
('24.02.2013', 15),
('25.02.2013', 11),
('26.02.2013', 14),
('27.02.2013', 15),
('28.02.2013', 7),
('01.03.2013', 7),
('02.03.2013', 8),
('03.03.2013', 14),
('04.03.2013', 13),
('05.03.2013', 24),
('06.03.2013', 17),
('07.03.2013', 14),
('08.03.2013', 12),
('09.03.2013', 13),
('10.03.2013', 13),
('11.03.2013', 14),
('12.03.2013', 30),
('13.03.2013', 13),
('14.03.2013', 23),
('15.03.2013', 26),
('16.03.2013', 19),
('17.03.2013', 23),
('18.03.2013', 19),
('19.03.2013', 25),
('20.03.2013', 37),
('21.03.2013', 50),
('22.03.2013', 42),
('23.03.2013', 44),
('24.03.2013', 36),
('25.03.2013', 45),
('26.03.2013', 34),
('27.03.2013', 32),
('28.03.2013', 36),
('29.03.2013', 46),
('30.03.2013', 59),
('31.03.2013', 25),
('01.04.2013', 35),
('02.04.2013', 45),
('03.04.2013', 52),
('04.04.2013', 41),
('05.04.2013', 27),
('06.04.2013', 35),
('07.04.2013', 46),
('08.04.2013', 40),
('09.04.2013', 36),
('10.04.2013', 54),
('11.04.2013', 38),
('12.04.2013', 52),
('13.04.2013', 36),
('14.04.2013', 39),
('15.04.2013', 45),
('16.04.2013', 28),
('17.04.2013', 27),
('18.04.2013', 44),
('19.04.2013', 39),
('20.04.2013', 40),
('21.04.2013', 38),
('22.04.2013', 49),
('23.04.2013', 47),
('24.04.2013', 48),
('25.04.2013', 93),
('26.04.2013', 47),
('27.04.2013', 120),
('28.04.2013', 72),
('29.04.2013', 57),
('30.04.2013', 105),
('01.05.2013', 67),
('02.05.2013', 83),
('03.05.2013', 113),
('04.05.2013', 56),
('05.05.2013', 68),
('06.05.2013', 58),
('07.05.2013', 75),
('08.05.2013', 37),
('09.05.2013', 38),
('10.05.2013', 44),
('11.05.2013', 34),
('12.05.2013', 35),
('13.05.2013', 38),
('14.05.2013', 42),
('15.05.2013', 34),
('16.05.2013', 29),
('17.05.2013', 42),
('18.05.2013', 42),
('19.05.2013', 44);

-- --------------------------------------------------------

--
-- Structure de la table `countries`
--

CREATE TABLE IF NOT EXISTS `countries` (
  `countryID` int(11) NOT NULL AUTO_INCREMENT,
  `country` varchar(255) NOT NULL,
  `short` varchar(3) NOT NULL,
  PRIMARY KEY (`countryID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=237 ;

--
-- Contenu de la table `countries`
--

INSERT INTO `countries` (`countryID`, `country`, `short`) VALUES
(1, 'Argentina', 'ar'),
(2, 'Australia', 'au'),
(3, 'Austria', 'at'),
(4, 'Belgium', 'be'),
(5, 'Bosnia Herzegowina', 'ba'),
(6, 'Brazil', 'br'),
(7, 'Bulgaria', 'bg'),
(8, 'Canada', 'ca'),
(9, 'Chile', 'cl'),
(10, 'China', 'cn'),
(11, 'Colombia', 'co'),
(12, 'Czech Republic', 'cz'),
(13, 'Croatia', 'hr'),
(14, 'Cyprus', 'cy'),
(15, 'Denmark', 'dk'),
(16, 'Estonia', 'ee'),
(17, 'Finland', 'fi'),
(18, 'Faroe Islands', 'fo'),
(19, 'France', 'fr'),
(20, 'Germany', 'de'),
(21, 'Greece', 'gr'),
(22, 'Hungary', 'hu'),
(23, 'Iceland', 'is'),
(24, 'Ireland', 'ie'),
(25, 'Israel', 'il'),
(26, 'Italy', 'it'),
(27, 'Japan', 'jp'),
(28, 'Korea', 'kr'),
(29, 'Latvia', 'lv'),
(30, 'Lithuania', 'lt'),
(31, 'Luxemburg', 'lu'),
(32, 'Malaysia', 'my'),
(33, 'Malta', 'mt'),
(34, 'Netherlands', 'nl'),
(35, 'Mexico', 'mx'),
(36, 'Mongolia', 'mn'),
(37, 'New Zealand', 'nz'),
(38, 'Norway', 'no'),
(39, 'Poland', 'pl'),
(40, 'Portugal', 'pt'),
(41, 'Romania', 'ro'),
(42, 'Russian Federation', 'ru'),
(43, 'Singapore', 'sg'),
(44, 'Slovak Republic', 'sk'),
(45, 'Slovenia', 'si'),
(46, 'Taiwan', 'tw'),
(47, 'South Africa', 'za'),
(48, 'Spain', 'es'),
(49, 'Sweden', 'se'),
(50, 'Syria', 'sy'),
(51, 'Switzerland', 'ch'),
(52, 'Tibet', 'ti'),
(53, 'Tunisia', 'tn'),
(54, 'Turkey', 'tr'),
(55, 'Ukraine', 'ua'),
(56, 'United Kingdom', 'uk'),
(57, 'USA', 'us'),
(58, 'Venezuela', 've'),
(59, 'Yugoslavia', 'yu'),
(60, 'European Union', 'eu'),
(61, 'Albania', 'al'),
(62, 'Algeria', 'dz'),
(63, 'American Samoa', 'as'),
(64, 'Andorra', 'ad'),
(65, 'Angola', 'ao'),
(66, 'Anguilla', 'ai'),
(67, 'Antarctica', 'aq'),
(68, 'Antigua and Barbuda', 'ag'),
(69, 'Armenia', 'am'),
(70, 'Aruba', 'aw'),
(71, 'Azerbaijan', 'az'),
(72, 'Bahamas', 'bz'),
(73, 'Bahrain', 'bh'),
(74, 'Bangladesh', 'bd'),
(75, 'Barbados', 'bb'),
(76, 'Belarus', 'by'),
(77, 'Benelux', 'bx'),
(78, 'Benin', 'bj'),
(79, 'Bermuda', 'bm'),
(80, 'Bhutan', 'bt'),
(81, 'Bolivia', 'bo'),
(82, 'Botswana', 'bw'),
(83, 'Bouvet Island', 'bv'),
(84, 'British Indian Ocean Territory', 'io'),
(85, 'Brunei Darussalam', 'bn'),
(86, 'Burkina Faso', 'bf'),
(87, 'Burundi', 'bi'),
(88, 'Cambodia', 'kh'),
(89, 'Cameroon', 'cm'),
(90, 'Cape Verde', 'cv'),
(91, 'Cayman Islands', 'ky'),
(92, 'Central African Republic', 'cf'),
(93, 'Christmas Island', 'cx'),
(94, 'Cocos Islands', 'cc'),
(95, 'Comoros', 'km'),
(96, 'Congo', 'cg'),
(97, 'Cook Islands', 'ck'),
(98, 'Costa Rica', 'cr'),
(99, 'Cote d''Ivoire', 'ci'),
(100, 'Cuba', 'cu'),
(101, 'Democratic Congo', 'cd'),
(102, 'Democratic Korea', 'kp'),
(103, 'Djibouti', 'dj'),
(104, 'Dominica', 'dm'),
(105, 'Dominican Republic', 'do'),
(106, 'East Timor', 'tp'),
(107, 'Ecuador', 'ec'),
(108, 'Egypt', 'eg'),
(109, 'El Salvador', 'sv'),
(110, 'England', 'en'),
(111, 'Eritrea', 'er'),
(112, 'Ethiopia', 'et'),
(113, 'Falkland Islands', 'fk'),
(114, 'Fiji', 'fj'),
(115, 'French Polynesia', 'pf'),
(116, 'French Southern Territories', 'tf'),
(117, 'Gabon', 'ga'),
(118, 'Gambia', 'gm'),
(119, 'Georgia', 'ge'),
(120, 'Ghana', 'gh'),
(121, 'Gibraltar', 'gi'),
(122, 'Greenland', 'gl'),
(123, 'Grenada', 'gd'),
(124, 'Guadeloupe', 'gp'),
(125, 'Guam', 'gu'),
(126, 'Guatemala', 'gt'),
(127, 'Guinea', 'gn'),
(128, 'Guinea-Bissau', 'gw'),
(129, 'Guyana', 'gy'),
(130, 'Haiti', 'ht'),
(131, 'Heard Islands', 'hm'),
(132, 'Holy See', 'va'),
(133, 'Honduras', 'hn'),
(134, 'Hong Kong', 'hk'),
(135, 'India', 'in'),
(136, 'Indonesia', 'id'),
(137, 'Iran', 'ir'),
(138, 'Iraq', 'iq'),
(139, 'Jamaica', 'jm'),
(140, 'Jordan', 'jo'),
(141, 'Kazakhstan', 'kz'),
(142, 'Kenia', 'ke'),
(143, 'Kiribati', 'ki'),
(144, 'Kuwait', 'kw'),
(145, 'Kyrgyzstan', 'kg'),
(146, 'Lao People''s', 'la'),
(147, 'Lebanon', 'lb'),
(148, 'Lesotho', 'ls'),
(149, 'Liberia', 'lr'),
(150, 'Libyan Arab Jamahiriya', 'ly'),
(151, 'Liechtenstein', 'li'),
(152, 'Macau', 'mo'),
(153, 'Macedonia', 'mk'),
(154, 'Madagascar', 'mg'),
(155, 'Malawi', 'mw'),
(156, 'Maldives', 'mv'),
(157, 'Mali', 'ml'),
(158, 'Marshall Islands', 'mh'),
(159, 'Mauritania', 'mr'),
(160, 'Mauritius', 'mu'),
(161, 'Micronesia', 'fm'),
(162, 'Moldova', 'md'),
(163, 'Monaco', 'mc'),
(164, 'Montserrat', 'ms'),
(165, 'Morocco', 'ma'),
(166, 'Mozambique', 'mz'),
(167, 'Myanmar', 'mm'),
(168, 'Namibia', 'nb'),
(169, 'Nauru', 'nr'),
(170, 'Nepal', 'np'),
(171, 'Netherlands Antilles', 'an'),
(172, 'New Caledonia', 'nc'),
(173, 'Nicaragua', 'ni'),
(174, 'Nigeria', 'ng'),
(175, 'Niue', 'nu'),
(176, 'Norfolk Island', 'nf'),
(177, 'Northern Ireland', 'nx'),
(178, 'Northern Mariana Islands', 'mp'),
(179, 'Oman', 'om'),
(180, 'Pakistan', 'pk'),
(181, 'Palau', 'pw'),
(182, 'Palestinian', 'ps'),
(183, 'Panama', 'pa'),
(184, 'Papua New Guinea', 'pg'),
(185, 'Paraguay', 'py'),
(186, 'Peru', 'pe'),
(187, 'Philippines', 'ph'),
(188, 'Pitcairn', 'pn'),
(189, 'Puerto Rico', 'pr'),
(190, 'Qatar', 'qa'),
(191, 'Reunion', 're'),
(192, 'Rwanda', 'rw'),
(193, 'Saint Helena', 'sh'),
(194, 'Saint Kitts and Nevis', 'kn'),
(195, 'Saint Lucia', 'lc'),
(196, 'Saint Pierre and Miquelon', 'pm'),
(197, 'Saint Vincent', 'vc'),
(198, 'Samoa', 'ws'),
(199, 'San Marino', 'sm'),
(200, 'Sao Tome and Principe', 'st'),
(201, 'Saudi Arabia', 'sa'),
(202, 'Scotland', 'sc'),
(203, 'Senegal', 'sn'),
(204, 'Sierra Leone', 'sl'),
(205, 'Solomon Islands', 'sb'),
(206, 'Somalia', 'so'),
(207, 'South Georgia', 'gs'),
(208, 'Sri Lanka', 'lk'),
(209, 'Sudan', 'sd'),
(210, 'Suriname', 'sr'),
(211, 'Svalbard and Jan Mayen', 'sj'),
(212, 'Swaziland', 'sz'),
(213, 'Tajikistan', 'tj'),
(214, 'Tanzania', 'tz'),
(215, 'Thailand', 'th'),
(216, 'Togo', 'tg'),
(217, 'Tokelau', 'tk'),
(218, 'Tonga', 'to'),
(219, 'Trinidad and Tobago', 'tt'),
(220, 'Turkmenistan', 'tm'),
(221, 'Turks_and Caicos Islands', 'tc'),
(222, 'Tuvalu', 'tv'),
(223, 'Uganda', 'ug'),
(224, 'United Arab Emirates', 'ae'),
(225, 'Uruguay', 'uy'),
(226, 'Uzbekistan', 'uz'),
(227, 'Vanuatu', 'vu'),
(228, 'Vietnam', 'vn'),
(229, 'Virgin Islands (British)', 'vg'),
(230, 'Virgin Islands (USA)', 'vi'),
(231, 'Wales', 'wa'),
(232, 'Wallis and Futuna', 'wf'),
(233, 'Western Sahara', 'eh'),
(234, 'Yemen', 'ye'),
(235, 'Zambia', 'zm'),
(236, 'Zimbabwe', 'zw');

-- --------------------------------------------------------

--
-- Structure de la table `demos`
--

CREATE TABLE IF NOT EXISTS `demos` (
  `demoID` int(11) NOT NULL AUTO_INCREMENT,
  `date` int(14) NOT NULL DEFAULT '0',
  `game` varchar(255) NOT NULL DEFAULT '',
  `clan1` varchar(255) NOT NULL DEFAULT '',
  `clan2` varchar(255) NOT NULL DEFAULT '',
  `clantag1` varchar(255) NOT NULL DEFAULT '',
  `clantag2` varchar(255) NOT NULL DEFAULT '',
  `url1` varchar(255) NOT NULL DEFAULT '',
  `url2` varchar(255) NOT NULL DEFAULT '',
  `country1` char(2) NOT NULL DEFAULT '',
  `country2` char(2) NOT NULL DEFAULT '',
  `league` varchar(255) NOT NULL DEFAULT '',
  `leaguehp` varchar(255) NOT NULL DEFAULT '',
  `maps` varchar(255) NOT NULL DEFAULT '',
  `player` varchar(255) NOT NULL DEFAULT '',
  `file` varchar(255) NOT NULL DEFAULT '',
  `downloads` int(11) NOT NULL DEFAULT '0',
  `votes` int(11) NOT NULL DEFAULT '0',
  `points` int(11) NOT NULL DEFAULT '0',
  `rating` int(11) NOT NULL DEFAULT '0',
  `comments` int(1) NOT NULL DEFAULT '0',
  `accesslevel` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`demoID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `demos`
--

INSERT INTO `demos` (`demoID`, `date`, `game`, `clan1`, `clan2`, `clantag1`, `clantag2`, `url1`, `url2`, `country1`, `country2`, `league`, `leaguehp`, `maps`, `player`, `file`, `downloads`, `votes`, `points`, `rating`, `comments`, `accesslevel`) VALUES
(1, 1361401200, 'BF3', 'Nous', 'Adversaires', 'Nous', 'Adv', 'http://', 'http://', 'al', 'fr', 'Tournois RdP', 'http://', 'bazar-seine', 'pleins de monde, et moi', 'Sleep Away.mp3', 0, 0, 0, 0, 2, 0);

-- --------------------------------------------------------

--
-- Structure de la table `failed_login_attempts`
--

CREATE TABLE IF NOT EXISTS `failed_login_attempts` (
  `ip` varchar(255) NOT NULL DEFAULT '',
  `wrong` int(2) DEFAULT '0',
  PRIMARY KEY (`ip`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `failed_login_attempts`
--

INSERT INTO `failed_login_attempts` (`ip`, `wrong`) VALUES
('37.160.12.180', 1);

-- --------------------------------------------------------

--
-- Structure de la table `faq`
--

CREATE TABLE IF NOT EXISTS `faq` (
  `faqID` int(11) NOT NULL AUTO_INCREMENT,
  `faqcatID` int(11) NOT NULL DEFAULT '0',
  `date` int(14) NOT NULL DEFAULT '0',
  `question` varchar(255) NOT NULL DEFAULT '',
  `answer` text NOT NULL,
  `sort` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`faqID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `faq`
--


-- --------------------------------------------------------

--
-- Structure de la table `faq_categories`
--

CREATE TABLE IF NOT EXISTS `faq_categories` (
  `faqcatID` int(11) NOT NULL AUTO_INCREMENT,
  `faqcatname` varchar(255) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `sort` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`faqcatID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `faq_categories`
--


-- --------------------------------------------------------

--
-- Structure de la table `files`
--

CREATE TABLE IF NOT EXISTS `files` (
  `fileID` int(11) NOT NULL AUTO_INCREMENT,
  `filecatID` int(11) NOT NULL DEFAULT '0',
  `date` int(14) NOT NULL DEFAULT '0',
  `filename` varchar(255) NOT NULL DEFAULT '',
  `filesize` varchar(255) NOT NULL DEFAULT '',
  `info` text NOT NULL,
  `file` varchar(255) NOT NULL DEFAULT '',
  `mirrors` text NOT NULL,
  `downloads` int(11) NOT NULL DEFAULT '0',
  `accesslevel` int(1) NOT NULL DEFAULT '0',
  `votes` int(11) NOT NULL,
  `points` int(11) NOT NULL,
  `rating` int(11) NOT NULL,
  `poster` int(11) NOT NULL,
  PRIMARY KEY (`fileID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `files`
--

INSERT INTO `files` (`fileID`, `filecatID`, `date`, `filename`, `filesize`, `info`, `file`, `mirrors`, `downloads`, `accesslevel`, `votes`, `points`, `rating`, `poster`) VALUES
(1, 1, 1361456193, 'Un zoli wall', '198966', 'Un zoli wall', 'bf3[1].jpg', '', 36, 0, 2, 19, 10, 1),
(2, 1, 1361716282, 'Sources PSD APAX', '618016', '', 'adrien.zip', '', 31, 0, 2, 20, 10, 1),
(3, 1, 1362304212, 'wallpaper du site', '1732406', 'Création By Dartoch', 'fond.jpg', '', 4, 2, 4, 37, 9, 5),
(4, 1, 1367235639, 'V2 wallpaper du site ', '1796183', '', 'fond 2 .jpg', '', 1, 2, 0, 0, 0, 5);

-- --------------------------------------------------------

--
-- Structure de la table `files_categorys`
--

CREATE TABLE IF NOT EXISTS `files_categorys` (
  `filecatID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `subcatID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`filecatID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `files_categorys`
--

INSERT INTO `files_categorys` (`filecatID`, `name`, `subcatID`) VALUES
(1, 'Fonds d''écrans', 0);

-- --------------------------------------------------------

--
-- Structure de la table `forum_announcements`
--

CREATE TABLE IF NOT EXISTS `forum_announcements` (
  `announceID` int(11) NOT NULL AUTO_INCREMENT,
  `boardID` int(11) NOT NULL DEFAULT '0',
  `readgrps` text NOT NULL,
  `userID` int(11) NOT NULL DEFAULT '0',
  `date` int(14) NOT NULL DEFAULT '0',
  `topic` varchar(255) NOT NULL DEFAULT '',
  `announcement` text NOT NULL,
  PRIMARY KEY (`announceID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `forum_announcements`
--


-- --------------------------------------------------------

--
-- Structure de la table `forum_boards`
--

CREATE TABLE IF NOT EXISTS `forum_boards` (
  `boardID` int(11) NOT NULL AUTO_INCREMENT,
  `category` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `info` varchar(255) NOT NULL DEFAULT '',
  `readgrps` text NOT NULL,
  `writegrps` text NOT NULL,
  `sort` int(2) NOT NULL DEFAULT '0',
  `topics` int(11) NOT NULL DEFAULT '0',
  `posts` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`boardID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Contenu de la table `forum_boards`
--

INSERT INTO `forum_boards` (`boardID`, `category`, `name`, `info`, `readgrps`, `writegrps`, `sort`, `topics`, `posts`) VALUES
(1, 1, 'Présentation de la Meute', 'Composition et Histoire de la Meute ', '', '', 1, 8, 20),
(3, 1, 'Demande de Recrutement', 'Vous souhaitez nous rejoindre ', '', 'user', 1, 10, 104),
(4, 3, 'Présentez votre TEAM', 'Ici , on vous découvre .. ', '', 'user', 1, 1, 4),
(5, 3, 'Demande de Match', 'Vous désirez nous affronter', '', 'user', 1, 6, 43),
(6, 4, 'Saloon', 'Ici , on discute de tous et de rien !! ', '', 'user', 1, 29, 288),
(7, 5, 'Réunions & Comptes rendus', '', '2', '2', 1, 22, 331),
(8, 4, 'News', '', '', 'user', 1, 5, 26),
(9, 6, 'Les 1 Ans de la Meute APAX', '', '', 'user', 1, 1, 10),
(10, 3, 'Préparation Match ', '', '2', '2', 1, 12, 107),
(11, 4, 'Partenariat', 'Partenariat actuel et a venir', 'user', 'user', 1, 2, 13),
(12, 1, 'Demande de Recrutement terminé ', '', 'user', 'user', 1, 12, 166);

-- --------------------------------------------------------

--
-- Structure de la table `forum_categories`
--

CREATE TABLE IF NOT EXISTS `forum_categories` (
  `catID` int(11) NOT NULL AUTO_INCREMENT,
  `readgrps` text NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `info` varchar(255) NOT NULL DEFAULT '',
  `sort` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`catID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Contenu de la table `forum_categories`
--

INSERT INTO `forum_categories` (`catID`, `readgrps`, `name`, `info`, `sort`) VALUES
(1, '', 'MEUTE APAX', '', 2),
(3, '', 'DUEL CONTRE APAX', '', 3),
(4, '', 'HORS DE LA GUERRE', '', 4),
(5, '2', 'FORUM PRIVE', '', 1),
(6, '', 'EVENEMENTS', '', 5);

-- --------------------------------------------------------

--
-- Structure de la table `forum_groups`
--

CREATE TABLE IF NOT EXISTS `forum_groups` (
  `fgrID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL DEFAULT '0',
  PRIMARY KEY (`fgrID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `forum_groups`
--

INSERT INTO `forum_groups` (`fgrID`, `name`) VALUES
(1, 'Old intern board users'),
(2, 'Membre APAX');

-- --------------------------------------------------------

--
-- Structure de la table `forum_moderators`
--

CREATE TABLE IF NOT EXISTS `forum_moderators` (
  `modID` int(11) NOT NULL AUTO_INCREMENT,
  `boardID` int(11) NOT NULL DEFAULT '0',
  `userID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`modID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Contenu de la table `forum_moderators`
--

INSERT INTO `forum_moderators` (`modID`, `boardID`, `userID`) VALUES
(1, 9, 10),
(5, 3, 2),
(6, 3, 4),
(7, 3, 5),
(8, 3, 10),
(9, 12, 2),
(10, 12, 4),
(11, 12, 5),
(12, 12, 10);

-- --------------------------------------------------------

--
-- Structure de la table `forum_notify`
--

CREATE TABLE IF NOT EXISTS `forum_notify` (
  `notifyID` int(11) NOT NULL AUTO_INCREMENT,
  `topicID` int(11) NOT NULL DEFAULT '0',
  `userID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`notifyID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=65 ;

--
-- Contenu de la table `forum_notify`
--

INSERT INTO `forum_notify` (`notifyID`, `topicID`, `userID`) VALUES
(11, 24, 27),
(17, 36, 34),
(28, 45, 26),
(56, 96, 89),
(58, 91, 34),
(61, 102, 34);

-- --------------------------------------------------------

--
-- Structure de la table `forum_posts`
--

CREATE TABLE IF NOT EXISTS `forum_posts` (
  `postID` int(11) NOT NULL AUTO_INCREMENT,
  `boardID` int(11) NOT NULL DEFAULT '0',
  `topicID` int(11) NOT NULL DEFAULT '0',
  `date` int(14) NOT NULL DEFAULT '0',
  `poster` int(11) NOT NULL DEFAULT '0',
  `message` text NOT NULL,
  PRIMARY KEY (`postID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1254 ;

--
-- Contenu de la table `forum_posts`
--

INSERT INTO `forum_posts` (`postID`, `boardID`, `topicID`, `date`, `poster`, `message`) VALUES
(1, 1, 1, 1361456101, 1, 'Premier sujet sur le fofo'),
(2, 1, 1, 1361649718, 2, 'sa fonctionne nickel '),
(3, 1, 1, 1361653398, 4, 'Je suis là'),
(4, 1, 1, 1361655973, 5, 'Moi aussi'),
(5, 1, 1, 1361700350, 1, 'Ahhhh !!!\r\n\r\nBon, GP, je te passe en full admin, je te demande juste un truc, si tu sais pas à quoi sert une option, n''y touche pas :D\r\n\r\nWebspell est très légèrement instable. De plus, j''ai ajouté un forum PRIVE qui n''est visible QUE par les APAX ;)'),
(6, 7, 2, 1361738164, 5, 'Je vous retranscris une idée qu''a eu GP et qui je pense pourrai être pas mal du tout:\r\n\r\n[I]"plus sérieusement une section évolution en escouade avec objectif d''etre les plus complémentaire possible , un pilote a besoin d''un artilleur ou réparateur pour un tank , un assaut peut avoir besoin de couverture associé a un soutien en munition etc............. \r\n\r\nc''est a la fois de la stratégie et bien préparer les escouades pour une progression plus rapide et s''abitué a évoluer ensemble que ce sois les nouveaux joueurs ou les anciens"[/I]\r\n\r\nDonc que penseriez vous de créé des escouades avec des spé, par exemple une escouade blindé (pilote,reparateur,marqueur), une escouade infanterie (2assault standar,1 assault fumi, 1 soutien), 1 escouade reco,...\r\n\r\nCela permetrai a ces joueur d''amelioré leur jeux d''équipe et comme cela on aura des équipe prete peut importe la map et le style de match (pour 4v4 squad-r on met l''escouade assault et pour un 12v12 on met 3escouades utile sur la map)..\r\n\r\nQuand pensez vous?'),
(7, 7, 2, 1361783680, 1, 'J''approuve l''idée. Par contre, il faut savoir à l''avance qui préfère être avec qui, et espérer qu''il n''y ai pas de "doublons". Le seul problème de cette team, c''est que comme on s''entends tous bien, je me vois mal dire "Je veux bien jouer avec Dartoch , mais surtout pas avec Pignouf30 " ^^\r\n\r\nSinon, \r\n\r\n\r\nJeune ingénieur cherche escouade pour réparations ... chauuuudes ;)'),
(8, 7, 2, 1361806389, 5, 'c''est sur, apres faut voir dans quel poste les membre se sente le mieu, on va pas metre quelqun en blindé s''il ne s''y sens pas alaise... ^^'),
(9, 7, 2, 1361809832, 1, 'Tout connement, quelle classe préférez vous ?\r\nEt, en fonction des retours, faut trier les membres dans la bonne section :)'),
(10, 7, 2, 1361810764, 5, '[COLOR=#FF0000]Escouade 1  Infanterie[/COLOR]\r\n-Assault Médic\r\n-Assault Médic\r\n-Assault Fumi\r\n-Soutien\r\n\r\n[COLOR=#FF0000]Escouade 2 Blindé[/COLOR]\r\n-Pilote char (assault medic)\r\n-Tireur char (Ingé repa)\r\n-Pilote jeep (Ingé repa)\r\n-Tireur jeep (Assault medic)\r\n\r\n[COLOR=#FF0000]Escoude 3 Aerien[/COLOR]\r\n-Pilote Chasseur\r\n-Pilote Chasseur\r\n-Pilote hélico (Assault med)\r\n-Tireur hélico (Ingé repa)\r\n\r\n[COLOR=#FF0000]Escouade 4 Eclaireur[/COLOR]\r\n-Pilote jeep  (Assault med)\r\n-Tireur jeep  (Soutien)\r\n-Sniper (Das)\r\n-Sniper (drone)\r\n\r\nQuand pensez vous?'),
(11, 7, 2, 1361821765, 2, 'c''est très intéressant de progresser dans les escouades mais pour réussir  faudrait que les joueurs s’implique d''avantage sur le forum , on vas essayer de mettre en place ce type de stratégie mais faudrait savoir qui a envie de faire quoi ? par exemple la seul classe ou je me sent pas très a l''aise pour l''instant c''est l''assaut '),
(12, 6, 3, 1361822034, 2, 'je trouve le forum un peu austère sa serai sympa d''avoir plus de couleur dans la mise en page ou des icônes etc...\r\n\r\n'),
(13, 7, 2, 1361824528, 5, 'Se qu''on peut faire c''est demander au joueur a quelle place ils se sente le mieu, par exemple pour moi c''est:\r\n\r\nEscouade 1 - soutien\r\nEscouade 2 - pilote jeep\r\nEscouade 4 - pilote jeep + snipe\r\n\r\nEt voila apres on rassemble tout les choix et on essaye les escouade pour voir se que cela donne et si il y a des changement a faire\r\nDe plus on peut, sur la page principale du site, marqué les differentes escouade ;)\r\n\r\nJe vous invite donc (tous^^) a faire comme je vien de faire pour que l''on puisse bosser dessus ;)'),
(14, 6, 3, 1361824943, 5, 'Je bosse dessus gp, que se soit sur le fond du site que sur la banniere ainsi que sur les icone du forum d''ailleur j''ai déjà fait des icones, faut que je voi avec benco pour les metre en place, j''essaye de ten metre un ou deux si dessous pour donné une idée ;)\r\n[URL=http://www.casimages.com/img.php?i=1302250942066025510905336.png][img]http://nsm08.casimages.com/img/2013/02/25//1302250942066025510905336.png[/img][/URL]\r\n\r\n[URL=http://www.casimages.com/img.php?i=1302250941476025510905335.png][img]http://nsm08.casimages.com/img/2013/02/25//1302250941476025510905335.png[/img][/URL]\r\n\r\nEn sachant quelles seront beaucoup plus petite ;)'),
(15, 6, 3, 1361825283, 5, 'De plus si vous voulez des avatars dite le moi, je vais voir pour faire des avatars type que l''on poura personalisé pour chaque membre, un peut comme la photo (pas l''avatar) que j''ai sur mon profil du site'),
(16, 6, 3, 1361828216, 8, 'c''est clair que ca pourrait etre cool un petit avatar perso vu que je suis une buse en informatique même si ca a l''air simple'),
(17, 7, 2, 1361844710, 4, 'pourquoi dans l''escouade 4 mettre un drone et un das, on pourrait mettre un marqueur lazer et/ou das ou drone, si tu veux détruire un hélico ennemie rapidement, tu le marque et l''ingénieur fait le reste c''est très efficace. Sinon, je vous le dis tout de suite je suis à l''aise, en Assault médic, ingénieur répa, et le soutien c4 lol. Sinon pour les membres de mon escouade, je peux être avec n''importe qui de la team.'),
(18, 6, 3, 1361860607, 2, 'les icônes sont sympa ! '),
(19, 6, 3, 1361871710, 5, 'Merci gp, j''ai essayé de mètre des couleur vive pour donner un peut plus de visibilité au icone contrairement a ceux fourni d''office\r\n\r\nJe vous tiendrai au jus pour les avatars, je ferai un fichier PSD type, comme sa j''aurai juste a faire les finition pour chaque membre, le plus long c''est de trouver les idée, apres c''est rapide ^^'),
(20, 7, 2, 1361871910, 5, 'En fait je n''est pas mis de marqueur laser pour les snipes car les marqueur sont facilement détectable et du coup les position des snipes sont dévoilé, après j''ai mis un das et un drone pour avoir autant le capteur de proximité que le détecteur longue distance, mais bon tout cela est modifiable bien sur ^^\r\nMerci pour ta reponce ma caille ;)'),
(21, 6, 3, 1361872884, 1, 'A la limite, je peux vous coder une page vous permettant de convertir automatiquement l''une de vos photos en avatar + photo (aux dimensions du site en fait).\r\n\r\nDites moi si ça vous semble utile que je m''y atèle ;)'),
(22, 7, 2, 1361873134, 1, 'L''idée est pas mal, mais j''ai quelques réticences quand à l''implication que ces escouades auront en matchs...\r\n\r\nPour progresser, on est bien d''accord qu''il faut jouer en escouades le plus souvant, or, je ne penses pas qu''en 4v4, l''escouade "aérienne" puisse piper grand chose, si tu vois ce que je veux dire.\r\n\r\nAprès, il faudrait idéalement voir pour 2 types d''escouades :\r\n\r\n- 1 compo "16x16"\r\n- 1 compo "4v4"\r\n\r\n(voir plus encore), ce qui permettrait d''être toujours taquets ^^'),
(23, 7, 2, 1361875645, 2, 'l''escouade aérienne est utile sur les map dédier , un hélico fait très mal sur les troupes au sol mais sans appuis des avions il ne peut être efficace si il est rapidement détruit par l''aviation , de toute façons pour  les avions leurs cible prioritaire sais l''hélico !'),
(24, 6, 3, 1361882356, 5, 'ouai sa pourrai être pas mal, mais il faudra que cette page soit accessible seulement par les membres de la meute ^^'),
(25, 6, 3, 1361885350, 1, 'Bof, ça ne fait que redimensionner des images, je sais pas si une super sécurité de la mort qui tue serait réellement utile ici ^^'),
(26, 7, 2, 1361885410, 1, 'Je suis d''accord avec toi waza-gp_one :)'),
(27, 7, 2, 1361895150, 8, 'Pour ma part je suis a l''aise en assault medic ou fumigene, ingenieur reparateur, tireur en helico apres niveau snipe il y a bien mieux dans la team '),
(28, 6, 4, 1361982669, 1, 'Evitez d''éditer les messages, mettez plutot une réponse.\r\n\r\nEt, à défaut de mieux, j''ai trouvé que ça. Par contre, c''est cool d''avoir un nouveau site, mais si personne ne réponds aux sujets, je vois pas l’intérêt ...\r\n\r\n\r\nRetestons : Vous êtes dispos pour un match ce soir 21h30 (ceux qui était présent pour la branlée) ?'),
(29, 3, 5, 1361998633, 5, '[COLOR=#FF0000]Pseudo:\r\nPrénom:\r\nAge:\r\nPsn:\r\nBattlelog:\r\n\r\nComment avez vous connu la meute?:\r\n\r\nPourquoi vouloir rejoindre nos rang?:\r\n\r\nParlez nous un peut de vous:[/COLOR]'),
(30, 6, 4, 1362048232, 5, 'pour ma part je suis blocké jusca samedi inclue, merci le taf et les arrêt maladie des collègues....'),
(31, 6, 4, 1362048717, 8, 'pour ma part je ne sais pas encore pour ce soir, je sais cette reponse ne fait pas trop avancer la chose je le saurais un petit peu plus tard dans la journée'),
(32, 7, 2, 1362078356, 5, 'Allez on se motive les filles ;)'),
(33, 5, 6, 1362121351, 4, 'Match 5vs5 promod\r\nmap 1 : Pic de damavan\r\nmap 2 : Metro'),
(34, 5, 6, 1362128574, 1, 'Quand, où ? ^^\r\n\r\nJ''en suis s''il manque un joueur =)'),
(35, 7, 2, 1362128606, 1, '[quote=Dartoch]Allez on se motive les filles ;)[/quote]\r\n\r\nça me rappelle les 4Frags après les EAS ...'),
(36, 5, 6, 1362157017, 4, 'Lundi 4 mars 2013 à 21h sur le serveur Apax\r\nMatch 5vs5 contre les SFG\r\nMap 1 : Pic de damavan\r\nMap 2 : Metro\r\nPromod\r\nentrainement le plus vite possible sur le serveur \r\nVoilà c''est mieux Ben qui va pas lire sur facebook et le gens de facebook qui ne vont pas lire sur le forum, de plus il est noté dans la section match.'),
(37, 7, 2, 1362191497, 6, 'Pour ma part sa serai plus Escouade 4 : sniper \r\nLeBetz.'),
(38, 7, 2, 1362210747, 5, 'je m''en doutait un peut ;) dès que j''ai un peut de temps (la c''est la folie au boulot....) je me penche sur les escouades, par contre si vous pouviez motiver les autre pour venir rep sa serai simpas, plus j''aurai de monde et plus je pourai poffiné les escouade ^^'),
(39, 12, 7, 1362324853, 16, 'bonjour à tous \r\n\r\nje commence\r\nPseudo: mika\r\n Prénom : Mickael\r\n Age : 23ans (24 le 20 mars)\r\n Psn: mika44500\r\n\r\n \r\nComment avez vous connu la meute?: sue une partie d''île wake avec un super pilote d''hélico : waza-gp_one (je viens de sa part\r\n \r\nPourquoi vouloir rejoindre nos rand?: un joueur régulier de plus dans vos rang et le souhait de rejoindre une team sérieuse\r\n \r\nParlez nous un peut de vous: je suis agent de maintenance pour la SNCF pationné d''automobile et des boxers (de superbe chien)\r\n\r\nMerci de m''avoir lu'),
(40, 12, 7, 1362327004, 5, 'Notre 1ere demande de recrute sur le nouveau site, sa se faite !!! lol\r\nPlus sérieusement, bienvenu a toi mika et bon courage pour ta demande de recrutement ;)'),
(41, 12, 7, 1362328934, 1, '[IMG]http://lmfasso.free.fr/menu/champagne.png[/IMG]\r\n\r\n\r\nBonne chance Mika :)'),
(42, 6, 8, 1362329055, 1, 'Yep la meute.\r\n\r\nJe reviens de New York, en 2047 où la planete est sous le joug des SELL.\r\n\r\nBref, il déboite. Le mode multi est un mix entre du Call of duty et de l''Unreal Tournament ... Sinon, il vous le faut absolument !!!\r\n\r\n[IMG]http://upload.wikimedia.org/wikipedia/commons/5/59/Crysis_3_logo.png[/IMG]\r\n\r\n:)'),
(43, 5, 6, 1362329166, 1, 'Bien joué pignouf ^^\r\n\r\nSorry pour le facebook, je penses pas forcément à y aller, alors que sur le site ... :D'),
(44, 12, 7, 1362337527, 2, 'slt \r\n merci pour ton inscription sur le forum \r\n je t''est demander de t''inscrire  car tu a un bon teamplay et un bon niveau \r\npour la marche a suivre c''est plutôt simple jouer avec nous , montrer ce que tu sais faire dans les classes que tu maitrise le mieux d''ailleurs les quels j''ai vu ingé et assaut ?  \r\n envois des demandes d''amis sur le psn a un maximum de joueur apax que tu croisera ici et sur le psn \r\n\r\n '),
(45, 12, 7, 1362339495, 1, 'Par contre, tu pourrait nous envoyer ton battlelog ?'),
(46, 7, 9, 1362339651, 1, 'Salouté les zamis, j''ajoute ce post histoire que vous me disiez ce qu''il vous manque sur le site.\r\nJe vais m''attaquer aux icones du forum afin de bien différencier une réponse d''un sujet lut, et ce genre de choses.\r\n\r\nLes admins du site, j''aimerais également vous prendre un peu de votre temps pour vous montrer comment marche le site afin de pouvoir le gérer aussi bien que moi et pouvoir trier les membres, catégoriser les esquades ou les matchs ;)\r\n\r\nSur ce, bonne soirée :)'),
(47, 7, 9, 1362340142, 1, 'Bien joué mon dartochh pour ton fond !!!\r\n\r\nJuste une petite demande, a voter bien sur. Il manque à mon goût une légère lueur diffuse sous le logo afin de pouvoir bien lire le titre, sinon... m''attendait à un truc bidon, superbement surpris :D'),
(48, 7, 9, 1362340851, 5, 'un truc bidon? bourique va ^^\r\nj''y avait pensé aussi, faut que je trouve commen maintenant ^^'),
(49, 7, 9, 1362345215, 6, 'Pourquoi c''est marqué EVIL GAMES Future Entertaiment en haut du forum? '),
(50, 6, 8, 1362345279, 6, 'Si tu me l''achete y a pas de soucis ;)'),
(51, 12, 7, 1362381205, 16, 'c''est bien ça assault et ingénieur mes préférés.\r\nPour battlelog c''est : mika44600'),
(52, 7, 9, 1362387801, 1, 'Normalement, y''a pu :D'),
(53, 6, 8, 1362387844, 1, '50e ^^\r\n\r\nMa revue sur le jeu :\r\n\r\nhttp://www.jeuxvideo.com/avis/playstation-3-ps3/44456-crysis-3-1-1-1.htm#avis_1121375'),
(54, 12, 7, 1362387911, 8, 'Bonne chance pour ton recrutement!\r\nbmdc64 si tu veux me rajouter'),
(55, 7, 9, 1362388181, 8, 'J''aime beaucoup le fond du site GG'),
(56, 7, 9, 1362388666, 5, 'Merci bien ^^ j''ai un bou galéré pour trouver une idée original mais bon avec du temps on y arrive ;)'),
(57, 12, 7, 1362392755, 1, 'ben_ftwc pour ma part, j''en oublis mes bonnes manières :)'),
(58, 7, 2, 1362392817, 1, 'Par contre, les phases de tournois approchent vites, on sait quand les esquouades seront fixés qu''on commence dés à présent à player en meute ?'),
(59, 7, 9, 1362395664, 8, 'GG adrien tres bonne idée'),
(60, 12, 7, 1362398439, 16, 'ajouté et ajouté'),
(61, 1, 1, 1362403920, 16, 'histoire de passer pour un inculte, c''est quoi la signification de APAX? '),
(62, 1, 1, 1362411720, 5, 'oula, sa va chercher loin, faut demander a Psy ^^'),
(63, 7, 9, 1362412688, 2, 'pareil vraiment bien sa saute au yeux ce mélange d''images coupées '),
(64, 12, 7, 1362416194, 1, 'Heu, qui en premiers ?\r\nPck, si tu commence à faire des jaloux ... =D'),
(65, 12, 7, 1362418635, 16, 'j''ai ajouté les deux l''un derrière l''autre avec quelques secondes d''intervales...... j''ai bon là???'),
(66, 7, 9, 1362423106, 5, 'ravi que sa vous plaise ;)\r\n'),
(67, 12, 7, 1362423745, 8, 'Bon pour moi et n''hesites a venir pour qu''on joue ensemble'),
(70, 12, 7, 1362467933, 16, 'dès que je me connecte, j''essai d''aller sur le serveur APAX directement'),
(73, 5, 11, 1362486696, 11, 'Comme vous le savez ce tournoi ce passe en 5vs5 régle promod est seul les 2 meilleur équipe sont prise.\r\nDonc la disponibilité des TSG est de Jeudi soir 21H ou Dimanche soir 21h sachant que les Qualification ce termine Dimanche a minuit.\r\nJ''attend vos réponse.'),
(74, 5, 11, 1362486865, 1, 'Faudrait que tu mette le jours, mais sinon, c''est impec !\r\n\r\nJ''en suis mecton ;) (jeudi de préférence)'),
(75, 5, 11, 1362491097, 8, 'je suis dispo si c''est jeudi les gars, faut que l''on forme le T5 pour le match et qu''on commence l''entrainement des ce soir si c''est possible pour avoir une meilleure finalité que la derniere fois. auly, est ce que tu connais les maps?'),
(76, 12, 7, 1362491102, 2, 'on est pas tout le temps sur notre serveur sa dépend de l''effectif dispo car il faut être minimum 4 pour lancer le serveur et que la partie puisse ce remplir '),
(77, 5, 11, 1362494978, 1, 'De mon côté, j''appuie grandement le message de bmdc64 , IL NOUS FAUT DES ENTRAÎNEMENTS, notamment les strats et un LIG sans quoi, on va prendre la même fessée que contre les TGS ( http://88.191.146.2/apax/index.php?site=clanwars_details&cwID=3 )\r\n\r\nPar contre, je serait exceptionnellement injoignable ce soir si un tel entrainement est mis en place. Néanmoins, si un participant pouvait ce dévouer pour faire un rapport que je puisse être au jus en rentrant (~23h ...)'),
(78, 7, 2, 1362499758, 3, 'une tres bonne idée, moi tout sauf pilote avion'),
(79, 6, 12, 1362514464, 16, 'salut à tous\r\n\r\nJe voudrais savoir si parmi les membres, il y a des chasseurs de trophés afin d''obtenir ces fameux platine. Moi en tous cas je me casse les dents sur certains jeu et en particulier BF3. il y en a un ou il faut transporter le porteur du drapeau en hélico, chose quasi impossible afin la multitude d''engin, rpg, igla, présent sur le drapeau ennemi.\r\n\r\nAlors, des mordus de platine?'),
(80, 5, 13, 1362527323, 11, 'Les EcLypsia eSport demande un scrim regle promod 5vs5 samedi 15H est leur map Pic De Damavad.'),
(81, 12, 7, 1362557690, 16, 'Je vais savoir quand si je suis admis dans votre team? \r\n'),
(82, 5, 13, 1362564246, 1, 'Samedi 15H ? Va pour moi :)'),
(83, 6, 12, 1362564296, 1, 'Ziva, on s''pète les trophées ce soir si tu veux :D'),
(84, 12, 7, 1362564391, 1, 'Pas en trois jours :p\r\nIl faut que manu passe par là, et il est en phases de révisions en ce moment, donc il faut attendre. Si ça peut te rassurer, Itte à attendu 3 semaines, et moi 2 :)\r\n\r\nPar contre, tu peux quand même passer sur le serveur APAX ^^'),
(85, 12, 7, 1362572880, 16, 'ok sa marche '),
(86, 6, 12, 1362572960, 16, 'pas de soucis moi il me reste plus qu''à tuer un mec en étant passager de la moto. '),
(87, 6, 12, 1362575084, 1, 'T''as réussi tout les coops ??'),
(88, 5, 11, 1362582658, 8, 'Bonjour messieurs, je laisse ce message pour vous solliciter afin d''organiser un entrainement ce soir vers 8-9h car le match est demain alors essayons de se faire au moins un entrainement sur notre serveur, un 5vs5 apax s''il y a assez de monde avec deux maps et deux sides dans chaque. sinon on ira avec notre bite et notre couteau je ne suis pas contre mais ca sera pas assez efficace'),
(89, 5, 11, 1362587207, 1, 'Par contre, comme dit plus haut, je suis pas dispo ce soir, si cela fait sauter ma place pour le TN, pas de prob :D'),
(90, 5, 11, 1362587763, 8, 'C était pas hier que tu n étais pas dispo?'),
(91, 12, 7, 1362593930, 2, 'faut juste être patient car tu a le niveau requis pour intégrer l''équipe sa peut être rapide comme long !! de plus tu est actif sur le forum et online \r\ncontinue comme sa!'),
(92, 5, 11, 1362599444, 8, 'quel leader designé par manu s''occupe de créer le T5 pour demain?'),
(93, 7, 14, 1362605839, 20, 'Bonjour, je viens vous proposer un match en conquête ou ruée 12 vs 12.\r\nIl se jouerait en 2 maps et deux manches sur chacune. Je vous laisse le choix d''une map et d''un mode.\r\nIl se jouerait le samedi 23 Mars à 21H.\r\n\r\n\r\n\r\n\r\nRèglement : \r\n\r\n\r\n1- Descriptif général\r\nMode de jeu: 12vs12 en conquête (Normal) ou ruée (Normal)\r\nSystème de jeu: nombre de manche impaire pour éviter une égalité(3,5,7,...)\r\n\r\n2- Restrictions des classes par Escouade\r\n- un Assaut par Escouade (limite la réanimation abusive)\r\n- un Soutien par Escouade (limite le tir en continue abusif)\r\n- un Ingénieur par Escouade (limite le tir de roquette abusif)\r\n- un Éclaireur par Escouade (limite les respawns mobile abusif)\r\n\r\nEn cas de plusieurs escouades de 3 personnes par exemple, il faudra maintenir de nombre maximum de 3 classes de chaque par équipe.\r\n\r\nDurant un match, il est interdit de ramasser une classe d''un soldat mort, sauf si identique à la votre.\r\nImportant: Il est interdit de changer de classe durant la même manche.\r\n\r\n3- Restrictions des armes\r\n- un fusil à pompe par Escouade\r\n\r\n4- Restrictions des équipements\r\n- Aucune\r\n\r\n5- Restrictions des gadgets\r\n- Aucune\r\n\r\n6- Restrictions des spécialisations Escouade\r\n- Aucune\r\n\r\n7- Divers\r\n- pas de SPAWNKILL\r\n- pas de base principale assiégée (on reste au dernier point capturé sans avancer sur la base ennemie)\r\n- respect de l''adversaire (FAIRPLAY dans la victoire comme dans la défaite)\r\n- on ne répond pas aux provocations (c''est aux Leaders de Team, ou en cas d''absence à son représentant direct, de gérer le problème)\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\nMerci d''avance '),
(94, 5, 11, 1362610509, 19, 'jessai dorganiser tous ca pour dimanche je tenvooi un message pour confirmer essai de me dire si cest tjrs Ok'),
(95, 7, 14, 1362612054, 4, 'ça m''intéresse ton 12vs12, j''essaie de rallier 12 joueurs à ton match.'),
(96, 7, 14, 1362632106, 6, '1assault par escouade c''est moche... '),
(97, 6, 12, 1362638961, 16, 'oui j''ai tous réussi et je viens de finir les trophés d''END GAME. S''il t''en manque un je supose que c''est liquidation totale, j''ai passé pas mal de temps pour celle-ci. Et désolé pour hier, je me suis endormi avec les poules. Ce soir ci tu es chaud.'),
(98, 7, 14, 1362641755, 2, 'slt je trouve très intéressant cette idée de limité les classes ! plutôt que d''interdire \r\npourrai t’ont partir sur une date sait a dire dimanche 17/03 a  14h30 \r\nsa nous laisserai le temps de rassembler les joueurs jusqu''a la date et savoir combien participe '),
(99, 7, 15, 1362648187, 1, 'Salut les filles. Une demande de match à été effectuée sur le site pour un 12v12 ( [URL=http://88.191.146.2/apax/index.php?site=forum_topic&topic=14&type=ASC&page=1]Fiche du match[/URL] ).\r\n\r\nHistoire de faire gagner du temps à l''orga ( Pignouf30 ?), si vous pouviez à la suite de ce post laisser vos dispos, en sachant que le match est prévu pour le [B][COLOR=#87CEEB]dimanche 17/03 a 14h30[/COLOR][/B].\r\n\r\nThanks !'),
(100, 7, 14, 1362648333, 1, '[quote=darkyoyo19-1]7- Divers\r\n- pas de SPAWNKILL\r\n- pas de base principale assiégée (on reste au dernier point capturé sans avancer sur la base ennemie)\r\n- respect de l''adversaire (FAIRPLAY dans la victoire comme dans la défaite)\r\n- on ne répond pas aux provocations (c''est aux Leaders de Team, ou en cas d''absence à son représentant direct, de gérer le problème)\r\n[/quote]\r\n\r\nJ''aime bien ce concept :)'),
(101, 7, 15, 1362656902, 8, 'Présent pour cette date et heure'),
(102, 7, 15, 1362657094, 12, 'dispo vers 21h pas avant'),
(103, 7, 15, 1362664071, 1, 'Je serais là !'),
(104, 6, 12, 1362664121, 1, 'T''inquiète, même moi j''étais pas super présent, j''avais zappé un anniversaire, du coup, ''me suis pointé sur le psn vers 23h ^^'),
(105, 7, 15, 1362676993, 6, 'Je suis dispo'),
(106, 7, 15, 1362680854, 2, 'disponible ùais faut que l''adversaire valide la date et l''heure'),
(107, 7, 15, 1362687308, 3, 'la :)'),
(108, 7, 15, 1362691110, 5, 'normalement je sera présent ;)'),
(109, 7, 15, 1362727752, 4, 'Je serais dispo, pour le moment je compte 7 personnes disponible pour la date de Gp_one dimanche 17 mars à 14h30. Allez les gars plus que 5 joueurs pour cet évènement mémorable ça va nous rappeler des souvenirs. '),
(110, 7, 15, 1362735044, 1, '[SIZE=4][COLOR=#87CEEB]Pour savoir si j''utilise le calendrier à l''avenir, pourriez vous valider également votre présence / absence sur cette page : [URL=http://88.191.146.2/apax/index.php?site=calendar&tag=17&month=3&year=13]Calendrier de l''équipe[/URL]. Si je me rends compte que c''est concluant, je penses faire des fiches pour les orgas ;)[/COLOR][/SIZE]'),
(111, 7, 15, 1362764499, 5, 'c''est fait ^^ bon par contre va faloir se metre sur les squad rapidement... soit 3 squade a formé ;)'),
(112, 7, 15, 1362782221, 1, 'Je veux bien être avec lebetz et ita, je commence à comprendre comment ils jouent :D'),
(113, 6, 12, 1362796667, 11, 'Lol j''ai Bf3 a 100% et a la sortie de End Game j''ai eu tout les trophée sans aide ^^.\r\nPs:pour le trophée faire un kill passagé de la mote je te donne une astuce tu gare ta moto a un endroit fréquenté tu change de place et tu attend que un pigeon passe et tu a ton trophé.\r\nJe suis a 2 trophée platine (Bf3,Mw2 et bientot Naruto generation et Ac3) aprés pour d''autres trophée faut aller voir pignouf il en a 15 platine.--"'),
(114, 7, 15, 1362796794, 11, 'Franchement pour faire les escouade ou nimporte qui Dartoch je men fou d''etre avec qui du moment ou je suis dans une escouade sa me va ^^.'),
(115, 7, 15, 1362819003, 5, 'je vais surtout essayé de les faire par raport au kit que l''on peut utilisé (assault, suport, ...) par contre il me faudrai la liste complete des personne dispo pour le match, sinon faudra certainement les refair... donc allez les loup on se lance ;)\r\n\r\ntu sera présent pour le match auly?\r\nEs qu''il serai possible d''avoir les maps qui seron joué?'),
(116, 6, 12, 1362819128, 5, 'perso c''est plus les plaque que j''essaye de débloque et yen a certaine c''est tendu a avoir.... surtout quand tu na pas la description pour savoir commen les recupéré...'),
(117, 6, 12, 1362825854, 11, 'Je sais pas dartoch si ta vue quelle sont les plaque les plus rares ??? mais franchement il y en a que l''on aurat jamais --"'),
(118, 7, 15, 1362825939, 11, 'Sa depend dartoch et franchement va voir mon battlelog dartoch je suis polyvalent mon nombre d''heure avec chacune de classes sont égal'),
(119, 6, 12, 1362827158, 11, 'Ps: Dartoch voici toutes mes plaques.\r\nhttp://battlelog.medalofhonor.com/bf3/fr/#!/bf3/fr/soldier/aulyro1996/dogtags/332711084/ps3/'),
(120, 7, 15, 1362828797, 5, 'je le sait bien mais c''est qu''il nous manque encore 5 loups ;)'),
(121, 6, 12, 1362848192, 4, 'Euh j''ai que 11 platines et non 15, ensuite pour l''entraide des trophées faut savoir quel jeu tu as. Si jamais quelqu''un veut faire les trophées tomb raider pour le multijoueur je suis disponible pour l''aider.'),
(122, 7, 15, 1362851595, 5, 'bon en atendant d''avoir toute les info manquante j''ai déja fait cela, vous en penser quoi?\r\n\r\n[COLOR=#FF0000]esc 1\r\n\r\nassault:    pignouf\r\nsoutien:    jujub\r\ningenieur:  ben\r\nsniper:     lebetz[/COLOR]\r\n\r\n[COLOR=#87CEEB]esc 2\r\n\r\nassault:    bmdc\r\nsoutien:    dartoch\r\ningenieur:  Gp\r\nsniper:     [/COLOR]\r\n\r\n[COLOR=#FFA500]esc 3\r\n\r\nassault:\r\nsoutien:\r\ningenieur:\r\nsniper:[/COLOR]'),
(123, 7, 15, 1362852500, 1, 'Moi ça m''va :D'),
(124, 7, 15, 1362870236, 4, 'Alors j''ai des retours, on voudrait changer l''heure du match 14h30 c''est trop tôt, sinon pour les escouades je le dis haut et fort je veux être au sol. \r\nSinon si mon escouade est celle proposé, vos spécialisations seront : \r\nPignouf = sprint\r\nJujub = Munition avec le c4 biensur lol\r\nBen = explosif avec soit stinger/Igla ou swam/rpg\r\nlebetz = suppression avec das si autorisé.\r\nCette escouade est basé sur la prise de flag et utilisera les jeeps pour ce déplacer ou à pieds.'),
(125, 7, 15, 1362895524, 2, 'on prendra pas de carte aérienne de toute façons , faut partir sur bazar \r\net pour l''horaire ?  si changement faut voir sa rapidement\r\n de toute façon tant que l''on a pas 12 joueurs on participe pas !'),
(126, 7, 15, 1362901247, 5, 'oui tres rapidement meme, car si on change l''horaire sa risque de changer les joueurs présent aussi....'),
(127, 7, 15, 1362937440, 8, 'Ça me va très bien aussi pour l escouade et pour le changement d horaires il n y a pas de souci pour moi je serais dispo toute la journée et soirée comprise, en espérant qu on soit 12 pour pas louper ce match ça va être bien sympa '),
(128, 7, 14, 1362955453, 20, 'Un dimanche en plein après-midi ça risque d''être pas très pratique pour nous. Vous ne voulez pas faire le match un vendredi ou samedi soir ? ;-)'),
(129, 7, 15, 1362986145, 4, 'alors j''ai du retour de l''adversaire, il voudrait faire la match un vendredi soir ou un samedi soir, ils ne peuvent pas le dimanche après-midi.'),
(130, 6, 8, 1362986305, 4, 'Si tu me l''achète ya pas de souci aussi mdr'),
(131, 7, 15, 1362993321, 5, 'dans se cas sa risque d''etre compromi pour ma part...'),
(132, 7, 15, 1362999254, 1, 'Relou ces gens !'),
(133, 6, 8, 1363001421, 1, 'Bandes de couillons :p'),
(134, 6, 16, 1363001472, 1, 'Salut les filles. Y''a des joueurs de TEKKEN 6 ici ? Car je vous avouerais que jouer sur internet deviens très vite relou du fait que les mecs te callent dans un coins et te font 15x la même prise ...'),
(135, 7, 15, 1363025469, 2, 'forfait pour moi je ne joue pas le soir \r\n'),
(136, 7, 14, 1363025753, 2, 'ben on a souvent de la disponibilité l’après midi le weekend et moins le soir \r\npour moi c''est uniquement l’après midi que je joue '),
(137, 6, 16, 1363027630, 2, 'j''y ai jouer mais vite lassé du online répétitif et les salons sont pas térrible a la limite tekken 5 étai mieux coté online ou je jouai avec bryan fury !!!!'),
(138, 6, 16, 1363033404, 1, 'Mouais, mais si on joue ensembles, ça peux être lol :p'),
(139, 7, 15, 1363033449, 1, 'Erf :/'),
(140, 6, 16, 1363071782, 2, 'désoler je n''est pas garder ce jeu '),
(141, 7, 14, 1363079025, 6, 'Je voulais savoir un truc , c''est par rapport au armes. regle pro-mod ou regle a la wolegene ? '),
(142, 6, 16, 1363079916, 1, 'Ah ouais, carrement XD'),
(143, 7, 14, 1363079976, 1, 'A la rache (http://www.risacher.com/la-rache/index.php?z=3)\r\n*\r\nBon oké, petit troll :p'),
(144, 7, 17, 1363080623, 1, '[ALIGN=center][IMG]http://www.pompier-raid-aventure.com/blog/wp-content/uploads/2012/09/we_need_you1.jpg[/IMG]\r\n[/ALIGN]\r\n\r\nSalut les gars, dites moi. J''aurais besoin de quelques "éléments", appellons les des membres apax (lol) pour garnir un peu le site.\r\n\r\nIl me faudrait :\r\n\r\n- 1 ou 2 newsers\r\nIl poste régulièrement des nouveautés sur la team, des comptes rendus ou des évents auquels nous participons. Peut également poster des news sur des sorties de film ou jeu, why not ;)\r\n\r\n- 1 ou 2 modérateurs forum \r\nNous avons un forum, peu visité pour le moment, mais qui tends à le devenir. Il faut surveiller les abus de langages.\r\nEt, si la team compte partir en guerre, tournois cecicela :\r\n\r\n- 1 Warranger\r\nSe charge d''organiser le match de A à Z, c''est à dire qu''il prépare ses soldats, le serveur, le battlelog et pense en fin de match soit à parler avec un newser soit prépare lui même son rapport de match qui sera affiché sur le site. (Idéalement, il faudrait un admin serveur)\r\n\r\nVoilou, de plus, si manu tu as un peu de temps, il faudrait qu''on s''capte pour appliquer le domaine sur le serveur pour le référencement (google).'),
(145, 7, 17, 1363080790, 6, 'Y a un jeu qui va sortir qui a l''air super super cool , c''est Sniper Warrior 2 un truc comme sa :)'),
(146, 7, 17, 1363080867, 1, 'Un peu de sérieux dude :p'),
(147, 7, 17, 1363081032, 6, 'Je cite "Peut également poster des news sur des sorties de film ou jeu, why not " donc mon commentaire precedent est très serieux . DUDE'),
(148, 7, 14, 1363081279, 6, 'Uun [B]peu[/B][S][/S] de serieux soldat Ben. Merci.\r\nApaX_LeLe'),
(149, 7, 17, 1363082026, 1, 'Le but du post est d''attribuer les rôles, pas de lancer un troll :)'),
(150, 7, 17, 1363084735, 8, 'Je suis pas un admin mais ça me dirait bien d organiser les match '),
(151, 7, 17, 1363086460, 2, 'que de bonnes idées mais pour que cela fonctionne faut que les membres s''implique plus ce qui n''est pas tellement le cas actuellement \r\n sa me fait penser exactement a ce qui c''est produit chez les waza ou y'' avais un forum qui tournais convenablement avec des joueurs impliqués et d''autre pas du tout , il faudrait que les futures recrutement s''implique pour que les anciens suivent sous peine de ne pas participé au match ou événement '),
(152, 7, 17, 1363086629, 2, 'bmbc pourquoi pas mais cela implique du temps a consacrée a aller prospecter auprès des team a toi de savoir si tu a le temps !'),
(153, 6, 16, 1363086973, 4, 'Désolé j''ai pas Tekken 6, autres jeux ?'),
(154, 7, 17, 1363089872, 8, 'B du temps j en ai même si je vais être en révision je peux le trouver par contre faut voir avec Manu je ne suis pas un leader c est juste que si personne ne se présente je serais la après pour aller chercher  des matchs il faut avoir le roster de prêt, faut faire des matchs je pense pour faire parler de nous et garder une certaine longévité '),
(155, 7, 17, 1363091539, 5, 'pour ma part ya pas de souci pour les tache a faire, suffi de me le dire et je m''y atèle ;)'),
(156, 7, 17, 1363094458, 1, 'Heu, pour simplifier la tâche du waranger, bien évidement, ESL est également présent sur les plateformes de "salon", sous le nom de console.net\r\n\r\ncf : http://www.consoles.net/fr/bf3-ps3/\r\n\r\nOu genre, ces tournois là http://www.consoles.net/fr/bf3-ps3/cdf13/cup24/   ;)\r\n\r\nAprès, Dartoch en gros, c''est toi qui décide quand tu met une annonce hein ? :p'),
(157, 6, 16, 1363094510, 1, 'J''ai pas grand chose niveau jeux.\r\n\r\n- Tekken 6\r\n- Crysis 3\r\n- BF3\r\n- Max Payne 3\r\n\r\nAprès les autres, c''est pas du multi ^^'),
(158, 12, 18, 1363103847, 25, 'Bonjour a tous! \r\n\r\nPSN: AtOnIuM_GhAuX\r\nBATTELOG: AtOnIuM_GhAuX\r\n\r\n\r\nJe me présente Jaouen 16 ans je joue a BF3 depuis ces débuts et ne manquerait pas une petite sessions entre amis.\r\nJe cherche a rejoindre une team compétitive ou reigne la bonne ambiance et un jeu d''equipe.\r\n\r\n\r\nMon palmares:\r\n\r\n\r\nJ''ai jouer avec plusieurs compte qui me ménent a un total d''environ 350 heures de jeu.\r\nJ''ai fais partie de quelques team ou j''ai pu faire valoir mon teamplay lors de match, tournoi, entrainement,training...\r\nEn ce qui s''agit du ratio  RAPPORT V/M 3.69 ( je tien a noté une petite baisse car je ne jouais plus en escouade).\r\nRATIO V/D 6    SCORE MIN 731.\r\nMa classe principal assaut avec la M16A3 comme arme de prédilection notamment pour s''entrainer afin de gérer les trounois.\r\nJ''ai rencontrer quelques un des votres dans une parti conquête metro je cherchais donc une team et leur teamplay m''avais l''air pas mal donc je me suis informer :)\r\nJ''ai pu notamment faire quelques parties avec gyom-88.\r\n\r\n\r\nVoilà si vous désirez me posez des questions plus précises je suis la à bientot j''espere dans l''arêne :)))\r\n'),
(159, 12, 18, 1363104896, 1, 'Encore un Assault M16A3 :/\r\nInnovez les mecs, y''a presque 200 armes !\r\n\r\nSinon, add ben_ftwc histoire qu''on game un peu ensembles :o'),
(160, 7, 17, 1363112551, 2, 'faudra en discuter avec psy-4 , sa devrai pas poser de soucis , faut bien qu''il y est un minimum d''implication '),
(161, 6, 16, 1363112628, 2, 'bf3 encore heureux !!'),
(162, 12, 18, 1363113218, 2, 'bienvenue , merci pour ta candidature\r\nvu le niveau qui semble être la tiens sa ne devrai pas poser de problème , faudra tout de même faire quelque test afin de valider ton teamplay , avec certain d''entre nous et donc nous rajouter dans tes contact \r\nen espérant que tu sois un joueur régulier que ce sois sur bf3 et sur le forum \r\nune petite question combien de fois a tu reset tes stats pour obtenir ce ratio ? \r\n'),
(163, 12, 18, 1363114543, 25, 'Alors pour répondre à ta question j''ai eu 3 comptes c''est le dernier celui-ci lvl 22 donc mon ratio n''a jamais été reset et je tien à préciser que je rush :) voilà et je vous ajouterais chacun bien entendu afin de jouer ensemble ;)'),
(164, 12, 18, 1363121891, 5, 'Salut et bienvenu sur le forum ;)\r\nA parement un bon niveau reste a voir ton état d''esprit ;)\r\nsi tu est co demain aprem on pourrai se faire quelque partie, ajoute moi que l''on voit cela\r\nbon courage pour ta demande de recrutement \r\n\r\npsn:dartoch'),
(165, 12, 18, 1363122360, 25, 'Merci bien je t''ajouterais demain sans problème pour ce qui sera de ma disponibilité demain je confirme rien mais je ferais mon possible . Heureux de faire ta connaissance :)'),
(166, 12, 18, 1363125142, 8, 'Salut bon courage pour ta demande ajoute moi et on se fera des partie dans la soirée demain '),
(167, 6, 16, 1363165543, 4, 'Désolé j''ai pas les 3 jeux que tu propose.'),
(168, 12, 18, 1363196403, 22, 'Bonne chance à toi et add Mister_reveur on se fera quelques parties :P'),
(169, 12, 18, 1363196858, 5, 'alors j''ai joué un peut avec lui aujourd''hui et donc bon joueur, bonne mentalité, mature pour son age\r\nA mon avis une bonne recru qui peut avoir sa place a nos coté\r\nJe vous laisse le rajouté sur psn et faire quelque partie avec lui pour vous faire une idée mais pour moi c''est un +1 ;)'),
(170, 6, 16, 1363257945, 1, 'Tant pis :D'),
(171, 6, 16, 1363258002, 1, 'Par contre, j''ai finit Tekken & Crysis, donc me revoila sur les champs de combats.\r\n\r\nPréparez vos motos, et vos béliers (LeBetz, MONTE SUR CETTE MOTO ET TAIS TOI !!!), va y''avoir du frag !!! :D'),
(172, 6, 19, 1363266458, 1, 'Salut a vous les filles.\r\n\r\nJe m''en viens à vous avec une petite liste des prochaines nouveautés du site.\r\n\r\n[B]Communautée[/B]\r\n- Ajout d''un bouton de partage (Facebook, Twitter, Google Plus, Mail) sur les news, actualités et topics du forum\r\n- Ajout d''une Facepile sur le site ([URL=http://tinyurl.com/brvcgkz]C''est quoi une facepile ? [/URL])\r\n- Permission à chaques membres le désirant de disposer de leur propre page "statique" permetant de présenter playlists, vidéos, images ou texte de leur choix ([URL=http://88.191.146.2/apax/index.php?site=static&staticID=1]Exemple avec ma page[/URL])\r\n\r\n[B]Matchs / Rapports[/B]\r\n- Ajout d''un module externe permetant aux admins de générer des rapports sur le site avec tout les détails du match afin de générer un document propre pour chaques revues de matchs.\r\n- Mise en place du calendrier avec rappel automatique par email afin de préparer à l''avance les escouades et matchs interposés (permettant ainsi de prévoir 2, 3 ou 4 matches en même temps ! C''est l''outil que je vous avais demandé de testé pour le T12)\r\n\r\n[B] Autres [/B]\r\n- Profils : Création d''un module externe pour permettre le redimentionnement de vos images (Avatar + Photo) afin de vous faciliter les transferts (Merci à ApaX_LeLe pour l''idée).\r\n\r\n\r\nSi jamais vous souhaitez voir apparaître sur votre site d''autres modifications, je vous laisse le faire ici :)'),
(173, 6, 4, 1363283025, 12, 'faut relancer l''idée'),
(174, 6, 4, 1363294562, 8, 'up!'),
(175, 7, 17, 1363298681, 11, 'Ouep ben je peut poeter un New pour ce que je tes dit aujourd''hui sur Fb comme sa peu etre sa se remettra ou dans le pire des cas l''on serat fixé ^^'),
(176, 6, 4, 1363338579, 1, 'Faudrait taxer les droits admin à qqn. Mais les motivés, j''pense qu''on peux toujours s''inscrire à un TN sur esl ... =)\r\n'),
(177, 7, 17, 1363338637, 1, '[quote=Aulyro1996]Ouep ben je peut poeter un New pour ce que je tes dit aujourd''hui sur Fb comme sa peu etre sa se remettra ou dans le pire des cas l''on serat fixé ^^[/quote]\r\n\r\nà une seule condition, les mots soulignés en rouge, corriges les XD\r\n\r\n\r\nEdit : Tu est passé Rédacteur Aulyro1996 ,fait en bon usage :)'),
(178, 6, 4, 1363346590, 5, 'le probleme c''est que pour avoir les droit admin faut un serveur et on en n''a plus pour l''instant, j''en prendrai un dans le week end ou au pire lundi ;)'),
(179, 6, 4, 1363346766, 1, 'Oké :)'),
(180, 7, 2, 1363377812, 6, 'J''ai demande a Zeacross son choix par rapport au escouade vu qu''elle ne peut pas accéder au forum elle ma répondu l''escouade 1 Assaut médecin.'),
(181, 7, 2, 1363390159, 15, 'Pour moi c''est très simple, soit le snipe, soit le soutien. Et de préférence avec Lebetz, histoire de pouvoir se faire des petits challenges dans l''escouade (j''ai une réputation a défendre :p).'),
(182, 7, 14, 1363390660, 15, 'Je suis partant pour le match. Néanmoins, il se peut que je doive vous laisser avant la fin de la rencontre.'),
(183, 7, 2, 1363423863, 5, 'ok je prend tout cela en compte ;)'),
(184, 7, 14, 1363423967, 5, 'ya pas de news pour le match donc je ne sait pas si cela va se faire....'),
(185, 7, 14, 1363436845, 2, 'faudra leurs proposer une autre date et horaire , sa prouve encore qu''il est très compliquer d''organiser ce type de match '),
(186, 5, 20, 1363437015, 22, 'Salut à toute la meute j''ai reçu une demande de la Team Tear of Skyline XIII, Ils veulent faire des matchs contre nous environ toutes les semaines pour des entrainements, Donc je vous fais parvenir leur demande pour voir si sa vous interesse. Gloire a notre meute et montrons leur nos crocs.\r\n\r\nEdit: Le Samedi de 17H à 23H'),
(187, 7, 2, 1363444183, 1, 'T''as une ébauche là déjà ?'),
(188, 5, 20, 1363444270, 1, 'Très très bonne idée. Au pire, passes nous leurs site, et passes leurs le notre, ainsi, ça permettrait de partager les entrainements :)'),
(189, 5, 20, 1363455114, 5, '[quote=ben_ftwc] passes nous leurs site, et passes leurs le notre, ainsi, ça permettrait de partager les entrainements :)[/quote]\r\n\r\n+1 ;)'),
(190, 5, 20, 1363461030, 22, 'Leur leader a déjà le site, voici le leur http://tear-of-skyline.xooit.com/index.php il ma prévenu ils sont plus pour faire du T4 que du T5.\r\nIls communiquent plus par battlelog mais leurs site est assez actif.\r\n\r\nEdit: leur site n''est plus actif ils communiquent que par Battlelog et Csl voici le lien battlelog du leader: http://battlelog.battlefield.com/bf3/fr/user/JeanneDaArc/'),
(191, 7, 2, 1363512329, 5, 'ouaip, jme tire un peut les tiff mais bon...\r\n'),
(192, 7, 2, 1363514166, 5, '[URL=http://www.casimages.com/img.php?i=1303171055176025510979425.png][img]http://nsm08.casimages.com/img/2013/03/17//1303171055176025510979425.png[/img][/URL]\r\n\r\n[SIZE=4][COLOR=#B22222]Vous en pensez quoi? des modif a faire?[/COLOR][/SIZE]\r\n\r\nJe n''est pas encore toute les demande de place des membres de la meute pour les escouades, il me manque:\r\n\r\n\r\n-papa kenouzz\r\n-gyom\r\n-black skyread\r\n-ittecilef\r\n-mister_reveur\r\n-typex59\r\n-ousou\r\n-lucas-74\r\n-bio-gamer\r\n-mvgusta\r\n-faveval\r\n-fisher'),
(193, 1, 21, 1363516623, 5, '[ALIGN=center][SIZE=4][COLOR=#0000FF]Meneur :[/COLOR][/SIZE]\r\n \r\n[COLOR=#FF4500] WAZA-PsY-4-x[/COLOR]\r\n \r\n[COLOR=#0000FF][SIZE=4]Co-leadeur :[/SIZE][/COLOR]\r\n[COLOR=#FF4500]\r\n WAZA-gp_one \r\n \r\n  gyom-88 \r\n\r\nPignouf-30\r\n\r\nDartoch\r\n[/COLOR]\r\n \r\n[COLOR=#0000FF][SIZE=4]Recruteur :[/SIZE][/COLOR]  \r\n\r\n\r\n [COLOR=#FF4500]\r\n   Ankoleloup\r\n\r\nbmdc64/Reaper\r\n\r\n slimmorle\r\n\r\n  [/COLOR]\r\n \r\n[COLOR=#0000FF][SIZE=4]Membres :[/SIZE][/COLOR]    \r\n         [COLOR=#FF4500]\r\n                 \r\n                PsYkOsE \r\n \r\n                jujube-basque \r\n \r\n                ItadakimasTB \r\n \r\n                lucas-74_44 \r\n \r\n                kenouzZ-28 \r\n   \r\n                Bio-gamer \r\n \r\n                lord-beckett59 \r\n \r\n                clems-206 \r\n \r\n                ousou \r\n \r\n                aulyro \r\n \r\n                Mvgusta3 \r\n \r\n                sweelfor \r\n \r\n                Rakoto \r\n \r\n                Lebetz \r\n \r\n                lil-blakstar	 \r\n \r\n               Misteur-reveur \r\n \r\n                faveval \r\n \r\n                fisher \r\n\r\nRyukio\r\n\r\nRS-C77\r\n\r\nmika44500\r\n \r\n ben_ftwc\r\n\r\nbl4ck_skyread\r\n\r\nittecilef\r\n\r\nToutoun80740\r\n\r\nTweeZy\r\n\r\nTypex59\r\n\r\nSimrreuhh\r\n\r\nLawrent\r\n\r\nsicilianno35\r\n\r\n hugo_AK47_95\r\n\r\nalexx93190\r\n\r\nTarty51\r\n\r\nYosusu44\r\n\r\nhasma_tik\r\n\r\nx-LE-GAULOIS\r\n[/COLOR]\r\n\r\n[/ALIGN]'),
(194, 7, 2, 1363528432, 1, 'Hormis le fait que tu m''ai caché ce détail, tu risque d''avoir du mal dans la 2 et la 3e escouade :p\r\n\r\nSinon, mec ... Juste GG. C''est pas grand chose, je sais, mais quand même, pour le temps que tu as passé à penser la chose, et à te faire chier à nous pondre un graphique toussatoussa, je tiens (au noms des autres qui ne passeront pas sur le site) à te remercier grandement.\r\n\r\nKiss <3'),
(195, 7, 14, 1363529217, 1, 'Par contre, si les joueurs sont dispos, on peut faire un 6v6 en interne, ça nous ferait progresser le teamplay, et de 12 joueurs en même temps qui plus est ;)'),
(196, 6, 22, 1363540631, 27, '[SIZE=3][FONT=comic sans ms][ALIGN=center][COLOR=#0000FF]S[/COLOR]alut je tien a m''excuser pour mes absence soudaine mais les cours me prennent 90% de mon temps libre donc quand je suis pas sur mes devoir je dort donc je voudrai que vous me laissiez une chance de revenir dans la meute car c''est avec vous que j''ai envie d''avancer et pas une autre team . \r\nMerci NosKill4Ever [/ALIGN][/FONT][/SIZE]'),
(197, 6, 22, 1363543384, 5, 'Salut mon grand, c''est tout a fait compréhensible que tu face passé tes cour en priorité ;)\r\nJe t''es mis les droit pour que tu puisse voir toute les partie du forum, donc n’hésite pas a regarder les poste dans ces partie, certaine sont en cour et importante :p\r\nBon courage pour tes cour et a très vite sur le champ de bataille ;)'),
(198, 12, 7, 1363543535, 1, 'Juste un petit UP pour Mika les mecs :)'),
(199, 7, 23, 1363544297, 5, 'Salut a tous, donc comme je l''avais dit dans un autre post, je vais reprendre un serveur pour la meute demain, par contre j''aurais voulus savoir se que vous voudriez trouver dessus?\r\nDes modes et map de "base" comme on peut trouver lors de match ou bien des modes différant comme par exemple le nouveau capture de drapeau? tout en sachant que l''on pourra modifié, se n''est pas définitif ^^'),
(200, 12, 7, 1363544499, 2, 'pas vu encore psy4 , de mon point de vu il sera un bon apport pour l''équipe '),
(201, 6, 22, 1363544683, 2, 'pas de soucis des que sera dispo tu fera ton retour !'),
(202, 7, 2, 1363544752, 10, 'Yep :) Bon dsl de ne pas avoir était super actif sur ceNew site mais on vas dire que les révisions ne m''aide pas trop :)\r\nBon pour cette idée  je trouve ça vraiment pas mal , je vais remettre un petit post sur FB pr que tt le monde réponde !!\r\nPr moi personnellement j''me débrouille pas trop mal en char même si lord est carrément meilleur que moi , par contre le pilote d''hélico n''a plus de secret for me , viper birds , je suis comme un enfant dans un manège :) '),
(203, 12, 7, 1363544870, 5, '[quote=waza-gp_one] de mon point de vu il sera un bon apport pour l''équipe [/quote]\r\n\r\nLa meme pour moi ;)'),
(204, 6, 22, 1363548237, 1, 'Justement, j''ai ta photo sur mon pare-brise pour pas t''oublier va !\r\n\r\nRamenne vite ton cul !!! :p'),
(205, 7, 23, 1363566074, 1, 'Si c''est pas plus cher et rapide à changer, je partirais bien sur de l''End Game ^^\r\n\r\nPar contre, qui aura les accès pour organiser des matchs ?'),
(206, 12, 7, 1363595610, 8, 'Up! +1'),
(207, 7, 2, 1363595940, 8, '+1 pour toi dartouche c''est nickel'),
(208, 7, 23, 1363596271, 8, 'salut je pense qu''il te faut quand même des maps de base pour que le serveur se remplisse et tourne genre map plus infanterie comme metro bazar seine puis avec vehicule genre kharg tempete de feu et puis de la CDD et apres pour les modes conquête ou ruée qui est plutôt cool et qui change. Après ce n''est que mon avis fait le comme tu le sens.\r\nEncore un petit +1 pour toi quand même aussi.'),
(209, 7, 23, 1363597352, 1, 'Une ruée, je suis un peu gavé de la conquete (près de 90h de jeu non-stop en CQ :D )'),
(210, 7, 23, 1363611552, 5, 'Sa y est le serveur est en ligne depuis se matin ;)'),
(211, 7, 23, 1363617562, 1, 'Gg :D\r\n\r\nDu coup, t''as mis quoi dessus ?'),
(212, 7, 23, 1363620545, 5, 'pour l''instant du standard pour qu''il se remplisse facilement, la preuve il est plein depuis se matin ^^'),
(213, 7, 23, 1363634953, 1, 'Aller zou, j''m y rends :p'),
(214, 7, 23, 1363635399, 2, 'je passe ma commande de map , alors ile de wake en rué , golf oman en conquête , kharg en rué , métro en conquête ,\r\nfrontière Caspienne en conquête \r\ncapture du drapeaux pour la map avec les hélico d''attaque  kiasar  !!\r\naprès certaine map sont mieux en rué quand conquête du a leurs trop grande taille \r\npar exemple autoroute de téhéran , canaux de noshar , pic de damavand en rué elle sont adapté\r\n\r\nidem pour les map armor kill mais indéfendable en rué ! \r\n'),
(215, 7, 23, 1363685041, 1, 'Métro en CQ, je met mon véto si tu me permet. 9 fois sur 10, on termine la partie en spawnkill ... Que ce soit dans les couloirs au fond du métro, ou dans les batisses à sniper âme qui vive. En ruée, je suis plus OK.\r\n\r\nPar contre, serait-il possible de le passer en 200 ou 300% ? ^^'),
(216, 7, 23, 1363687297, 5, 'il est déjà en 200% benco ^^'),
(217, 7, 23, 1363694464, 1, '[SIZE=3]Ah ! J''aime.\r\n[/SIZE]\r\n[SIZE=1]( t''as pu voir mon mp facebook ? )[/SIZE]'),
(218, 6, 24, 1363720433, 27, '[COLOR=#4169E1][FONT=courier new][SIZE=4][ALIGN=center][SIZE=5][B]S[/B][/SIZE]alut la meute je fait des signature contre une balle de M16A3 si vous en voulez une a la suite de ce message un poste avec ce que vous voulez dedans et une image précis (si vous en avez une :) )\r\n[/ALIGN][/SIZE][/FONT][/COLOR]'),
(219, 6, 12, 1363724905, 4, 'Up'),
(220, 7, 23, 1363725170, 4, 'J''aimerai que l''on interdise le base rape sur tous les maps du serveur. Il n''y a plus d''intérêt à jouer sinon. Je préconise que l''équipe dominante reste sur le dernier drapeau et d''attendre l''ennemie. Merci. Ou de mettre une difficulté au jeu, soit mettre le tir allié actif, ou avoir un chef d''escouade qui serait le seul respawn de l''escouade. Ou enlever la Camkill, et le spot.'),
(221, 6, 24, 1363728526, 1, 'Ahhhhh, hey si tu veux, tu me fournit une vingtaine de frames, et je code un générateur de signatures :p'),
(222, 7, 23, 1363728769, 1, 'On peut paramêtrer tout ça pignouf ???\r\n\r\nSi oui, c''est énorme !!!\r\n\r\nSi non ... Tu sais toi même que personne ne respectera cette règle (Je citerais Métro CQ ...) mais bon, ce serait l''idéal !');
INSERT INTO `forum_posts` (`postID`, `boardID`, `topicID`, `date`, `poster`, `message`) VALUES
(223, 7, 23, 1363757312, 4, 'A part le Base rap, tu peux paramétrer tout le reste. Le tir allié, avoir qu''une seule personne sur qui on peut respawn le chef de groupe, enlever la killcam et le spot on peut meme enlever les véhicules et faire de l''infantrie sur toutes les maps.'),
(224, 6, 24, 1363765868, 27, '[FONT=comic sans ms]Tu veux dire quoi par frames :) ? \r\nEt voilà une nouvelle création\r\n[url=http://image.noelshack.com/fichiers/2013/12/1363770712-aedqdqwdq.gif][img]http://image.noelshack.com/fichiers/2013/12/1363770712-aedqdqwdq.gif[/img][/url] [/FONT]'),
(225, 6, 24, 1363770424, 1, 'Par trames, j''entends des fonds, où il ne reste que le texte à placer :)'),
(226, 7, 23, 1363770463, 1, 'Ah nan, pas touche à mes tutures ! ^^'),
(227, 6, 24, 1363770669, 27, 'Pas de problème :) je t''en ferais un peut chaque jour et comme sa on aura notre générateur de signature ;)'),
(228, 7, 23, 1363778095, 5, 'pour ma part ya pas de souci, je peut metre dans les regles "spawnkill et base rape interdit", ont voit si sa fonctionne pendant une semaine et si sa marche pas et bien on ajoutera de la dificulté au serveur'),
(229, 6, 24, 1363778194, 1, ';)'),
(230, 6, 24, 1363778276, 5, 'Et bien on dirait que j''ai de la concurance ;)'),
(231, 6, 24, 1363783347, 10, '[quote=Dartoch]Et bien on dirait que j''ai de la concurance ;)[/quote]\r\n\r\nmais non la concurence est la pour te faire evoluer :)\r\n'),
(232, 7, 23, 1363784237, 10, 'merciiiiiiiiiiiiiiii Dartoch !! est UP pr baserap et spawkil'),
(233, 12, 18, 1363784465, 10, '[SIZE=3][SIZE=4][SIZE=5][COLOR=#FFA500]Salut Atonium , je n''est pas eu le temps de te le dire la dernier fois , jai bien aimé ton teamplay et ton calme vue la branlé qu''on n''a prise alors je n''es qu''un truc a te dire !! WELCOME , alors maintenant montre nous qu''on a eu raison de te faire confiance !![/COLOR][B][SIZE=4][FONT=courier new][ALIGN=center][/ALIGN][/FONT][/SIZE][/B][/SIZE][/SIZE][/SIZE]'),
(234, 12, 7, 1363784752, 10, '[B]Salut Mika , bon c''est dernier temps je n''es as trop eu le temps de vadrouiller sur le site , mais gp ma parlé de toi , alors il en convient que tu as largement ta place parmis nous !!\r\nDonc aujourd''hui tu es un APAX , WELCOME , profite parmis nous , et montre nous que t''as l''envie de montrer les crocs :)[/B][SIZE=3][COLOR=#FFA500][FONT=courier new][ALIGN=center][/ALIGN][/FONT][/COLOR][/SIZE]'),
(235, 12, 7, 1363786471, 1, 'Ahhhh, bienvenu mon milka, Manu, tient pour faire l''image Facebook [IMG]http://data.tumblr.com/Lj0Mvf5GP608v39dKLDpFJba_500.jpg[/IMG] ^^'),
(236, 7, 23, 1363786544, 1, 'Par contre, toch, tu pourrais mettre à jours le welcome message sur le serveur ? Le site est à présent www.meute-apax.fr '),
(237, 6, 24, 1363786785, 1, 'Voit ça comme un partenaire qu''un adversaire :p\r\n\r\nT''façon, j''ai la même signature depuis les 4Frags, comptez pas sur moi pour la changer, tu comprendras aisément j''pense toch ^^'),
(238, 12, 18, 1363786918, 1, 'C''est quoi cette agression manu ?\r\n\r\nBienvenu à toi AtOnIuM_GhAuX . J''ai bien eu ton invitation sur PSN, mais pas eu l''occasion de jouer avec toi, néanmoins, a une prochaine :d'),
(239, 7, 17, 1363787038, 1, 'Bon, je déterre un peu un vieu sujet (lol), mais j''ai eu que le retour de Dartoch Aulyro1996 et bmdc64 , me dites pas que sur toute la meute, seuls 20% des membres s''investissent !!'),
(240, 7, 17, 1363787766, 10, 'T''inquiète on vas les forcer a venir ^^ !!'),
(241, 7, 17, 1363789576, 1, '[quote=WAZA-PsY-4-x]T''inquiète on vas les forcer a venir ^^ !![/quote]\r\n\r\nBoarf, je penses que l''initiative serait plus motivante qu''un croc sous la gorge, mais s''il faut ça ...'),
(242, 6, 24, 1363795844, 27, 'Non pas de la concurrence mais je veux bien te faire des tuto ;p non si tu a besoin d''aide hésite pas et si j''en ai besoin je viendrais te voir ;)'),
(243, 7, 23, 1363799958, 5, 'Pas de problème, dès que je me connecte je m''en occupe ;)'),
(244, 12, 18, 1363800104, 5, 'Ben regarde la video de manu et tu comprendra ^^ on sait fait plaisir avec nosk :p\r\n\r\nBienvenu a toi Ato !!!!!'),
(245, 12, 7, 1363800205, 5, 'Bienvenu parmi nous Mika !!! '),
(246, 6, 24, 1363800445, 5, 'T''en fait pas Nosk, je déconnai :p sa fait plaisir de voir que les membres s''investisses :) donc aucun probleme et si tu a besoin d''un coup de main tu peut compté sur moi ^^'),
(247, 12, 7, 1363805039, 27, 'Bienvenue :) a bientôt sur le champ de bataille '),
(248, 12, 18, 1363807019, 25, 'Merci a tous :) Je n''ai pas pu rencontrer tout le monde encore et tous vous ajouter sur ps3 mais j''avais quelques exams a reviser pour jeudi et vendredi :) donc je serais la ce week-end sans probleme.\r\nJ''espère vous faire part de l''honneur que j''ai de faire partie de vos rangs :))\r\nA bientôt sur le champ de bataille !!'),
(249, 12, 18, 1363816050, 10, 'Loool pas fait expret pour l''agression. :) '),
(250, 7, 23, 1363816150, 10, 'Loool est sans fautes orthographe ^^ bienvenue ac un e ^^'),
(251, 7, 17, 1363816670, 10, 'Rien a branler on CROC !!'),
(252, 12, 7, 1363818824, 16, 'Super!!! Par contre je crois qu''il n''y a pas de camouflage violet et blanc. '),
(253, 7, 2, 1363828126, 15, '[quote=Dartoch][URL=http://www.casimages.com/img.php?i=1303171055176025510979425.png][img]http://nsm08.casimages.com/img/2013/03/17//1303171055176025510979425.png[/img][/URL]\r\n\r\n[SIZE=4][COLOR=#B22222]Vous en pensez quoi? des modif a faire?[/COLOR][/SIZE]\r\n[/quote]\r\n\r\nMmmmm perso je ne suis pas bon en tant qu''artilleur de jeep.\r\nTu pourrais pas faire un échange de poste avec Rakoto ?'),
(254, 7, 2, 1363847909, 27, 'Je trouve que c''est une très bonne idée mais il faudrait en faire plusieurs tableau car la c''est pour le 12Vs12 mais le 4Vs4 :) . \r\nBon je go m’entraîner a la jeep ! :D\r\n'),
(255, 7, 17, 1363853892, 1, 'Hehe, attaque manu lol'),
(256, 6, 24, 1363854268, 1, '[quote=Dartoch]T''en fait pas Nosk, je déconnai :p sa fait plaisir de voir que les membres s''investisses :) donc aucun probleme et si tu a besoin d''un coup de main tu peut compté sur moi ^^[/quote]\r\n\r\nVoir sur moi, mais c''est cher lol'),
(257, 7, 2, 1363856180, 5, 'faut que je le remete a jour suite au demande des membres, j''essaye de faire sa le plus rapidement possible ;)'),
(258, 7, 23, 1363856307, 5, 'lol t’inquiète Manu, j''en est profité pour les corrigé ^^ \r\nLes nouvelles règles sont mise en place donc maintenant reste plus qu''a les faire respecté ;)'),
(259, 7, 23, 1363860566, 4, 'donc du coup les règles du base rap interdit a été mise en place, c''est cool !'),
(260, 7, 23, 1363862457, 1, '[quote=Pignouf30]donc du coup les règles du base rap interdit a été mise en place, c''est cool ![/quote]\r\n\r\n+1.\r\n\r\nT''façon, j''pense qu''a coups de kicks, ça devrait vite rentrer dans les têtes ;)'),
(261, 12, 7, 1363864502, 10, 'Si tu désire mètre une signature , j''ai mis la conduite a tenir sur le Fb , voilà MEUUUUUUUH :) '),
(262, 6, 25, 1363864714, 10, '[B][FONT=arial]Salut la Meute , bon vue les likes et ma petite centaine de vue , désirez vous que je refasse une petite vidéo commenté , hélicoptère , tank ou même assaut !!\r\nEn expliquant quelques une de mes astuces précieuse ou autres chose , si vs avez des idées n''hésitez pas !! Voiloup , a très vite les loups :) [/FONT][/B][COLOR=#FFA500][/COLOR]'),
(263, 6, 25, 1363866315, 27, 'Oui ! je veux tes secret ! :) pourquoi pas avec moi pour te réparer un peut d''xp fait pas de mal :D'),
(264, 7, 23, 1363866613, 2, 'vas y'' avoir du boulot a faire respecté les règles !!!'),
(265, 12, 7, 1363866782, 1, 'Meuh\r\n[URL]http://boitam.eu/[/URL]'),
(266, 6, 25, 1363866833, 1, '[quote=NoSkill4Ever]Oui ! je veux tes secret ! :) pourquoi pas avec moi pour te réparer un peut d''xp fait pas de mal :D[/quote]\r\n\r\nHan, j''balance pas, mais y''en à ici qui veulent gratter des points ! ^^'),
(267, 7, 9, 1363867884, 2, 'y''aurai moyen d''avoir une catégorie pour parler de bf4  et de la ps4 pour apporter les news ?'),
(268, 7, 9, 1363867982, 5, 'Sa doit pouvoir se faire, surtout que les news de bf4 devrait ariver debut de semaine prochaine ^^'),
(269, 7, 9, 1363868129, 2, 'surtout que y''a des nouvelles rumeurs a propose de bf4 sur ps4!!! '),
(270, 6, 25, 1363868217, 5, '+1 pour une nouvelle video ^^\r\nEn plus sa nous fait connaitre encore plus donc pourquoi s''en priver ;)'),
(271, 7, 9, 1363868467, 5, 'oui ils en ont parlé se matin, il va être présenté dans quelque jours et d’après les première critique qui ont filtré sa va être une tuerie, il devrait utilisé la puissance de la ps4 a 100% donc niveau graphisme sa va être le top, reste a voir l''esprit du jeux et le gameplay..'),
(272, 6, 25, 1363868930, 6, 'Ou une video sur tes deplacements '),
(273, 6, 25, 1363869116, 10, 'Jeudi prochain alors j''aurais besoin d''un gunner au Viper , jvais montrer qu''on peut survivre mm ac les iglas :)  ou alors je fais une partie ac une arme merdique pour vous montrer que c''est l''emplacement qui est plus important que l''arme en elle même.!!'),
(274, 6, 24, 1363869170, 6, 'Comment vous faite pour mettre une signature j''y arrive pas :D'),
(275, 6, 25, 1363869180, 10, 'Oui je trouve les déplacement plus important pr vs , mais je prend une arme merdique ou pas !? '),
(276, 6, 24, 1363869256, 6, 'Ah j''ai rien dit sa marche ^^'),
(277, 6, 25, 1363869345, 6, 'Y a aucune arme merdique ^^ Faut savoir les jouer tout simplement'),
(278, 6, 24, 1363869395, 10, 'Je t''es mis le tuto sur Fb , tu peux en mettre seulement 2 ;)'),
(279, 7, 9, 1363869906, 10, 'Vs avez une idées du prix de la play4 sachant que la 3 était a 399 me semble t''il ?'),
(280, 7, 9, 1363870417, 1, '[quote=waza-gp_one]y''aurai moyen d''avoir une catégorie pour parler de bf4  et de la ps4 pour apporter les news ?[/quote]\r\n\r\nCatégorie Forum ou Catégorie de News sur le site ?\r\n\r\n\r\n\r\nsur le forum '),
(281, 6, 25, 1363870462, 1, 'RPG ? :p\r\n\r\nS''il te faut un gunner, je suis dispo ma caille, ''me demerde plutôt pas mal, mais en soirée par contre :)'),
(282, 6, 25, 1363870998, 10, 'LOL lebetz merci pour ce commentaire , je vais étudier ça de Très prêt !!\r\n\r\nC bon j''ai étudier , OUI y''a des armes merdique ;) '),
(283, 6, 25, 1363874604, 6, 'Du genre?'),
(286, 6, 25, 1363877256, 10, 'PP-2000 '),
(288, 7, 9, 1363878806, 5, '[quote=WAZA-PsY-4-x]Vs avez une idées du prix de la play4 sachant que la 3 était a 399 me semble t''il ?[/quote]\r\n\r\nelle était plus cher que sa, 300euros c''est le prix a la quelle tu peut la trouvé maintenant alors que la ps4 est prévu a 300 350 euros dès le départ pour concurrencé la nouvelle xbox'),
(290, 6, 25, 1363879123, 5, 'dans se cas : "Les mecs va falloir tous se ramené en médecin, manu va avoir besoin de réa !!!!! " lol'),
(291, 6, 25, 1363880980, 10, 'Non même pas vrai , je vais tester la PP-2000 allez , bon je vais peut être le faire rouler dessus mais ça va le faire ^^'),
(292, 7, 9, 1363881782, 1, 'Annoncée à 600e à la [URL=http://jeux-video.fnac.com/Console-PS4-Sony-Console-Playstation-4-Sony-Prix-provisoire-Precommande-garantie-au-prix-de-lancement/a5769984/w-4]Fnac [/URL] =) '),
(295, 6, 25, 1363888382, 8, '+1 pour une video commentée car j''aime entendre ta voix d''enfant'),
(296, 12, 7, 1363888450, 8, '[quote=ben_ftwc]Meuh\r\n[URL]http://boitam.eu/[/URL][/quote]\r\n+1'),
(297, 7, 9, 1363892330, 2, 'sur le forum '),
(298, 6, 27, 1363896835, 10, '[ALIGN=center][SIZE=4][COLOR=#FF4500][B]Bon nous allons tester ça pendant 2 semaines : les binômes de choc\r\n\r\nJe vous demande de choisir 3 joueurs au max avec qui vous voudriez jouez continuellement , je m''explique,  le rôle de votre coéquipier  et vous  sera de se suivre continuellement , si un décide d''attaquer, l''autre sera obligé de le suivre , cela augmentera votre capacité a jouer en équipe et a parler au micro !!\r\nVotre choix ne doit passe  faire en fonction des autres demandes mais de votre envie , je ferais les duo de choc en fonction de vos choix et de mon ressentit , il ne faudra pas se vexer si vs vs retrouvez pas ac celui qui vs vouliez , ce n''est que pour 2 semaines , et ça commence ce week-end , ça reste un test alors prenez le ac le smile [/B][/COLOR][/SIZE][/ALIGN]\r\n[COLOR=#32CD32]\r\n[B]WAZA-PsY-4-x:[COLOR=#FF4500] Zea / Bmcd / pignouf [/COLOR]\r\nGyom-88 :[COLOR=#FF4500]yuri / atonium[/COLOR]\r\nGP-one : [COLOR=#FF4500]mika / batiste / Typex[/COLOR]\r\nYuri: \r\nPignouf :[COLOR=#FF4500] Anko / yuyu / manu [/COLOR]\r\nPSyko:\r\nDartoch [COLOR=#FF4500]:gp / batist / pignouf[/COLOR]\r\nClems :\r\nBen: [COLOR=#FF4500]Lebtez / batiste / dartoch [/COLOR]\r\nJuju:[COLOR=#FF4500] kenouzz / pacome / Itecilef [/COLOR]\r\nAnko :[COLOR=#FF4500] pignouf / yuyu / juju[/COLOR]\r\nIta:[COLOR=#FF4500] Lebetz / pignouf / juju [/COLOR]\r\nKenouzz: \r\nLebetz:][COLOR=#FF4500] ita / Ben / pignouf [/COLOR]\r\nMister:\r\nSxeelfor:\r\nZea:[COLOR=#FF4500] Bmcd64 / dartoch / Lord[/COLOR]\r\nFisher :\r\nBlack: \r\nAulyro:\r\nMvgusta : [COLOR=#FF4500]Lord / noskill / Mister [/COLOR]\r\nBmcd :[COLOR=#FF4500] PsY / Gp / dartoch [/COLOR]\r\nTypex :[COLOR=#FF4500] black /rakoto / Sweelfor[/COLOR]\r\nAtonium : \r\nMila : \r\nNoskill: [COLOR=#FF4500]MVGUSTA / pignouf/ dartoch [/COLOR]\r\nRS-C77:\r\nRakoto: y[COLOR=#FF4500]uri / Gyom / lebetz[/COLOR]\r\nLord:[COLOR=#FF4500] zea /aulyro / dartoch[/COLOR]\r\nLucas:\r\n\r\n\r\n[/B][/COLOR]'),
(299, 6, 27, 1363898112, 1, 'LeBetz,bmdc,toch'),
(300, 7, 9, 1363898370, 1, 'Bah,y''a le salon non ?\r\nAu pire, je t''ajoute un sous forum moins bordel :)\r\n\r\nPar contre, faudra penser a le modéré ;)'),
(301, 6, 27, 1363898411, 3, 'il manque ite, ou titi\r\n\r\nmoi, papa/pacome/ite'),
(302, 6, 27, 1363898553, 24, 'SALUT\r\nN''importe temps que vous me supporté :) :p\r\nMoi je suis avec qui veut bien se mettre avec moi :) \r\n'),
(303, 6, 27, 1363898722, 10, 'Dsl Typex , faut une réponse ;) '),
(304, 6, 27, 1363898848, 3, 'non pas ita mais ittecilef'),
(305, 6, 27, 1363899007, 24, 'Manu je veux pas m''imposer , je suis la que le week end :( '),
(306, 6, 27, 1363899313, 10, 'Je t''es pas demandé d''être meneur lool juste de faire un choix , t''inquiète je m''arrangerais pour ça !!'),
(307, 6, 27, 1363899328, 15, 'Lebetz, Pignouf, jujub'),
(308, 6, 27, 1363899810, 3, 'je veux pas ben non mais lol lis bien l''admin, loul'),
(309, 6, 27, 1363900107, 1, 'Mdrrr juju. Pète les plombs Ce soir lol '),
(310, 7, 2, 1363900524, 17, 'Pour moi c''est soutient ou assaut avec jujub itadakimas papa-kenouze voila'),
(311, 6, 25, 1363901806, 10, '[quote=bmdc64]+1 pour une video commentée car j''aime entendre ta voix d''enfant[/quote]\r\n\r\n\r\nHihihi , mon micro a une voix d''enfant , ma voix elle aime sucer des cigare ^^'),
(312, 6, 27, 1363904998, 28, 'yuri et atonium'),
(313, 7, 14, 1363905181, 20, 'Samedi 30 ? Dimanche 31 ? Samedi 6 ? Dimanche 7 ? \r\nComme vous voulez, on peut tenter l''après-midi si vous voulez même si ça nous arrange pas. ;-)'),
(314, 12, 7, 1363905231, 16, 'sinon sa veut dire quoi APAX?'),
(315, 6, 27, 1363930028, 27, 'Pour moi MvGusta , Pignouf , Dartoch :D'),
(317, 1, 1, 1363930595, 27, 'apax ou hapax (nom masculin) - Définition Mediadico\r\nMot, expression, tournure linguistique, que l''on n''a observée qu''une seule fois à une époque donnée.\r\n\r\nVoilà je Souhaite remercier Google de tout part ! '),
(318, 6, 27, 1363936281, 2, 'pour moi tout dépend si infanterie ou véhicule , mais je pense avec dartoch , mika44500 ,  bmcd , typex\r\nj''ai fait en fonction de mes horaires de jeu \r\nsinon c''est une bonne initiative ces binôme pour acquérir plus d''automatisme '),
(319, 8, 28, 1363936760, 2, 'voici une rumeur plutôt intéressante \r\n\r\n[COLOR=#FF0000]Battlefield 4 : Des infos leakées ?La grosse rumeur du jour provient du site chinois chiphell.com qui affirme avoir obtenu des indiscrétions sur le très attendu Battlefield 4. Celles-ci seraient tirées de l''édition d''avril du magazine Edge qui devait à l''origine se charger de présenter le jeu en parallèle de son annonce officielle, prévue le 26 mars prochain à la GDC. On vous laisse donc découvrir tout ceci en vous rappelant que ces informations ne sont pour l''heure pas officielles.\r\n\r\n \r\n\r\n \r\n\r\n \r\n\r\n \r\n\r\nAspect technique :\r\n\r\n    - Utilisation d''un moteur Frostbite 2.5\r\n    - Gestion dynamique de la météo : pluie, tempêtes de sable, brouillard...\r\n    - Retour du système Destruction 4.0 avec des environnements encore plus destructibles que dans Bad Company 2\r\n    - Nouvelle technologie de tessellation pour l''eau, le terrain et les NPC\r\n    - Seule la version PC sera compatible avec DirectX 11\r\n    - 60 images par seconde en 720p pour les versions PS4 et Xbox 3\r\n\r\n \r\n\r\nCampagne solo :\r\n\r\n    - L''histoire pourrait se dérouler en 2020 au cœur d''une guerre entre les USA et la Chine\r\n    - Deux campagnes distinctes\r\n    - Durée de vie de 5 à 6 heures\r\n    - On verra la tour Perle de l''Orient de Shanghai s''effondrer\r\n\r\n \r\n\r\nMultijoueur :\r\n\r\n    - 64 joueurs sur PC, PS4 et Xbox 3 et seulement 24 sur PS3 et Xbox 360\r\n    - Le système de modification des armes de BF3 sera conservé et amélioré\r\n    - Liste non exhaustive des maps : Diaoyu Island, Tiananmen, Shanghai Bund, Xizhimen, Xizhimen Rush\r\n    - Nouveaux véhicules, dont les tanks M1A2 TUSK et PLA Type 99 ainsi que les avions J20, Y20 et Xianglong\r\n    - Aires de combats aériens plus étendues\r\n    - Retour du système de Commandants\r\n    - Micro-transactions censées ne pas déséquilibrer le jeu\r\n    - Plusieurs maps d''ores et déjà prévues en DLC : Centrale électrique de Dalian, Le Champ Pétrolifère de Daqing, La Grande Muraille de Chine et le grand classique Wake Island\r\n\r\n \r\n\r\nEnfin, sachez qu''une sortie en novembre 2013 est évoquée. Il ne reste donc plus qu''à attendre une probable confirmation officielle qui interviendra au plus tard dans 6 petits jours.[/COLOR]'),
(320, 6, 27, 1363937265, 8, 'Manu, GP et dartouche pour ma part'),
(321, 6, 27, 1363938388, 6, 'Merci pour cette belle faute de frappe manu sur mon nom ahahh.\r\nSinon mes choix sont: ItadakimasTB (mon apprenti) Ben_FTWC et Pignouf30 '),
(322, 6, 27, 1363940794, 4, 'Itadakimas, Anko, Yuri, Manu, lebetz, dartoch, enfait tu peux me mettre avec n''importe qui ca va joué.'),
(323, 6, 27, 1363941641, 9, 'moi je m''en fous'),
(324, 1, 1, 1363946058, 10, 'En gro c''est mots sont ultra rare , tellement rare est discret qu''on se demande si ils existent vraiment ! Ils sont unique vue qu''ils ont était utilisé une seul fois !!\r\nOui on a était cherché loin , mais c ttjrs mieux que la plus part des noms merdique ( TESS , Dick , WEED , et j''en passe :) '),
(325, 7, 14, 1363948680, 10, '[FONT=tahoma][B]Bon j''ai regardé par rapport a mes disponibilité , histoire de bien préparer ce match , le seul week de libre que je peux avoir est le samedi dimanche 13 & 14 avril , je te laisse choisir le jour et l''heure  vue qu''on a était assez indécis jusqu''à présent !! Je te remercie d''avance !! Par contre 12 joueurs je ne suis pas sur d''avoir les 12 a 100% , j''attends ta réponse et je ferais un sondage rapide pour voir si je peux 12 ou si j''assure ac 8 !![/B][/FONT]'),
(326, 1, 1, 1363950324, 27, 'Je suis tout a fais d''avis avec toi l’originalité est la plus grande des armes '),
(327, 6, 27, 1363952698, 5, 'Alors pour ma part:\r\n\r\nGp / Bmdc / Pignouf ;)\r\n\r\nps: très bonne idée Manu ;)'),
(328, 6, 27, 1363955139, 12, 'moi je veux les plus nuls yuri, gyom et lebetz'),
(329, 6, 27, 1363967357, 24, 'PAR contre je veux bien etre avec GP si possible? :)'),
(330, 7, 14, 1363968781, 20, 'Samedi 13 à 21H ça vous va ? Ou alors dimanche 14 vers 15h. Je vous laisse inviter des contacts et des amis à vous qui ne font pas parti de la team si vous n''êtes pas 12. ;-)\r\nPour le choix de la map, la notre sera Pic de damavand en ruée. Je vous laisse choisir la votre (mode de jeu et map)\r\n'),
(331, 6, 25, 1363969930, 3, 'a moustache?'),
(332, 12, 7, 1363976943, 1, 'Faut aller voir dans le salon, "1e sujet de test"'),
(333, 6, 25, 1363977760, 1, 'blonds *'),
(334, 9, 29, 1363978001, 1, '[ALIGN=center][IMG]http://sphotos-g.ak.fbcdn.net/hphotos-ak-ash4/221662_282731445194096_1303580804_n.jpg[/IMG]\r\n- bataille only C4 sur métro ( jveux que ça pete dans tt les sens ) \r\n- Course robot nem sur frontière Caspienne \r\n- Photo de Famille sur Tour Ziba\r\n\r\nÉvidement le mieux serait que certains joueurs ( le plus possible ) possède skype sur l''ordi ou en application pour la soirée , comme ça les consignes pourront être donné aux adversaire , puis c''est toujours d''autres d''entendre ça victime râler :) \r\n\r\nAlors a vos manettes , je vous proposerez un date pour voir si ça vs arrange !!\r\nPsY-4-x[/ALIGN]'),
(335, 6, 25, 1363979007, 6, 'Bah la PP 2000 c''est pas de la merde je la joue et c''est loin d''etre de la merde sauf en tir de loin\r\n'),
(336, 9, 29, 1363982717, 10, 'Alors on peut tester samedi. 13 avril !! Vers 20 h !! La photo se fera sur tempête de feu ac tt les véhicule jeep hélicoptère d''attaque et de reconnaissance  tank en bas de la colline et les APAX en forme de A au milieu de la colline ..'),
(337, 8, 28, 1363983029, 5, 'premier aperçu du gameplay le 27 mars !!!!! \r\n\r\nhttp://battlefield.com/fr/battlefield-4'),
(338, 6, 27, 1363990028, 5, 'WAZA-PsY-4-x:\r\n\r\nVoici vos binômes , je vous rappelle que c''est juste un premier jet , on fera évoluer les binômes avec le temps . Pour les consignes , cela démarre demain matin , pour les plus perspicaces vous remarquerez que certains on 2 binômes , mais c''est pour palier aux absents . Si les 3 sont la , vous devenez des triplés , si 2 joueurs sont la mais pas dans le même binômes , il en convient que vous pouvez créer un binôme de substitution ..\r\nVotre rôle pendant une semaine , et de ne jamais vous quitter , toujours rusher ensemble , on fera un débriefing en fin de semaine , jouez le jeu et vous verrez ça vs sera bénéfique .\r\n\r\n\r\nPsY / Bmcd \r\nPsY / Zea ( lord absent )\r\nZea / lord \r\nGyom / yuyu\r\nPignouf / Anko \r\nGp / mika\r\nBen / lebetz\r\nJuju / Ittecilef \r\nJuju / dartoch \r\nKenouzz / ita \r\nMister / sweelfor\r\nBlack / aulyro \r\nNoskill / Mister \r\nTypex / rakoto\r\nAtonium / Lucas \r\nYuri / Black\r\nDartoch / Gp_one\r\n\r\nWAZA-PsY-4-x'),
(339, 6, 27, 1363990196, 16, 'j''ai pas eu l''occasion de jouer avec grand monde alors : GP-one, gyom et ben.'),
(340, 1, 1, 1363990541, 16, 'ok merci'),
(342, 6, 27, 1364038593, 25, 'De même je n''ai surement pas encore rencontrer tout le monde donc pas de préférence au niveau du teamplay donc pour moi pas de problème avec lucas :)'),
(343, 9, 29, 1364044074, 1, 'Et à ce moment, manu se réveille avec le pinceau dans la colle :D'),
(344, 7, 2, 1364056659, 25, 'Moi ces assaut basic ou fumi (c''est ici que j''exele) ou alors possibilité soutien :)'),
(345, 7, 2, 1364066093, 26, 'Perso je peux taper dans la reco'' ( vous connaissez mes talents au verrou ) , je peux taper dans l''escouade blindé ( artilleur , pilote en T-90 ) ou bien me mettre avec Juju , Thierry et Dartouch je crois bien . Voilà en gros je peux faire le bouche trou '),
(348, 7, 2, 1364112919, 5, '[SIZE=4][COLOR=#87CEEB]Et voila après les quelque modif demandé et du coup quelque ajustement\r\nAlors ???[/COLOR][/SIZE]\r\n\r\n[URL=http://www.casimages.com/img.php?i=1303240915556025511005113.png][img]http://nsm08.casimages.com/img/2013/03/24//1303240915556025511005113.png[/img][/URL]'),
(349, 7, 2, 1364121698, 26, 'Thierry se mettrait en Sniper DAS ? il est chaud ? mdr'),
(350, 7, 2, 1364130256, 5, 'Pour cette place, on peut soit se mettre en snipe et du coup donné un 3eme appuis ou alors se mettre en soutien pour réapprovisionné du coup s''il est pas chaud au snipe il peut passé soutien, c''est pas un problème ;)'),
(351, 7, 2, 1364132186, 1, 'Je t''avouerais que de loins, j''ai lu "Chartilleur", j''ai rigolé.\r\n\r\n\r\nSinon, l''escouade de choc la 2e non ? :p'),
(352, 7, 2, 1364144547, 5, 'j''ai essayé de mettre tout les membres a leur meilleur place donc in game sa devrai envoyé du lourd !!!! ;)'),
(353, 7, 2, 1364146485, 10, '[url=http://www.casimages.com/img.php?i=13032406334692568.jpg][img]http://nsa34.casimages.com/img/2013/03/24/mini_13032406334692568.jpg[/img][/url]\r\n\r\nÇa a pas mal ragkit en face :) Tankiste de père en fils ^^ \r\nJe me débrouille vraiment bien en hélicoptère mais si Gp désire la place du pilote , c''est pas un Soucy pour moi !! Assaut tank hélicoptère jsuis pas chiant comme mec ^^'),
(354, 12, 30, 1364148252, 37, 'Pseudo: toutoun80740\r\nPrénom: Anthony\r\nAge: 23 ans (né le 5 Juin 1989)\r\nPSN: toutoun80740\r\nBattlelog: http://battlelog.battlefield.com/bf3/fr/soldier/toutoun80740/stats/198202460/ps3/\r\n\r\nComment avez vous connu la meute? j''ai connu la meute grâce a la page Facebook\r\n\r\nPourquoi vouloir rejoindre nos rang? je cherche une Team sérieuse, qui aime faire des tournois,match de team ect\r\net qui c''est jouer pour le fun.\r\n\r\nParlez nous un peut de vous:je suis ouvrier en démolition, j''aime le sport, les sortis en amis ect...\r\nj habite a Saint-Quentin dans l''Aisne (Picardie).\r\n\r\nVoila en espérant avoir une réponse positive de votre part, Merci et a bientôt.\r\n'),
(355, 12, 30, 1364149086, 10, 'Salut Anthony , c moi Manu ou WAZA-PsY-4-x que tu as eu sur Facebook :) \r\nBienvenue dans notre tanière , pas mal de APAX vont venir voir ton post et te répondre , pour ma part je ne pourrais pas jouer avant fin de semaine , donc je compte sur la meute pour me dire l''avis sur ce futur ou pas APAX , je vous laisse le découvrir et le recruter , j''attends vos réponse , j''officialiserais l''annonce en milieu de semaine , bon courage anthony et joue ac le plus d'' apax que tu peux ;) '),
(356, 7, 2, 1364153005, 2, 'je suis une quiche en assault même si j'' essai de m''améliorer , je préfère sois les tank ou bien évidement l''hélico de toute façons faut 2 p!ilote minimum car quand sa marche pas avec l''un l''autre prend le relais '),
(357, 12, 30, 1364153355, 2, 'slt tu pourra rajouté mon pseudo psn : waza-gp_one\r\non essaiera de fairte quelque partie \r\ntu joue quel classe principalement ? \r\nquel type de carte ? Metro etc.... infanterie ou véhicule etc....'),
(358, 12, 30, 1364154174, 8, 'salut toutoun ajoute bmdc64 qu''on se fasse quelque parties, bon courage en tout cas pour ton recrutement'),
(359, 7, 2, 1364154714, 10, 'Oui voilà , mets moi remplaçant hélicoptère et tank , j''ai un bon niveau dans les 2 , je pourrais alterner , a moi que tu sois un excellent gunner gp alors la on pourrais être ensemble , car j''avoue jsuis moins bon en gunner !!\r\n\r\n\r\nsa vas gunner je me débrouille bien,  sa dépend du pilote aussi , j''ai vu que la map serai tempête de feu , faut vraiment avoir de gros pilote d''avion sinon sa vas être compliqué , il faudra déployer un marqueur laser !'),
(360, 7, 2, 1364160769, 5, 'ok je refait la modif demain matin ^^'),
(361, 12, 30, 1364160902, 5, 'Salut toutoun et bienvenu sur notre site (tu me comprendra..lol ) \r\nbon courage a toi pour ta demande de recrut ;)'),
(362, 7, 14, 1364162784, 20, 'Verdict ? '),
(363, 12, 30, 1364163698, 37, '[quote=waza-gp_one]slt tu pourra rajouté mon pseudo psn : waza-gp_one\r\non essaiera de fairte quelque partie \r\ntu joue quel classe principalement ? \r\nquel type de carte ? Metro etc.... infanterie ou véhicule etc....[/quote]\r\n\r\nje joue principalement en assault et en soutien, ma carte de predilection est Operation metro'),
(364, 9, 29, 1364167826, 15, 'C''est une bonne idée de fêter l''anniversaire de la team ! J''ai hâte d''y être :p'),
(365, 7, 2, 1364168077, 15, 'Ah ! C''est mieux :D\r\n\r\nBon bah il n''y a plus qu''à se lier d''amitié avec ses collègues.\r\n\r\n"Lebetz, tu peux me filer la savonnette par terre stp ?" *Troll mode ON mouahahahahahaha*'),
(366, 12, 30, 1364199024, 1, 'Z''en avez pas marre de Métro ? Oses me dire que tu joue à la M16A3, et on te trouveras un compère (Oui oui pignouf, je parles bien de toi !  :p )\r\n\r\n\r\nAjoute ben_ftwc et bonne chance pour ton recrutement :)'),
(367, 7, 14, 1364199096, 1, 'Y''a pas eu de news ce weekend. Je vais rameuter le lead et t''aura ta réponse darkyoyo19-1 ^^'),
(368, 7, 2, 1364199270, 1, '[quote=itadakimasTB]Ah ! C''est mieux :D\r\n\r\nBon bah il n''y a plus qu''à se lier d''amitié avec ses collègues.\r\n\r\n"Lebetz, tu peux me filer la savonnette par terre stp ?" *Troll mode ON mouahahahahahaha*[/quote]\r\n\r\n\r\nitadakimasTB , pas touche à mon binôme hein? C''est MON bizu, je te le prêterais quand j''aurais fini avec lui ! '),
(369, 9, 29, 1364201975, 5, 'de même pour moi je serai présent !!! ;)'),
(370, 7, 2, 1364203219, 5, 'Et voila, changement apporté ;)\r\n\r\n[URL=http://www.casimages.com/img.php?i=1303251020516025511009812.png][img]http://nsm08.casimages.com/img/2013/03/25//1303251020516025511009812.png[/img][/URL]'),
(371, 7, 14, 1364203853, 10, '[B]Ok pour dimanche 15h !! \r\n[FONT=courier new]\r\nOn choisiras tempête de feu en conquête , pour les restrictions de notre cotes :\r\n\r\n\r\n- Conquête / Tempête de feu .\r\n- 100% des tickets .\r\n- Pas de fusils a pompe / C4 sur objectif \r\n- la partie ce fera en 2 manches comme la ruée \r\n\r\nAjoute moi WAZA-PsY-4-x [/FONT][/B]'),
(372, 7, 2, 1364207252, 1, 'De mon coté, je suis plus du genre à bouffer 2 places dans un char, quand je tires pas, je suis en 3e pers à indiquer au Laser, c''est autorisé en match le laser hein ?'),
(373, 9, 29, 1364207279, 1, 'Franchement, ça va envoyer du pâté les mecs !'),
(374, 12, 30, 1364212360, 37, '[quote=ben_ftwc]Z''en avez pas marre de Métro ? Oses me dire que tu joue à la M16A3, et on te trouveras un compère (Oui oui pignouf, je parles bien de toi !  :p )\r\n\r\n\r\nAjoute ben_ftwc et bonne chance pour ton recrutement :)[/quote]\r\n\r\nJ''ai beaucoup jouer a la M16A3, Mais je change régulièrement d arme,en ce moment pour ma classe assaut je joue avec AEK-971. Lorsque j aurais débloquer tous les accessoires de l''AEK-971 je changerai d arme.et ainsi de suite pour les autres classe.\r\n'),
(375, 12, 30, 1364214087, 1, 'Ah, me voila rassuré ! ^^\r\n\r\n<HS> J''avais un hammster, tountoun, le prends pas mal hein ? </HS>'),
(377, 12, 30, 1364216995, 37, '[quote=ben_ftwc]Ah, me voila rassuré ! ^^\r\n\r\n<HS> J''avais un hammster, tountoun, le prends pas mal hein ? </HS>[/quote]\r\n\r\nNon tkt! mon surnom viens de mes neveux, ils n''arrivaient pas a dire "tonton" ,alors ils disaient "toutoun"\r\nc est tous bête! mais c''est resté!'),
(378, 12, 30, 1364218942, 1, 'Ah, pas mal ^^'),
(379, 7, 15, 1364230943, 5, '[COLOR=#FFA500]Donc le match se fera le dimanche 14/04 a 15h\r\nLe match se jouera en 2 partie:[/COLOR]\r\n\r\n[COLOR=#32CD32]//////////////////////////////////////[/COLOR]\r\n[COLOR=#FFA500]- Ruée / Pic de damavand [/COLOR] \r\n[COLOR=#32CD32]//////////////////////////////////////[/COLOR]\r\n[COLOR=#FFA500]- Conquête / Tempête de feu .\r\n- 100% des tickets .\r\n- Pas de fusils a pompe / C4 sur objectif \r\n- la partie ce fera en 2 manches comme la ruée[/COLOR] \r\n[COLOR=#32CD32]//////////////////////////////////////\r\n[/COLOR]\r\n\r\n[COLOR=#FFA500]Les membres présent sont:\r\n\r\n- Dartoch\r\n- Psy-4\r\n- Gp\r\n- Gyom\r\n- Bmdc\r\n- Jujube\r\n- Ben\r\n- Mika\r\n- \r\n- Pignouf\r\n- Lebetz\r\n-\r\n\r\n\r\n(la liste sera mise a jour suivant les réponses des membres)\r\n\r\nLa fiche d''escouade pour se match sera donné le plus rapidement possible, sachant bien évidement que plus vite les réponse de participation seront donné et plus vite la fiche sera faite...[/COLOR]'),
(380, 7, 15, 1364234337, 25, 'Moi je ne serais pas la dimanche j''aurais bien voulu mes j''ai un voyage linguistique scolaire notamment, toute le semaine je profite pour vous le dire :)'),
(381, 7, 15, 1364234339, 1, 'T''oublis personne ? :p'),
(382, 7, 15, 1364238318, 16, 'moi je peux être la.'),
(383, 7, 15, 1364239354, 5, 'ok Mika et donc je supose benco ;)\r\nBon voyage ato et profite en bien ^^ (mission pour toi: nous faire une photo avec le tag ou logo apax sur un monument ou site connu, par exemple si tu va en Angleterre, fait porté le logo Apax a un des garde de la reine ^^)'),
(384, 6, 31, 1364241215, 34, 'Se soir je serais sur le serveur APAX qui voudra me rejoindre ? :)\r\najouter moi TweeZy_D-Ace ou laisser un message :)'),
(385, 10, 32, 1364244229, 10, '[SIZE=4][FONT=times new roman][B][ALIGN=center][URL=http://www.casimages.com/img.php?i=130325094520818936.jpg][img]http://nsa33.casimages.com/img/2013/03/25/130325094520818936.jpg[/img][/URL][/ALIGN]\r\n\r\n[SIZE=4][ALIGN=center][COLOR=#C0C0C0]Escouade A[/COLOR] = [COLOR=#FF0000]Rouge[/COLOR]\r\n[COLOR=#C0C0C0]Escouade B[/COLOR] = [COLOR=#0000FF]Bleu[/COLOR]\r\n[COLOR=#C0C0C0]Escouade C[/COLOR] = [COLOR=#32CD32]Vert [/COLOR][/ALIGN][/SIZE]\r\n\r\n[COLOR=#FF0000]\r\nChar N°1 :\r\n- WAZA-PsY-4-x\r\n-BMCD64\r\n              \r\nChar N°2 :\r\n-Ben\r\n-Zea\r\n[/COLOR]\r\n[COLOR=#0000FF]Char N°3 :\r\n-Jujube\r\n-Dartoch\r\n    [/COLOR]              \r\n\r\n[COLOR=#0000FF]Avion de chasse N°1 : - LaPsYKosE\r\nAvion de chasse N°2 : - Pignouf30[/COLOR]\r\n\r\n\r\n[COLOR=#32CD32]\r\nViper : \r\n-WAZA-Gp_one\r\n-Gyom-88\r\n\r\nHélicoptère de reconnaissance ou jeep :\r\n- un sniper : poser un marquer laser sur colline du spaw / balise sur bravo :/ lebetz / \r\n- un assaut ou soutien ou ingénieur  : transporteur du snipeur : Mika [/COLOR] \r\n                                                                 \r\n[/B][/FONT][/SIZE]\r\n\r\n[I]Ceci est un premier échantillon , je ne sais pas encore qui participera réellement ou pas ...[/I]'),
(386, 10, 33, 1364244435, 10, '[SIZE=4][ALIGN=center][URL=http://www.casimages.com/img.php?i=130325094802431403.jpg][img]http://nsa34.casimages.com/img/2013/03/25/130325094802431403.jpg[/img][/URL][/ALIGN]\r\n\r\n\r\n[SIZE=5][ALIGN=center][COLOR=#C0C0C0]Escouade A[/COLOR] = [COLOR=#FF0000]Rouge[/COLOR]\r\n[COLOR=#C0C0C0]Escouade B[/COLOR] = [COLOR=#0000FF]Bleu[/COLOR]\r\n[COLOR=#C0C0C0]Escouade C[/COLOR] = [COLOR=#32CD32]Vert [/COLOR]\r\nEscouade D = [COLOR=#FF1493]Rose[/COLOR][/ALIGN][/SIZE]\r\n\r\n\r\n[B][ALIGN=center][SIZE=4]DÉFENSEUR [/SIZE][/ALIGN][/B]\r\n\r\n[COLOR=#0000FF]Hélicoptère : \r\n-\r\n-\r\n-[/COLOR]\r\n\r\n[COLOR=#FF0000]Équipe N°1 :\r\n-\r\n-\r\n-\r\n[/COLOR]\r\n[COLOR=#32CD32]Équipe N°2 :\r\n-\r\n-\r\n- \r\n[/COLOR]\r\n[COLOR=#FF1493]Équipe N°3 :\r\n-\r\n-\r\n-\r\n[/COLOR]\r\n\r\n[B][ALIGN=center][SIZE=4]ATTAQUANT [/SIZE][/ALIGN][/B]\r\n\r\n\r\n[COLOR=#FF0000]Équipe N°1 + Jeep :\r\n-Assaut : Pignouf\r\n-Ingénieur : Gp_one\r\n-Soutien : Mika\r\n-Sniper : lebetz ( Sniper ou autre arme + DAs + balise ) \r\n\r\n\r\n\r\n[/COLOR]\r\n\r\n[COLOR=#32CD32]Équipe N°2 :\r\n-Assaut : Lapsykose\r\n-Soutien : Zea\r\n-Ingénieur : Bmcd64\r\n-Sniper :Ben ( pas de snipe , juste M5k ect ...micro drone )\r\n[/COLOR]\r\n\r\n[SIZE=4][COLOR=#FF1493]Équipe N°3 :\r\n-Assaut : Gyom \r\n-Ingénieur : Dartoch \r\n-Soutien : juju\r\n-Snper : WAZA-PsY-4-x ( pas de snipe , juste M5k ect )+ balise au plus près \r\n[/COLOR][/SIZE]\r\n\r\n\r\n\r\n[/SIZE]\r\n\r\n\r\n'),
(387, 6, 31, 1364289220, 1, 'ben_ftwc si tu veux m''add ;)'),
(388, 10, 33, 1364289334, 1, 'Ben sniper, y''a pas un problème ? ^^\r\n\r\nEt, ce n''est que mon avis, ne serait-il pas plus "logique" d''essayer de mettre les binômes dans une même escouade ?'),
(389, 10, 32, 1364289444, 1, 'Là aussi, le sniper et moi, ça ne fait pas bon ménage :(\r\n\r\nà la rigueur, Betzouille ou ita, si vous pouviez m’entraîner à cet art majestueux qu''est la campe, je serait partant histoire de pas pourrir ton plan ( :) )'),
(390, 7, 15, 1364289518, 1, '[quote=Dartoch]si tu va en Angleterre, fait porté le logo Apax a un des garde de la reine ^^[/quote]\r\n\r\nMec, tu fais ça, t''es mon héro, et t''aura droit à ta photo dans la bannière du site !'),
(391, 10, 32, 1364293298, 4, 'En espérant, qu''ils n''ont pas de connaissance en aéroporter. J''ai peur qu''ils nous mettent une raclée dans les airs.'),
(392, 10, 32, 1364295278, 5, 'a mon avis c''est sur sa que tout va se joué...\r\n\r\nPour ma part Manu, j''avais fait cela, dit moi se que tu en pense:\r\n\r\n[URL=http://www.casimages.com/img.php?i=1303261149466025511013687.png][img]http://nsm08.casimages.com/img/2013/03/26//1303261149466025511013687.png[/img][/URL]\r\n\r\nDonc explication ^^\r\n\r\n-dans les chars 1 tireur en ingé pour répa et 1 assault pour réanimé son ingé s''il se fait tué\r\n-dans la jeep 1 snip et 1 soutien pour réalimenté le snip\r\n\r\nVoila, j''ai fait de cette façon car d’après les règles donné on a droit qu''a 3 classe de chaque me semble t''il ^^\r\nJ''avais aussi mis Ben en ingé car niveau snip... vaut mieux lebetz, ben serai capable de nous tiré dessus :p\r\nPar contre je ne sait pas se que vaut Mika en pilote tank donc gros point d’interrogation'),
(393, 10, 32, 1364295479, 2, 'il faut que ces topic sois en privée\r\n\r\npignouf c''est pour cela qu''il faut déployer un laser pour le marquage des véhicule aérien au cas ou est pas la supériorité aérienne\r\ndonc faut aussi avoir un ou 2 ingé en javelin'),
(394, 10, 32, 1364295605, 10, 'Ok remplace lord par Ben ,  il prendra le char sur les 6 tankistes : PsY Dartoche jujube zea Ben batiste !! Il faut que je sache les 3 meilleur pilote pour mettre les 3 autres en artilleur !!'),
(395, 10, 33, 1364295757, 5, 'alors pour celle la j''ai pas encore fini non plus ^^ mais je métrai lebetz en snipe et Ben en ingé\r\nApres pour le reste faut que je regarde plus sérieusement se que j''ai fait ^^\r\n'),
(396, 10, 33, 1364295827, 2, 'BEN le sniper n''est pas forcement utile ce qui l''est sais sa balise et son das je peu très bien m''en occupé je joue souvent éclaireur avec ump45 das et balise'),
(397, 10, 32, 1364295945, 1, 'Je suis pas mauvais en tireur.\r\n\r\n\r\nAu faite, petit trait d''humour.\r\n\r\n[COLOR=#87CEEB]A quoi sert un pistolet ? A pistoler ??\r\nUne mitraillette ? A mitrailler ??\r\nUn tank ? A T''ENCULER ?!? [/COLOR]'),
(398, 10, 32, 1364296315, 5, 'c''est déjà fait gp, j''ai vérifié ^^\r\n\r\nok Manu donc sa nous donne:\r\n\r\nblindé:\r\n\r\nPsy\r\nbmdc\r\nben\r\nzea\r\njujube\r\ndartoch\r\n\r\n\r\njeep:\r\n\r\nlebetz\r\nmika\r\n\r\n\r\nviper\r\n\r\ngp\r\ngyom\r\n\r\n\r\nchasseur\r\n\r\npignouf\r\n???\r\n\r\n\r\nPour le meilleur tankiste, ya qu''un moyen c''est le terrain et essayé différente possibilité meme si on sait déja que certain sont meilleur que d''autre, par exemple manu tu sera plus a ta place en pilote, perso je suis plus alaise en tireur/marqueur que pilote mais si ya besoin je m''y métrait '),
(399, 10, 33, 1364296561, 5, 'je peut le prendre aussi avec la M39 et lunette 3,4 ou kobra, tu sait que j''aime bien me fofillé partout Manu ;)'),
(400, 10, 33, 1364297722, 10, 'Tt de façon c un peu compliqué sur le papier !! Donc mercredi soir on bosseras juste Pic de damavand  dans les 2 sens et tte les bases , en hors classement sans adversaire !!Skype sera recommandé !! Je mets une annonce sur fb , qu''elle  heure désirez vous !?'),
(401, 10, 33, 1364298211, 1, 'Idéalement 21h pour moi'),
(402, 7, 34, 1364298333, 1, 'Yep, je vais poster ici régulièrement des captures ou autres statistiques. Je n''empèche personne de commenter, mais ne mettez pas "J''en ai rien à foutre", dans ses cas là, ne lisez pas le post ;)\r\n\r\nPour les autres, vous verrez ici le coté "communautée" du site, autant en termes de visites que de référencement (position sur google).\r\n\r\n'),
(403, 7, 34, 1364298496, 1, '[ALIGN=center][IMG]http://image.noelshack.com/fichiers/2013/13/1364298154-capture.png[/IMG][/ALIGN]\r\nDu coup, j''attaque avec cette petite capture de google, les termes "Meute Apax" nous font arriver en 4e position. Il faudrait que nous arrivions 1er !\r\n\r\n[ALIGN=center][IMG]http://image.noelshack.com/fichiers/2013/13/1364298422-capture.png[/IMG][/ALIGN]\r\nIdéalement, si on pouvait arriver à un ratio de 1 visite mini / heure (0.85 actuellement) ce serait koul !'),
(404, 10, 33, 1364298536, 5, 'pour ma part je peut pas le soir mais je serai co toute l''aprem donc je ferai du pic de damavand en boucle et si ya du monde de motivé vous aurez qua me rejoindre ;)'),
(405, 7, 34, 1364298796, 5, '1er sa va être dure vu que c''est le battlelog qui est 1er avec énormément de visite... pour les autres sa devrait le faire vu que c''est l''ancien site et un post de manu en 3eme\r\nPour augmenté les visites faudrait refaire une vague de pub pour la meute dans les différent site et forum de présentation de team, sa pourrai aidé ;)'),
(406, 7, 34, 1364303018, 1, 'Yep, et, idéalement, il faudrait poster le nom de la team comme ça :\r\n\r\n[URL=http://www.meute-apax.fr/]Meute Apax[/URL]\r\n\r\n(Le lien sur Meute Apax, et non le lien direct, pour un humain, c''est pareil, mais pas pour google ;)'),
(407, 10, 33, 1364303075, 1, 'Ah, c''est mieux. J''ui une tuerie avec le drone (Ita & betzouille peuvent attester ^^)'),
(408, 10, 32, 1364303749, 4, 'Je n''es pas eu encore de nouvelle de psyko !!!\r\n\r\nOui je sais que je serais plus pilote que gunner mais je parlais plus pour vous 5 , je ne sais pas lesquels choisir pour pilote le char ne sachant pas trop votre level en tant que pilote !!'),
(409, 10, 33, 1364303914, 4, 'pour la défense, simple, une équipe sur B, une équipe sur A, et une équipe qui part à l''assaut des ennemis.'),
(410, 10, 33, 1364305014, 1, 'Je suis pas totalement d''accord avec toi Pignouf30\r\n\r\nJe verrais plutôt 1 équipe en A, une sur B, comme toi, mais la 3e, sniper/reco et qui informe de loin, une info allant plus vite qu''un perso, je penses qu''on peut gagner en efficacité (après, l''entrainement nous confirmera je pense)'),
(411, 10, 32, 1364312421, 10, 'Le message de pignouf est iMcomprehensible car c moi qui l''est modifier sans faire expret , j''ai effacé son message et créer le mien iw´m sorry :)'),
(412, 7, 15, 1364315645, 25, 'Lol ca va être dur de les faire parler déjà ^^'),
(413, 7, 34, 1364323133, 5, 'ok c''est enregistré ^^ si j''ai un peut de temps demain je ferait un tour sur les sites les plus vue ;)\r\n'),
(414, 7, 15, 1364323291, 5, 'Pas besoin de les faire parlé, récupaire le logo de la meute, tu l''imprime et comme sa quand tu sera la-bas tu leur met dans les main (ils ont pas le droit de bougé ^^) et tu prend une photo :p'),
(415, 10, 32, 1364323484, 5, 't’inquiète Manu je me pose les mêmes question ^^ mais bon on vas bien y arrivé ;)\r\n\r\net pour le message, pignouf va gueulé !!!! lol'),
(416, 10, 33, 1364323767, 5, 'en fait vous avez raison tout les deux ^^ les "sentinelles" qui donne des info sur la position des joueurs adverse serait très utile et le fait de monter la défense au plus haut pour les ralentir est aussi un très bon point donc bon, a testé sur le terrain ^^'),
(417, 10, 33, 1364324657, 2, 'ne pas perdre de vu qu''il faut défendre le point le plus difficile (  a prendre pour l''ennemi )\r\npareil en attaque prendre le point le plus compliqué en premier '),
(418, 10, 33, 1364329984, 16, 'c''est pas un peu risqué, une escouade complète dans la jeep? Ils vont nous attendre sa va sentir le javelin à plein nez je pense.'),
(419, 10, 33, 1364331397, 2, 'le missile guidé de l''hélico aussi peu faire mal ! a la jeep !'),
(420, 10, 33, 1364332515, 16, 'bien vu j''avais oublié leur hélico'),
(421, 7, 14, 1364336684, 20, 'as de pompe ? Un pompe max par escouade ne va pas ? \r\nTu veux un règlement par match ??'),
(422, 7, 34, 1364338426, 1, ';)'),
(423, 7, 34, 1364384328, 1, 'Voilà, j''ai eu ma licence Analytics. Voici un exemple des stats que je peux recevoir (la capture représente ~ 1% du potentiel analytics !)\r\n\r\n[ALIGN=center][IMG]http://image.noelshack.com/fichiers/2013/13/1364384266-capture.png[/IMG][/ALIGN]'),
(424, 7, 34, 1364384737, 5, 'Pff connaisse même pas Vierzon chez Analytics..... lol'),
(425, 7, 15, 1364385874, 25, 'Ahah alors si je les voient pk pas :) le logo actuelle c''est quoi ? ^^'),
(426, 7, 14, 1364386491, 6, 'C''est une arme de kikooLOLILOL le fusil a pompe. ^^ \r\n'),
(427, 7, 15, 1364387471, 5, '[URL=http://www.casimages.com/img.php?i=1301141103466025510759849.png][img]http://nsm08.casimages.com/img/2013/01/14//1301141103466025510759849.png[/img][/URL]'),
(428, 7, 34, 1364389839, 1, 'Non, mais ils connaissent Coulommiers héhé'),
(429, 7, 34, 1364390697, 1, 'Analytics - Toutes les données du site Web Mon tableau de bord 20130224-20130326.pdf (115 KB)\r\nhttps://mega.co.nz/#!jQc02JbZ!NOjxlHQLpE1KPcOZbYZ0Mntmi8FJD32sHAZOJwnSFXE'),
(430, 8, 28, 1364395679, 2, 'toute première vidéo pour ceux qui l''on loupé \r\n\r\nhttp://www.jeuxvideo.com/extraits-videos-jeux/0004/00047946/battlefield-4-playstation-4-ps4-17-minutes-de-la-campagne-solo-fishing-in-baku-00111710.htm'),
(431, 7, 14, 1364396630, 1, 'Comme la RPG, m5k, M16A3, MTAR ...\r\n\r\nHEY, on s''fait un match qu''avec des nem, repair tool & drone ?'),
(432, 7, 14, 1364397828, 6, 'L''arme la plus cheate c''est le couteau , elle one shot :p'),
(433, 7, 14, 1364399291, 1, '* que dans le dos !'),
(434, 7, 14, 1364410188, 2, 'le couteau !!! on vois que tu n''a pas connus bc2 on faisais des carnage avec , dans bf3 sa tiens plus de la chance '),
(435, 7, 15, 1364418440, 5, 'Bon étant donné que Zea nous a quitté, nous alons avoir besoin d''une personne en plus, il nous manque donc encore deux membres pour se match'),
(436, 7, 14, 1364419881, 20, 'Vous allez vous mettre d''accord ou pas ? :-P\r\n1 pompe par escouade ou pas du tout de pompe ? '),
(437, 7, 14, 1364450780, 2, 'on a 2 règle pas de pompe et pas de c4 sur objectif , rien de choquant finalement '),
(438, 7, 15, 1364463761, 1, 'Main gauche et Main droite, ça t''irais ?\r\n\r\nMais sérieusement, sur +40 membres, y''a pas moyen de trouver 12 joueurs ><'),
(439, 7, 14, 1364464072, 1, 'C''est vrai que le pompe abusif fait rager, mais 1 pompe sur le match, je trouves pas ça "excessif".'),
(440, 7, 34, 1364479475, 1, 'Les dernieres stats viennent de tomber !\r\n\r\n([URL=http://sd-27781.dedibox.fr/owncloud/public.php?service=files&t=4b8564a4696b24bf787c4300c034b5a4]26-03-2013[/URL])\r\n([URL=http://sd-27781.dedibox.fr/owncloud/public.php?service=files&t=7bb6cb461fa2369de447164bb21afa77]27-03-2013[/URL])'),
(441, 1, 36, 1364483646, 34, 'Bonjour a toute la meute me voici devant vous je me présente je suis nouveau dans la meute je m''appelle Alexis ou TweeZy je joue souvent au snipe après il faut que je récupère mon niveau car je joue souvent a Black Ops II et Medal of Honor donc j''ai pu l''habitude de Battlefield 3 mais je suis prêt a m’entraîner avec vous pour retrouver mon niveau ;)\r\n[IMG]http://sphotos-e.ak.fbcdn.net/hphotos-ak-frc1/901213_409869165776006_1279762367_o.jpg[/IMG] '),
(442, 10, 33, 1364487013, 16, 'vue qu''hier c''est passé à la trappe, vous comptez prévoir quand un entrainement?'),
(443, 7, 14, 1364495987, 6, 'Sa ferai 4pompe dans chaque equipe .\r\n'),
(444, 10, 33, 1364545714, 10, 'On A Bossé la l''attaque hier , je sais pas si pignouf aura le temps de bosser le defense ce week sinon mardi soir !!'),
(445, 12, 37, 1364546637, 43, 'Pseudo, PSN et Battlelog  : Simrreuhh\r\nPrénom : Simon\r\nAge : 22 ans\r\nPSN : Simrreuhh\r\n\r\nBonjour à tous, j''ai rencontré la TEAM APAX par le biais de bmdc64 que je connais irl et qui m''a proposé de faire quelques parties avec des joueurs de la TEAM, certains m''ont donc déjà croisé ces derniers temps. J''attendais d''avoir un niveau un peu correct pour postuler. J''aimerai faire partie de la meute pour développer mon teamplay et profiter de l''ambiance animée des chats vocaux ( non mais allo ).\r\n\r\nSinon pour parler un peu de moi, quand je ne joue pas à BF3, c''est que je bosse ( je suis graphiste ), que je regarde des séries ou que je joue à autre chose. \r\n\r\nJe vous remercie d''avance de prendre le temps de me répondre !'),
(446, 12, 37, 1364547523, 5, 'Salut Simrreuhh et bienvenu sur notre site ;)\r\nSi tu veut faire quelque partie tu peut me rajouté (Dartoch)\r\nA tres vite in game ;)'),
(447, 7, 14, 1364549005, 20, '[quote=ApaX_LeLe]Sa ferai 4pompe dans chaque equipe .\r\n[/quote]\r\n\r\nNon, 3 pompes par équipes ( 3 escouades de 4 ) et puis encore c''est 3 pompes maximum, ça paraît évident que dans tempête de feu nous ne l''utiliserons pas. '),
(448, 12, 37, 1364549643, 1, 'J''avais cru lire "merci de perdre le temps de me lire" ... Putain de vendredi hein ? :p\r\n\r\n\r\nAdd ben_ftwc et, tu pourrais nous filler quelques unes de tes créas ? (anciennement du métier, dev à présent)'),
(449, 7, 14, 1364549711, 1, '[quote=darkyoyo19-1]ça paraît évident que dans tempête de feu nous ne l''utiliserons pas. [/quote]\r\n\r\nC''est mal me connaître moi et mon sniper-Spas12 ... ;)'),
(450, 10, 33, 1364549752, 1, 'Bien l''entrainement hier. Reste la déf, mais ça devrait le faire :D'),
(451, 10, 33, 1364553161, 5, 'moi j''ai fait que le début, fallait que je me deco.. va falloir me faire un topo ;)\r\n'),
(452, 12, 37, 1364556754, 43, 'J''ai posté quelques  travaux dans la [URL=http://88.191.146.2/site/index.php?site=gallery&galleryID=2]galerie[/URL], n''hésitez pas à jeter un coup d''oeil !'),
(453, 12, 37, 1364559350, 1, 'Pas mal, j''aime beaucoup. C''est de la créa "pure" ou du brush majoritaire ?'),
(454, 12, 37, 1364559833, 43, 'Merci ! C''est majoritairement de la photomanipulation, avec très peu de brush.'),
(455, 7, 14, 1364567146, 6, 'Je connais pas mes tables de multiplication SORRY.');
INSERT INTO `forum_posts` (`postID`, `boardID`, `topicID`, `date`, `poster`, `message`) VALUES
(456, 12, 38, 1364567799, 21, 'Pseudo: lawrent33\r\nPrénom: laurent\r\nAge: 41\r\nPsn: lawrent33\r\nBattlelog: lawrent3331\r\n\r\nComment avez vous connu la meute?: en jouant avec le frangins Julien et Thierry; Pignouf; Mika44500; Papa et les autres...\r\n\r\nPourquoi vouloir rejoindre nos rand?: Parce que Julien me la proposé; qu''on s''entend et rigole bien et que c''est plus sympa de tracer en meute\r\n\r\nParlez nous un peut de vous: J''habite à Toulouse, je travaille à Airbus, j''aime voyager, prendre des photos et les repas entre amis.\r\n'),
(457, 12, 38, 1364568044, 1, 'Je suis désolé, on à assez de membres qui ne prennent pas leurs balles pour t''accepter :p\r\n\r\nGros +1 ;)'),
(458, 12, 37, 1364584557, 8, 'obligé de mettre un +++++1 pour ce monsieur je peux m''occuper du recrutement si vous voulez pour une histoire de neutralité... '),
(459, 10, 33, 1364588824, 16, 'sauf que c''était pas marqué que vous avez prévu un entraînement'),
(460, 12, 38, 1364635661, 5, 'Salut Lawrent et bienvenu sur notre site\r\nBon courage a toi pour ta demande de recrutement ;)'),
(461, 10, 33, 1364635790, 5, 'C''est vrai que c''était juste marqué sur FB... Mais bon c''était pour les déplacement donc faudra voir avec les membres qui était présent pour récupéré les infos\r\nDe plus va vraiment faloir finalisé les scad pour bossé corectement cette semaine ;)'),
(462, 10, 33, 1364637914, 16, 'l''idéal serait de privilégier le fofo avant FB je pense.'),
(463, 10, 33, 1364643225, 2, 'tant qu''il continueront les 2 facebook et le forum on finis par loupé des info , perso je ne peut pas faire d''entrainement le soir '),
(464, 10, 33, 1364647549, 16, 'on loupe des infos et pourra nous pénaliser lors des prochains match à venir'),
(465, 10, 33, 1364648791, 1, 'Je vous propose autre chose. Ne mettez plus rien sur FB, je vais ajouter sur le fofo un bouton "Partager sur Facebook" qui permettra de mettre en live les infos sur le site et le facebook, comme ça, toutes les infos sont partout, et "idéalement", les commentaires arriverais sur le site ;)'),
(466, 10, 32, 1364796934, 5, 'Bon alors pour le meilleur tankiste manu, je dirais Bmdc, on a fait quelque partie hier avec les tank c''est c''est lui qui s''en sortait le mieu meme si le fait de m''avoir en gunneur la beaucoup aidé :p\r\n\r\nDonc je dirait:\r\n\r\n[COLOR=#FFA500]pilote:[/COLOR]\r\n[COLOR=#32CD32]Waza-psy-4\r\nBmdc\r\nJujube[/COLOR]\r\n\r\n[COLOR=#FF4500]gunneur/repa:[/COLOR]\r\n[COLOR=#32CD32]Ben \r\nDartoch\r\n??????[/COLOR]\r\n\r\nReste plus qu''a trouvé un 3eme gunneur vu que Zea n''en est plus, peut etre dans les ptit nouveau?'),
(467, 12, 38, 1364797266, 5, 'Alors on a fait pas mal de partie hier avec lawrent et donc c''est un +1 pour ma part ;)'),
(468, 12, 37, 1364797400, 5, 'Vu les partie que l''on a fait ensemble hier je met un +1 pour Sim ;) en plus cela nous fera un graphiste de plus (on va commencé a être nombreux ^^ )'),
(469, 12, 30, 1364797494, 5, 'Membre accepté donc je ferme ;)'),
(471, 10, 32, 1364813035, 8, 'j''avoue que j''avais un tres bon gunner/ réparateur avec moi c''est pour ca que j''ai donné l''impression de "maitriser" mais bon écoute est-ce le choix définitif? comme ça je m''entrainerai en tank toute la semaine!\r\nen tout cas avec dartouche c''est une affaire qui roule.'),
(472, 10, 32, 1364813318, 1, 'pige pu rien l''ben ... a l''entrainement, je devait droner en sniper, et là, gunner et répa oO\r\n\r\n[COLOR=#87CEEB][SIZE=5]NARMOL[/SIZE][/COLOR] !'),
(473, 12, 38, 1364813568, 1, '[quote=Dartoch]Alors on a fait pas mal de partie hier avec lawrent et donc c''est un +1 pour ma part ;)[/quote]\r\nidem, mais, 1 message en 20 jours sur le site ... Donc [COLOR=#FF0000]-1[/COLOR], mais pas définitif !'),
(474, 12, 38, 1364816196, 21, 'OK Ben, je vais essayé de forcer un peu ma nature, mais il est vrai que je suis bien plus actif sur le champ de bataille que sur le forum...\r\n\r\nPour tous ceux qui ne me connaissent pas encore, je ne suis pas du style flamboyant à 50 ou 100 kills par match...\r\nJe suis plutôt un besogneux travailleur de l''ombre qui joue beaucoup en soutien et assaut (version toubib), avant tout pour l''équipe et les objectifs car seule la victoire est belle; ce qui ce fait bien souvent au détriment de mes stats...\r\n\r\nA bientôt sur le battlefield\r\n\r\nLaurent\r\n\r\n'),
(475, 10, 32, 1364816920, 5, 'Psy donnera l’escouade définitive Bm mais ya de forte chance que cela soit comme sa ;)\r\n\r\nBenco, tu est en snipe sur pic en ruée et en blindé pour tempête de feu ^^'),
(476, 12, 38, 1364817055, 25, 'Bienvenue sur le forum et bonne chance pour ta demande j''espere te voir bientot sur le champ :)\r\nTu pourras gouter a la furtivité et la puissance de ma M16 ^^'),
(477, 12, 37, 1364817210, 25, 'Salut Sim je vois qu''ont a a faire a un graphiste en herbe ( sa ce dis? ^^) apparament tu a l''air de bien t''entendre avec les membres. J''attend de voir sur le terrain bonne chance :)'),
(478, 9, 29, 1364817427, 25, 'Présent :)'),
(479, 10, 32, 1364834829, 8, 'ok entrainement intensif en tank pour moi alors'),
(480, 10, 32, 1364835738, 4, 'Ben, quand on a fait l''entrainement c''était sur la map pic de damavan, là c''est opération tempête de feu, on ne joueras pas la meme chose.'),
(481, 7, 14, 1364851275, 20, 'Alors, pas de pompe du tout ? Ce serait pratique dans damavand en ruée; .6°'),
(482, 7, 14, 1364896497, 34, 'Yo(play) la meute je sais que je suis nouveau parmi vous mais est-ce-que j''ai ma place pour le match ? :) j''ai envie de faire mes preuve je veux en découdre au cœur d''un bon gros match :D'),
(483, 7, 15, 1364897519, 34, 'Moi je suis d''accord pour y participer :D après avoir ou me placer je suis plus assaut: KH2002 , P90 , AK-74M , F2000 , PP-19 ou Snipe: SV-98 , M40A5 , M98B , L96 meme si je suis plus en assaut pour rea et soins :D'),
(484, 7, 34, 1364914590, 1, 'Petit "temps mort" ce weekend ? \r\n[URL=http://sd-27781.dedibox.fr/owncloud/public.php?service=files&t=75a4e86e43aba895e6f80203dbf152b7]02-04-2013[/URL]'),
(485, 7, 34, 1364914686, 1, 'Ceux qui souhaitent avoir un accès en "temps réels" de ses statistiques, demandez le moi [URL=http://www.meute-apax.fr/site/index.php?site=messenger&action=touser&touser=1]par message privé[/URL] en pensant à bien ajouter votre adresse mail :)'),
(486, 7, 15, 1364922759, 5, 'C''est bien noté Tweezy ;)\r\n\r\n[COLOR=#FFA500]Les membres présent sont:\r\n\r\n- Dartoch\r\n- Psy-4\r\n- Gp\r\n- Gyom\r\n- Bmdc\r\n- Jujube\r\n- Ben\r\n- Mika \r\n- Pignouf\r\n- Lebetz\r\n- Tweezy\r\n-\r\n\r\nremplacent:\r\n_ Aulyro\r\n- \r\n\r\n(la liste sera mise a jour suivant les réponses des membres)\r\n\r\n[/COLOR]\r\n\r\nIl nous manque encore 1 membre et le match se joue dans 12 jours donc on se motive les filles !!!! Pensez a ceux qui se creuse la tête pour les escouades ;)'),
(487, 11, 39, 1364923443, 5, '[SIZE=4]Vous souhaitez faire un partenariat avec la Meute Apax ?[/SIZE]\r\n\r\nPour que toute demande soit étudiez avec attention, nous vous demandons de bien indiqué les renseignement si dessous:\r\n\r\n- Présentation de la personne postant la demande et de sont groupe s''il y a\r\n- Comment nous avez vous connu ?\r\n- Quelle est votre projet ?\r\n- Qu’attendez vous de nous ?\r\n- Que proposez vous ?\r\n\r\nSe ne sont que quelques question, mais répondu correctement cela facilitera votre demande ;)\r\n'),
(488, 12, 38, 1364932704, 26, 'Un Felicetti de plus ? ... pas de soucie ! c''est avec vous que je me tape les meilleurs fou rires ! :D'),
(489, 7, 15, 1364941379, 11, 'Salut a tous alors voila je ne sais pas si je serait présent pour ce match je préfére me mettre en remplacant au cas ou. ( Je préfére laisser ma place a des joueurs meilleur que moi en ce moment le petit loup que je suis est parti chasser un peu ailleur affronter ou enseigner l''art de la chasse a de jeune belette de mon lycée donc voila tout mais c''est pas pour autan que le jeune loup n''a pas besoin de se faire défoncer par les plus expérimenté, les plus rageux, les plus vieux, les plus fifou... Enfin voila je me met en remplacant (encore une fois je n''est dit que de la merde XD et je sens venir la réponse de certain tes bizarre XD ou il est chelou ton commentaire^^)'),
(491, 7, 15, 1364985516, 1, 'Boarf, quand tu vois ce que d''autres mettent, le tient n''est pas si "bizarre" que ça :p\r\n\r\nEssaye de valider ta présence qu''on puisse attaquer les entraînements à 12 !'),
(492, 10, 32, 1364986458, 5, '\r\n[COLOR=#FF0000]\r\nChar N°1 :\r\n- WAZA-PsY-4-x\r\n- Ben\r\n              \r\nChar N°2 :\r\n- BMCD64\r\n- Dartoch\r\n[/COLOR]\r\n[COLOR=#0000FF]Char N°3 :\r\n- Jujube\r\n- Tweezy\r\n    [/COLOR]              \r\n\r\n[COLOR=#0000FF]Avion de chasse N°1 : - (LaPsYKosE)\r\nAvion de chasse N°2 : - Pignouf30[/COLOR]\r\n\r\n\r\n[COLOR=#32CD32]\r\nViper : \r\n-WAZA-Gp_one\r\n-Gyom-88\r\n\r\nHélicoptère de reconnaissance ou jeep :\r\n- un sniper : poser un marquer laser sur colline du spaw / balise sur bravo :/ lebetz / \r\n- un assaut ou soutien ou ingénieur  : transporteur du snipeur : Mika [/COLOR] \r\n     \r\n\r\n[COLOR=#32CD32]En gros cela devrait donné cela si je ne me trompe ;) [/COLOR]'),
(493, 7, 14, 1364986873, 22, 'si il manque des joueurs je suis présent.'),
(494, 10, 32, 1364991166, 1, 'MANU, COUCHES TOI, J''ALLUMES !!!'),
(495, 7, 14, 1364991201, 1, 'Reveur, tu peux aller le mettre sur le topic interne ?'),
(496, 7, 15, 1364991270, 1, 'Apparement, y''aurais Mister_Reveur de dispo toch'),
(497, 10, 32, 1364991492, 16, 'Pour transporter mon V.I.P, je prend plutôt la jeep, moins repérable (plus difficilement) je pense.'),
(498, 10, 32, 1364994817, 1, 'C''est sur, au moins, le javelin est silencieux ... :p\r\n\r\n- AAAAAAAAHHHH jeep !!!\r\n- Apu jeep :D\r\n\r\n'),
(499, 11, 40, 1364999632, 50, 'Présentation : Bonjours à vous, je me présente, je m''appel romain, 18 ans, sous le pseudo de XxCrAzYyKiLLeRxX (désolé pour le pseudo un peux compliquer) mon Skype : roro62000 Facebook de game : Crazller Callof. Mon Label sous le nom de la Atylaa Games seras présenté ci-dessous !\r\n\r\nConnaissance : Je vous ais connues grâce à Tweezy un membres de la meute ! Et Dartoch avec le deuxième membres à qui j''ai parler de nos projets ! \r\n\r\n\r\nPrésentation du projet : Parlons maintenant de notre projet ! Tout d''abord, mon équipe et moi sommes en train de monté notre label. Nous recherchons avant tout, des teams pour nous représenté sur différents jeux. \r\nNous représenté c''est à dire : Si vous faites des vidéos envoyé les nous avec votre intro personnel, nous y rajouterons l''intro de notre label, et nous les publierons sur notre chaine youtube, le Facebook du label (twitter inclus) et notre site (voir peut être plus loin si création d''une WebTV) et nous représenté également sur votre page Facebook et votre site (ce seras également fait de notre côté). Je voudrais parvenir enfin aux projet de notre label !\r\nDans notre label nous voulons prochainement créez notre propre jeu en ligne, organiser des tournois sur certains jeux (avec des lots à gagné), peut être des LOBBY sur Call Of Duty, webradio, notre propres marque... Etc.. \r\nNous avons déjà une production pour des intros faisant partis du label sous le nom :  "WeeZy/GuiiZi Prod".\r\n\r\nCe que nous attendons de vous : Nous attendons de vous, des vidéos, un partenariat solide, et que vous rentriez dans notre label. Je chercherais aussi un grapheur pour la création d''un logo pour notre label ! \r\n\r\nCe que je vous propose : Je vous propose de devenir les représentant principal en générale, et directe du label sur BattleField 3 ! \r\n\r\nContacter moi par message pour plus d''informations soit sur Skype ou par mon pseudo sur ps3 !\r\nTout mes remerciement et à très vite je l''espère !\r\n\r\n                                                                                                     Romain. \r\n\r\n'),
(500, 7, 14, 1365001177, 4, 'Alors on utilise le pompe ou pas pompe ?'),
(501, 10, 32, 1365002805, 16, 'apres avec l''hélico sa peut aller très vite aussi stinger, hélico attaque ennemi, chasseur.'),
(502, 10, 32, 1365008603, 5, 'je pense aussi que la jeep est plus adapté sur cette map'),
(503, 7, 15, 1365009063, 5, '[ALIGN=center][COLOR=#87CEEB]Les membres présent sont:\r\n[/COLOR]\r\n[COLOR=#DEB887][SIZE=4]- Dartoch\r\n- Psy-4\r\n- Gp\r\n- Gyom\r\n- Bmdc\r\n- Jujube\r\n- Ben\r\n- Mika \r\n- Pignouf\r\n- Lebetz\r\n- Tweezy\r\n- Mister_Reveur\r\n\r\nremplacent:\r\n_ Aulyro\r\n- Ankoleloup\r\n- Ryukio\r\n- Noskill4ever[/SIZE] [/COLOR]\r\n[/ALIGN]\r\n\r\n(Si d''autre membres sont intéréssé, il reste encore des place dans les remplacents, donc n''ésité pas ;) )\r\n\r\n[COLOR=#87CEEB][SIZE=4]Psy-4 donnera la feuille de match définitive rapidement mais vous pouvez déjà tous vous entraînez sur les maps qui seront joué pour le 12VS12, en sachant que la plupart des place ne changerons pas sur se qui a déjà été donné[/SIZE][/COLOR]'),
(504, 11, 40, 1365010659, 5, 'Salut Crazy et bienvenu sur notre site ;)\r\n\r\nDonc comme je te l''ai dit, pour ma part je trouve que cela peut être une bonne idée, cela ne coute rien d''essayé en tout cas ^^\r\n\r\nPar contre où en est tu au niveau de l’installation de tes structures? (site, chaîne youtube, page FB,...) ? Sont elles déjà créé au es en cour? Si elles sont créé, peut tu nous fournir les liens que l''on regarde un peut?\r\n\r\nPour se que l''on peut faire, comme je te l''ai dit, c''est de faire un "logo-lien" vers ta page (youtube ou autre) pour que ceux qui vienne sur notre site puisse se rendre sur ton adresse, ainsi que de fournir des vidéos (voir déjà avec celle que l''on a déjà)\r\n\r\nPour se que l''on attend, sait d''avoir un lien vers notre site sur tes différentes page (que cela nous apporte des visites aussi de notre coté ) ainsi que de la pub pour notre meute sur les vidéos que tu peut faire \r\n\r\nDe plus, connaissant les loups de la meute, je sait que plusieurs d''entre nous (dont moi) attende avec impatience la sortie de la Ps4 et de Battlefield 4, et donc on sera surement plusieurs à les prendres dès leur sortie, se qui pourrait te donné des vidéos rapidement sur se support et se jeux ;) Du coup on te demandera l''exclusivité de battlefield 4 pour la Meute\r\n\r\nJe fini en te précisent que cela est se que moi je propose, donc je ne ferait rien tend que le grand chef n''aura pas donné sont accord !!!\r\n\r\nSur ceux je te souhaite une bonne fin de journée ;)'),
(505, 7, 14, 1365026568, 20, 'Ce n''est pas dans notre habitude d''interdire des armes dans un jeu sensé être réaliste. On doit pouvoir accéder  à une grande variété d''armes sans exceptions. Elles sont équilibrées. Vous trouvez que le pompe fait mal dans BF... Il devrait être doublé en puissance pour se rapproché de la réalité. Faites comme vous voulez mais interdire des armes dans un jeu qui nous propose une grande variété est un peu dommage...'),
(506, 7, 14, 1365055902, 2, 'pour le pompe je trouve qu''il est suffisamment puissant , par contre il est trop précis même a moyenne portée , en face a face tu n''a aucune chance pratiquement'),
(507, 10, 32, 1365056636, 2, 'faudrait peut être penser a utiliser l''hélico de reconnaissance il est résistant et puissant 2 joueurs suffise un pilote et un mitrailleur réparateur il peut être utile sur cette map faudrai s''entrainer avec '),
(508, 11, 40, 1365072570, 1, 'Ce n''est que mon avis hein, mais entre nous, un "label" fait plus penser à une maison de disques qu''a une prod (movie making = production, pas label).\r\n\r\nHistoire de s''impliquer un peu dans ton projet, pourrais tu nous dire là où VOUS êtes impliqués (liens YT, site web, book, ...) car, monter un partenariat, c''est pas une mauvaise idée, mais si nous ne sommes là que pour apporter des vues, je ne vois pas l’intérêt.\r\n\r\nMaintenant, si vous avez besoin d''aide, de conseil ou d''expert pour votre projet, pourquoi pas. \r\n\r\nLe premier point d’intéressement de la team serait sa propre progression, si on peut participer à la progression de d''autres teams/sponsors, cela reste dans cette optique, par contre, de créer votre image, non. \r\n\r\n[URL=https://twitter.com/AtylaaGames]Twitter [/URL]: 2 followers (vous surement ?)\r\n[URL=http://www.facebook.com/pages/Atylaa-Games/499218053471997499218053471997]Facebook [/URL]: 10 likers\r\nUn site web ? [URL]http://atylaagames.wix.com/atylaa-games[/URL]  (Page 404, site NOT FOUND), petite recherche, et on tombe sur [URL]http://atylaagames.wix.com/atylaa-games-[/URL], page en maintenance, aucun contenu hormis des fleurs et des colonnes ;) bref, pas très très sérieux pour se permettre de démarcher\r\n\r\nPersonnellement, je suis pas franchement chauds patate pour ce projet pour le moment, tant que la structure n''est pas rodée, cela ne sert à rien de s''investir, aujourd''hui, votre projet est bien, mais d''ici demain, comme rien n''est en place, qu''est-ce qui nous dis que vous n''abandonnerez pas votre projet ?\r\n\r\nEntre nous, et cela à titre informatif, vous n''obtiendrez probablement pas de partenariat / sponsoring avec autant d''éléments "contre" vous, en sachant que ça ne m''aura coûté que 5mn sur un moteur de recherche ^^'''),
(509, 6, 41, 1365075219, 1, 'Yep, voici quelques unes de mes créas, avant de passer du coté obscur des développeurs :)\r\n\r\n[IMG]http://image.noelshack.com/fichiers/2013/14/1365074580-23991-1332240359212-3440166-n.jpg[/IMG]\r\nModé 3D de fin de stage\r\n\r\n[IMG]http://image.noelshack.com/fichiers/2013/14/1365074634-243263-1896531346134-778674-o.jpg[/IMG]\r\nModé 3D de 2 ''SpeedCopter'', petits copters contrôlables en bluetooth\r\n\r\n[IMG]http://image.noelshack.com/fichiers/2013/14/1365074634-322670-2541405467584-108893597-o.jpg[/IMG]\r\nFausse publicité pour Bouygues Tel, concours (3e)\r\n\r\n[IMG]http://image.noelshack.com/fichiers/2013/14/1365074634-379809-2558759341420-1054167700-n.jpg[/IMG]\r\nFan art Assassin''s Creed (Héhé, pas mal Ezio à Paris ? :D)\r\n\r\n[IMG]http://image.noelshack.com/fichiers/2013/14/1365074634-387925-2558760621452-1005570672-n.jpg[/IMG]\r\nVoici "Mme Ben", travail d''expositions et de retouche PSD\r\n\r\n// Projet 3D //\r\n\r\n[IMG]http://image.noelshack.com/fichiers/2013/14/1365074633-55793-1555048009264-212828-o.jpg[/IMG]\r\n[IMG]http://image.noelshack.com/fichiers/2013/14/1365074633-55683-1555047529252-1670455-o.jpg[/IMG]\r\n[IMG]http://image.noelshack.com/fichiers/2013/14/1365074633-53552-1555048929287-5592641-o.jpg[/IMG]\r\n[IMG]http://image.noelshack.com/fichiers/2013/14/1365074633-57000-1555050409324-2176221-o.jpg[/IMG]\r\n\r\n[IMG]http://image.noelshack.com/fichiers/2013/14/1365074635-578244-3528416542244-302579700-n.jpg[/IMG]\r\nWebdesign d''essai\r\n\r\n\r\nVoila, qu''on ne me dises plus "Tu parle sans avoir de connaissances en graphisme", tu te reconnaîtra, mais, tu sais maintenant que je parles en connaissance de cause ;)\r\n\r\nLes autres, juste, profitez :p\r\n\r\n[toggle=Toutes les miniatures, au cas où j''en aurais zappées dans le post][url=http://www.noelshack.com/2013-14-1365074580-23991-1332240359212-3440166-n.jpg][img]http://image.noelshack.com/minis/2013/14/1365074580-23991-1332240359212-3440166-n.png[/img][/url][url=http://www.noelshack.com/2013-14-1365074633-53552-1555048929287-5592641-o.jpg][img]http://image.noelshack.com/minis/2013/14/1365074633-53552-1555048929287-5592641-o.png[/img][/url][url=http://www.noelshack.com/2013-14-1365074633-55683-1555047529252-1670455-o.jpg][img]http://image.noelshack.com/minis/2013/14/1365074633-55683-1555047529252-1670455-o.png[/img][/url][url=http://www.noelshack.com/2013-14-1365074633-55793-1555048009264-212828-o.jpg][img]http://image.noelshack.com/minis/2013/14/1365074633-55793-1555048009264-212828-o.png[/img][/url][url=http://www.noelshack.com/2013-14-1365074633-57000-1555050409324-2176221-o.jpg][img]http://image.noelshack.com/minis/2013/14/1365074633-57000-1555050409324-2176221-o.png[/img][/url][url=http://www.noelshack.com/2013-14-1365074634-57450-1556265559702-65277-o.jpg][img]http://image.noelshack.com/minis/2013/14/1365074634-57450-1556265559702-65277-o.png[/img][/url][url=http://www.noelshack.com/2013-14-1365074634-243263-1896531346134-778674-o.jpg][img]http://image.noelshack.com/minis/2013/14/1365074634-243263-1896531346134-778674-o.png[/img][/url][url=http://www.noelshack.com/2013-14-1365074634-322670-2541405467584-108893597-o.jpg][img]http://image.noelshack.com/minis/2013/14/1365074634-322670-2541405467584-108893597-o.png[/img][/url][url=http://www.noelshack.com/2013-14-1365074634-379809-2558759341420-1054167700-n.jpg][img]http://image.noelshack.com/minis/2013/14/1365074634-379809-2558759341420-1054167700-n.png[/img][/url][url=http://www.noelshack.com/2013-14-1365074634-387925-2558760621452-1005570672-n.jpg][img]http://image.noelshack.com/minis/2013/14/1365074634-387925-2558760621452-1005570672-n.png[/img][/url][url=http://www.noelshack.com/2013-14-1365074635-528975-3940950735341-13730698-n.jpg][img]http://image.noelshack.com/minis/2013/14/1365074635-528975-3940950735341-13730698-n.png[/img][/url][url=http://www.noelshack.com/2013-14-1365074635-578244-3528416542244-302579700-n.jpg][img]http://image.noelshack.com/minis/2013/14/1365074635-578244-3528416542244-302579700-n.png[/img][/url][/toggle]'),
(510, 6, 41, 1365080395, 1, 'Suite des créas, projet d''étude \r\n\r\n[url=http://www.noelshack.com/2013-14-1365080357-171218-1706718720937-1448130-o.jpg][img]http://image.noelshack.com/minis/2013/14/1365080357-171218-1706718720937-1448130-o.png[/img][/url]\r\n[url=http://www.noelshack.com/2013-14-1365080357-171218-1706718760938-8146882-o.jpg][img]http://image.noelshack.com/minis/2013/14/1365080357-171218-1706718760938-8146882-o.png[/img][/url]\r\n[url=http://www.noelshack.com/2013-14-1365080357-171218-1706718800939-317625-o.jpg][img]http://image.noelshack.com/minis/2013/14/1365080357-171218-1706718800939-317625-o.png[/img][/url]\r\n[url=http://www.noelshack.com/2013-14-1365080357-171218-1706718840940-3272503-o.jpg][img]http://image.noelshack.com/minis/2013/14/1365080357-171218-1706718840940-3272503-o.png[/img][/url]\r\n[url=http://www.noelshack.com/2013-14-1365080357-171218-1706718880941-7707206-o.jpg][img]http://image.noelshack.com/minis/2013/14/1365080357-171218-1706718880941-7707206-o.png[/img][/url]\r\n[url=http://www.noelshack.com/2013-14-1365080357-171845-1706720680986-3414906-o.jpg][img]http://image.noelshack.com/minis/2013/14/1365080357-171845-1706720680986-3414906-o.png[/img][/url]\r\n'),
(511, 11, 40, 1365085660, 50, 'Salut a toi Ben, alors je voulais t''annoncer que pour le moment tout est en "construction" rien est encore vraiment lancer car j''attend de trouver et de créer un logo pour notre production. Je voudrais te dire également qu''abandonner n''est pas dans mon but ! Je ne me lancerais pas dans de tels projets si ce n''est que pour abandonner au bout de 2 mois ! Loin de la ! Je suis quelqu''un de très sérieux ! Tout d''abord nous recherchons de notre côté des partenariat solide et des personnes sérieuses pour nous représenté (team sur différents jeux, etc..) avant de lancer tout sa. Nous n''en recherchons que peux pour le moment pour voir les résultats du  tout ! Tu as trouver le site, le Facebook et le twitter de la Atylaa Games,  tu as indiquer le nombre de personnes qui aimes notre pages, et bien pour te dire ces 13 personnes ne sont que des personnes participant a nos projets, c''est pour cela qu''ils ont aimer, Oui il n''y a que 13 personne qui aime notre page Facebook, mais il faut te dire également que tout sa n''est pas encore lancer mais en constructions ! Car il faut ce dire que se lancer dans de tels projets sans rien, sans être organiser, c''est comme si on se jeté d''un avion sans parachutes ! Pour te dire avant de lancer le projet, je vais organiser quelques petits évents sur différents jeux pour attiré l''intension des personnes et rapporter un peux d''argents pour financer le tout ! (c''est à dire faire de la pub grâce à Facebook etc) Bref ! Tout sa pour vous dire que vous avez du temps pour y réfléchir, et d''en parler entre vous, car ce n''est pas encore totalement lancer ! Il y a encore des choses à amélioré dont le site !\r\nEncore merci à vous de m''avoir répondu aussi vite ! \r\n                                                                                             Romain '),
(512, 11, 40, 1365087950, 1, '[quote=XxCrAzYyKiLLeRxX] Il y a encore des choses à amélioré dont le site ![/quote]\r\n\r\nNe le prends pas mal, mais, si tu met autant de fautes sur les topics de présentation, tu va te manger un bon vieux troll :p\r\n\r\nPourquoi ais-je dis "Pour abandonner dans 2 jours", car le temps me l''a montré, peu importe le domaine, seuls 4% des projets "persos" sont abordés, fautes de temps, de moyens, de motivation, tout simplement ;)\r\n\r\nAprès, si vous avez besoin de conseil, comme dit plus haut, pourquoi pas, maintenant, nous sommes encore en travaux, le site n''est pas complètement achevé, je trouves ça quelque peu "déplacé" de commencer les recrutements, si tu veux imager, c''est comme si je t''embauchais dans ma boite, mais que je te disais "Bah, faut faire la peinture et installer les meubles, mais t''inquiète, après y''aura d''autres salariés hein ?" ^^\r\n\r\nCe que je peux te conseiller, afin de partir sur de bonnes bases :\r\n\r\n- Compte toi une bonne équipe, disponible, compétente et, qui aime ce qu''elle fait, car sans intérêt, comment motiver tes troupes ???\r\n\r\n- Sortez un site, idéalement différents (les productions proposant des sponsorings + tournois sont nombreux, vous devez vous démarquer)\r\n\r\n- Les rentrées d''argent, c''est biiiiiiieeeeeennnnnnnn plus tard, sinon je monétiserais tout mes sites zombie et serait millionnaire :p\r\n\r\n- Booste ta communauté Facebook et/ou twitter (généralement, les deux suivent une courbe plus ou moins identique)\r\n\r\nUne fois tout ses points réunis, crois moi, ça ira nettement mieux pour toi, déjà, regardes. Un mec lambda comme moi qui tape ton nom sur Google, Toc, premier résultat, je tombe sur un site sympa, sans fautes avec quelques évents qui m''attirent, je reste. Le design est laid, le site est lourd, le contenu absent, je me barres, et il en va de même pour tout le monde. \r\n[toggle=Rapport d’intéressement][img]http://image.noelshack.com/fichiers/2013/14/1365087721-capture.png[/img] Cette image est prise depuis nos stats, comme tu peux le voir, les gens restent moins de 10 secondes majoritairement ... et cette tendance se retrouves sur tout les sites, même Facebook !!![/toggle]\r\n\r\nEn tout cas, je te souhaites bien du courage pour ton projet, je reste en amont si tu as la moindre question (que ce soit sur l''audit de ton site et du conseil en "community management" pour n''en citer que 2)\r\n\r\nJe sais qu''il s''agit d''une tâche ingrate et difficile, donc, sincèrement, bonne chance !'),
(513, 12, 38, 1365105783, 3, '+1 pour laurent, et non celui la ce n''est pas mon frere, bon courage l''ami ^^'),
(514, 12, 38, 1365177230, 4, '+1 pour Laurent, Je suis ok pour qu''il rejoins notre équipe.'),
(515, 12, 38, 1365179185, 5, 'De meme pour moi ;)'),
(516, 7, 15, 1365267314, 35, 'hey! je suis la comme repmlacement si besoin, no soucis ;-)'),
(517, 7, 15, 1365285396, 5, 'ok je note Anko ;)'),
(518, 6, 42, 1365357532, 5, 'Salut a tous\r\nEs qu''il y aurai des membres qui serai créé une intro pour les vidéos de la meute? Une belle intro comme on peut le voir sur les vidéo de certaine team serai pas mal, suffi de voir celle des Infamous ( http://youtu.be/lbLNK9SCUB4 )\r\nPersonnellement la dessu je n''y connais rien :p'),
(519, 6, 42, 1365357575, 6, 'Je crois que Tweezy en est capable ;)'),
(520, 8, 28, 1365408501, 5, 'Battlefield 4 devrai sortir le 29/10/2013 !!!! en espérent que cela signifie aussi la sortie de la Ps4....\r\n\r\nhttp://www.ps4france.com/battlefield-4-sortirait-le-29-0ctobre-4141/'),
(521, 8, 28, 1365443198, 1, 'Vous pourriez passer ce topic en tan que news ?'),
(522, 10, 32, 1365443280, 5, 'Serait il possible d''avoir un récap de la strat pour que les membres qui n''était pas la pour l''entrainement puisse bossé cette semaine ? Merci d''avance ;)'),
(523, 10, 33, 1365443324, 5, 'Serait il possible d''avoir un récap de la strat pour que les membres qui n''était pas la pour l''entrainement puisse bossé cette semaine ? Merci d''avance ;)'),
(524, 8, 28, 1365443957, 5, 'oui sa doit être fesable ;)'),
(525, 10, 32, 1365446763, 1, 'Fallait venir ! :D'),
(526, 10, 32, 1365446787, 1, 'Et heu, refaites pas la même blague le jour du match hein ? :p'),
(527, 6, 42, 1365446951, 34, 'Sa parle de moi ? :D'),
(528, 10, 33, 1365452995, 6, 'Pour le premier point en attaque : \r\nEscouade 1 a gauche \r\nEscouade 2 middle \r\nEscoude 3 Jeep rush \r\nApres je sais plus :p'),
(529, 6, 42, 1365498929, 27, '(A suprimé)'),
(530, 6, 42, 1365499255, 1, 'Je cède mon tour ^^'),
(531, 7, 43, 1365524492, 34, 'Coucou tous le monde voila j''ai fait la fouine et j''ai trouver sa pour des match ou des tournoi :D\r\n[URL]http://www.consoles.net/fr/[/URL]\r\nce n''est qu''une proposition :D'),
(532, 7, 43, 1365533766, 5, 'C''est prévu tweezy, mais pour l''instant faut déjà que l''on est une scade qui tienne la route pour fair de l''esl c''est pour cela que l''on n''y sait pas encore mis ;)\r\n'),
(533, 7, 14, 1365540534, 20, 'Tout de façon il est équilibré, je ne vois pas en quoi c''est gênant, vous avez le droit à toutes les armes, nous aussi donc c''est équitable, ça revient au même que d''interdire.'),
(534, 7, 14, 1365586089, 1, 'Toutes les armes ? C''est à dire que je peux prendre mon pignouf ? :p'),
(535, 7, 43, 1365586170, 1, 'ESL ? Toch, dis moi pas qu''c''est pas vrai, TOI parler de ESL ? :p\r\n\r\nPerso, j''ai encore mes contacts chez eux s''il faut activer la team plus vite, avoir été sur les EAS, ça aide :)'),
(536, 7, 15, 1365619582, 27, '[ALIGN=center]Je ne pense pas qu''il y ai une épidemie de maladie mais au cas ou je suis la :) pour croquer des ...(je l''aisse libre votre imagination (perverse))[/ALIGN]'),
(537, 7, 44, 1365623622, 27, 'Intro http://www.youtube.com/watch?v=1fQapaaFubE&feature=youtu.be \r\nPoster vous en penser quoi ?\r\nCe qu''il manque '),
(538, 7, 14, 1365659804, 4, 'Et oui ! t''as le droit de m''utiliser contre les adversaires, mais me lance pas trop fort non plus.'),
(539, 7, 15, 1365667541, 5, 'Ok c''est noté ;)\r\n\r\nVuq ue manu est débordé avec ses exam, je vous posterait les escouades dans la matiné ;)'),
(540, 7, 14, 1365667690, 5, 'Par contre, plus sérieusement, le match est dans 3 jours donc sa serai bien que on est une réponse définitive pour le pompe... je fait appelle au leadeur et co-leadeur pour donné une reponce a cette question, merci d''avance ;)'),
(541, 10, 33, 1365671750, 5, '[URL=http://www.casimages.com/img.php?i=1304121005166025511076154.png][img]http://nsm08.casimages.com/img/2013/04/12//1304121005166025511076154.png[/img][/URL]\r\n\r\n[COLOR=#FFA500]Changement fait suite a l''indisponibilité d''Anko\r\nJuju n''étant pas sur d''etre présent, Aulyro tu doit te tenir pres a prendre sa place ;)[/COLOR]\r\n'),
(542, 10, 32, 1365673156, 5, '[URL=http://www.casimages.com/img.php?i=1304121005216025511076155.png][img]http://nsm08.casimages.com/img/2013/04/12//1304121005216025511076155.png[/img][/URL]\r\n\r\n[SIZE=5][COLOR=#FFA500]objectif des équipes:[/COLOR][/SIZE]\r\n\r\n[SIZE=4][COLOR=#8B0000]chasseurs:[/COLOR][/SIZE]\r\n[COLOR=#DEB887]éliminé le viper et dégager les airs le plus possible[/COLOR]\r\n\r\n[SIZE=4][COLOR=#8B0000]viper:[/COLOR][/SIZE] \r\n[COLOR=#DEB887]aidé a l''élimination du viper et attaqué les chars en partant du point le plus près de notre spawn au plus éloigné[/COLOR]\r\n\r\n[SIZE=4][COLOR=#8B0000]jeep/helico de reco:[/COLOR][/SIZE]\r\n[COLOR=#DEB887]placé un marqueur laser pour aidée a l''élimination des véhicules aérien et s''assuré que les toits soit dégager\r\n[/COLOR]\r\n[SIZE=4][COLOR=#8B0000]char:[/COLOR][/SIZE]\r\n[COLOR=#DEB887]N°1 et N°2 fonce jusqu''au point du milieu pour le prendre, puis N°1 va sur le point le plus près de leur spawn pour le prendre tendit que le char N°2 se met entre ces deux point pour aidé a la destruction des char ennemie et les empêché de prendre a rever par le midel\r\n\r\nN°3 prend le point le plus près de notre spawn et le protège en empêchant que l''on nous back par se point (attention au parachute-c4 !!!! )\r\n[/COLOR]\r\n[COLOR=#32CD32]Pour les gunners[/COLOR][COLOR=#DEB887], priorité a la réparation !!!! mais prenez les iglas pour les N°2 et 3 au cas ou il y aurait besoin d''aide pour abattre les véhicules aérien[/COLOR]\r\n[COLOR=#32CD32]Pour les "tankistes"[/COLOR][COLOR=#DEB887], surtout ne pas laissé un de nos blindé a l’ennemie, donc s''il le faut, explosé avec votre char, mieux vos un char détruit qu''un char pour l’ennemie [/COLOR]'),
(543, 7, 44, 1365674000, 1, 'Aller, détruisons ton travail :p\r\n\r\nNon, plus sérieusement, super propre, je n''ai que 3 choses à redire.\r\n\r\n1) Le fait d''animer un logo, pour en afficher un autre à la fin, on perd un peu le message, on à plus l''impression qu''on est "partenaires" du 1e logo (le X orange)\r\n2) La typo utilisée pour l''url YT est pas superbement lisible, dommage !\r\n3) http://www.youtube.com/user/meuteapax, vla long non? Met le logo YT, et à coté, meuteapax :p (exemple: [img]http://www.clauer.fr/wp-content/uploads/2012/11/Pub-2-Metro-NYC.jpg[/img])\r\n\r\nMais, franchement, bravo dude ;)'),
(544, 10, 33, 1365674036, 1, 'J''ai toujours du mal avec Sniper Ben XD'),
(545, 10, 33, 1365675190, 8, 'manque plus qu''à s''entrainer en soutien '),
(546, 10, 33, 1365676496, 5, 't’inquiète benco, tu est en sniper M5k ou autre et micro drone, surtout micro drone pour nous localisé l’ennemie ;)\r\n\r\nEt oui Bm, va falloir bossé ^^ je serait in game cette aprem si tu veut qu''on se mette un peut en blindé pour finalisé notre binôme :p '),
(547, 7, 44, 1365683006, 27, '[ALIGN=center]Ok je vois pôur corrigé sa merci [/ALIGN]'),
(548, 10, 33, 1365703481, 5, 'Avis au personne qui on suivi l''entrainement, tout le monde ne connais pas la strat donc sa serai sympas de nous la faire partagé qu''on est le temps de l''assimilé d''ici dimanche.... merci d''avance !!!! ;)'),
(549, 6, 42, 1365706007, 34, 'pourquoi s''abonner a un inconnue alors que je peux en faire des intro ? :P\r\nperso j''irai pas m''y abonner :p '),
(550, 6, 42, 1365707530, 5, 'fait toi plaisir tweezy et fait en une, plus ya de fou et plus on se marre ^^'),
(551, 10, 33, 1365764698, 8, 't inquiete je crois en notre binome je me suis pas mal entrainé en blindé apres tout depend de ce qui produise comme jeu les 2099'),
(552, 10, 33, 1365771284, 16, 'Pour les stratégies attaque- défense comment sa ce passe? Comment on attaque les différents points? et comment on les défends? est-il possible de prévoir une petite révision avant demain 15h?'),
(553, 7, 15, 1365780314, 22, 'désolé, je ne trouvais pas le forum privé pour vous dire que je serais présent Dimanche'),
(554, 7, 15, 1365781002, 5, 'Pas de souci on ma fait passé le message ;)'),
(555, 10, 33, 1365781217, 5, 'moi aussi Bm je suis confian et heureusement sinon sa ne serai pas la peine de le faire ^^\r\nMika, j''ai fait une strat sur tempete qui marche dans les 2 sens, par contre pour pic de damavan je ne sait pas...\r\npour info jai mis le serveur en version match donc vous pouvez allez dessus si vous souhaitez vous entrainez ;)'),
(556, 7, 15, 1365784389, 5, 'RDV a 14h30 (max 14h45) dimanche sur notre serveur pour le match, que l''on est le temps de mètre les escouades en place\r\n\r\nPensé a :\r\n\r\n-vérifié vos atout par escouade\r\n-vérifié votre tenu (noir sur tempête sa se voie de loin... )\r\n-vérifié vos équipements pour les véhicules par équipage (2 scanners ne serve a rien...)\r\n- et bien sur vérifié vos strat, si vous avez des question n''hésité pas'),
(557, 10, 33, 1365784937, 3, 'si  c''est ce dimanche je suis la je comprend plus la on a sauté des semaines? ^^'),
(558, 10, 33, 1365791183, 5, 'lol jujub ^^ oui le 14 c''est se dimanche ;)'),
(559, 10, 32, 1365791897, 2, 'pour les atouts des  tank je pensais rechargement rapide pour le pilote pour  le tireur  blindage et le marqueur le détecteur \r\n fumigène et missile guidé ou mitrailleuse lourde '),
(560, 10, 32, 1365795684, 5, 'pour ma part je pensait:\r\n\r\npilote: blindage / fumée infra / mitrailleuse coaxe\r\n\r\ntireur: scanner\r\n\r\nMais vue que l''on est que deux part blinder, on pourra pas avoir plus...\r\n\r\nGp faut que vous regardiez vos équipement de viper avec gyom, comme sa vous serez prés avant meme le match ;)'),
(561, 8, 45, 1365796012, 26, 'Bon , il est venu le temps de se dire au revoir ... ça va peut-etre vous surprendre mais bon. L''aventure APAX s''arrete ici pour moi ... Pourquoi ? Personnellement , je n''ai plus les meme sensation , la meme envie qu''au début. Je ne retrouve plus les meme délires que j''avais avec l''ensemble de la meute. Mes collègues de jeu ce sont réduit à : Anko , Juju , Lawrent et Thierry et c''est avec eux seuls que je me sens à l''aise et que j''ai vraiment des bons délires. Je sais pas pourquoi mais j''ai l''impression que l''ambiance à changer et que chacun joue " dans son coin " , ce n''est que mon point de vue ;) . On est plus d''une trentaine et je ne connais pas certains APAX ! je ne dois pas etre le seul dans ce cas ;) ... En tout cas je voudrais vous remerciez tous et en particulier Anko qui m''a aidé à rentrer dans la meute et avec qui je me marre bien ... Juju ! alors lui c''est mon ami mon copain que quand il pète il trou son slip ! #SébastienPatrick ;) et bien sur Manu qui m''a accordé sa confiance durant ces mois en votre présence. Sans oublier les autres , je ne vais pas tous vous citez ( sinon je publie un roman direct ... qui sera en vente au mois d''Octobre dans toutes les Fnac de France d''ailleurs , avec séances dédicaces le 12 et 13 Octobre entre 10H et 17H à la Fnac de Troyes , venez nombreux mdrr ) ... J''ai passé d''agréable moments avec vous , de bonnes parties , de bons délires avec certains . Je ne vous oublie pas , vous etes ma 1ere team donc c''est juste impossible ... Enfin voilà , je vais me taire sinon vous allez en avoir pour 2H à lire tout ça. MERCI A TOUS ET BONNE CONTINUATION ! ( je peux toujours venir vous bottez le cul sur votre terrain ;) )'),
(562, 12, 38, 1365796205, 5, 'Un ptit up pour notre ptit laurent ;)'),
(563, 12, 37, 1365796259, 5, 'Et un ptit up pour sim ;)'),
(564, 9, 29, 1365797512, 5, 'J-1 avant la soirée d''anniv !!!!!!!! vivement demain soir !!!!!!!! ;)'),
(565, 12, 37, 1365798043, 8, 'simon envoie une photo de ta raie manu aime bien,ca peut accélérer les choses il aime bien... un petit UP quand même'),
(566, 7, 15, 1365798068, 27, '[ALIGN=left][B]Pour le match de demain j''aimerais qu''on s''entraine les remplacent plus quelle que membres contre les joueur (c''est une proposition :) ) a Demain [/B][/ALIGN]'),
(567, 12, 38, 1365798523, 8, 'quelques parties avec toi et très bonne impression donc évidemment un gros UP pour ton recrutement'),
(568, 10, 32, 1365798909, 8, 'justement je voulais t''en parler des atouts dartoch donc ce que tu a mis est tres bien donc on part comme ca pour dimanche'),
(569, 7, 14, 1365803768, 20, 'Bon, si vous voulez enlever le pompe pour quelque raison valable ou non que ce soit, enlevez-le au moins ce sera régle, depuis le temps. Les personnes à Contacter le jour du match pour le serveur sur la ps3 sont : Talion2099 (chef), balnave (co-leader), Benalexyanberang(ambassadeur), Julox-fr (chef d''escouade)'),
(570, 7, 14, 1365838902, 5, 'pas de souci dark c''est noté, par contre vous voulez le faire sur votre serv ou sur le notre? pour ma part peut importe ;)'),
(571, 10, 32, 1365838977, 5, 'ok sa marche mon grand ;)'),
(572, 10, 32, 1365843277, 8, 'si tu es dispo aujourd hui essaye de le mettre sur le site je reviendrai voir ca a midi par la histoire qu''on se fixe un petit entrainement avec les conditions de demain on est jamais assez entrainé'),
(573, 7, 14, 1365853387, 34, 'Question : \r\nJ''ai le droit d''utiliser ces armes la en: \r\n- assaut = KH2002 , F2000 , AEK-971 ?\r\n                                          - éclaireur = M40A5 , M98B , L96 ?\r\n                                          - Arme de Poing = M412 REX , 44 magnum ?\r\n                                          - Général = MP7 , UMP-45 , P90 , M5K , PP19 ? '),
(574, 7, 15, 1365869794, 2, 'je laisse ma place pour demain trop compliqué ces temps si avec l''heure de la sieste de la petite je ne pourrai pas jouer avec le micro , donc je préfère laisser ma place '),
(575, 10, 46, 1365888754, 5, 'Suite au dernier entrainement fait se soir, et suite a l''absence de Gp demain, certain changement on du etre fait\r\nVous trouverez donc les escouade definitive si dessous\r\n\r\n[SIZE=4]tempete de feu:[/SIZE]\r\n\r\n[URL=http://www.casimages.com/img.php?i=1304131120076025511082575.png][img]http://nsm08.casimages.com/img/2013/04/13//1304131120076025511082575.png[/img][/URL]\r\n\r\n[SIZE=4]Pic de damavan:[/SIZE]\r\n\r\n[URL=http://www.casimages.com/img.php?i=1304131120006025511082574.png][img]http://nsm08.casimages.com/img/2013/04/13//1304131120006025511082574.png[/img][/URL]\r\n\r\n[COLOR=#DEB887][SIZE=4]Les chef d''escouade pour cette map seron:[/SIZE]\r\n\r\n- Pignouf\r\n- Waza-psy-4-x\r\n- Gyom\r\n\r\nSuivez leurs indications[/COLOR]\r\n\r\n[SIZE=4][COLOR=#87CEEB]Rappel de strat pour tempete de feu:[/COLOR][/SIZE]\r\n\r\n[SIZE=4][COLOR=#8B0000]chasseurs:[/COLOR][/SIZE]\r\n[COLOR=#DEB887]éliminé le viper et dégager les airs le plus possible[/COLOR]\r\n\r\n[SIZE=4][COLOR=#8B0000]viper:[/COLOR][/SIZE] \r\n[COLOR=#DEB887]aidé a l''élimination du viper et attaqué les chars en partant du point le plus près de notre spawn au plus éloigné[/COLOR]\r\n\r\n[SIZE=4][COLOR=#8B0000]jeep/helico de reco:[/COLOR][/SIZE]\r\n[COLOR=#DEB887]placé un marqueur laser pour aidée a l''élimination des véhicules aérien et s''assuré que les toits soit dégager\r\n[/COLOR]\r\n[SIZE=4][COLOR=#8B0000]char:[/COLOR][/SIZE]\r\n[COLOR=#DEB887]N°1 et N°2 fonce jusqu''au point du milieu pour le prendre, puis N°1 va sur le point le plus près de leur spawn pour le prendre tendit que le char N°2 se met entre ces deux point pour aidé a la destruction des char ennemie et les empêché de prendre a rever par le midel\r\n\r\nN°3 prend le point le plus près de notre spawn et le protège en empêchant que l''on nous back par se point (attention au parachute-c4 !!!! )\r\n[/COLOR]\r\n[COLOR=#32CD32]Pour les gunners[/COLOR][COLOR=#DEB887], priorité a la réparation !!!! mais prenez les iglas pour les N°2 et 3 au cas ou il y aurait besoin d''aide pour abattre les véhicules aérien[/COLOR]\r\n[COLOR=#32CD32]Pour les "tankistes"[/COLOR][COLOR=#DEB887], surtout ne pas laissé un de nos blindé a l’ennemie, donc s''il le faut, explosé avec votre char, mieux vos un char détruit qu''un char pour l’ennemie [/COLOR]\r\n\r\n[COLOR=#FF0000]Pour les chars, je vous conseille de metre les atout suivant:\r\n\r\npilote: blindage / fumée infra / mitrailleuse coaxe\r\n\r\ntireur: scanner[/COLOR]\r\n\r\nJ''espere que vous aurez tous bien pris note des changements ;)\r\n\r\nBonne nuit a tous et a demain !!!! Rdv a 14h30 pour finalisé les atouts et dispatché les joueur dans les escouade pour début du match a 15h'),
(576, 6, 42, 1365925365, 27, 'Dartoch l''expression c''est plus on est petit plus on rit :p '),
(577, 7, 14, 1365960838, 5, '[COLOR=#87CEEB]Bien joué a tous !!!!!\r\nCela n''a pas été simple car nous avion une bonne team en face (bon teamplay) la preuve veut qu''il a fallu joué les point sur la dernière manche[/COLOR]\r\n\r\n[COLOR=#FFA500]Sur pic de damavan, un bon jeux de notre part même si notre strat demande a être pofiné en attaque[/COLOR]\r\n[COLOR=#DEB887]attaque: 5 relais détruit\r\ndéfense: 2 relais perdu \r\nrésultat: apax "5" / 2099 "2"[/COLOR]\r\n\r\n[COLOR=#FFA500]Sur tempete de feu, vu qu''il n''y a pas eu d''entrainement, c''était un peut le bordel ;) mais on a su se reprendre sur la 2eme partie et du coup remporté le match :p[/COLOR]\r\n[COLOR=#DEB887]allé:   apax "0" / 2099 "12"\r\nretour: apax "31" / 2099 "0"\r\nrésultat: apax "31" / 2099 "12"\r\n[/COLOR]\r\n[COLOR=#87CEEB]Encore un gros GG a tous !!!!! Et un gros GG aussi a la team 2099 qui a fait un tres beau match;)[/COLOR]\r\n'),
(578, 7, 14, 1365968424, 27, '[B][ALIGN=center]Gg au gagnant mais surtout grand bravo au participant [/ALIGN][/B]'),
(579, 7, 14, 1365974455, 10, 'Il déclare un match nul sur leur site !! Donc ce que je propose une revanche ac cette fois-ci nos règles !! RÈGLES PROMOD !  Et notre maps , notre territoire de chasse préfère BASARD !! Vs êtes d''accord ?'),
(580, 7, 14, 1366009864, 8, 'partant pour ma part'),
(581, 6, 47, 1366010392, 16, 'Bonjour à tous\r\n\r\nAprès le match de dimanche, une "superbe" suprise pour moi : la console s''éteint puis impossible de la rallumé, j''ai essayé sur d''autre prise pas de changement. Donc pour moi la meute APAX s''arrête avec regret cette année et RDV 2014 sur PS4 si je ferai toujours parti de cette bande de loups.\r\n\r\nMessieurs bon jeu à tous.'),
(582, 12, 38, 1366013382, 4, 'Je re Up un coup, si jamais vous avez pas vu les gars !!!!'),
(583, 7, 14, 1366014203, 5, 'une égalité? ils ne savent pas compté? 5 relais détruit contre 2 pour eux et plus de 20 tickés d’écart sur tempête de feu... chez moi sa fait pas une égalité... on ne doit vraiment pas avoir les même règles...\r\n\r\nMais bon si ya besoin et que je suis dispo je serait présent bien sur ;)'),
(584, 6, 47, 1366020687, 8, 'Oh putain t''inquiete on se retrouvera sur ps4 '),
(585, 6, 47, 1366021813, 1, 'Ah merdeeee !\r\nT''as pu établir un diagno ? '),
(586, 7, 14, 1366021958, 1, 'Dispo, même si je ne trouves pas super fairplay de leur part d''annoncer un nul. '),
(587, 7, 48, 1366022266, 1, 'Salut à tous, le titre du topic est un peu évocateur, c''est voulu.\r\n\r\nA mon humble avis, la meute comporte trop de loups, et malheureusement, pas tous actifs. Peut être envisager de clôturer les recrutements pour le moment, et voir pour "mettre sur la touche"\r\nles absents du site. Genre des gens qu''on rencontre juste avant le match ou ce genre de choses, qui n''ont jamais participes à un entrainement, qui ont 2 messages sur le forum en l''espace de 3 mois, ou ce genre d''éléments.\r\n\r\nJ''espère ne pas m''attirer les foudres de tout le monde, mais c''est dommage de perdre des joueurs car la team est en stand-by, tellement les membres sont nombreux, plus aucuns matchs n''est organisé (avant les 2099, il n''y avait eu que 3 essais non fructueux de match (Pignouf && Aulyro)). \r\n\r\nEncore une fois, ce n''est que mon ressenti, mais de plus en plus d''anciens fichent le camp, et c''est vraiment dommage.\r\n\r\nA bon entendeur ;)'),
(588, 6, 47, 1366023579, 16, 'j''ai plus d''alim'),
(589, 7, 14, 1366026985, 6, 'C''est parce que sa fais 2/2 au niveaux des sides'),
(590, 7, 14, 1366038066, 5, 'certe 2/2 au niveau des side mais vu qu''il y a egalité, on detail les points comme sa se fait tout le temp... donc oui pas tres fairplay de leur part mais bon c''est pas grave ;)'),
(591, 6, 47, 1366038237, 5, 'a ouai et si c''est l''alime intern c''est mort.... jespere que c''est juste temporaire sinon je te dit a dans quelque mois sur ps4 ^^\r\nps: sa ne t''empeche pas de venir sur le site, FB, ... tu fait toujour partie de la meute, meme si pour l''instant tu a des probleme matériel ;)'),
(592, 6, 47, 1366038789, 16, 'je regarde ce qui se passe sur le fofo et là c''est chaud pour le rachat d''un ps3 donc j''attend prime de fin d''année et donc ps4'),
(593, 6, 47, 1366046442, 5, 'c''est sur que quite a reprendre une console, autant atendre la ps4, je te comprend '),
(594, 6, 47, 1366047498, 16, 'Sa va être long j''ai les doigts qui me démangent!!!!!'),
(595, 6, 47, 1366050545, 1, '[quote=mika44500]Sa va être long j''ai les doigts qui me démangent!!!!![/quote]\r\n\r\nTa vie sexuelle ne nous intéresse nullement ma p''tite vache ;o'),
(596, 6, 47, 1366051471, 2, 'a sa c''est vraiment pas de chance , sais quoi comme version ? une slim ou fat? sa clignote rouge l''alim ?'),
(597, 7, 14, 1366053314, 27, '[ALIGN=center][B]C''est un moyen de vous provoquer mordez les une bonne fois pour toute :p [/B][/ALIGN]'),
(598, 7, 48, 1366056497, 8, 'salut benito, je ne pense pas que mettre le recrutement en stand by soit une bonne idée car si tu regarde les dernieres recrues  elles sont tres présentes et surtout on line que ce soit tweezy, toutoun ou mika meme si ce dernier a un petit souci de play et je peux te dire que les deux nouveaux prétendants simreuh et lawrent sont tres présent et on a besoin de cette présence apres ce n''est que mon avis, je laisse le choix aux grandes instances de la meute'),
(599, 6, 47, 1366058492, 16, 'c''est une slim et je n''ai aucun voyant'),
(600, 6, 49, 1366107182, 21, 'Salut,\r\n\r\nDepuis un match en compagnie de Baptiste, quand je consulte Battlefeed, ce sont ses informations qui remontent.\r\nDe même après redémarrage du jeux et de la console.\r\nCe n''est pas d''un intérêt primordial OK, mais bon si quelqu''un a une idée??\r\nMerci.\r\n\r\nA+ les Loups\r\n\r\n'),
(601, 6, 49, 1366113395, 1, 'Le battlefeed remonte toutes les notifs de tes amis :)');
INSERT INTO `forum_posts` (`postID`, `boardID`, `topicID`, `date`, `poster`, `message`) VALUES
(602, 7, 48, 1366113497, 1, 'Le coté "standby", c''est plus histoire de modérer un peu le flux des membres. Je vais voir pour sortir un graph des logs au site (nom, inscrit depuis le, dernière connection, nombre de messages), tu va voir ce que je veux dire ;)'),
(603, 7, 48, 1366116639, 1, 'Voilà, les stats viennent de parler. \r\n\r\nVoici comment y acceder >> [URL=http://www.meute-apax.fr/site/stats.php]Acceder aux statistiques d''utilisation et de présence sur le site[/URL] '),
(604, 6, 49, 1366116699, 5, '[quote=lawrent33]\r\nDepuis un match en compagnie de Baptiste, quand je consulte Battlefeed, ce sont ses informations qui remontent.\r\nDe même après redémarrage du jeux et de la console.\r\n\r\n[/quote]\r\n\r\nJe doit être a l’ouest parsque je comprend pas se qui t’arrive mon ptit lawrent... ^^'),
(605, 7, 48, 1366116836, 5, 'sa ne marque que t''es stat benco, faudrait un visu global pour voir vraiment se que cela donne ;)'),
(606, 7, 48, 1366120733, 1, 'Heu, j''ai tout le monde moi ^^''\r\n\r\nben_ftwc (position : 1)\r\nMembre depuis le : 21/02/2013, soit 54 jours\r\nDernière connexion : 16/04/2013\r\nNombre de messages : 158\r\nSoit envirron 3 messages par jours ( 2.9259259259259 pour être exact ...)\r\n\r\n\r\nDartoch (position : 2)\r\nMembre depuis le : 23/02/2013, soit 52 jours\r\nDernière connexion : 16/04/2013\r\nNombre de messages : 133\r\nSoit envirron 3 messages par jours ( 2.5576923076923 pour être exact ...)\r\n\r\n\r\nwaza-gp_one (position : 3)\r\nMembre depuis le : 23/02/2013, soit 51 jours\r\nDernière connexion : 16/04/2013\r\nNombre de messages : 47\r\nSoit envirron 1 messages par jours ( 0.92156862745098 pour être exact ...)\r\n\r\n\r\nWAZA-PsY-4-x (position : 4)\r\nMembre depuis le : 24/02/2013, soit 50 jours\r\nDernière connexion : 15/04/2013\r\nNombre de messages : 37\r\nSoit envirron 1 messages par jours ( 0.74 pour être exact ...)\r\n\r\n\r\nbmdc64 (position : 5)\r\nMembre depuis le : 24/02/2013, soit 50 jours\r\nDernière connexion : 15/04/2013\r\nNombre de messages : 37\r\nSoit envirron 1 messages par jours ( 0.74 pour être exact ...)\r\n\r\n\r\nmika44500 (position : 6)\r\nMembre depuis le : 03/03/2013, soit 44 jours\r\nDernière connexion : 16/04/2013\r\nNombre de messages : 30\r\nSoit envirron 1 messages par jours ( 0.68181818181818 pour être exact ...)\r\n\r\n\r\nPignouf30 (position : 7)\r\nMembre depuis le : 23/02/2013, soit 51 jours\r\nDernière connexion : 16/04/2013\r\nNombre de messages : 26\r\nSoit envirron 1 messages par jours ( 0.50980392156863 pour être exact ...)\r\n\r\n\r\nApaX_LeLe (position : 8)\r\nMembre depuis le : 24/02/2013, soit 52 jours\r\nDernière connexion : 16/04/2013\r\nNombre de messages : 24\r\nSoit envirron 0 messages par jours ( 0.46153846153846 pour être exact ...)\r\n\r\n\r\nNoSkill4Ever (position : 9)\r\nMembre depuis le : 17/03/2013, soit 30 jours\r\nDernière connexion : 16/04/2013\r\nNombre de messages : 20\r\nSoit envirron 1 messages par jours ( 0.66666666666667 pour être exact ...)\r\n\r\n\r\nAtOnIuM_GhAuX (position : 10)\r\nMembre depuis le : 12/03/2013, soit 29 jours\r\nDernière connexion : 10/04/2013\r\nNombre de messages : 13\r\nSoit envirron 0 messages par jours ( 0.44827586206897 pour être exact ...)\r\n\r\n\r\ndarkyoyo19-1 (position : 11)\r\nMembre depuis le : 06/03/2013, soit 37 jours\r\nDernière connexion : 12/04/2013\r\nNombre de messages : 12\r\nSoit envirron 0 messages par jours ( 0.32432432432432 pour être exact ...)\r\n\r\n\r\nTweeZy (position : 12)\r\nMembre depuis le : 21/03/2013, soit 24 jours\r\nDernière connexion : 14/04/2013\r\nNombre de messages : 11\r\nSoit envirron 0 messages par jours ( 0.45833333333333 pour être exact ...)\r\n\r\n\r\nAulyro1996 (position : 13)\r\nMembre depuis le : 24/02/2013, soit 47 jours\r\nDernière connexion : 12/04/2013\r\nNombre de messages : 10\r\nSoit envirron 0 messages par jours ( 0.21276595744681 pour être exact ...)\r\n\r\n\r\njujub3 (position : 14)\r\nMembre depuis le : 23/02/2013, soit 49 jours\r\nDernière connexion : 13/04/2013\r\nNombre de messages : 8\r\nSoit envirron 0 messages par jours ( 0.16326530612245 pour être exact ...)\r\n\r\n\r\nitadakimasTB (position : 15)\r\nMembre depuis le : 02/03/2013, soit 22 jours\r\nDernière connexion : 25/03/2013\r\nNombre de messages : 6\r\nSoit envirron 0 messages par jours ( 0.27272727272727 pour être exact ...)\r\n\r\n\r\nMister_reveur (position : 16)\r\nMembre depuis le : 09/03/2013, soit 36 jours\r\nDernière connexion : 14/04/2013\r\nNombre de messages : 5\r\nSoit envirron 0 messages par jours ( 0.13888888888889 pour être exact ...)\r\n\r\n\r\ntypex59 (position : 17)\r\nMembre depuis le : 12/03/2013, soit 17 jours\r\nDernière connexion : 29/03/2013\r\nNombre de messages : 4\r\nSoit envirron 0 messages par jours ( 0.23529411764706 pour être exact ...)\r\n\r\n\r\nPaPa_KeNoUzZ (position : 18)\r\nMembre depuis le : 17/03/2013, soit 27 jours\r\nDernière connexion : 13/04/2013\r\nNombre de messages : 4\r\nSoit envirron 0 messages par jours ( 0.14814814814815 pour être exact ...)\r\n\r\n\r\ntoutoun80740 (position : 19)\r\nMembre depuis le : 22/03/2013, soit 24 jours\r\nDernière connexion : 15/04/2013\r\nNombre de messages : 4\r\nSoit envirron 0 messages par jours ( 0.16666666666667 pour être exact ...)\r\n\r\n\r\nMrRako (position : 20)\r\nMembre depuis le : 26/02/2013, soit 38 jours\r\nDernière connexion : 04/04/2013\r\nNombre de messages : 3\r\nSoit envirron 0 messages par jours ( 0.078947368421053 pour être exact ...)\r\n\r\n\r\nSimrreuhh (position : 21)\r\nMembre depuis le : 29/03/2013, soit 1 jours\r\nDernière connexion : 30/03/2013\r\nNombre de messages : 3\r\nSoit envirron 3 messages par jours ( 3 pour être exact ...)\r\n\r\n\r\nlawrent33 (position : 22)\r\nMembre depuis le : 09/03/2013, soit 38 jours\r\nDernière connexion : 16/04/2013\r\nNombre de messages : 3\r\nSoit envirron 0 messages par jours ( 0.078947368421053 pour être exact ...)\r\n\r\n\r\nXxCrAzYyKiLLeRxX (position : 23)\r\nMembre depuis le : 02/04/2013, soit 9 jours\r\nDernière connexion : 11/04/2013\r\nNombre de messages : 2\r\nSoit envirron 0 messages par jours ( 0.22222222222222 pour être exact ...)\r\n\r\n\r\nAnkoleloup (position : 24)\r\nMembre depuis le : 21/03/2013, soit 16 jours\r\nDernière connexion : 06/04/2013\r\nNombre de messages : 1\r\nSoit envirron 0 messages par jours ( 0.0625 pour être exact ...)\r\n\r\n\r\nIttecilef (position : 25)\r\nMembre depuis le : 04/03/2013, soit 17 jours\r\nDernière connexion : 21/03/2013\r\nNombre de messages : 1\r\nSoit envirron 0 messages par jours ( 0.058823529411765 pour être exact ...)\r\n\r\n\r\ngyom-88 (position : 26)\r\nMembre depuis le : 17/03/2013, soit 30 jours\r\nDernière connexion : 16/04/2013\r\nNombre de messages : 1\r\nSoit envirron 0 messages par jours ( 0.033333333333333 pour être exact ...)\r\n\r\n\r\nYuri (position : 27)\r\nMembre depuis le : 05/03/2013, soit 12 jours\r\nDernière connexion : 17/03/2013\r\nNombre de messages : 1\r\nSoit envirron 0 messages par jours ( 0.083333333333333 pour être exact ...)\r\n\r\n\r\nlord-beckett59 (position : 28)\r\nMembre depuis le : 24/02/2013, soit 47 jours\r\nDernière connexion : 12/04/2013\r\nNombre de messages : 1\r\nSoit envirron 0 messages par jours ( 0.021276595744681 pour être exact ...)\r\n'),
(607, 7, 48, 1366134512, 5, 'la médaille d''argent pour moi.... va falloir que j''augmente mes poste pour visée celle en or... lol'),
(608, 6, 25, 1366134698, 5, 'Alors manu, tu a eu le temps de te pencher sur cette nouvelle vidéo? sa serai sympas dans faire une scénarisé, pourquoi pas une reprise d''un film connu ;)'),
(609, 11, 40, 1366134790, 5, 'quelqu’un a des news du ptit crazy et de sont projet ?'),
(610, 6, 50, 1366135142, 5, 'Donc voila un ptit jeux que j''avais fait en d''autre temps et d''autre lieu ^^\r\n\r\nLe but: trouver de quel film est tirée l''image proposé, celui qui trouve poste une autre image et ainsi de suite\r\n\r\nDonc je lance les hostilités ;)\r\n\r\n[URL=http://www.casimages.com/img.php?i=100120111244602555280181.jpg][img]http://nsm02.casimages.com/img/2010/01/20//100120111244602555280181.jpg[/img][/URL]\r\n\r\nC''est assez simple mais bon c''est le début ^^'),
(611, 9, 29, 1366135274, 5, 'Bon la soirée étant tombé a l''eau pour cause d''entrainement, une autre date est prévu ? j''ai envi de soufflé les bougies moi... :('),
(612, 9, 29, 1366138647, 27, '[B][ALIGN=center]Moi aussi [/ALIGN][/B]'),
(613, 6, 49, 1366144702, 21, 'Si je comprend bien, le battlefeed remonte tous les déblocages de mes amis ainsi que les miens à la suite les uns des autres?\r\n'),
(614, 6, 49, 1366191354, 5, 'oui c''est sa ;)'),
(615, 11, 40, 1366199561, 1, 'Je pense que son projet est tombé à l''eau, y''a pas eu de changement coté Facebook/Twitter/Website. J''aurais tendance à dire "JE TE L''AVAIS BIEN DIS" mais ce serait mal vu :p'),
(616, 6, 25, 1366199626, 1, 'Genre, Bienvenu chez les loups, Vingt Milles Dents sous les Terres ... =p'),
(617, 6, 50, 1366199792, 1, 'Equilibrium, je l''ai revu y''a 2 jours, donc trop simple :p\r\n\r\nMon tour ? :\r\n\r\n[IMG]http://www.themoviebuff.net/wp-content/uploads/2012/08/2005_lord_of_war_007.jpg[/IMG]'),
(618, 6, 50, 1366205057, 27, 'Lord of War (J''espere :) )\r\n[IMG]http://image.noelshack.com/fichiers/2013/16/1366205034-images.jpg[/IMG]\r\n'),
(619, 11, 40, 1366205125, 27, '[ALIGN=center][B]Il avais l''air pas mal sont projet mais bon...[/B][/ALIGN]'),
(620, 12, 38, 1366205191, 27, 'j''ai jouer avec lui et je lui offre un Grand [SIZE=5][COLOR=#32CD32]+1[/COLOR][/SIZE]\r\n'),
(621, 6, 50, 1366210277, 5, 'retour vers le futur ^^\r\n\r\nallez on augmente un peut\r\n\r\n[URL=http://www.casimages.com/img.php?i=100120122745602555280571.jpg][img]http://nsm02.casimages.com/img/2010/01/20//100120122745602555280571.jpg[/img][/URL]'),
(622, 6, 25, 1366210348, 5, 'ouai sa pourrait etre fun :p\r\n'),
(623, 12, 38, 1366210435, 5, 'Allez Manu, va faloir regarder les poste de recrutement ;)'),
(624, 6, 50, 1366211576, 21, '"né un 4 juillet"\r\n[URL]http://images.allocine.fr/r_640_600/b_1_d6d6d6/medias/nmedia/18/83/23/81/19672449.jpg[/URL]'),
(625, 6, 50, 1366218940, 2, 'kevin kostner / danse avec les loups ? '),
(626, 6, 50, 1366219113, 5, 'c''est exacte Gp, a toi d''affiché ;)'),
(627, 7, 51, 1366219239, 5, 'Je viens de prolongé le serveur pour 1 mois donc j’espère voir plein de loups dessus ;)'),
(628, 6, 50, 1366223260, 2, '\r\n\r\n'),
(629, 6, 50, 1366223357, 2, '[IMG]http://fr.web.img4.acsta.net/r_640_600/b_1_d6d6d6/medias/nmedia/18/70/03/08/19110424.jpg[/IMG]'),
(630, 12, 38, 1366223882, 2, 'rajoute mon id psn '),
(631, 12, 37, 1366223995, 2, 'rajoute mon id psn'),
(632, 7, 51, 1366224058, 2, 'ok sa marche'),
(633, 6, 50, 1366226624, 5, 'Very bad trip ^^ atention au tigre ;)\r\n\r\n[URL=http://www.casimages.com/img.php?i=1304170925346025511096794.jpg][img]http://nsm08.casimages.com/img/2013/04/17//1304170925346025511096794.jpg[/img][/URL]\r\n'),
(634, 7, 51, 1366228868, 8, 'UP pour toi dartouche'),
(635, 6, 50, 1366229501, 21, 'Christian Slater / Windtalkers, les messagers du vent ?'),
(636, 9, 29, 1366229748, 8, 'ca serait avec grand plaisir'),
(637, 7, 48, 1366231992, 16, 'c''est pour ça que ta créé ce jeu sur le salon.... pti malin!!!'),
(638, 7, 14, 1366266281, 4, 'ah mais messieurs, ils n''avaient pas précisé comment été la gagne. Donc je trouve ça normale qu''on finisse par un nul 2/2. Bon c''est sur que si on l''aurait fait aux tickets, on est gagnant mais c''est pas le cas. Donc passons à autres choses. Préparez-nous un autre match.'),
(639, 7, 48, 1366274635, 5, 'non pas du tout c''est juste pour incité les membres a venir sur le site ;)'),
(640, 6, 50, 1366274715, 5, 'exact lawrent ;) a toi de posté :p'),
(641, 6, 50, 1366275538, 8, 'vas y envoie laurent je suis chaud chaud'),
(642, 3, 52, 1366279228, 73, 'bonjours a tous et a toutes moi c''est nono770firedan97(=pseudo psn et battlelog ) je m''apelle Nolan j''ai 14 ans je joue a battlfield 3 depuis maintenant trois semaine j''ai un asser bon niveau voir bon ( sans me venter ^^ )j''ai découvert la team APAX grace a un de mes amis . Si je postule aujourd''hui c''est parce que je cherchai une team dans la quel je pourrai jouer,m''ammuser,et deconner . voila merci ^^ '),
(643, 6, 50, 1366281500, 21, 'Un de mes favoris, désolé pour les plus jeunes mais il date un peu...\r\nMais je pense que vous l''apprécierez...\r\n[URL]https://fbcdn-sphotos-h-a.akamaihd.net/hphotos-ak-prn1/p480x480/71464_410853492346762_712581167_n.jpg[/URL]'),
(644, 7, 51, 1366282514, 34, 'moi je suis souvent dessus :D mais jamais personne avec moi :('),
(645, 7, 48, 1366282795, 34, 'On a parler de moi :P moi je suis toujours présent :D et toujours d''attaque pour aller en game sur le serveur :D '),
(646, 3, 52, 1366283150, 34, 'coucou nono on verra bien si tu est pris mais nous on regarde pas le niveau mais le team play :) tien si tu veux ajoute moi ApaX_TweeZy on fera quelque game ensemble si tu veux :)'),
(647, 12, 38, 1366283256, 34, 'Yo(play) bon courage a toi Lauwrent :) je suis de tous coeur avec toi :)'),
(648, 11, 40, 1366283300, 34, 'moi je peux en avoir :)'),
(649, 7, 48, 1366283958, 1, '[quote=TweeZy]On a parler de moi :P moi je suis toujours présent :D et toujours d''attaque pour aller en game sur le serveur :D [/quote]\r\n\r\n TweeZy (position : 12)\r\nMembre depuis le : 21/03/2013, soit 24 jours\r\nDernière connexion : 14/04/2013\r\nNombre de messages : 11\r\nSoit envirron 0 messages par jours ( 0.45833333333333 pour être exact ...)\r\n\r\n0,5 msg par jours, s''moche !'),
(650, 7, 48, 1366284005, 1, '[quote=Dartoch]la médaille d''argent pour moi.... va falloir que j''augmente mes poste pour visée celle en or... lol[/quote]\r\n\r\nAccroche toi bien va mon p''tit toch, tu parles au Maître là :p'),
(651, 7, 14, 1366284155, 1, 'Beh j''ai cru comprendre qu''il y''aurait un match retour, mais peut être que je me trompe.\r\n\r\nPar contre, pour te répondre pignouf, je penses qu''entrainer des membres est primordial comparé à faire des matchs ;)\r\n\r\nIl faudrait plutôt organiser une esquade d''une dizaine de joueurs présents et motivés, issus de la meute of course pour partir en entrainements, et après, partir en cups ;)\r\n\r\nEncore une fois, ce n''est que mon avis tralala et tout ce qui suit ;)'),
(652, 6, 50, 1366284286, 1, 'Wolfen (1982 ou 81 si je ne m''abuse ?)\r\n\r\n\r\n[img]http://static.neatorama.com/stacy/rhapsody.jpg[/img]\r\n\r\n\r\n;)'),
(653, 11, 40, 1366284405, 1, '[quote=NoSkill4Ever][ALIGN=center][B]Il avais l''air pas mal sont projet mais bon...[/B][/ALIGN][/quote]\r\n\r\nL''idée est même très bonne, c''est leur implication nulle qui ne l''est pas ...\r\nEn 2 semaines, n''importe qui de motivé et avec un tout petit peu de publicité réussi à péter au moins 50 likers FB, ont un site web de présentation, là entre nous ... C''est comment dire, bagdad après le passage des ricains, kathrina et 1h de Tank ...'),
(654, 6, 27, 1366284605, 1, 'Dites, je déterre un peu les topics, mais j''avais adoré le concept, y''aurais moyen de relancer l''idée ?'),
(655, 6, 50, 1366285768, 21, 'Wayne''s world (scène mythique sur Bohemian Rhapsody) \r\n\r\nAttention catégorie chef d’œuvre:\r\n[URL]http://images.allocine.fr/r_640_600/b_1_d6d6d6/medias/nmedia/18/35/49/60/18399033.jpg[/URL]'),
(656, 6, 50, 1366290593, 1, 'Reservoir Dog ?\r\n\r\n[IMG]http://images.wikia.com/desencyclopedie/images/archive/7/72/20100624174745!Orange-mecanique.jpg[/IMG]'),
(657, 7, 48, 1366291469, 34, '[quote]TweeZy (position : 12)\r\nMembre depuis le : 21/03/2013, soit 24 jours\r\nDernière connexion : 14/04/2013\r\nNombre de messages : 11\r\nSoit envirron 0 messages par jours ( 0.45833333333333 pour être exact ...)\r\n\r\n0,5 msg par jours, s''moche ![/quote]\r\n\r\nen meme temps j''ai pas d''ordi perso c''est celui de ma mère donc voila peux pas tous le temps aller dessus :('),
(658, 3, 52, 1366293105, 73, 'oki d''accord ba peut etre a toutaleur (ps:ta un micro ??)\r\n^^\r\n'),
(659, 3, 52, 1366294139, 73, 'et je recherche aussi une team avec beaucoup de team play donc si vous regarder le team play il y a pas de problème ^^'),
(660, 3, 52, 1366296719, 27, 'Moi c''est NoSkill4Ever ajoute moi \r\nA bientôt sur le champs de guerre '),
(661, 6, 50, 1366303959, 5, 'Un classique ^^ "orange mécanique" ;)\r\n\r\n'),
(662, 6, 50, 1366304043, 5, 'Allez un de mes film préféré ^^\r\n\r\n[URL=http://www.casimages.com/img.php?i=1304180656126025511099371.jpg][img]http://nsm08.casimages.com/img/2013/04/18//1304180656126025511099371.jpg[/img][/URL]'),
(663, 6, 27, 1366304120, 5, 'Ouai sa serai pas mal ;) surtout si c''est sur notre serveur ^^ (ok je sort....lol)'),
(664, 11, 40, 1366304367, 5, 'Malheureusement c''est sauvant comme cela que sa se passe... tu a une bonne idée, tu lance les premières fondation et blop, plus rien car tu n''a pas 10 000 visites et autant de popularité que certain youtuber... mais bon sait on jamais, peut être vas t''il se remotivé et relancé sont idée :p'),
(665, 7, 53, 1366304806, 5, 'J''aurait voulus lancé une ptite idée, il y a quelque temps, la meute organisait des soirée a thème et je me suis dit: "Et si on lançais des journée a thème?" \r\nJe m''explique, par exemple mètre le serveur pendant 1 journée avec un thème (le mercredi pour qu''il y ai plus de monde) avec des thème comme only pistol, only cut, hardcore max (vie a 10%), ...\r\ncela pourrait permettre d’attiré du monde et de nous faire connaitre\r\nUne foi la journée fini on remet tout en ordre bien sur ^^\r\n\r\nAlors, vous en pensez quoi?'),
(666, 7, 14, 1366306070, 2, 'on a tous des disponibilité différente d''ou la difficulté a s''entrainer '),
(667, 6, 25, 1366317041, 8, 'manu essaye de me chanter une comptine comme lui http://www.youtube.com/watch?v=1SqaNVS7b9M\r\navec ta plume cela ne devrait pas etre compliqué.'),
(668, 6, 50, 1366319189, 21, 'Patsy Kensit (alias madame Liam gallagher) et Mel Gibson "l''arme fatale 2"\r\n\r\nLe mec le plus "shiter" des années 80\r\n[URL]http://images.allocine.fr/r_640_600/b_1_d6d6d6/medias/nmedia/18/36/14/93/18957000.jpg[/URL]'),
(669, 6, 50, 1366319490, 16, 'Shwarzy : Commando'),
(670, 6, 50, 1366321244, 16, '[URL=http://www.hostingpics.net/viewer.php?id=124218photo1.jpg][IMG]http://img4.hostingpics.net/pics/124218photo1.jpg[/IMG][/URL]'),
(671, 7, 14, 1366350771, 4, 'Pour répondre à Ben, trouve moi 10 joueurs pour un entrainement en conquete 5vs5 ou 8 joueurs pour un 4vs4 squad rush, comme ça tu es sur d''avoir 2 équipes pour un match à venir, sachant qu''il faut s''entraîner sur toutes les maps présente en match. Ensuite pour tes cups, c''est plus des championnats si tu pense à l''ESL enfin pour nous se serais plus du CSL, avec les championnats 5vs5 ou 4vs4 squad rush. Ensuite et bien je suis d''accord avec Gp, on a tous des horaires différents. Sinon, si vous voulez vous entraîner plus sérieusement je vous invite à faire du VERSUS, sur le site de l''ESL.'),
(672, 6, 27, 1366351278, 4, 'Je le redis je peux jouer avec n''importe qui de la meute. Le problème que j''ai remarqué c''est que tout le monde veux faire son propre chemin sur le champs de bataille. Je proposerai plutôt ceci. Celui qui est chef d''escouade, donne les ordres, et les 3 autres suit, c''est tout. Ensuite, sur un match 5vs5 on peut pas faire que du binôme, vu qu''il y aura forcément un qui sera tout seul. Que en 4vs4 squad rush là tu peux lol.'),
(673, 7, 48, 1366353120, 4, 'C''est plutôt les inactifs du jeu qui faut regarder et non ceux qui vont sur le forum ou sur le facebook. Parce que s''ils ont regarde tes stats, on peut en virer beaucoup des joueurs qui ne vont pas sur le forum, pourtant ils sont très présent sur le jeu.'),
(674, 7, 48, 1366357588, 5, 'Il ne faut pas les viré mais les motivé a venir plus souvent sur le site c''est tout ;)'),
(675, 6, 27, 1366357749, 5, 'Bonne idée Pignouf, et sa sera surement plus facile a mettre en place ;)'),
(676, 6, 50, 1366363852, 21, '"La chute du faucon noir" \r\n\r\nMika, tu as augmenté le niveau de difficulté...\r\nJe vais essayer de suivre ;-)\r\n\r\n[URL]http://images.allocine.fr/r_640_600/b_1_d6d6d6/medias/nmedia/18/35/85/57/19721178.jpg[/URL]'),
(677, 12, 37, 1366381703, 43, 'Merci pour le up ! A très vite en jeu !'),
(678, 6, 27, 1366387237, 34, 'Coucou pour les binôme je me vois bien avec Ryukio :) et sinon j''ai un vieux pote a moi de mon ancienne team qui serai ok pour rejoindre la team j''essaye de le motiver :) et juste non ben aucun troll :P sérieux si un recruteur pour m''ajouter pour après parler avec mon pote sa serai cool :D merci bien et en cas ou mon binôme serai Ryukio mon pote et moi :D'),
(679, 6, 50, 1366387255, 5, 'A oui tu va cherché loin ^^ "Voyage au bout de l''enfer" début 80 si je ne me trompe...\r\n\r\n[URL=http://www.casimages.com/img.php?i=1304190603196025511102254.jpg][img]http://nsm08.casimages.com/img/2013/04/19//1304190603196025511102254.jpg[/img][/URL]'),
(680, 7, 53, 1366387962, 34, 'moi perso je suis pour je mais un gros UP pour se projet a une condition :)\r\nque tu face un thème Only Snipe :)'),
(681, 7, 53, 1366388274, 5, 'Sa changera toute les semaines ;)'),
(682, 7, 53, 1366388478, 34, 'oui mais si tu fait une journée only snipe préviens moi :D je viendrai m’amuser comme un fou :D '),
(683, 7, 53, 1366389077, 5, 'Si c''est accepté il y aura un planing sur 1 mois et biensur les proposition des membres de la meute et voir meme des joueurs extérieur si c''est fesable'),
(684, 7, 53, 1366392184, 8, 'tres bonne idée en tout cas encore une fois dartouche je suis chaud chaud pour ça'),
(685, 6, 50, 1366396862, 16, 'les larmes du soleil.'),
(686, 6, 50, 1366397131, 16, 'allez un facile \r\n\r\n[URL=http://www.hostingpics.net/viewer.php?id=147041198708g.jpg][IMG]http://img4.hostingpics.net/pics/147041198708g.jpg[/IMG][/URL]'),
(687, 6, 50, 1366401532, 8, 'full metal jacket \r\n\r\nje n arrive pas a mettre l''image suis je a chier ?'),
(688, 6, 50, 1366442817, 16, 'Je ne pense pas, j''ai pas mal cherché aussi. Moi je fais comme ça : tu héberges ton image qui est enregistrée dans ton pc avec un site comme : http://www.hostingpics.net/ , tu suis la marche à suivre et tu copie le lien : lien forum(1).\r\n\r\nDu coup je me permet : \r\n\r\nUn film choc\r\n\r\n[URL=http://www.hostingpics.net/viewer.php?id=46880100212147.jpg][IMG]http://img15.hostingpics.net/pics/46880100212147.jpg[/IMG][/URL]'),
(689, 6, 27, 1366443008, 16, 'Pour moi du coup je vais me contenter de regardé la composition finale des binomesou des escouades:bawling:. Bon jeu à vous tous '),
(690, 3, 52, 1366444467, 16, 'bienvenue à toi et surtout sois patient pour ton recrutement.'),
(691, 7, 48, 1366445588, 1, 'Du second degrée gnouf !!!\r\nThanks toch, on y vient :)'),
(692, 7, 53, 1366445764, 1, 'Idem !\r\nDe plus, je peux t''apprendre à utiliser le calendrier APAX pour mettre en avant un event, et les sondages pour proposer tes thèmes avec  le public ;'),
(693, 7, 14, 1366445961, 1, 'J''ai lancé l''idée. Seriez vous dispos pour prévoir des entraînements les soirs entre 21h && minuit ?'),
(694, 6, 27, 1366446159, 1, 'les mecs, on est quoi ... 30 ?\r\nSi on cotise 10e, on peut récuperer not'' vachette sur les champs de bataille ^^'),
(695, 6, 25, 1366446585, 1, 'putain bm ... dés le matin tu m''fait tapper des barres !!!\r\nJuste qu''il à du mal avec les temps, mais sinn, c''est juste e n o r m e, et  manu avec sa voix de pedobear, ça devrait passer plus que tout ;D'),
(696, 6, 55, 1366448261, 1, 'Les mecs, je suis tombé sur la chaine d''un malade mental ce matin, ce mec parodie toutes les parties de BF à la Mozin0r pour ceux qui connaissent.\r\n\r\nBref, faites vous votre avis > http://www.youtube.com/user/DiGiDiX\r\n\r\n#KeepSmile'),
(697, 6, 50, 1366448423, 1, 'Pariah ?\r\n\r\n[IMG]http://image.toutlecine.com/photos/i/n/g/inglourious-basterds-inglorious-basterds-19-08-2009-21-08-2009-5-g.jpg[/IMG]'),
(698, 11, 40, 1366449383, 1, 'Maybe ;)'),
(699, 6, 50, 1366449433, 21, '"Inglourious basterds"\r\n\r\nPour " Voyage au bout de l''enfer", c''était plutôt fin 70...\r\nUn facile, mais très bon:\r\n[URL]http://fr.web.img3.acsta.net/r_640_600/b_1_d6d6d6/medias/nmedia/18/64/43/01/19851849.jpg[/URL]\r\n '),
(700, 6, 50, 1366451926, 5, 'Le silence des angneaux ;)\r\n\r\n[URL=http://www.casimages.com/img.php?i=1304201200236025511104221.jpg][img]http://nsm08.casimages.com/img/2013/04/20//1304201200236025511104221.jpg[/img][/URL]'),
(701, 3, 52, 1366455351, 8, 'ajoute moi non qu''on se fasse d''autres bonnes parties\r\n'),
(702, 6, 50, 1366455640, 16, 'le dernier maitre de l''air\r\n\r\nBien vu ben pour Pariah\r\n\r\n[URL=http://www.hostingpics.net/viewer.php?id=893325e199006g.jpg][IMG]http://img15.hostingpics.net/pics/893325e199006g.jpg[/IMG][/URL]'),
(703, 6, 55, 1366456058, 16, 'bien sur!!!! terrible ce digidix ma préférée :\r\n\r\nhttp://www.youtube.com/watch?v=1HxwX2B4IF8'),
(704, 6, 50, 1366456946, 21, '"Jours de tonnerre" Nicole et Tom\r\n\r\n[URL]http://images.allocine.fr/r_640_600/b_1_d6d6d6/medias/nmedia/18/66/01/66/18902209.jpg[/URL]'),
(705, 6, 50, 1366458621, 16, 'blade runner\r\n\r\nVu qu''on est dans les anciens films :\r\n\r\nOn va corser un peu :\r\n\r\n[URL=http://www.hostingpics.net/viewer.php?id=810098197318g.jpg][IMG]http://img15.hostingpics.net/pics/810098197318g.jpg[/IMG][/URL]\r\n\r\n\r\n'),
(706, 3, 52, 1366462960, 25, 'Bienvenue sur le forum ajoute moi psn AtoNiUm_GhAuX a bientot bonne chance :)'),
(707, 12, 56, 1366541274, 75, 'Pseudo:\r\nPrénom: ludovic\r\nAge: 27\r\nPsn: hasma_tik\r\nBattlelog: hasma886\r\n\r\nComment avez vous connu la meute?:\r\nje vous est connue grâce a toutoun80740 \r\n\r\nPourquoi vouloir rejoindre nos rand?:\r\nje cherche un groupe sympas pour jouer a se jeux comme il se doit ,je joue pratiquement tout les jours\r\nje reste a votre disposition si vous avez des questions ;) \r\n\r\n\r\n\r\nParlez nous un peut de vous:\r\nje suis actuellement sans emploie j''ai donc du temps pour le jeu ,j''aime les animaux ,la pêche et la vitesse.\r\nje pense que nous pourons faire connaissance sur BTF a ++\r\n\r\n\r\n\r\nhasma_tik'),
(708, 12, 56, 1366547049, 16, 'bienvenue à toi sur le fofo et bientôt dans la meute.'),
(709, 12, 56, 1366548831, 5, 'Salut Hasma et bien venu sur notre site\r\nEn espérant te voir sur le champ de bataille je te dit bon courage pour ta demande de recrutement'),
(710, 6, 50, 1366549233, 5, 'bon a parement personne ne trouve (moi le 1er ^^) donc tu peut nous donné un indice mika?\r\n'),
(711, 6, 50, 1366549646, 16, 'pas de soucis : il pratiquait le nunchaku.'),
(712, 12, 56, 1366554447, 1, 'Juste pour le pseudo : +1'),
(713, 6, 50, 1366554622, 1, '[quote=Dartoch]bon a parement personne ne trouve (moi le 1er ^^) donc tu peut nous donné un indice mika?\r\n[/quote]\r\n\r\nT''es juste pas doué :p\r\n\r\nOpération Dragon\r\n\r\n\r\nAller, une facile [IMG]http://www.smartorrent.com/images/covers/5eme-element-1997-20-g.jpg[/IMG]'),
(714, 6, 50, 1366563598, 5, '"le 5eme élément " \r\n\r\n[URL=http://www.casimages.com/img.php?i=1304210701336025511109253.jpg][img]http://nsm08.casimages.com/img/2013/04/21//1304210701336025511109253.jpg[/img][/URL]\r\n\r\n(ps:va chier benco!!! lol )'),
(715, 6, 50, 1366569855, 2, 'force spéciale , très bon film d''ailleurs '),
(716, 6, 50, 1366570154, 2, '[IMG]http://fr.web.img3.acsta.net/medias/nmedia/18/36/26/41/18746493.jpg[/IMG]\r\n\r\npour indice certainement un de mes film préférer '),
(717, 6, 50, 1366570610, 5, 'Moi aussi j''aime beaucoup le film "300" autant pour le scénario que pour la façon d’être filmé ;)\r\n\r\n[URL=http://www.casimages.com/img.php?i=1304210857546025511109934.jpg][img]http://nsm08.casimages.com/img/2013/04/21//1304210857546025511109934.jpg[/img][/URL]'),
(718, 12, 37, 1366636568, 10, 'Salut sim , bon aprés avoir passé une bonne partie de l''aprem ensemble , jai put constater que tu avais un bon niveau , pour le teamplay on a pas put trop voir car GYOM faisait que gueuler :) Mais on aura le temps d'' Evaluer ca car aujourd''hui tu fais partie des APAX !! FELICITATION ET BIENVENUE DANS LA MEUTE !!\r\nAlors ajoute tt les APAX que tu crois et file vite mettre le TAG APAX ;) GG a toi '),
(719, 12, 38, 1366636812, 10, 'Salut lawrent , Alors avant tt je m''excuse du temps de ton recrutement mais j''ai étais débordé , évidement que tu correspond au genre de joueur qu''on recherche alors évidement que ca saura un gro OUI pr ta candidature , félicitation a toi et ta patience .. Alors ajoute APAX devant ton blase et montre nous que tu en as sous les crocs !\r\nEncore Bravo et a tres vite !!'),
(720, 12, 56, 1366636941, 10, 'Salut moi c Ven-To-Line , rejoin ns sur le champs de bataille , en esperant que tu auras assez d''air pour nous suivre ;)'),
(721, 6, 50, 1366636964, 16, 'le village \r\n \r\ndésolé mais j''adore : \r\n\r\n[URL=http://www.hostingpics.net/viewer.php?id=9670087fea6b17e93065d74869bfaea5525119.jpg][IMG]http://img4.hostingpics.net/pics/9670087fea6b17e93065d74869bfaea5525119.jpg[/IMG][/URL]'),
(722, 12, 37, 1366640827, 5, 'Bienvenu Sim ;)'),
(723, 12, 38, 1366640894, 5, 'Bienvenu parmis nous lawrent ;)'),
(724, 12, 37, 1366642146, 43, 'Wahou ! Je suis joie ! Merci de me donner ma chance, à très vite !'),
(725, 7, 57, 1366648241, 5, '[SIZE=5]Salut a tous, donc comme j''avais proposé, des journées spécial arrive!!!![/SIZE]\r\n\r\nJe m''explique, tout les mercredi, le serveur sera mis avec des règles bien spécial comme par exemple "journée sniper, pistol, c4 , smaw, harcore + , ..."\r\n\r\nDonc nous allons commencé se mercredi avec une journée "Hardcore +" (vie au minimum, dégât des armes au max, pas de carte, pas de spot,...)\r\nJe préparerait le serveur demain, comme sa vu que je ne peut pas être la le mercredi matin, un admin n''aura qu''a le lancer avec les option prés réglé\r\n\r\nJe vais faire de la pub sur le forum battlelog et je compte sur vous pour passé l''info ;)\r\n\r\nBonne fin d''aprem a tous !!!!!!  ;)'),
(726, 12, 56, 1366662987, 8, '[quote=WAZA-PsY-4-x]Salut moi c Ven-To-Line , rejoin ns sur le champs de bataille , en esperant que tu auras assez d''air pour nous suivre ;)[/quote]\r\n\r\n-1'),
(727, 12, 37, 1366663126, 8, '[quote=Simrreuhh]Wahou ! Je suis joie ! Merci de me donner ma chance, à très vite ![/quote]\r\n\r\n+1'),
(728, 6, 50, 1366719247, 1, 'Game of death :p\r\n\r\nUn peu plus drawle >\r\n[IMG]http://www.lunettesde.com/wp-content/uploads/2011/10/flic-de-beverly-hills-2-07-g.jpg[/IMG]'),
(729, 7, 57, 1366719314, 1, 'Tu pourrais mettre un post pour expliquer comment activer un mod pré-save ?'),
(730, 12, 37, 1366719356, 1, 'Bienvenu dude!'),
(731, 12, 56, 1366719393, 1, '[quote=bmdc64][quote=WAZA-PsY-4-x]Salut moi c Ven-To-Line , rejoin ns sur le champs de bataille , en esperant que tu auras assez d''air pour nous suivre ;)[/quote]\r\n\r\n-1[/quote]\r\n\r\n-1 manu, +1 bap :)'),
(732, 12, 38, 1366719457, 1, 'Bof, 1 mois d''attente quoi :p\r\nWelcome lawlaw, et, prépare toi à réa !'),
(733, 6, 50, 1366720012, 11, 'Salut a tous les mecs faites descendre le niveaux car la je ne connait aucun de vos film  depuis le niveaux et pourtant dieu sais le nombre de film que j''ai vue que ce soit ancien ou récent '),
(734, 7, 57, 1366720239, 11, 'Simple question qui sont les admin comme sa si j''ai a me plaindre d''un mec dans le jeu sa m''évitera d''envoyer un message a chaque apax de connecté'),
(735, 12, 56, 1366721872, 75, 'salut a tous et merci de votre accueille :)\r\nne t’inquiète pas pour mon asthme  malgré ma tendance a la fumette j''ai pas ma de souffle ^^lol \r\nj''aimerais s''avoir se que je doit faire pour intégré votre team ?'),
(736, 6, 50, 1366725883, 16, 'le flic de beverly hills : \r\n\r\nCelui là c''est un spécial pour Aulyro : \r\n\r\n[URL=http://www.hostingpics.net/viewer.php?id=790808EMI14266.jpg][IMG]http://img4.hostingpics.net/pics/790808EMI14266.jpg[/IMG][/URL]'),
(737, 7, 57, 1366739199, 5, 'je n''est plus la liste en tète auly, de mémoire je crois qu''il y a psy, gyom, gp, pignouf, ben, bmdc,jujube, moi\r\nje récupérerai la liste exacte et je la métrai sur le forum ;)\r\n\r\nEn fait ben c''est assez simple, tu vas dans multijoueur/ mes serveur/ géré mon serveur\r\nApres tu a mod et carte, vous aurez juste a mettre map= harcore+ et mod= extreme (classer) ou harcore+ (non classé)\r\n\r\nJe bosse toute la journée demain (merci patron...) donc je compte sur vous pour le lancer ;)\r\n'),
(738, 12, 56, 1366739481, 5, 'Pour intégré la meute c''est très simple:\r\n\r\n-être présent sur le site\r\n-être présent in game (pas 24/24 non plus ^^)\r\n-avoir tu teamplay, donc pour cela je te conseille d''ajouté le maximum de membre de la meute dans tes amis psn\r\n-avoir une bonne mentalité ;)\r\n\r\nEt voila avec tout sa sa devrai passé sans problème ;)\r\n\r\npsn: dartoch2'),
(739, 6, 50, 1366739623, 2, 'rasta......'),
(740, 6, 50, 1366739866, 5, 'si tu trouve pas auly alors la... ;)'),
(741, 6, 50, 1366742332, 11, 'Heureusement que je connait --" merci bien (balance man cadence man, Pourquoi tu fume man ???  on je ne fume pas man j''expire) vous me prenez pourquoi la --" Car j''attends de trouver vos images pour vous poser une colle sur une viellerie de film que j''adore ^^'),
(742, 6, 50, 1366742840, 16, 'allez vas-y balance je suis chaud patate!!!'),
(743, 3, 52, 1366743322, 10, 'Ton age peut poser peut etre un soucie , bref je verais ca par rapport a ton comportement et a ton teamplay !! bon courage'),
(744, 7, 57, 1366746374, 1, 'Je le met dés ce soir 23h30 ! ;)'),
(745, 7, 57, 1366746405, 1, 'Par contre, c''est quoi le map list ?   du close quarter ou du ruée & cie ?'),
(746, 7, 57, 1366795885, 5, 'ya déjà une map liste de faite nommé "hardcore+" avec de la conquete infanterie et conquete et domination'),
(748, 6, 50, 1366886428, 5, 'bon vu qu''auly ne poste pas d''image j''en met une et je relance !!!! ;)\r\n\r\n[URL=http://www.casimages.com/img.php?i=1304251242296025511120735.jpg][img]http://nsm08.casimages.com/img/2013/04/25//1304251242296025511120735.jpg[/img][/URL]'),
(749, 6, 50, 1366889833, 2, 'leon mais je suis vraiment pas sur !'),
(750, 6, 50, 1366890537, 5, 'a coté gp ^^ il était beaucoup plus jeune pour leon ;)\r\n'),
(751, 7, 57, 1366890902, 5, 'Alors résultat de la 1ere journée a thème:\r\n-152 joueurs unique\r\n-serveur remplis de 13h a 5h\r\n\r\nVerdic: la première journée c''est bien passé pour se que j''ai vu et entendu et le serveur est resté plein toute l''aprem et toute la nuit donc que du positif ^^\r\n\r\nDu coup on remet sa pour mercredi prochain avec du "only SV-98" donc tous a vos snip !!!! \r\nje ferai une maplist avec des taille de map variante '),
(752, 12, 59, 1366904176, 81, 'Pseudo:\r\nPrénom:\r\nAge: J''ai déjà 20 ans de conneries derrière le dos :) \r\n\r\nPsn: sicilianno35\r\n\r\nBattlelog: Pour battlelog soit sicilianno35 ou CiidzZ_HD- car j''ai refait un compte pour repartir a zéro. \r\n\r\nComment avez vous connu la meute?: J''ai connu la meute sur les forum de battlefield 3 , j''ai poster une demande de recrutement et j''ai eu une réponse dans mon post de WAZA-PsY-4-x après avoir regarder le platoon sur battlelog et le forum je suis intéresser par la team. \r\n\r\nPourquoi vouloir rejoindre nos rand?: J''aimerais rejoindre les rangs de la meute car a ce jours j''ai évoluer avec quelques bon joueurs , je suis rentrer dans deux teams mais qui n''ont pas durée longtemps , maintenant j''aimerais me construire un cercle de joueur de confiance avec qui continuer de progresser passer de bon moment fun et a la fois faire de belle parties sérieuse en Teamplay , et je pense pouvoir trouver cela chez vous. \r\n\r\nParlez nous un peut de vous: Alors moi c''est sicilianno35 ou Cédric comme vous préférer :) j''ai 20 j''ai commencer les fps en ligne sur l''ordinateur avec counter strike 1.6 après avec COD4. Je suis ensuite passer sur Mw3 et black ops sur PS3 après avoir été gaver par la série call of duty j''ai acheter battlefield 3 ou j''ai directement accrocher j''ai une pause de plusieurs mois je m''y suis remis sérieusement depuis janvier. Sinon au niveau caractère je peut dire que suis un mec super sympa toujours prêt a délirer et a la fois garder mon sérieux quand il le faut je ne rage pas ou très rarement uniquement en FFA contre un full team M5k :p. \r\n\r\nVoilà pour ma candidature j''espère quelle sera valider , à bientôt pour qu''on se calle quelques frags ensemble :). \r\n'),
(753, 12, 59, 1366905160, 10, 'Reee siciliano , bon deja bienvenue dans la taniere des APAX :) c''est ici qu''on vient se reposer quand on rencontre trop de campeur noobi sur les serveurs :)\r\nPour la suite c''est tt simple , ajoute les APAX que tu croises , bon yen a une trentaine mais tu croiseras souvent les memes , une petite dizaine de joueurs :) \r\n5 joueurs peuvent accepter ton recrtuement :\r\n gyom-88 ( le rageur de l''equipe ) ,\r\n pignouf ( le posé de l''équipe )\r\nWAZA-gp_one ( le pilote de l''équipe )\r\ndartoch ( l''un de mes webmaster :) )\r\net moi !!\r\n\r\n\r\n\r\nBref ajoute nous vite pour voir se que ca donne sur le champs !!'),
(754, 12, 59, 1366906019, 81, 'Pas de problème avec plaisir :) \r\nDes que je me connecte ce soir , car en général mes horaires de co son le soir jusqu''à très tard ^^ , ça sera bien de pouvoir jouer avec vous :). '),
(755, 6, 50, 1366909462, 27, 'L''Immortel ? Pas vraiment sur'),
(756, 6, 50, 1366910501, 16, 'il est pour nous ce film : l''empire des loups\r\n\r\n[URL=http://www.hostingpics.net/viewer.php?id=7851509971.jpg][IMG]http://img11.hostingpics.net/pics/7851509971.jpg[/IMG][/URL]'),
(757, 12, 59, 1366914887, 2, 'slt et bienvenue \r\najoute mon psn quand tu aura le temps pour ma part je joue plus la journée que le soir ! \r\n je déteste aussi les joueurs M5K !'),
(758, 12, 59, 1366915579, 81, 'C''est bien ça fait plaisir de voir que je suis pas le seul a détester cette arme ^^. \r\nDes que je peut je t''ajoute :)'),
(759, 6, 50, 1366918421, 1, 'AHAH milka !\r\n\r\nAmerican Pie : Les Sex Commandements \r\n\r\nJe monte d''un bon gros poil le niveau :\r\n\r\n[IMG]http://image.jeuxvideo.com/imd/m/max_payne_film.jpg[/IMG]'),
(760, 12, 59, 1366919278, 5, 'Salut sicilianno et bien venu sur notre site ;)\r\nUne demande bien présenté déjà, c''est déjà un bon point ;) apres il faut voir sur le champ de bataille et discuté un peut avec toi donc tu peut me rajouté (psn: dartoch2)\r\n\r\nBon courage a toi pour ta demande de recrutement et a tres vite j''espere ;)'),
(761, 6, 50, 1366920275, 16, 'c''est plus un poil c''est une sacrée touffe'),
(762, 12, 59, 1366920456, 8, 'velcom et bonne chance pour ton recrutement ajoute moi bmdc64'),
(763, 6, 50, 1366920884, 16, 'Max payne\r\n\r\nPar contre Ben renomme le nom de tes images :\r\n\r\n[URL=http://www.hostingpics.net/viewer.php?id=204402504g.jpg][IMG]http://img15.hostingpics.net/pics/204402504g.jpg[/IMG][/URL]'),
(764, 12, 59, 1366973449, 10, 'Fait gaffe a baptiste , il aime bien les enfants , alors si tu le trouve avec un petit garcon sur métro , ne tkt il file au casier ac lui :) :) :) :) :) :) :) :) :) :)'),
(765, 3, 60, 1366975100, 83, 'Salut la meute !!  alors pour les infos sa donne : \r\n\r\nPseudo: Dragon\r\nPrénom:Marco\r\nAge:30 ans\r\nPsn:Creative Dragon\r\nBattlelog:Creative Dragon\r\n\r\nComment avez vous connu la meute? De part le battlelog \r\n\r\nPourquoi vouloir rejoindre nos rand?:Pour avoir une equipe active investie dans le jeu car beaucoup de teams sont dead fin tu vois 30 players mais des moments meme pas 5 sont connectés.\r\n\r\nParlez nous un peut de vous:Et bien ,je bosse dans une pizzeria, je joue au basket , fais de la boxe anglaise, fais du Bodygraff et joue aux jeux videos ( fin que a batllefield en vrai car j''ai pas le temps de jouer a autre chose et faut dire que je kiff trop les fps) \r\n\r\nVoila j''espere que votre choix en ma faveur sera favorable \r\n\r\nYESSA !'),
(766, 12, 59, 1366976344, 81, 'Je me disais aussi la dernière fois que je suis passer a côté des casiers j''entendais des bruits étrange :p '),
(767, 3, 60, 1366978618, 5, 'Salut Dragon et bienvenu sur notre site ;)\r\nMoi c''est Dartoch, l''un des recruteur de la meute\r\nJe n''est pas eu l’occasion de joué avec toi mais je suis sur que l''on va remédié a cela ;)\r\nVu que l''on a pas mal de demande de recrutement en se moment, je te conseille de venir joué sur notre champ de bataille pour faire la connaissance des loups, de les ajouté en ami et de te faire connaitre bien sur que l''on voie se que tu a dans le ventre :p et surtout ton état d’esprit ;)\r\nLes autre personnes charger du recrutement sont: Psy-4, Gyom, Gp, Pignouf\r\nSur se je te souhaite bon courage pour ta demande de recrutement et a très vite in game ;)'),
(768, 3, 60, 1366979146, 8, 'ajoute bmdc64 je suis pas recruteur mais je suis un adepte de bonnes parties'),
(769, 3, 60, 1366981760, 83, 'yess ok ok , je passerai in game et procederai a l''ajout des loups !! \r\n\r\n\r\nbmdC : que le cul te pele , je connaissai pas cette expression, jvois pas ske sa veux dire lol : explication ?? sorry \r\n\r\n a bientot les gars  !! in game !! '),
(770, 3, 60, 1366986604, 28, 'salut créative dragon, passe ce soir sur le game play pour faire quelque partis! '),
(771, 12, 59, 1366986992, 28, 'salut cedric!! passe ce soir jouer avec pour une première prise de contacte!! \r\net je rage seulement quand je joue avec des mecs qui campe o sniper ou au bipied sans mm essayer de reprendre les drapeaux ;)'),
(772, 12, 59, 1366992682, 81, 'Je suis passer jouer sur votre serveur hier soir mais je pense que tout le monde était déco ^^ , mais je serais de la partie aussi ce soir vers 23h :) '),
(773, 3, 60, 1367001164, 37, 'Ajoute toutoun80740 , bienvenu et bonne chance pour ton recrutement'),
(774, 12, 56, 1367001360, 37, 'pour moi +1 :)'),
(775, 12, 56, 1367008137, 8, 'ajoute bmdc64 on s''est deja croisé un peu par contre surtout n''ajoute pas psy4, c''est une horreur, le mec est lourd'),
(776, 12, 59, 1367022955, 10, 'Aprés plus de 118 min a tes cotés pour la partie la plus longue de ma vie , j''ai eu le temps d''étudier ton skill et ton teamplay :) Il en ressort que tu as tapé a la bonne porte , on cherche des joueurs comme toi , posé et qui peut faire la difference en one one , bien sur ya des truc a bosser comme les déplacements ou certaine temporisation mais tu auras tt le temps de voir ca a nos cotés :) Alors bienvenue a  Toi APAX-sicilianno et montre ns que tu as les crocs assez acérés :) '),
(777, 12, 59, 1367037944, 81, 'Merci c''est vraiment gentil de me faire confiance :) \r\nVous ne le regretterez pas ;) \r\nTu verra que moi aussi je peut avoir très faim :p'),
(778, 12, 59, 1367045294, 5, 'Bienvenu parmi nous mon grand ;)'),
(779, 7, 34, 1367045914, 5, 'Salut a tous\r\nIl y a 1 mois, notre site était classé 4ème quand on tapais Meute Apax sur google, nous vous avions demandé d''avoir pour objectif de nous faire remonter en 1ère place pour que les joueurs puisse avoir un meilleur visuel sur la meute\r\n\r\n1 mois mois après voila le résultat: objectif ateind !!!! avec un record de presque 100 visiteur avant hier, nous avons ateind la 1ère place !!!!!\r\n\r\nFélicitation a tous !!!!! Et quand je voie qu''aujourdhui a 9h on en est déjà a plus de 50 visiteur, cela présage que du positif !!!!!\r\n\r\nEncore bravo a tous et continuez comme sa !!!!  ;)'),
(780, 7, 34, 1367051690, 8, 'félicitations messieurs mais ce n''est qu''un début maintenant il faudrait qu''on s''investisse pas mal en versus pour encore plus se faire connaitre et maintenir un bon recrutement tout en restant raisonnable bien entendu '),
(781, 6, 50, 1367059544, 21, 'J''suis pas très calé en film Gay...\r\nT''aurai pas un indice...'),
(782, 12, 59, 1367059940, 21, 'Félicitations et bienvenu à Toi'),
(783, 12, 61, 1367063330, 86, 'Bonjour a tous et a toutes je me présente je m''appele Hugo Maria, j''ai 14 ans, je fais du foot en tant que Gardien. Je suis né le 05/09/1998 je suis d''origine Portugaise.\r\nJ''habite en France dans la région IDF (dans le département : 95) je suis Parisien.\r\n\r\nJe suis fan des jeux vidéos surtout des FPS !!!!!!\r\nMon ancienne team était la GFB ( Gloire Force Bravoure ) dont l''ex leader était slimmorle.\r\n\r\nJe suis venu postuler ma candidature chez les APAX car on ma beaucoup parler de vous !!! (en bien biensur);) et j''ai beaucoup aprécier jouer avec: WAZA-PsY-4-x,LePsYk0s3Du64 et SweelFor.\r\n\r\nMes horaires: \r\ncomme je suis toujours en 3eme, je ne peux pas jouer la semaine (en soir) mais que le week-end. je suis dispo le vendredi soir, samedi soir (pour les matchs). mais apres les vacances (apres les 13mai) je vais devoir faire un petit break pour me concentre sur mon brevet et mon Histoire Des Arts.\r\n\r\nCerte, je n''ai que 14 ans mais je suis mature. Tous ce que l''on me demande de faire je fais !...\r\n\r\nje croit que c''est tout :) merci d''avoir lu et j''espere qu''un jour je serais un APAX ! :D    '),
(784, 12, 59, 1367064004, 1, 'Pignouf le posé `\\o/'),
(785, 6, 50, 1367064087, 1, 'x) [URL=http://boitam.eu/]Page perso de Milka[/URL]'),
(786, 7, 34, 1367064199, 1, '[quote=bmdc64]félicitations messieurs mais ce n''est qu''un début maintenant il faudrait qu''on s''investisse pas mal en versus pour encore plus se faire connaitre et maintenir un bon recrutement tout en restant raisonnable bien entendu [/quote]\r\n\r\n\r\nExact. Je vais sortir un PDF avec les stats du site depuis sa mise en ligne'),
(787, 7, 34, 1367064417, 1, 'Voila :)\r\n\r\n[URL]http://www.meute-apax.fr/analytics.pdf[/URL]'),
(788, 12, 61, 1367064436, 5, 'Salut Hugo et bienvenu sur notre site\r\n\r\nEn effet on évite de prendre des membres aussi jeune mais bon comme on dit "qui ne tente rien n''a rien" ;)\r\nIl va donc nous falloir voir ta maturité, ton teamplay et surtout ton état d''esprit ^^\r\nPour cela il n''y a qu''une solution c''est joué a nos coté, je t''invite donc a joué le plus possible avec les loups de la meute pour que l''on apprenne a te connaitre \r\nJ’espère pouvoir faire une ptite partie avec toi prochainement, en attendant je te dit bon courage pour ta demande de recrutement et a très vite in game ;)'),
(789, 12, 61, 1367064638, 1, '[quote=hugo_AK47_95]Tous ce que l''on me demande de faire je fais !...[/quote]\r\n\r\nJacques à dit ... :D'),
(790, 6, 50, 1367066264, 8, 'rendez vous dans ma cellule et tape au fond?'),
(791, 12, 61, 1367066290, 10, 'Hugo , Vue la soirée passé j''ai le plaisir de te souhaiter le bienvenue !! Montre nous que tu assumeras complement le role de loup maintenant !! Félicitation , et bienvnue parmis nous :)'),
(792, 12, 62, 1367067221, 89, 'bonjour a tous je me pressente slimmorle ancien leadeur de la team gfb met classe préférer sont soutien (100 étoile ) pilote de char et aussi meccano en se moment j essaye évoluer en médecin arme favori (qbb95 classer mondialement )\r\nc est avec grand plaisir que je vous rejoins j ai de expérience de tant que organisateur de match et création de tournoi donc 1 fini et 1 en cour je précise que pour celui en cour je suis juste organisateur un grand merci a vous de m accueillir '),
(793, 12, 63, 1367069612, 87, 'Pseudo:\r\nPrénom: jean-christophe\r\nAge: 22ans \r\nPsn: tarty51\r\nBattlelog: Tarty51\r\n\r\nComment avez vous connu la meute?: slimorle qui ma parlé de vous, slimorle je te lache pas :)\r\n\r\nPourquoi vouloir rejoindre nos rand?: je recherche une team ambitieuse , avec du fairplay et du teamplay . \r\n\r\nParlez nous un peut de vous:    ba voila moi grand fan de BF , je suis assez polyvalent au niveau des classes . vehicule le mieux gérer le LAV <3, joueurs tres agressif au niveau de la gâchettes mes toujours a la réanimations ou tous service du meme genre pour les coéquipiers, bon voila jvai pas étalé mon skill ;) je rigole j''espere avoir une reponse bon jeu a vous les APAX'),
(794, 12, 63, 1367074805, 10, 'Slt tarty , bon deja couper le meneur a la 1ere partie c pas trop cool !! lol \r\nBon pas de soucie pr toi , tu as un  trés bon niveau donc bienvenue a toi , maintenant que tu es un loup APAX n''oublie pas de sortir les crocs pr c putains de campeur au bipied ^^\r\n\r\nFélicitation et bienvenue dans l''aventure !!'),
(795, 12, 62, 1367074962, 10, 'Slt slim , bon je crois qu''on a déja pas mal parlé ensemble , donc je n''es pas grand chose a te dire a part BIENVENUE et jespere que ton intégrassion se passera bien meme si je n"en doute pas !Si ya le moindre soucie entre membres ou n''importe , n''hesite pas a venir me voir , jkiff désamorcer les bombes !   \r\n\r\nAllez bienvenue encore et maintenant sort tes putains de crocs ^^'),
(796, 12, 63, 1367079923, 87, 'ok chef :D'),
(797, 12, 59, 1367081321, 81, 'Merci tout le monde :) '),
(798, 12, 61, 1367083592, 5, 'bienvenu parmis nous ;)'),
(799, 12, 63, 1367083674, 5, 'bienvenu parmis nous ;)\r\n'),
(800, 12, 62, 1367083752, 5, 'Bienvenu parmis nous ;)'),
(801, 12, 63, 1367088738, 8, 'il m''a bien fait chier cet aprem, j aime ca bienvenue parmis nous en tout cas'),
(802, 12, 63, 1367096440, 87, 'Ah ba je vous et tous vue une vrai meute enragé sur les bleus jlai et aidé un peut merci les gas'),
(803, 12, 67, 1367096990, 91, 'Bonjour la meute je suis kevcha0205 et je souhaite vous rejoindre car je suis un loup de naissance. j ai 27ans je joue principalement l assaut et je me demerde pas trop mal avec un flingue.\r\nPs: je ne m avoue jamais vaincu tant que le dernier ticket et pas dessendu'),
(804, 12, 67, 1367098000, 8, 'salut kevcha, pour ma part t''es un bon joueur et ta l''air d''etre un bon gars pour le peu que j ai jouer avec toi donc bonne chance pour ton recrtutement et je te laisse entre les mains des mechants recruteurs, joue pas avec psy il est affreux il te réa pas ... etc, une vraie pute bref bonne chance');
INSERT INTO `forum_posts` (`postID`, `boardID`, `topicID`, `date`, `poster`, `message`) VALUES
(805, 12, 67, 1367098179, 10, 'Salut Kevcha , Bon aprés avoir discuté ac gyom , nous sommes d''accord que tu ns rejoigne vue le temps que tu as deja pâssé a nos cotés , par contre dis nous ton prénom , d''ou tu viens , ton expérience des FPS et les teams que ta croisé ^^ histoire de ce faire une petite image sur toi ^^'),
(806, 12, 67, 1367098478, 91, 'mon prenom ses kevin. kev pour les amis xD\r\nLes team que j ai deja croiser les siritual en conquete, les annonymous en ruée, les iron equinox cinquete et d autre encore dont jne me souvien pas mdr.\r\nmon experience ds les fps vien de mon plus jeune age j ai commence a joue a la console a 3 ans et mon premier a 10 ans sur la ps one xD avec duke nukem \r\n'),
(807, 12, 67, 1367098528, 91, 'ps: et les APAX bien sur mdr '),
(808, 7, 34, 1367099240, 10, 'aaaa je savais que j''avais bien bossé pr le recrutement , et c pas fini messieurs !! :)\r\nen tt cas GG a vous :)\r\n'),
(809, 12, 67, 1367099602, 5, 'bienvenu parmi nous Kevcha ;)\r\n'),
(810, 12, 67, 1367099823, 91, 'Merci les loups pour cette accueille '),
(813, 12, 61, 1367128477, 21, 'Félicitations et bienvenu dans la Meute'),
(814, 12, 62, 1367128614, 21, 'Bienvenu à toi Slim'),
(815, 12, 63, 1367128836, 21, 'Bienvenu dans la Meute '),
(816, 12, 67, 1367129136, 21, 'Si t''es bien le Kevcha qui m''a plombé le cul ces derniers jours, je suis content de pouvoir jouer avec toi la prochaine fois...\r\nBienvenu à toi Kev'),
(817, 12, 67, 1367129922, 91, 'Ces bien moi lawrent33 mdr et heureux d etre parmis vous pour tout ceux qui voudrai m inviter sur le psn et sur le battlelog ces kevcha0205'),
(818, 1, 70, 1367138093, 88, 'salut tout le monde moi c''est Alexandre, connu sous le pseudo alexx93190. Né le 21 juin 1991, j''habite en région Parisienne dans le 93.\r\n\r\nFan des jeux vidéos depuis ma tendre enfance mais joue aux FPS depuis peu (depuis BF3 pour dire, décembre 2011 très précisément). Je suis également handballeur au sein du club de ma ville depuis 8 années et un amoureux du football et surtout du Paris Saint Germain.\r\n\r\nEtant ancien membre de la team GFB depuis 1 semaine mais étant très bien accueilli par Slimmorle (l''ex leader) et toute l''équipe, j''ai décidé de les suivre et la team APAX était la plus intéressante a nos yeux.\r\n\r\nMon but étant de progresser au maximum tout en jouant teamplay le plus possible, je n''hésiterais pas a me sacrifier sur le champ de bataille afin de sauver un coéquipier et à suivre tous les ordres que l''on me donnera ainsi que les stratégies mises en place.\r\n\r\nEtant à la recherche d''un emploi, je suis disponible tous les jours mais un week end sur deux pour les besoin de la team (entrainements et match).\r\n\r\nvoilà voilà c''est à peu près tout ce qu''il y a à savoir sur moi, à bientôt sur le champ de bataille les amis !'),
(819, 1, 71, 1367143305, 89, 'Bonjour a tous je me présente cyril alias slimmorle 26 ANS je vie depuis 6 ans a erquinghem lys dans le nord 59 je suis né a Dunkerque (59) ou j ai passer la plus par de mon temps dans le quartier des glacis (glacis fornia beach)j ai deux enfants un garçon qui s appelle matheo qui a 2 ans et 1 fille mayline qui aura 1 ans ancien leadeur de la team gfb qui a tenu  1 ans et demi met passion dans la vie sont les jeux vidéo ( jeux favoris les tekken fifa juste causse 2 bf saint row et les gta ) je suis un anti caull of lol je kiff aussi la musique et le catch ensuite j aime aussi la pèche surtout la pèche a anguille après se que je n aime pas c est les gros raciste les rageux mon bo pere \r\net les mauvaise[ALIGN=center][/ALIGN] nouvelle voila en gros se qu il faut savoir sur moi un grand big up a la team apax je tien aussi a remercier waza psy et a vous de nous acuellir dans votre team haouuuuuuuuuu'),
(820, 11, 40, 1367146240, 10, 'Est OUI sans grosse motivation , ta pas grand chose ^^'),
(821, 7, 72, 1367162422, 1, 'Salut les mecs, je poste ce message suite à l''entrainement.\r\n\r\nQuelques points sont à revoir ... \r\n\r\nLes mecs, les APAX, on est une unité, ARRÊTEZ DE VOUS RAGEZ A LA GUEULE PUTAIN !!! Mise à part la partie avec des strats (bmdc // manu), l''autre side n''était qu''un gros bordel. Pas de strats ou de cover, psa d''infos, les mecs qui parlent de leurs ratio, enfin bref ... Franchement, je risque de stopper les entraînements & les matchs, ça sert à rien de prévoir tout le serveur pour au final ne faire qu''un gros FFA ... \r\n\r\nJe suis presque  certain de m''attirer vos foudres, mais merde quoi ... Si une escouade se monte réellement pour partir en cup, je penses ne plus jouer qu''avec ses personnes. Bonne journée, et encore une fois, arrêtez de rager dans tout les sens, surtout envers les membres de la meute, c''est ridicule !'),
(822, 7, 72, 1367165311, 10, 'C''était un premier entraînement , tous les joueurs ne se connaissait pas , la première manche a était plutôt sympathique avec de beau placement , la deuxième un peu plus compliqué du aux départ de certains  !! Donc pour moi ça était même si on a plein de truc a revoir et a bosser :) '),
(823, 6, 50, 1367166704, 16, 'non \r\n\r\nindice : football américain'),
(824, 1, 73, 1367172298, 10, '[SIZE=4]  Meneur                                                 \r\n\r\n-----------------------------------------------------------WAZA-PsY-4-x-----------------------------------------\r\n-\r\nCo-leader : \r\n\r\n    gyom-88 \r\n\r\nWAZA-GP_one\r\n \r\n,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,\r\n\r\nRecruteur :                                             \r\n\r\nPignouf30\r\n\r\n Dartoch \r\n\r\nAnkoleloup\r\n\r\nDartoch \r\n,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,, [/SIZE]\r\nMembres :  \r\n        \r\n                 \r\n-------------------------------PsYkOsE---------------------------------Misteur-reveur-------------------------------sicilianno35--------------------------\r\n  \r\n-------------------------------faveval---------------------------------slimmorle------------------------------------ItadakimasTB--------------------------                         \r\n\r\n-------------------------------fisher----------------------------------hugo_AK47_95---------------------------------lucas-74_44--------------------------\r\n\r\n-------------------------------RS-C77----------------------------------alexx93190-----------------------------------AtOnIuM_GhAuX--------------------                      \r\n\r\n-------------------------------Tarty51---------------------------------mika44500------------------------------------Kevcha0205-----------------------\r\n \r\n-------------------------------lord-beckett59--------------------------ben_ftwc-------------------------------------clems-206-----------------------------                               \r\n\r\n-------------------------------bmdc64----------------------------------ousou78--------------------------------------bl4ck_skyread-----------------------\r\n  \r\n-------------------------------aulyro----------------------------------ittecilef--------------------------------------------Mvgusta3----------------------                                \r\n\r\n-------------------------------Toutoun80740----------------------------sweelfor-------------------------------------TweeZy------------------------------\r\n \r\n-------------------------------Rakoto-----------------------------------Typex59-------------------------------------Lebetz---------------------------------\r\n\r\n-------------------------------Simrreuhh--------------------------------lil-blakstar--------------------------------Lawrent---------------------------------\r\n \r\n,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,'),
(825, 7, 72, 1367172411, 8, 'je comprends ce que tu veux dire ben mais ce n''etait que le premier entrainement donc ca me parait normal qu''il y est des dechets faut qu''on s''ameliore '),
(826, 3, 74, 1367172979, 92, 'Prenoms :jonathan\r\nPsn:wolf16000\r\nBattlelog  :wolf16000\r\n Age:15ans (mature)\r\n\r\nBonjours la apex je voudrais rejouindre vos rend pour avoir une team ambitieuse et qui joue lobjectife a font jai se jeu depuis 2semaine et jtrouve que jmens sort plutot pas mal je fais que du positife et je joue avec toute les classe .\r\nBon jeu a vous.'),
(827, 3, 74, 1367174308, 10, 'Salut WOLF , Depuis quelques jour nous avons eu une sacrée activité au niveau du recrutement , et j''ai deja de trés bon joueurs a mes cotés , de plus ton age et ton inexperience de BF3 ne rentre pas dans nos criteres .. Ce n''es pas un non categorique , mais tu vas devoir patienter si tu désires vraiment ns rejoindre et ns montrer que tu n''es pas un de ses kikoo qui as acheté BF3 car il coutait 25 euros !! En tt cas bonne continuation pr la suite , et bon courage , tu trouveras ton bonheur ici :http://battlelog.battlefield.com/bf3/fr/forum/view/2832654490213645395/'),
(828, 7, 72, 1367174838, 89, 'je suis nouveau met avec mon expérience je trouve que sa aller je pense juste qu il faut encore entraîner et que les gens habitue un truc je j ai Aprecier  c est que les gens sont a écoute et que pense que tout les membre peuvent apporter leur touche personnelle et c est technique juste arrêter de rager les loups autant mordre quan on aura de la chère fraîche lol'),
(829, 3, 74, 1367175273, 92, 'Daccord pas de probleme je suis en train de joue dans votre serveur et jai fait le meillieur score pour linstent'),
(830, 7, 72, 1367176648, 10, 'Oui le temps , voila ce qui ns faaut !! je vais essayer de trouver un match pr cette semaine en T 5 pr bosser petit a petit ac des groupes different !!'),
(831, 1, 75, 1367178321, 81, 'Salut a tout les APAX :) \r\nJe le présente moi c''est Cedric j''ai 20 ans ça fait maintenant 1 ans que j''ai acheter Bf3 et depuis janvier de cette année que j''y joue vraiment sérieusement. Avant jetais joueur des séries Call of duty et counter strike sur Pc. Je suis donc un grand fan des fps :) \r\nAprès pour vous parler de moi j''aime bien sortir voir mes potes aller en boîte profiter de la vie quoi petite cuite de temps en temps :p. Sinon je suis étudiant a la fac de droit jai bientôt mes examen le stress aura ma peau ^^. A part ça sur le jeux je privilégie le team play j''aime pas jouer tout seul , ça peut m''arriver de rager mais dans des cas d''utilisation abusive de la m5k ou de camp bref tous ces noob qui nous pourrisse le jeux je finirais par avoir leur peaux a coup de defib ! \r\nEn général je vais beaucoup plus jouer le soir vers 23h car pour moi même si les jeux vidéo sont ma passions ça passe quand même après le reste. \r\nPour mon pseudo avant je jouais avec le compte sicilianno35 mais j''en ai refait un nouveaux pour repartir de zéro :) : CiidzZ_HD- \r\nDonc hésiter pas a m''ajouter pour se faire de bonnes parties ensemble :) \r\n\r\nAller à plus tout le monde in game histoire qu''on se calle quelque frag ensemble et qu''on montre a tout le monde de quoi notre meute affamé est capable :) \r\n\r\nCedric. '),
(832, 7, 72, 1367178778, 8, 'ce serait cool que tu nous fasse un T5 intra meute avec des equipes héterogenes et equilibré, d''une part ca nous entrainera pour les T5 et ca nous entrainera aussi a s''ecouter et a s''organiser et si chacun arrive a capter les informations importantes et a dire des choses essenielles on sera carrement plus pret parce que il y a toujours l''objectif versus ou CSL je sais plus ce que c''est en tout cas pour moi je suis chaud chaud apres c''est une proposition t ''en fais ce que tu veux au mieux tu peux la mettre dans l''anu ca fait toujours plaisir :)'),
(833, 11, 40, 1367181268, 5, 'la preuve avec la meute, la on est remonter a bloc et on voie le résultat ;)'),
(834, 3, 76, 1367181806, 93, 'Pseudo: Shuya\r\nPrénom: Shuya\r\nAge: 29 ans\r\nPsn:\r\nBattlelog: PtitShuya ([URL]http://battlelog.battlefield.com/bf3/user/PtitShuya/[/URL])\r\n\r\nComment avez vous connu la meute?: \r\npar Benftwc et après quelques recherches sur le net il s''est avéré que vous êtes la dernière meute de loups a vivre en toute liberté :)\r\n\r\nPourquoi vouloir rejoindre nos rand?: \r\nje souhaite vous rejoindre car je recherche une meute où les loups ne sont pas la pour juger,mais pour jouer en toute égalité et je me suis dit qu''une louve trouverai sa place a vos côtés! \r\n\r\nParlez nous un peut de vous:\r\nAbandonnée,je me suis retrouvée à la rue car mon maître n''aimait pas les loups aimant mordre la vie a pleines dents :D bien qu''ayant les crocs acérés je reste une louve domestiquée qui a besoin de grands espaces où le sang peut gicler ^^'),
(835, 7, 72, 1367217298, 4, 'Je vois pas pourquoi vous ragez, le mec il est positionné il t''attend, il te shoot. Ensuite tu trouveras toujours plus fort que toi. Et de plus au lieu, de gueuler parce que monsieur t''as tué, tu mets ta bouche dans les poches et tu préviens tes partenaires que le monsieur en question est positionné à tel endroit. Et de plus, je suis arrivé en retard sur l''entrainement et je m''en excuse, mais bon à mon avis j''ai bien fait. Et je pense à mon avis, que l''on a meilleurs temps de faire plusieurs T5 ou T4, car un T8 ou T12 c''est le bordel. Voilà. '),
(836, 12, 63, 1367220668, 2, 'bienvenue et rajoute mon pseudo quand tu aura 5 min sur le psn '),
(837, 12, 67, 1367220870, 2, 'bienvenue , rajoute mon id psn quand tu a 5 min '),
(838, 12, 62, 1367221041, 2, 'slt et bienvenue , quand t''aura 2 min rajoute mon id psn   '),
(839, 12, 61, 1367221193, 2, 'bienvenue rajoute mon id psn quand tu le pourra '),
(840, 3, 76, 1367221606, 5, 'Salut Shuya et bienvenu sur notre site\r\nTu peut me rajouté sur le psn (dartoch2) pour que l''on fasse quelque partie ensemble histoire de voir si tu est une louve pure race ou bien un chihuahua ^^\r\nBon courage pour ta demande de recrutement et a tres vite ;)'),
(841, 1, 71, 1367221883, 2, 'tu suis quel émission de catch ? '),
(842, 7, 72, 1367222038, 5, 'Je n''était pas la mais je me doute un peut de se qui c''est passé, je commence a très bien vous connaitre ^^\r\nSe qu''il faut bien se dire, c''est que se n''est pas en une après midi que l''on fait des entrainement au top de chez top, il faut que tout le monde trouve sa place et bien sur si tienne (les chef d''escouade donne les ordre et les autre exécute en donnant les information utile.) Pour cela il faut du temps et de l''envi, et je suis sur qu''on en est largement capable\r\n\r\nJ''en profite au passage pour vous rappelé que nous avons un compte csl (http://www.consoles.net/eu/bf3/team/7682894/)\r\nJe vous conseille donc de vous y inscrire ;)\r\nMdp: Apaxxx'),
(843, 12, 61, 1367222920, 86, 'Merci a tous les loups des APAX !!!!! AOOUUUUUU (ps: rien a voir avec la team, mais ma soeur s''appele lou donc elles est super contente XD )'),
(844, 1, 71, 1367223066, 86, 'Slimm un grand fan de catch ??? comme moi moi je suis RAW et SMACKDOWN '),
(845, 1, 71, 1367226401, 89, 'raw et smackdown d ailleur j ai bien aimer qun il on difuser westelmania je kiff hundertaker orton et triple h'),
(846, 1, 71, 1367230723, 2, 'je regarde plus trops les raw et smackdown je passe direct au main event le dernier wrestlemania étai pas mal ! quoi que undertaker contre cm punk ma un peu déçus \r\nune petite section catch sa vous branche ?'),
(847, 3, 77, 1367231549, 94, 'Pseudo: weapons345\r\nPrénom: Maxime\r\nAge: 15 ans (16 en juillet)\r\nPsn: weapons345\r\nBattlelog: http://battlelog.battlefield.com/bf3/fr/soldier/weapons345/stats/323694420/ps3/\r\n\r\nComment avez vous connu la meute?: Sur un topic battlelog.com qui recensait toutes les teams BF3\r\n\r\nPourquoi vouloir rejoindre nos rand?: Vous m''avez l''air actif et plutôt sympa ;), Rien que ce matin, je suis allez sur votre serveur j''ai mis mon micro et j''ai fait la connaissance d''un membre vraiment cool, et mature, sit out les membres sont comme sa .. :D\r\n\r\nParlez nous un peut de vous: je ne joue quasiment qu''en assaut, j''aime le teamplay et je suis axer M16 :D Pour parler de ma vie perso, J''habite a Paris, Je suis en 2nd bac pro ^^, Sinon je joue aussi a d''autres jeux, notamment Minecraft et StarCraft II sur PC, sur PS3 je ne joue qu''à BF3, GT5 et GTA4 :p\r\n'),
(848, 3, 76, 1367231679, 1, 'Tosh, faudrait vraiment qu''on corriges le template de demande de recrutement, car "rejoindre nos rang" ... :D\r\n\r\nSinon, +1 of course :-)'),
(849, 7, 72, 1367239982, 25, 'Pour ma part bonne première partie avec plusieurs tentative de bac ect avec Reaper malgré quelques incompréhensions en escouade... Après 2nd partie un peu catastrophique mes on s''améliore avec le temps !'),
(850, 6, 50, 1367249446, 16, 'alors les pti loups vous bloquez??'),
(851, 3, 76, 1367252546, 10, 'Mdrrrrr oui j''avoue c pas crédible ^^ \r\n\r\nJ''espere te voir rapidement sur le champs de bataille a te prendre tous les murs possible ^^ Puis on discutera de l''avenir d''un loup autiste dans les APAX xD'),
(852, 3, 76, 1367256799, 5, 'Je Vous Merde !!!!!! lol et puis c''est très bien "rejoindre nos rang", non? :p'),
(853, 6, 50, 1367257012, 5, 'perso je voie pas du tout :(\r\n'),
(854, 10, 78, 1367260829, 89, 'T8 \r\nescouade de 4 pour le lav\r\n4 mecano\r\nrôle + équipement\r\nIl faudrais essayer c est de mettre 4 personne dans le lav (se que je conseille pou le lav blindage fumer ou vision thermique et les obus apfsds-t) une personne qui conduit et regarde devant lui l autre en deuxième place couvre et tir se tous se qui bouge et deux meccano qui répare prener le premier point et detruiser le lav ensuite jouer en A et b \r\nescouade de 4 médecin ou 3 médecin et 1 soutien\r\nil faudrait que cette escouade garde point B rien de plus simple\r\nLes point apportant \r\n 1)ceux qui sont dans le lav appuyer le plus souvent sur strat pour voir quand le lav ennemi aparé \r\n 2)ceux dans le lav essaye de tirer dans la ruelle pour aider l escouade sur b\r\n 3)si en cas il ni a pas de soutien au début un joueur se met en soutien et cash un caisse de munition sur b sa peu aider\r\nT12 \r\nidem que pour le T8 sauf que on rajoute une escouade de 4 si posible 4 medecin et cette escouade ocupe du dernier point une foi que tout les point sont capturer nous formons une ligne et en bouche tout les sorties en gros spawn kill\r\nvoila une petites strat a essayer votre avis les loups merci '),
(855, 3, 76, 1367261459, 1, '[quote=WAZA-PsY-4-x]Mdrrrrr oui j''avoue c pas crédible ^^ \r\n\r\nJ''espere te voir rapidement sur le champs de bataille a te prendre tous les murs possible ^^ Puis on discutera de l''avenir d''un loup autiste dans les APAX xD[/quote]\r\n\r\nApaX_Autiste ... ça rox j''trouve !!!\r\n\r\nToch, faut recruter de l''autiste !\r\n\r\n( Manu, sort de la queue s''il te plait, les enfants sont pas admis :D )'),
(856, 10, 78, 1367264332, 5, 'Sa peut etre pas mal, a essayé ;)\r\n'),
(857, 3, 76, 1367268915, 10, 'mdrrrr bon d''accord ^^'),
(858, 6, 50, 1367300142, 16, '2ème indice mais en image \r\n\r\n[URL=http://www.hostingpics.net/viewer.php?id=97167414999.jpg][IMG]http://img15.hostingpics.net/pics/97167414999.jpg[/IMG][/URL]'),
(859, 6, 50, 1367317266, 5, '"Mi-temps au mitard" ?'),
(860, 6, 50, 1367319177, 16, ':biggthumpup:'),
(861, 6, 50, 1367320244, 25, 'lol g2'),
(862, 3, 76, 1367320473, 25, 'Bonjour Shuya bonne chance pour t''as candidature a bientot sur le terrain :)'),
(863, 6, 50, 1367320625, 5, '[URL=http://www.casimages.com/img.php?i=1304300119336025511138095.jpg][img]http://nsm08.casimages.com/img/2013/04/30//1304300119336025511138095.jpg[/img][/URL]'),
(864, 6, 50, 1367346529, 2, 'kill bill 2\r\n\r\nfacile j''adore ce ce film le 1 et le 2'),
(865, 6, 50, 1367346875, 2, '[IMG]http://images.allocine.fr/medias/nmedia/18/35/16/36/20072580.jpg[/IMG]'),
(866, 7, 79, 1367350336, 34, 'Coucou a tous j''avais une idée en tête celle de la Spécialisation je m''explique c''est très simple chaque loups a c''est préférence de classe mais il faut que chacun sois le meilleur dans sont kit :) donc exemple pendent 1 ou 2 semaines un groupe de 4 ou 8 personnes iront dans des serveur juste pour jouer avec un kit en particulier :) \r\nje me porte garant du kit sniper si certains sont pour ajouter ApaX_TweeZy '),
(867, 10, 78, 1367364646, 1, 'Pas mal slim. Thanks'),
(868, 6, 50, 1367364818, 93, 'Alien la ressurection ^^\r\n\r\n[IMG]http://image.noelshack.com/fichiers/2013/18/1367365018-tatanka.jpg[/IMG]'),
(869, 6, 50, 1367384592, 2, 'danse avec les loups'),
(870, 3, 76, 1367388583, 91, 'Bonne chance pour ton recrutement Shuya et a bientot in game xD'),
(871, 3, 77, 1367394594, 94, 'Personne ? :/'),
(872, 3, 77, 1367395690, 5, 'désolé weapons, on est un peut débordé en se moment, beaucoup de recrutement en cour et d’évènement a organisé ;)\r\nPour ton recrutement je te conseille de passé le plus de temps possible a nos coté in game pour te faire connaitre et que l''on voie ton état d''esprit et ta maturité\r\n\r\nBon courage a toi pour ta demande de recrutement ;)'),
(873, 3, 77, 1367397404, 8, 'bonne chance pour ton recrutement, ta l''air d''avoir un bon niveau j''ai vite fait jouer avec toi'),
(874, 3, 77, 1367404330, 94, 'Ok merci  ! Je passerais sur le serveur cette aprem ;)'),
(875, 6, 80, 1367411667, 95, '[COLOR=#FF0000]Heia  tous c''est xTyFlo , je sais pas si vous vous souvenez de moi mais ay fort longtemps j''était parmi vous , mais hélas les études on repris le dessus :( , ayant une nouvelle envie de bf3 surtout avec le dernier pack end game  *-* , et l''envie de rejoué , je me retourne vers vous la apax oui ^^ la seul est unique team que j''ai eu sur Bf3 ^^ , c''était pour savoir si vous vouliez encore de moi aprés 4-5 mois d''inactivité total xD , et comme je me suis pas vu dans la liste des menbres je fais ce topic pour savoir si je suis dans la meute ou pas ? \r\n\r\n\r\nvoila merci de me répondre , tchouss :D  [/COLOR]'),
(876, 6, 80, 1367423390, 5, 'Salut Tyflo, on a tous des priorité dans la vie et les étude, pour ceux qui sont encore dedant en est une ^^ (meme si faire un ptit tour sur le site ou FB ne prend pas une journée... :(  )\r\nIl ne me semble pas que tu est été évincé de la meute, le fait que tu ne soit pas dans la liste ne doit etre qu''un oubli du a ton absence derniere, mais cela devrai etre rectifié je pense ;)'),
(877, 6, 80, 1367427628, 95, 'je suis passé sur le fb en mettant un message , la team apx n''est pas inactive j’espère ? :( , jai pus voir que jj_herroz n''était plus la , naist69 aussi , clem aucune  nouvelle sa fais beaucoup quand même , en espérant que la apax sois active '),
(878, 6, 80, 1367428332, 2, 'on est actif ,  certain on fait le choix d''aller dans d''autre équipe ce qui arrive assez souvent en fonction des joueurs'),
(879, 6, 80, 1367428798, 95, 'pourtant naist et heroz , je m''entendais bien avec les deux m''enfin :( '),
(880, 10, 78, 1367451866, 10, 'j''avoue que mecs dans le Lav  , ca peut faire la difference :)'),
(881, 6, 80, 1367452064, 10, 'loool inactif ???? xD , je crois qu''on a jamais était autant actifs sur le serveur fb et site :) d''ailleurs je commence a saturer un peu looool (  10 h de bf3 aujoird''hui , jvais me couché lool)'),
(882, 5, 81, 1367462609, 98, 'demande de T12 le samedi 18 mai, 21h30\r\n\r\n[COLOR=#FF0000]maps [/COLOR]: \r\n-la notre : opération tempête de feu \r\n-la votre : ?\r\n\r\n[COLOR=#32CD32]règles [/COLOR]:\r\n\r\nno Rules\r\n\r\n[COLOR=#4B0082]serveur[/COLOR] :\r\n\r\nEndLine.Nameless\r\n\r\n\r\nmerci de me confirmer vos disponibilités rapidement pour que nous puissions valider cette date ou éventuellement une autre si, celle-ci ne vous convient pas.'),
(883, 4, 82, 1367481057, 84, 'Bonjour la meute APAX !!!!\r\n\r\nAprès avoir lu votre présentation et vous avoir vu plusieurs fois sur les serveurs battle... Je me permet de venir vous présenter notre team \r\n\r\nNous sommes une team créée il y a peu de temps par 3 amis, les [FLAM] "French Legion Of Armored Men" \r\nNous sommes actuellement au nombre de 6 et nous avons une grande envie de nous développer ( recrutements, acquisition serveur TS et serveur battlefield ). Nous avons crée cette team afin de participer à différents types de match amicaux (pour le moment ) mais aussi afin de jouer régulièrement avec d''autres membres battlefield 3 dans la bonne humeur et le plaisir !\r\n\r\n\r\nLe Staff de la team [FLAM] :\r\n\r\nChoub62 (leader, webmaster)\r\nArRuaCeiroOo (leader)\r\nPuPa_Jim (leader)\r\ntHe-BlacKWatch (soldat) \r\nBoXeUr3112 (soldat)\r\net Django-_-Beast (soldat recruté recement)\r\n\r\nNous espérons acquérir de nouveaux membres motivés et sympathiques très bientôt afin de pouvoir jouer sur d''autres types de matchs!!!!!\r\n\r\nN''hésitez pas à venir faire un tour sur notre site (venez vous y présenter) et sur notre battlelog pour plus d''informations ou pour nous défier \r\n[URL]http://clanflam.xooit.fr/portal.php[/URL] ou sur [URL]http://battlelog.battlefield.com/bf3/fr/platoon/2832655241672019085/stats/[/URL]\r\n\r\nA très bientôt, bon frag\r\n\r\nL''équipe [FLAM]'),
(884, 3, 83, 1367488238, 99, 'Pseudo: Eiiwox\r\nPrénom: Nathan\r\nAge: 15 au mois d''Aout\r\nPsn: Eiiwox\r\nBattlelog: http://battlelog.battlefield.com/bf3/fr/soldier/Eiiwox/stats/873208766/ps3/\r\n\r\nComment avez vous connu la meute?: J''ai connu la meute sur un topic créé sur le Battlelog\r\n\r\nPourquoi vouloir rejoindre nos rang?: J''ai choisit vos rang car j''adore joué teamplay et dans la bonne humeur. De plus j''adore les loups car il sont soudés comme une escouade dans Battlefield 3.\r\n\r\nParlez nous un peut de vous: Je suis un gamer depuis que j''ai 5 ans. Je joue beaucoup aux FPS (BF3) et aux jeux de Foot (FIFA 13) mais aussi aux jeux sur ordinateur (Minecraft, LOL)\r\n\r\nVoilà, merci à vous d''avoir lu ma candidature.\r\n\r\nCordialement Eiiwox.\r\n'),
(885, 3, 84, 1367490354, 100, 'Je suis x-LE-GAULOIS dit CRIXUS LE GAULOIS INVAINCU  j ai 26 ans mon prenom est (ROMAIN et oui c sérieux lol) j ai évolué jusqu au niveau 100 sur un precedent compte et je voudrais recommencer l aventure différemment j''ai postulé car j ai déjà vu votre clan sur le jeu et il faut dire que le discours niveau recrutement était bien fait ,je suis une personne qui a de l humour et sérieux quand il le faut voilà faite votre choix.'),
(886, 3, 84, 1367491204, 28, 'salut crixus, le début de t''as présentation m''a bien fais rire (étant un amateur de cette série ^^ )je suis le cofondateur de cette team avec PsY-4-X, commence par jouer avec nous et contre nous, fais toi connaitre de tout les ApaX, une quarantaine environs avec pas mal d''actif!! hésite pas à mettre ton micro pour bien rigoler et si ça le fait nous te le ferons savoir très vite!!!! ajoute moi sur le psn (gyom-88) et tt les autres.\r\n\r\nA mon signal déchaînes les enfers! ^^ '),
(887, 3, 83, 1367491526, 28, 'salut Eliwox, et bien d''abord d''avoir choisi notre team!! ton âge peut poser un problème pour ton recrutement tu as dû le voir sur le topic!! passe jouer sur le serveur ApaX, nous avons déjà fait quelques exceptions pour certains loups alors fais toi connaitre de la meute et suivant tes motivations nous verrons bien ce que cela peut donner ^^ \r\n\r\nA mon signal déchaînes les enfers  '),
(888, 6, 85, 1367491635, 89, 'Bonjour a tous je voudrais savoir si il y a des pilote avion et de helico ou si vous aimeriez le devenir car effectivement pour nos prochain match si nous ou la team qui nous affronterons propose une map aérien[FONT=arial][SIZE=3][COLOR=#00008B][ALIGN=center][/ALIGN][/COLOR][/SIZE][/FONT] sa serai bien avoir nos pilote sa peu vraiment nous aidez  '),
(889, 3, 84, 1367491638, 100, 'OK  PAS DE SOUCI CRIXUS EST TOUJOURS PRET je veux que le  SANG de mes ennemies se déverse sous les pieds du gaulois invaincu'),
(890, 3, 84, 1367492470, 28, 'j''adore ^^'),
(891, 3, 86, 1367494055, 101, 'Pseudo: C83Zinio\r\nPrénom: Alexandre\r\nAge: 14ans mais 15 an aux mois d''octobre\r\nPsn: C83Zinio\r\nBattlelog: C83Zinio\r\n\r\nComment avez vous connu la meute?: J''ai connu la meute sur Facebook.\r\n\r\nPourquoi vouloir rejoindre nos rang?: J''ai choisit vos rang car j''aime jouez en équipe et je chercher une Team.\r\n\r\nParlez nous un peut de vous: Je suis un gamer depuis que j''ai 8 ans. Je joue à BF3 et à GT5\r\n\r\nVoilà, merci à vous d''avoir lu ma candidature.\r\n\r\nCordialement C83Zinio\r\n'),
(892, 12, 87, 1367494964, 97, 'Pseudo: Yosusu44\r\nPrénom: Sullivan\r\nÂge: 16ans\r\nPsn: Yosusu44\r\nBattlelog: Yosusu44 (désolé pas de lien, je suis sur iPod)\r\n\r\n Comment avez vous connu la meute ?\r\nJe suis ancien GFB, c''est donc Slimmorle qui a conseiller cette team à plusieurs anciens GFB.\r\n\r\n Pourquoi vouloir rejoindre nos rang ?\r\nPour rester avec mes potes de l''ancienne GFB, puis ayant fait plusieurs parties sur le serveur APAX, j''ai vu que la team avait du niveau et que les membres étaient actif et matures.\r\n\r\n Parlez nous un peu de vous\r\nJe suis assez bon infanterie, je joue la plupars du temps en assaut, je suis aussi très bon conducteur de blindé léger et lourd, j''aime le teamplay, je n''aime pas jouer seul. IRL je suis au lycée en seconde générale'),
(893, 12, 87, 1367495376, 89, 'tres bon joueur et tres fair play il a oublier de rajouter sa map ^preferer la seine il et trop for dessus  big up poto'),
(894, 12, 87, 1367497964, 10, 'Up pr toi yosusu , j''espere que le meneur t''acceptera :) hihi bienvenue parmis nous , tu mérites largement ta place en esperant que tu sois heureux d''etre aujourdhui un loup , montre nous tes crocs et prouve nous que nous avons eu raison de t''avoir fait confiance !! Arrache moi tous c''est campeur :) \r\n\r\nEncore félicitaiton , et ajoute les loups et la page fb !!! bye'),
(895, 6, 85, 1367506225, 95, 'Moi j''aimerai bien apprendre mais sauf que pour les véhicules aériens, je me crash obligatoirement alors xD.... '),
(896, 6, 80, 1367506341, 95, 'ah c''est cool sa :D ! je me dépeche de finir rapport de stage , Dossier pse , anglais etc et j''arrive :D !'),
(897, 6, 85, 1367507307, 91, 'Moi jss pilote de jet et je demerde assez en viper et le little je gere aussi voilou pour ceux qui veule aprrendre aller en superiorite aerienne sa monte plus vite que en conquete pour debloque les equiment de jet '),
(899, 6, 80, 1367508315, 95, 'aussi j''aimerai bien etre accepter sur battle log car depuis que je vous ait rencontré je suis tjr en attente sur : http://battlelog.battlefield.com/bf3/fr/platoon/2832655241278487312/ donc après 6 mois j''aimerai être accepté quoi x) '),
(900, 12, 87, 1367511979, 94, 'J''ai jouer avec toi et tu ma l''air bien sympa, bon niveau :) +1'),
(901, 3, 86, 1367512328, 5, 'Salut C83Zinio, bon alors déjà -1 pour ta présentation, il y a une fiche a utilisé en copié collé pour fournir toute les information, donc je t''invite a reprendre ta demande en suivant les indication donné ;)'),
(902, 3, 84, 1367512555, 5, 'Salut le Gaulois et bien venu sur notre site ;)\r\nPourai tu donné les infos demandé dans la fiche type de recrutement (psn, battlelog,...)?\r\nMerci d''avance\r\nBon courage pour ta demande de recrutement ;)\r\n\r\n(psn: dartoch2)'),
(903, 4, 82, 1367513127, 5, 'Salut Choub et bien venu sur notre site ;)\r\nJe n''est pas eu l''occasion de joué avec vous (du moins je croie ^^ ) mais j’espère que cela changera sous peut ;)\r\nN''hésité a venir sur notre serveur pour que nous fassions connaissance et pour se faire de bonne partie ensemble ;)\r\nSi vous souhaité faire des matchs, on devrait pouvoir s''arrangé :p\r\nJ’essayerai de passé sur votre site rapidement, en attendant je vous dit bon courage pour votre création de team et a très vite ;)'),
(904, 3, 83, 1367514734, 8, 'bonne chance Eiiwox ajoute moi ou viens sur notre serveur on s''y croisera'),
(905, 3, 84, 1367515140, 8, 'Yes Domina, bonne chance le gaulois ajoute moi ou va sur le serveur et on s''y croisera'),
(906, 3, 86, 1367516460, 28, 'salut C8, ton age pourrait  être un problème pour ton recrutement\r\n'),
(907, 3, 86, 1367517472, 101, 'qu''elle age mini mais je joue que avec des personne de 18, 30 ans'),
(908, 3, 84, 1367518314, 100, 'Merci les gars mon pseudo et x-LE-GAULOIS '),
(909, 3, 86, 1367520671, 5, 'normalement l''age mini c''est 18 ans, même si il nous arrive de faire des exceptions, mais tout d’abord rempli le formulaire ^^'),
(910, 6, 88, 1367528355, 10, 'je vous laisse le lien les louloups :)\r\n\r\nhttp://youtu.be/aLGPG5at18w'),
(911, 5, 81, 1367536128, 104, 'Merci d''avoir supprimé mon profil\r\n\r\ndois-je en déduire que vous avez peur d''un T12 ? un T8 vous plairez plus ?\r\n\r\nPas la peine de supprimé mon profil une seconde fois, je suis juste la pour une demande de scrim amical, non foutre la merde comme certain le pense (CC GP^^^^) \r\n\r\ndonc merci de me répondre au plus vite et en cas de refus, merci d''expliquer la raison, et non de supprimer mon profil pour éviter d''éventuelle conversations.'),
(912, 5, 81, 1367537278, 2, 'cc \r\npas moi qui est supprimé ton profile ! je pense que tu peut toujours rêver pour ton T8\r\nperso et sa n''engage que moi je n''est pas envie de  jouer contre toi que ce sois en match comme en amical \r\net je trouve sa assez culotté de ta part de revenir ici !'),
(913, 5, 81, 1367569609, 5, 'Salut Naist\r\n\r\nAlors pour un T12, sa m''étonnerai mais bon c''est on jamais, ils faut qu''on voie sa avec les admins et chef d''escouade\r\n\r\nEnsuite pour ton problème de profil, il me semble que tu avait quitté la meute avant que nous arrivions sur notre nouveau site, donc si tu n''était pas inscris c''est normal qu''il n''y est pas ton profil....\r\n\r\nEnfin, c''est ton 1er mess sur le site et tu fait déjà de la provoc ([I]dois-je en déduire que vous avez peur d''un T12 [/I])... je ne croie pas que se soit très malin sachant les problèmes que tu a eu avec certain de la meute avant ton départ... les noname aurait mieux fait d''envoyé une personne neutre pour cette demande... enfin se n''est que mon avis ^^'),
(914, 6, 88, 1367569763, 5, 'Belle petite vidéo, juste le hurlement quelque seconde trop longue pour être bien calé avec la vidéo ^^\r\nDe plus c''était une bonne petite partie, même si certain des loups nous on donné des difficulté ... :p'),
(917, 10, 91, 1367572233, 10, 'Inscrivez vous ici , pas de blabla , présent ou pas présent !!\r\nMerci\r\n\r\nVoici les regles :   \r\n\r\n[B] T12 ( A COMFIRMER , T8 peut etre [/B]) en conquete / 2 maps\r\n\r\n- La map se joue en deux manches (Une fois RUS, et l''autre USA).\r\nUne map est gagnée si l''équipe remporte les 2 manches. Si chaque équipe a remporté une manche, l''équipe gagnante est celle qui aura gagné avec le plus de tickets restants.\r\n\r\nSi l''équipe A gagne la 1ère manche 40-0 et perds la suivante 0-27, elle aura gagné la map, car ses tickets restants sont supérieurs au tickets restants de l''équipe adverse ( 40<27)\r\n\r\n\r\n\r\n________________________________\r\n\r\n\r\nClasses et armes.\r\n\r\n\r\n\r\n◘ ASSAUT \r\nArme principale : Toutes les armes autorisées, excepté les fusils à pompe automatiques et semi-automatiques \r\nArme secondaire : Toutes les armes autorisées \r\nGadget 1: Médikit / arbalète / M320 / M320 fumigène / GS M320 \r\nGadget 2: Défibrillateur\r\n\r\n◘INGENIEUR \r\nArme principale: Toutes les armes autorisées, sauf fusils à pompe automatiques et semi-automatiques\r\nArme secondaire: Toutes les armes autorisées \r\nGadget 1: Stinger / IGLA - JAVELIN - RPG7V2 / SMAW\r\nGadget 2: Robot NEM - Outil de réparation / Mine\r\n\r\n◘SOUTIEN \r\nArme principale : Toutes les armes autorisées, excepté les fusils à pompe automatiques et semi-automatiques \r\nArme secondaire: Toutes les armes autorisées\r\nGadget 1: Munition\r\nGadget 2: C4 / mortier / arbalète\r\n\r\n◘SNIPER \r\nArme principale: Toutes les armes autorisées, excepté fusils à pompe automatique et semi-automatique, ainsi que snipers automatiques\r\nArme secondaire: Toutes les armes autorisées \r\nGadget 1: Marqueur laser / Micro-drone / DAS / arbalète\r\nGadget 2: Balise radio \r\n\r\n\r\n\r\n[B]PRESENT : WAZA-PsY-4-x / Gyom-88 / LaPsYkoSe / Tarty / Alex / Ryukio / Kevcha / Slim / Tweezy / Reaper / Black ben[/B]\r\n\r\n\r\nComposition : Elle vous seras donné dimanche soir , tous le monde ne pourras pas participer vue les présent , mais ceux qui ne participe pas a celui la seront prioritaire pr le prochain !! merci de votre comprehension\r\n\r\n'),
(918, 10, 91, 1367574047, 87, 'présent chef :)'),
(919, 10, 91, 1367574648, 30, 'J''en suis ! Prenez gardez à vos miches !'),
(920, 10, 91, 1367574679, 88, 'présent !'),
(921, 5, 81, 1367575358, 10, 'C''est moi qui est supprimé ton profil , je trouve ca assez culotté que tu reviennes vue le boxon que tu as foutu ici ;) Alors je ne pense pas que ca vienne de la peur vue le niveau que tu as , Evidemment je ne parle pas de ton équipe qui doit surement avoir un excellent niveau !! Bref , rapelle toi que BF3 c''est pour le fun , et la seul personne qui pourrait me faire peur c''est celle qui te fera comprendre que grande gueule ne rime pas avec grand homme ;)\r\nAllez a plus jule(s) et bon courage pour ton looooong chemin vers la maturité !!  '),
(922, 10, 91, 1367575846, 10, '[FONT=courier new][B]Ca sera pas le 18 juin mais je reponds a l''apelle !![/B][/FONT]'),
(923, 10, 91, 1367576751, 2, 'absent'),
(924, 3, 86, 1367577169, 14, 'Aie aie ton age, c''est plutôt jeune quand même mais après comme a dit dartoch il y a des exceptions mais commence déjà par faire une bonne fiche de présentation et après on verra, voila sinon bah bonne chance mec, ciao  \r\n'),
(925, 10, 91, 1367580096, 91, 'Présent si vous vouler bien d un loup enrager a vos cotés mouhahaha'),
(926, 4, 82, 1367580465, 1, 'Salut Choub. VANNE DE MERDE >>>\r\n\r\nC''est qui votre capitaine les FLAM ? (hahahahahaha)\r\n\r\nS''il vous faut un coup d''main niveau web, je me met à votre disposition pour répondre à vos questions ;)\r\n\r\n\r\nben_ftwc'),
(927, 3, 86, 1367580543, 1, 'Ouch l''age :/\r\n\r\nTu devrais refaire ta fiche histoire d''être tranquillou =)'),
(928, 7, 92, 1367580643, 1, 'Salut les filles, Shuya m''as remontée une idée pas trop mal quand on regardes les 10 dernières postu, 75% sont des < 18 ans. Il faudrait peut être envisager 2 choses, soit fixer un age minimum d''entrée écrit en clair avec pour message : NE POSTULEZ PAS SI VOUS AVEZ PAS D''MOUSTACHE !\r\n\r\nSoit ouvrir une nouvelle section "APAX Junior" spécialement conçue pour eux ;)'),
(929, 3, 84, 1367580766, 1, 'Salut l''gaulois !\r\n\r\nJe t''invite à ajouter la fiche de renseignements histoire de pas avoir à t''emmerder 15x pour avoir les infos ^^\r\n\r\nTrès originale façon de poster une candi, j''aime ! ^^'),
(930, 3, 83, 1367580800, 1, 'Bonne chance pour ton recrutement, Cya IG (ben_ftwc)'),
(931, 12, 87, 1367580841, 1, 'HF Yosu :)'),
(932, 3, 77, 1367580867, 1, 'Bonne chance (ben_ftwc)'),
(933, 6, 93, 1367580906, 89, ' [FONT=arial][COLOR=#0000FF]                       entrainement dimanche 5 mai a 15H\r\n[SIZE=4]\r\nBonjour a tous je organise mon premiers entrainement dans la team apax je voudrai avoir le plus apax possible pour pourvoir faire un 12 vs 12 apax contre apax si nous somme moins je ferai genre un 10 vs 10 ou moins met je voudrai avoir que les apax .\r\nDétail :\r\n1)apprendre a jouer ensemble \r\n2)faite comme si vous étiez en match\r\n3)les escouades de 4 seront composer avec les apax que vous vous attendez le mieux\r\n4) définissez   dans votre escouade 1 chef escouade qui lui seul mettra voix équipe et passera toute les informations a l autre escouade ( tout information et bonne a prendre ex  si vous dite char a droite dite bien char neutra et char détruit se n est que un exemple )\r\n5)si vous êtes pilote aérien n esiter pas a vous entraîner car on en a besoin de pilote\r\n6)Mise en place des strat\r\nLes MAPS ( aller retour)\r\nMETRO\r\nKARG \r\nNombre de ticket 100 pourcent\r\nheure total de entrainement a peu entre 2H est 2H30 \r\nPS:tener moi aucouran pour votre presente via le site ou le msg que j ai poster sur facebook \r\nmerci\r\nAAAAAAAAAAAOUUUUUUUUUUUUUUUUUUUUUUUUUUU\r\n \r\n[/SIZE][/COLOR]\r\n[/FONT]\r\n '),
(934, 5, 81, 1367581034, 1, 'Peur ? Passe devant mon canon fiston ;)\r\n\r\nJ''en ai déjà bouffé des plus maigres et plus malades que toi !'),
(935, 6, 93, 1367581038, 81, 'Tu peut me compter dedans , ça fera plaisir un bon entraînement ! '),
(936, 10, 91, 1367581127, 1, 'J''ai fait l''aller, le retour me semble forcé !!!\r\n\r\nCheers ;)'),
(937, 6, 85, 1367581197, 81, 'Ben écouté moi ça me dirais bien , après le problème ce qui m''a dissuader de continuer a fond le pilotage c''est les pauvres mec qui passe leur partie avec un igla ou stinger dans les mains a te faire chier ou marquage laser + javelin c''est horrible de faire que des partie ou ton helico fait que de d''être verrouiller , mais sinon ouai pourquoi pas :). '),
(939, 6, 80, 1367581309, 1, 'Floooooooooooooooooooooooooooooo    REEEEAAAAAA !!! ^^\r\n\r\nTu peux add ben_ftwc, j''étais pas sur mon compte hier (ptit_shuya)'),
(940, 5, 94, 1367581385, 84, 'Bonjour les loups !!!\r\n\r\nSeriez vous interessés par un 4vs4 squad rush promod normal ce soir pour 21h30 sur votre serveur ( nous ne disposons pas du notre pour le moment )??\r\n\r\nSi vous êtes intéressés, nous sommes sûr d''être au complet et de pouvoir jouer :)\r\n\r\nGoo day à tous ;)'),
(941, 6, 85, 1367581389, 1, '''me demerde de mieux en mieux viper, mais me manque un binôme gunner :('),
(942, 6, 93, 1367581436, 1, 'Je reviendrais du Teknival, mais je vais essayer d''être clean IG ^^'),
(943, 5, 94, 1367581512, 1, 'Moi je suis dispo ! ( FIRST )\r\n\r\nPour 21h30, par contre, quelle map pour vous ?'),
(944, 6, 93, 1367581569, 10, 'Pas la , mais j''espere que tu auras du monde ^^'),
(945, 10, 91, 1367581666, 89, 'present aaaaouuuuuuuuuuuuuuu'),
(946, 5, 81, 1367583118, 8, 'personnellement je ne veux  pas j ai un peu peur de toi et de ton équipe, un jour j''ai fait un cauchemar ou tu me roulé dessus'),
(947, 5, 81, 1367583154, 8, 'Eeeeeeeeeeeeeennnnnnnnnnnnnooooooorrrrrrmmmmme Blllllllllaaague'),
(948, 5, 94, 1367583228, 84, 'nous prendrons metro je pense ou grand bazaar... ou caspien.... LOOL\r\n\r\nEnfin pour le moment c''est assez flou... :D'),
(949, 10, 91, 1367583371, 8, 'la faucheuse sera présente et cette fois ci je vais pas les louper'),
(950, 5, 94, 1367583441, 8, 'si vous etes pas assez il y a moyen que je sois la '),
(951, 7, 92, 1367584379, 37, 'c est pas une mauvaise idée pour les APAX Junior!!\r\n si sa ne serai pas trop galère a gérer nos charmante petite tête blonde???lol'),
(952, 5, 81, 1367588128, 1, '[quote=Apax_Reaper]Eeeeeeeeeeeeeennnnnnnnnnnnnooooooorrrrrrmmmmme Blllllllllaaague[/quote]\r\n\r\n\r\n[IMG]http://www.mes-coloriages-preferes.com/Images/Large/Personnages-celebres-Troll-face-Troll-face-lol-48104.png[/IMG]'),
(953, 5, 94, 1367588166, 1, '[quote=choub]nous prendrons metro je pense ou grand bazaar... ou caspien.... LOOL\r\n\r\nEnfin pour le moment c''est assez flou... :D[/quote]\r\n\r\nJ''ai cru comprendre :p\r\n\r\nOn voit ça pour ce soir t''façon ;)'),
(954, 6, 85, 1367589823, 81, 'Moi si tu veut je veut bien faire gunner , jaile bien piloter mais être en gunner aussi du moment que tous les ennemis en face meurt grâce a nous :) '),
(955, 10, 95, 1367590517, 89, 'Stratégie créé par le loup slimmorle\r\n\r\n                 Stratégie  métro\r\n                     Conquête T8 \r\n        COTER USA (départ prêt du c)\r\nComposition soldat escouades \r\nAlpha   : Medecin (sprint)\r\n             Medecin ( chargeur )\r\n            Medecin (grenade  )\r\n             Medecin (furtivité )\r\nBravo   medecin (sprint)\r\n            Medecin (chargeur)\r\n           Medecin (grenade)\r\n           Soutien    (explosif)\r\nDépart : 1 un soldat prend le premier point seul le reste part directement sur le point b une escouade part directement escalier rouge et l’autre escouade prend le premier escalier\r\nSuite du match : rien de plus simple on leur laisse le point A et gardons le b et c (couvrir les 3porte si dessous la position) \r\nPosition des escouades :\r\nEscouade alpha  2 médecin à gauche qui couvre  les porte de   gauche  2 medecin dans la billetterie couvre les porte du milieu \r\nEscouade bravo  2 medecin billetterie couvre porte milieu 1 soutien à droite o niveau du guichet poser o bi pied avec le medecin  couvre a droit \r\nEn cas ou la team arrive a passer : 2soldat devront penser à prendre le point A des que le point B et reprit tous les monde reprend sa position\r\nPOINT IMPORTANT\r\n1   ) SURTOUT NE PAS LES PRENDRE EN SPAWN KILL\r\n2) SPOTER UN MAXIMUM ET DONNER LE PLUS INFORMATION POSIBLE\r\n3) POUR LE SOUTIEN QUAND TU ET POSER A DROITE QUAN ON LES BLOG PENSE A METRE DES CLAYMORE A DROITE EN LIGNE ET POSE TES MUNITION JUSTE A COTER DE TOI POUR LES MEDECIN VIENNE SE RAVITALLIER\r\n4) POUR LES MEDECIN TUER PUIS REANIMER\r\n5) PENSER TOURJOUR A QUE A O MOINS 2 POINT SUR 3\r\n6) PENSER A être que 2 à prendre le drapeau et le reste couvre \r\n7)1 CHEF ESCOUADE PAR ESCOUADE LE SEUL QUI METERA LE MICRO EN EQUIPE AVEC LOTRE CHEF ESCOUADE\r\n\r\n\r\n\r\nSTRATEGIE METRO\r\nCONQUETE T8\r\nCoter russe (plus près de point A)\r\nComposition soldat esccouades \r\nAlpha  : medecin (sprint)\r\n             Medecin (chargeur)\r\n            Medecin ( grenade )\r\n             Medecin (furtiviter )\r\nBravo   medecin ( sprint)\r\n            Medecin (chargeur)\r\n           Medecin (grenade)\r\n           Soutien    (explosif)\r\nDépart : 1 seul loup prend le point A le reste part directement au point B\r\nSuite du match : escouade alpha prend le point B une foi le point B pris nous les bloquerons escouade bravo 2 medecin vous vous aller aider escouade alpha le reste de escouade bravo vous vous resterai dernier vous devrai couvrez le dernier escalator\r\nSi équipe en face et faible direction le dernier point et tout le loup en mode spawn kill nous formerons une ligne \r\nSi équipe en face prend les deux point et nous bloque la tout le monde se regroupe et les loups passe en force avec du fumigènes et 2 loup  parte au point c \r\nSi équipe en face commence à prendre le point A un seul part défendre le point (si possible un médecin et rush bien le reste continuer à défendre dans le cas où il on prit les point 2 loups prenne C et attend que le point a soi repris une foi reprise retour au position blocage \r\n1   ) SURTOUT NE PAS LES PRENDRE EN SPAWN KILL\r\n2) SPOTER UN MAXIMUM ET DONNER LE PLUS INFORMATION POSIBLE\r\n3) LE SOUTIEN PENSE A METRE DES MUNITION \r\n4) POUR LES MEDECIN TUER PUIS REANIMER\r\n5) PENSER TOURJOUR A QUE A O MOINS 2 POINT SUR 3\r\n6) PENSER A être que 2 à prendre le drapeau et le reste couvre \r\n7)1 CHEF ESCOUADE PAR ESCOUADE LE SEUL QUI METERA LE MICRO EN EQUIPE AVEC LOTRE CHEF ESCOUADE\r\n\r\n\r\nSTRATEGIE METRO\r\nCONQUETE T12\r\nCOTER USA (point le plus prêt de C)\r\nComposition soldats escouades \r\nAlpha  : medecin (sprint)\r\n             Medecin (chargeur)\r\n            Medecin (grenade  )\r\n             Medecin (furtivité  )\r\nBravo   medecin ( sprint)\r\n            Medecin (chargeur)\r\n           Medecin (grenade)\r\n           Medecin    (furtiviter)\r\nCHARLIE Medecin (  sprint)\r\n                Medecin  ( chargeur)\r\n              Soutien (explosive)\r\n              Soutien (furtiviter)\r\nDépart : 1 un soldat prend le premier point seul le reste part directement sur le point b une escouade part directement escalier rouge 1 escouade prend le premier escalier et une escouade part dernières escalier puis on bloque les 3 entres \r\nPosition des escouades\r\nEscouade alpha bloque entrer de gauche\r\nEscouade bravo bloque entres du milieu\r\nEscouade Chali bloque entre de droit\r\nEn cas ou la team arrive à passer : 2soldat devront penser à prendre le point A des que le point B et reprit tous les monde reprend sa position\r\nPOINT IMPORTANT\r\n1) SURTOUT NE PAS LES PRENDRE EN SPAWN KILL\r\n2) SPOTER UN MAXIMUM ET DONNER LE PLUS INFORMATION POSIBLE\r\n3) POUR LES  SOUTIEN  1 POSE UNE CAISE DANS LA BILLETERIE BIEN CACHER ET L OTRE SOUTIEN TU LA POSE A TES PIED A DROIT QUAN TU ET POSER\r\n4) POUR LES MEDECIN TUER PUIS REANIMER\r\n5) PENSER TOURJOUR A QUE A O MOINS 2 POINT SUR 3\r\n6) PENSER A être que 2 à prendre le drapeau et le reste couvre \r\n7)1 CHEF ESCOUADE PAR ESCOUADE LE SEUL QUI METERA LE MICRO EN EQUIPE AVEC LOTRE CHEF ESCOUADE'),
(956, 6, 80, 1367591045, 95, 'Ok sa marche , de plus Aftermach viens de ce finir de dl , end game est a 34 %\r\n\r\nmais c''est qui qui joué hier sur ptit_shuay la fille ou toi ? a moins que tu sois une fille ? o_O'),
(957, 6, 93, 1367591193, 95, 'j''ai jamais fait de matchs team sur bf3 et encore moins d’entrainements ^^ , mais j''aimerai i participé , sa depends quand cest aussi x)  et la carte que je déteste le plus ou je n’emmerde réellement quand je joue dessus c''est karg :''( !  '),
(958, 6, 85, 1367591267, 95, 'BMD moi je veut etre ton gunner ! :DDDD je kiff etre Gunner en hélicos '),
(959, 10, 95, 1367592003, 91, 'pour le T12 se ne serai bien d avoir un eclaireur avec un das pour cote americain un au centre ds le metro pres des 2 porte et coté ruse un au milleux pres du deuxieme esca???');
INSERT INTO `forum_posts` (`postID`, `boardID`, `topicID`, `date`, `poster`, `message`) VALUES
(960, 10, 96, 1367592916, 89, 'Stratégie crééee par le loup slimmorle\r\n\r\n                 [SIZE=4][S]Stratégie  métro:[/S][/SIZE]\r\n                     Conquête T8 :\r\n        COTE USA (départ prêt du c):\r\nComposition soldat escouades \r\nAlpha   :    Médecin (sprint)\r\n             Médecin ( chargeur )\r\n             Médecin (grenade  )\r\n             Médecin (furtivité )\r\nBravo   :    Médecin (sprint)\r\n             Médecin (chargeur)\r\n             Médecin (grenade)\r\n             Soutien    (explosif)\r\nDépart : 1) Un soldat prend le premier point [SIZE=3]seul[/SIZE] le reste part directement sur le point B une escouade part directement escalier rouge et l’autre escouade prend le premier escalier.\r\n\r\nSuite du match : rien de plus simple on leur laisse le point A et gardons le B et C (couvrir les 3 portes si dessous la position) \r\n\r\nPosition des escouades :\r\n\r\nEscouade alpha  2 médecins à gauche qui couvrent  les portes de gauche ,  2 médecins dans la billetterie couvrent les portes du milieu.\r\n\r\nEscouade bravo  2 médecins billetterie couvrent les portes.Milieu : 1 soutien à droite au niveau du guichet posé au bipied avec un médecin qui couvre le coté droit. \r\nAu cas ou la team arrive à passer : 2 soldats devront penser à prendre le point A dès que le point B est reprit puis tout le monde reprend sa position\r\nPOINTS IMPORTANT:\r\n1) SURTOUT NE PAS LES PRENDRE EN SPAWN KILL\r\n2) SPOTER UN MAXIMUM ET DONNER LE PLUS D''INFORMATIONs POSSIBLE\r\n3) POUR LE SOUTIEN QUAND IL EST POSE A DROITE (QUAND ON LES BLOQUENT) PENSER A MERTRE DES CLAYMORES A DROITE EN LIGNE ET POSER DES MUNITIONS JUSTE A COTE POUR QUE LES MEDECINS PUISSENT VENIR SE RAVITALLIER\r\n4) POUR LES MEDECINS TUEZ PUIS REANIMEZ.\r\n5) PENSER TOUJOURS A CE QU''ON EST AU MOINS DEUX POINTS SUR TROIS\r\n6) PENSER A ETRE QUE DEUX A PRENDRE LES DRAPEAUX LE RESTE COUVRE AUTOUR. \r\n7)1 CHEF D''ESCOUADE PAR ESCOUADE LE SEUL QUI METTRA LE MICRO EN EQUIPE AVEC L''AUTRE CHEF D''ESCOUADE\r\n\r\n\r\n\r\nSTRATEGIE METRO\r\nCONQUETE T8\r\nCoté russe (plus près du point A)\r\nComposition soldat esccouades \r\nAlpha  :     Médecin (sprint)\r\n             Médecin (chargeur)\r\n             Médecin ( grenade )\r\n             Médecin (furtiviter )\r\n\r\nBravo :      Médecin ( sprint)\r\n             Médecin (chargeur)\r\n             Médecin (grenade)\r\n             Soutien    (explosif)\r\n\r\nDépart : 1 seul loup prend le point A le reste part directement au point B\r\n\r\nSuite du match : escouade alpha prend le point B. Une fois le point B prit nous les bloquerons. Escouade bravo : 2 médecins : vous vous allez aider l''escouade alpha le reste de l''escouade bravo vous vous resterez derrière vous devrez les couvrir au niveau du dernier escalator.\r\nSi l''équipe en face est faible direction le dernier point et tous les loups se mettent en mode spawn kill nous formerons une ligne !\r\nSi l''équipe en face prend les deux points et nous bloque, la tout le monde se regroupe et les loups passent en force avec du fumigènes et 2 loup  partent au point C\r\nSi l''équipe en face commence à prendre le point A, un seul part défendre le point (si possible un médecin et rush bien. Le reste continue à défendre. Dans le cas où ils prennent les points, 2 loups prennent C et attendent que le point A soit repris une fois fait, retour en position blocage.\r\n \r\n1) SURTOUT NE PAS LES PRENDRE EN SPAWN KILL\r\n2) SPOTER UN MAXIMUM ET DONNER LE PLUS D''INFORMATION POSSIBLE\r\n3) LE SOUTIEN PENSE A METTRE DES MUNITIONS \r\n4) POUR LES MEDECINS TUEZ PUIS REANIMEZ\r\n5) PENSER TOUJOURS A PRENDRE AU MOINS DEUX POINTS SUR TROIS\r\n6) PENSER A ETRE QUE DEUX A PRENDRE LE DRAPEAU ET LE RESTE COUVRE \r\n7) 1 CHEF D''ESCOUADE PAR ESCOUADE, LE SEUL QUI METTRA LE MICRO EN EQUIPE AVEC L''AUTRE CHEF D''ESCOUADE\r\n\r\n\r\nSTRATEGIE METRO\r\n\r\nCONQUETE T12\r\n\r\nCOTE USA (point le plus prêt de C)\r\nComposition soldats escouades \r\n\r\nAlpha  :     Médecin (sprint)\r\n             Médecin (chargeur)\r\n             Médecin (grenade  )\r\n             Médecin (furtivité  )\r\nBravo  :     Médecin ( sprint)\r\n             Médecin (chargeur)\r\n             Médecin (grenade)\r\n             Médecin    (furtiviter)\r\nCHARLIE :    Médecin (  sprint)\r\n             Médecin  ( chargeur)\r\n             Soutien (explosive)\r\n             Soutien (furtiviter)\r\n\r\nDépart : 1) un soldat prend le premier point seul le reste part directement sur le point b. Une escouade part directement escalier rouge, 1 escouade prend le premier escalier et une escouade part au dernier escalier puis on bloque les 3 entrées. \r\n\r\nPosition des escouades\r\n\r\nEscouade alpha bloque l''entrée de gauche\r\nEscouade bravo bloque l''entrée du milieu\r\nEscouade Chali bloque l''entrée de droite\r\nAu cas ou la team arrive à passer : 2 soldats devront penser à prendre le point A dès que le point B est reprit tout le monde reprend sa position\r\nPOINT IMPORTANT\r\n1) SURTOUT NE PAS LES PRENDRE EN SPAWN KILL\r\n2) SPOTER UN MAXIMUM ET DONNER LE PLUS D''INFORMATION POSSIBLE\r\n3) POUR LES  SOUTIENS, 1 POSE UNE CAISSE DANS LA BILLETERIE BIEN CACHE ET L''AUTRE SOUTIEN LA POSE A SES PIED A DROITE QUAND IL EST POSE\r\n4) POUR LES MEDECINS TUEZ PUIS REANIMEZ\r\n5) PENSER TOUJOURS A GARDER AU MOINS DEUX POINTS SUR TROIS\r\n6) PENSER A ETRE QUE DEUX A PRENDRE LE DRAPEAU LES AUTRES COUVRENT\r\n7)1 CHEF D''ESCOUADE PAR ESCOUADE LE SEUL QUI METTRA LE MICRO EN EQUIPE AVEC L''AUTRE CHEF D''ESCOUADE\r\n\r\nStratégie métro\r\n\r\nConquête T12\r\nCoté russe (point plus prêt de a)\r\nAlpha   :    Médecin (sprint)\r\n             Médecin (chargeur)\r\n             Médecin ( grenade)\r\n             Médecin (furtiviter )\r\nBravo   :    Médecin ( sprint)\r\n             Médecin (chargeur)\r\n             Médecin (grenade)\r\n             Médecin    (furtiviter)\r\nCHARLIE :    Médecin ( sprint)\r\n             Médecin  (chargeur)\r\n             Soutien (explosive)\r\n             Soutien (furtiviter)\r\n\r\nDépart : 1 seul loup prend le point A le reste part directement au point B.\r\n\r\nSuite du match : escouade Alpha avec escouade Bravo vous prenez le point B et vous défendez le point ainsi que l'' escalier rouge et l''escalator. L''escouade Charli (les deux médecins) aider l’escouade alpha et bravo .Les deux soutiens vous  couvrez l''escalator et pensez à poser vos claymores aux escalators et couvrez.\r\n\r\nSi l''équipe en face est faible, direction le dernier point et tous les loups en mode spawn kill!! Nous formerons une ligne .\r\n\r\nSi l''équipe en face prend les deux points et nous bloque, la, tout le monde se regroupe et les loups passent en force avec du fumigènes. Deux loups  partent au point C \r\n\r\nSi l''équipe en face commence à prendre le point A, un seul part défendre le point (si possible un médecin et rush bien, le reste continue à défendre dans le cas où ils ont pris les points) 2 loups prennent C et attendent que le point A soit repris. une fois reprit retour en position blocage! \r\n\r\n1) SURTOUT NE PAS LES PRENDRE EN SPAWN KILL\r\n2) SPOTER UN MAXIMUM ET DONNER LE PLUS D''INFORMATIONS POSSIBLE\r\n3) LE SOUTIEN PENSE A METTRE DES MUNITIONS \r\n4) POUR LES MEDECINS TUEZ PUIS REANIMEZ\r\n5) PENSER TOUJOURS A PRENDRE AU MOINS DEUX POINTS SUR TROIS\r\n6) PENSER A PRENDRE QU''A DEUX LE DRAPEAU ET LE RESTE COUVRE \r\n7)1 CHEF D''ESCOUADE PAR ESCOUADE !LE SEUL QUI METTRA LE MICRO EN EQUIPE AVEC L''AUTRE CHEF D''ESCOUADE\r\n\r\n\r\n'),
(961, 5, 94, 1367597926, 84, 'alors les amis !!  Où en êtes vous??\r\n\r\n:D'),
(962, 4, 82, 1367600119, 84, 'Merci pour votre accueil les amis :) et merci pour les diverses invitations :)\r\n\r\nOn viendra envahir et en[FLAM]mer votre serveur de quelques balles alors... ^^\r\n\r\nA trés bientôt InGame ;)\r\n\r\n'),
(963, 5, 94, 1367600702, 4, 'vous prenez quelle map, pour nous ca sera Karg'),
(964, 10, 95, 1367601838, 89, 'Oui bonne idee pour le t12 sa peu etre bien le das a esayer '),
(965, 5, 94, 1367602730, 84, 'nous prendrons bazar, on vous rejoin sur votre serveur vers 21h.\r\nPar contre c''est ok pour régle promod 4v4 squad rush?\r\n\r\nGarder conscience que nous débutons... Vous serez notre seconde war pour le coup... Nous respecterons, bien évidement, les régles et l''esprit du jeu :) \r\nJ''éspère que vous apprécierez et prendrez du plaisir à jouer avec nous.'),
(966, 7, 92, 1367603459, 2, 'une équipe junior mais faut qu''ils est un peu de duvet !!! et des poils sous les bras !!!'),
(967, 10, 91, 1367603768, 6, 'Present'),
(968, 5, 94, 1367604690, 4, 'ok pour le promod 4vs4 on vous attend et nous respecterons les règles également, au plaisirs de jouer avec vous'),
(969, 6, 85, 1367604781, 2, 'pour l''hélico je conseil au gunner d''avoir les options suivante : furtivité , zoom ou infrarouge , missile téléguider plus réactif que le missile guider qui est souvent inutile si les tanks sont équipés du fumigène \r\n\r\npour le pilote sais selon la situation si y''a du lourd en face niveau avion faut le radar aérien et le CME a utiliser assez souvent au début de la partie pour ne pas être sur les radars des avions et ne jamais allez directement sur le point le plus éloigné sous peine de ce prendre quelques igla \r\n\r\nperso j''aimerai bien aussi avoir un gunner je pilote souvent seul et sais plus difficile car pas d''info '),
(970, 6, 93, 1367605091, 2, 'dispo de 14h a 17h le 5 mai'),
(971, 5, 94, 1367605919, 84, 'trés bien ! :)\r\n\r\nNous serons à 21h30, rassemblé, maximum sur votre serveur.\r\n\r\nJe sais pas si c''est parce que votre serveur n''est pas encore régler mais il y a les vehicules pour le moment sur votre serveur ^^\r\n\r\na toute ;)'),
(972, 10, 91, 1367606442, 10, 'Dsl lele , équipe constitiué, !! On a un tournoi qui arrive , tkt. Y aura des matchs :) '),
(973, 10, 97, 1367606725, 10, 'Pilote de chasse : LaPsYkose  ( Snipe balise )\r\nPilote de chasse : Pignouf     ( assaut )\r\nPilote d''hélico : WAZA-PsY-4-x ( Ingénieur ).         ESOUADE A\r\nGunner : Gyom ( Ingénieur )\r\n\r\n\r\nChar 1  \r\n\r\nPilote : Tarty   ( assaut )\r\nGunner: Reaper ( Ingénieur ).           ESCOUADE B\r\nMarqueur: Ben ( Ingénieur )\r\n\r\n\r\n\r\nChar 2 \r\n\r\nPilote : Slimmorle  ( assaut )\r\nGunner : Kevcha ( Ingénieur ).          ECOUADE C\r\nMarqueur : black ( Ingénieur )\r\n         \r\n             \r\n\r\nJeep : Alex ( assaut ). Escouade D\r\n             Tweezy ( balise et marqueur laser ) Escouade D\r\n             \r\n             \r\n\r\nMission Pilote : Abattre Avion et hélicoptère  / Une fois terminé parachuté sur le point le plus loin ( A ou toit D ) . Vous reprendrez vos avions si vs mourrez au sol ou si ça devient chaud la haut . En fonction de leurs niveau dans les airs , vous garderez le point opposé !!\r\n\r\nMission Char 1 & Mission char 2  , jouez la ensemble , pas trop serré pour ne pas vous gêner , prenez point B & C .. Vous ne vous vous occuperez que de ces points !!\r\n\r\nMission Jeep : Alex tu file amener tweezy dans les hauteur pour qu''il mette sont marqueur laser et ça balise pas trop loin du marqueur pour qu''il le remette si il le détruit !! Le premier marqueur doit être posé rapidement pour éliminer l''hélicoptère donc ne vas pas trop loin ...\r\nUne fois posé vous couvrez le point le plus proche ( bâtiment ou poste d''essence ) \r\n\r\nMission hélicoptère : abattre hélicoptère , char et tous ce qui bouge :) \r\n\r\n\r\nNous avons 4 escouades , donc si une escouade est décimé n''hesité pas a switcher d''escouade et revenir dans vos escouade respectif pour que vos camarades reviennent sur vous !'),
(974, 10, 91, 1367607052, 6, 'Pas de soucis.\r\nP''tre a un de ces quatres si la motivation revient '),
(975, 3, 77, 1367607060, 25, 'Salut bienvenue sur le forum profites d''aller visiter chaques topiques et de nous rejoindre en ligne :) bonne chance'),
(976, 10, 95, 1367607719, 14, 'Le DAS est interdit en promod ...'),
(977, 10, 97, 1367608288, 2, 'pas de  joueur c4 !! '),
(978, 3, 77, 1367608397, 81, 'Bonne chance pour ton recrutement :)'),
(979, 5, 94, 1367608585, 4, 'y a pas de véhicule en squad rush !!'),
(980, 5, 81, 1367609314, 81, 'Up pour Ben :) '),
(981, 7, 92, 1367609586, 5, 'Se n''est pas une mauvaise idée, mais en gardant bien en tête que se serons toujours des Apax au même titre que nous tous ;)'),
(982, 5, 81, 1367610011, 5, 'lol\r\nGardons bien en tète que la team NoName n''a pas le même état d''esprit que certain de leur joueur sans cité de nom...'),
(983, 6, 85, 1367610591, 5, 'perso je suis mieu en gunner, gp peut en témoigné ;)'),
(984, 6, 93, 1367610807, 5, 'j’essayerai d’être présent ;)\r\nPar contre slim, essaye de mètre les joueurs qui ferons le match ensemble, nous autre on fera les adversaire pour vous donné du fil a retordre ;)'),
(985, 5, 81, 1367615287, 104, 'ca parle toujours autantpour rien, pour ceux qui disent peur ?> trololololol blabla je vous attends quand vous voulez en 1v1, 2v2, 5v5 maintenant je suis pas la pour chercher la merde juste vous proposer un match, vous refuser, bah tant pis et à très bientôt pour ceux qui viendront me voir'),
(986, 10, 95, 1367620155, 91, 'le drone alors mdr'),
(987, 10, 91, 1367620399, 34, 'moi je suis ok pour être au sniper :D si sa gêne pas ? :)'),
(988, 5, 94, 1367622147, 4, 'Donc résultat on a perdu 4-1, on a quand meme sauver l''honneur en explosant une bombe sur 4, GG à eux meme s''ils avaient du mal à prendre leurs balles mais bon. Composition de notre équipe : Pignouf30, Slimmorle, hugo_AK47_95, ben_ftwc'),
(989, 6, 85, 1367622289, 37, 'Moi je veut bien être pilote de jet! '),
(990, 3, 77, 1367653586, 94, 'Merci a tous ! Je m''absente jusque lundi ;) petit week-end end sans internet ^^'),
(993, 6, 98, 1367659295, 95, 'Yop , j''aimerai savoir si il y aurait des gens de mon âge 18- 20 ans pour former un double en parties ?\r\nPar exemple je joue ingénieur , lui assaut , moi chauffeur , lui gunners ; vice hersa . Ainsi toujours jouer ensemble et voilà \r\n\r\n\r\nPs : addez xTyflo sur le psn , Psy accepte moi sur le psn , toujours en attente ^^'' \r\nEt je compte être accepter dans la section sur battle log ou ? .. '),
(994, 5, 94, 1367659412, 89, 'Beau petit match j avoue moi je dit revanche a refaire avec un petit entrainement gg a la team flam'),
(995, 6, 85, 1367659512, 89, 'merci a tous c est cool de votre part les loups aouuuuuuuuuuuuuuuuuuuuu'),
(996, 10, 97, 1367659798, 89, '[COLOR=#00008B][FONT=courier new][/FONT][/COLOR][SIZE=4]j approuve cette stratégie sa peu les faire aaaaaooouuuuuuuuuuuuuuuuuuuuuuuu[/SIZE]'),
(997, 6, 99, 1367663283, 89, 'Salut les loups comment vous allez ? Comme je vous avez dit je suis organisateur de tournoi donc la j ai creer un tournoi je vous met le lien allez http://battlelog.battlefield.com/bf3/fr/platoon/2832655391977818212/ donc faite tournez au team que vous connaisez  même équipe pas fr merci a tous aaaoooouuuuuuuuuuuu'),
(999, 5, 94, 1367664217, 5, 'Oui sa sent la revanche !!!! ;)\r\nBien joué les Flam ^^'),
(1000, 7, 92, 1367664922, 95, 'Ouf j''en n''est 19 ^^'),
(1001, 7, 92, 1367665678, 89, 'je tien juste a dire que moi j en ai ramener 2 donc julien et hugo des ancien de mon ancienne team et je souhaiterais qu il reste dans les apax normal car se sont de très bon élément après pour le reste je les le leadeur et le co leadeur choisir met je suis contre je tien juste a dire mon avis aaoouuu '),
(1002, 7, 79, 1367665774, 89, 'bonne idée moi je suis soutien et pilote de char '),
(1003, 7, 79, 1367666117, 95, 'Assault , ingé , soutien , guuner char ou pilote , gunner hélicos '),
(1004, 5, 94, 1367672554, 102, 'Salut a toute la meute!!!                                                                                                                                                                                                                                                                             Tout d abord merci d avoir accepté le match en mettant a disposition votre serveur pour que la rencontre puisse se faire! C etait une tres agreable premiere war  avec certainement une revanche en perspective ; ) si je ne me trompe je pense que vous avez perdu 3-1 (et non pas 4)!! Merci pour la bonne entente et au plaisir de vous affronter de nouveaux!! Nous reprendrons contact avec vous pour fixer une date et n hesitez pas a faire de meme!!                                                                                                                                                                         Pour la team [FLAM].                                                                                                                                                                                                                                                                                  ArRuaCeiroOo'),
(1005, 4, 82, 1367673611, 102, 'Salut a toute la meute!!! Merci a vous tous pour votre accueil et votre soutient^^ a tres bientot pour croiser des balles ; ) bien a vous ArRuaCeiroOo'),
(1006, 6, 85, 1367675438, 4, 'je piloté déjà les avions, ensuite je sais piloter les hélicos, bon c''est sur quand on a des igla et stinger au cul c''est pas facile pour les 2 engins, ensuite je suis assez bon gunner et j''essaie de m''entrainer le plus possible au téléguider qui est très chaud à manier.'),
(1009, 6, 98, 1367676465, 95, 'désolé de Uper mais il me font un double x) '),
(1010, 5, 94, 1367678883, 10, 'Bien joué quand même ;) la prochaine elle est pour ns !!'),
(1011, 7, 92, 1367679105, 1, 'Y''a pas de "Apax normals", y''a que des APAX !\r\n\r\nA présent, l''idée du Junior est au niveau des compet, certaines requiert un minimum de 18 ans pour participer, d''autre non. De ce fait, si nous voulons partir en cup, nous devons également garder ceci à l''esprit ;)'),
(1012, 6, 98, 1367679161, 1, 'Va s''y, je te suis ;)'),
(1013, 5, 94, 1367679271, 84, 'salut les [APAX] !!!\r\nGOOD GAME pour le match et merci à vous!!!!\r\n\r\nUn trés bon match, nous nous sommes bien amusés, au plaisir de faire un autre match :)\r\n\r\nN''oubliez pas de venir vous présenter : [URL]http://clanflam.xooit.fr/index.php[/URL] et nous défier à votre tour dés que possible :)\r\n\r\nA bientôt les loups loups ! ;)'),
(1014, 6, 98, 1367679335, 95, 'trop tard je me met avec aulyro , au pire triple IG ?'),
(1015, 5, 81, 1367679393, 1, 'Par les grands pouvoirs qui me sont conférés par notre dieu à tous : MySQL, je te déclare bannis de nos systèmes. Je te laisse aller répandre ta connerie ailleurs.\r\n\r\nTcho'),
(1016, 6, 93, 1367680417, 89, 'sa roule pas de souci je prepare sa c est pour sa j aimerai avoir le plus de loup possiible ps je suis admi sur le serv met j arive pas a le configurer merci'),
(1017, 6, 93, 1367686941, 5, 'bizarre que tu n’arrive pas a le configuré alors que je t''est mis les droit....'),
(1018, 7, 100, 1367738255, 5, 'Bonjour a tous les loups\r\n\r\nDonc après avoir vu que kevcha avait fait une demande de recrutement hier chez les anonymous et qu''il a quitté le groupe Apax sur le Battlelog a l''instant je vous annonce que j''ai pris la décision en t''en que co-lead de lui montrer la porte de sortie de la tanière\r\n\r\nJe ne conçoit pas qu''un loup puisse se comporté de la sorte !!!! Quand on veut changer d''équipe, on part de la première avant de demander a se faire recruté dans une autre et on en parle ouvertement au chef de la team\r\n\r\nNous somme une meute et je pense que si nous somme tous la c''est que nous avons "l''état d''esprit de la meute", TOUS LA LES UN POUR LES AUTRES QUE SE SOIT DANS LA PAIX OU DANS LA GUERRE !!!!\r\n\r\nJe prend donc l’entière responsabilité de L''ANNULATION  de l’appartenance de Kevcha a la Meute Apax, il est donc dès a présent rejeté de la meute, ces attribution sur le site sont retiré et il ne rejoint pas le groupe des "Ancien Apax" qui est un titre pour ceux qui on quitté la meute avec respect et honneur\r\n\r\nEn espérant que vous comprendrez ma décision, je vous souhaite une bonne journée a tous\r\nAaaaaaOoooooUuuuuuuuuuu !!!!!!!!!!!'),
(1019, 12, 67, 1367738430, 5, '[SIZE=5][COLOR=#8B0000]RECRUTEMENT ANNULER[/COLOR] [/SIZE]'),
(1020, 7, 100, 1367739497, 8, 'Et dire que c''est a cause d''un entrainement non suivi, il faut arreter de penser que les 40 autres loups ont les memes disponibilités que soi, donc t inquiete dartoch je te soutiens dans ta prise de decision, ca fait partie de ton nouveau job et bonne réaction, donc pour les prochains recrutement, les differents recruteurs assurez vous qu''un dévouement minimum existe et pas qu''un simple entrainement loupé et apres une semaine et quelques de présence le type se barre.\r\non change pas de team comme de string les filles.'),
(1021, 3, 77, 1367746146, 89, 'Bonne chance pour ton recrutement aaaaooouuuuuuuuuuuuuuuuuuuuuuu'),
(1022, 7, 100, 1367747736, 89, 'j avoue ta décision vous s avez les anonymous sont comme sa il hésite pas a recruter dans les team le pire que c est des merde sauf leur leadeur madforce dommage pour kev car il va regretter de être parti je c est de quoi je parle moi j ai pour principe que quand je suis dans une team je reste met bon tous le monde a pas la même ment aliter aaaooooouuuuuuuuuuuu'),
(1023, 7, 79, 1367748250, 25, 'A voir :) Assault'),
(1024, 6, 101, 1367751972, 81, 'Salut la meute :) je voulais savoir si parmis vous il y aurais des joueurs qui souhaite former une escouade full assault histoire de pouvoir tous se réa et pour jouer sur des maps fermer comme opération métro ou bazar avec pas mal de ticket , cela pour faire de grosse partie avec un bon ratio et ça permettra de mieux se connaître si il y en a d''intéresser hésitez pas a le contacter :). \r\n\r\nCiiDzZ. '),
(1025, 6, 102, 1367758775, 95, 'Yop je vous dis au revoir car je quitte la apax je suis dedans depuis plus de 10 mois , et je recherche autre chose maintenant , une meilleure structure , peut être que je vais le regretter , l''avenir me le dira. sur ce GL a vous ;)'),
(1026, 3, 103, 1367762289, 106, 'pseudo: Flav1996\r\nPrénom: flavien\r\nAge: 16 ans\r\npsn: Aofdflav1996\r\nBattlelog: http://battlelog.battlefield.com/bf3/fr/soldier/Aofdflav1996/stats/772086898/ps3/\r\n\r\nj''ai connu la meute grâce à un bon amis à moi Aulyro( qui fait partie de la meute )\r\n\r\nje voudrais intégré la team car aiyant souvant joué avec les membre de la meute je me suis toujours bien amusé et me suis bien amélioré\r\n\r\nje suis un grand fane de BF3 et de fps en général. sur battlefiels je joue un peut avec toute les classe, je respecte les règle des différent serveur et joue fair-play.à par sa je suis en première technologique STI SIN\r\nj''habite pas loin de Montauban  \r\n  \r\n'),
(1027, 7, 104, 1367765829, 89, '                             [FONT=verdana] Compte Rendu De Entrainement Dimanche 05/05/2013[/FONT][SIZE=4][/SIZE]  \r\n[SIZE=3]Map metro[/SIZE] : map tout a fait bien gérer organisation a fait sont effet tout le monde a réussi a prendre sa place les information on bien était donner et appliquer rien a dire \r\n\r\n[SIZE=3]Map karg [/SIZE]: map ou sont sait bien débrouiller met il y a  encore des point avoir le 1 ier mettre les joueurs dans la meme escouade( exemple ceux qui sont dans le même char les mettre ensemble) au sinon en gros sa va je trouve met je sens que on peu faire mieux \r\n\r\n\r\n[SIZE=4]point positives [/SIZE]\r\n1) Entrainement c est bien passer\r\n2) personne a rager sa j ai bien aimer \r\n3) 12 loup au moin  present a l heure de entrainement \r\n[SIZE=4]point négative[/SIZE]\r\nPour les point négative pas grand chose juste a quelque 1 venez a l heure car entrainement c était a 15H pas a 15H30 OU  16 H sa aurai éviter que sur karg c est le bazar car sur métro j avais déjà préparer les escouades pour karg avec les position de tout le monde et avec tel ou un tel dans les véhicule comme étai prévu dans la strat de waza sur karg  dsl j avais oublier que sur karg il y avais 2 char  pas 3 met bon pas grave \r\n[SIZE=4]\r\nPoint important[/SIZE]: map karg a travailler \r\n\r\nvoila pou le compte rendu de entrainement j ai bien aimer le gérer et je trouve qu il y a du très gros potentiel\r\nUn grand merci a tout les loup d avoir étais présent sa ma fait plaisir organiser cette entrainement \r\nAAAAAOOOOOOOOUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU'),
(1028, 3, 103, 1367775723, 5, 'Salut Flav et bienvenu sur notre site ;)\r\nUn des recruteurs prendra contact avec toi d''ici peut , en attendant je te dit bon courage pour ta demande de recrutement et a très vite in game ;)'),
(1029, 6, 102, 1367775826, 5, 'Une meilleur structure...? alors que tu a été absent 7 mois sur 10... enfin bon peut importe, bonne chance pour la suite'),
(1030, 12, 56, 1367776038, 5, 'Un ptit up pour se joueur qui est bien présent in game ;)'),
(1031, 3, 76, 1367776185, 5, 'bon je sait que je vais regretté se que je vais marqué mais bon... lol\r\n\r\nUne très bonne mentalité et même si le niveau n''est pas forcement très élevé, on a tous commencé un jour donc +1 pour Shuya'),
(1032, 6, 102, 1367776634, 2, 'tu vas nous manquer ,non  plus sérieusement bonne continuation '),
(1033, 3, 76, 1367777442, 89, 'salut\r\nrajoute moi aussi stp que on se fasse quelque partie bonne chance pour ton recrutemen'),
(1034, 3, 103, 1367777517, 89, 'salut je veu bien m ocuper de ton recrutement rajoute moi sur psn stp que on fasse quelque parti merci id slimmorle'),
(1035, 12, 56, 1367777682, 89, 'salu bonne chance pour ton recrutement aaaooouuuuuuuuuuuuuuuuuuu'),
(1036, 3, 103, 1367778407, 11, 'Lol XD je suis le Kikou lol qui va foutre la merde XDattend moi slimmorle pour le prendre en test stp que je joue contre vous pour lui faire peter un cable car c''est un petit rageux ^^ je prendrait des armes de pute Mouahahahahahah'),
(1037, 7, 104, 1367779975, 34, 'j''ai grave le seum je savais pas que y''avais entrainement :( '),
(1038, 3, 103, 1367780053, 106, 'je t''ai ajouté slimmorle j''attend ton méssage.\r\net toi aulyro je vais te montré qui s''est le  vrai kikou lol \r\n\r\n'),
(1039, 3, 103, 1367780588, 11, 'Lol j''ai ma réputation de E.T dans la Team ^^ donc le Kikou lol tes pas pret de prendre ma place'),
(1040, 6, 50, 1367781321, 11, 'je m''excuse car j''oublie d''aller sur le site donc voila pourquoi je n''est pas posté mon image sion le film c''est Danse avec les loup trop simple il a baissé le niveau XD avec kevin Costner'),
(1041, 6, 50, 1367782570, 93, 'bien joué :) \r\nj''en remets une dans la lancée ^^\r\n [IMG]http://image.noelshack.com/fichiers/2013/18/1367782540--.jpg[/IMG]'),
(1042, 3, 103, 1367782882, 1, 'Salut Flav. Bonne chance pour ton recrutement ;)'),
(1043, 7, 100, 1367783061, 1, 'On devrait instaurer un "Mur de la honte" ...\r\nBonne réaction toch. Heureusement que nous avons des nouveaux co-leads pour t''aider manu ;)'),
(1044, 7, 104, 1367783172, 1, 'Slimmorle, faudrait qu''on parle ensemble des entrainements. Je vais t''expliquer comment utiliser le calendrier du site afin de prévoir les présences et absence des joueurs. \r\n\r\nDe plus, excuses moi de ma non-présence, j''étais sur et certain qu''il étais à 5h (17h) et non 15h :/'),
(1045, 6, 50, 1367783185, 11, 'comment l''on met une image pour le jeu comme sa j''en mettrai une des quelle est trouvé l''image\r\n'),
(1046, 3, 76, 1367783253, 1, 'Pehp pehp pehp slipmou, on va s''détendre ! :p'),
(1047, 6, 80, 1367783404, 1, 'C''étais moi. Ptit_shuya est en fait ma voisine de couette s''tu veux ;)'),
(1048, 6, 50, 1367783451, 1, 'Choisit ton image, et tu clique sur le bouton "IMG" sur le forum. Après, tu colle l''adresse et tu poste ton message :)'),
(1049, 6, 102, 1367783487, 1, 'Serait-ce la fin du retour de l''ancien nouveau ?'),
(1050, 6, 101, 1367783519, 1, 'Ouail not ;)\r\n\r\nJ''ui opé'),
(1051, 3, 103, 1367787062, 8, 'ajoute moi aussi on pourra te tester chacun de notre coté et se concerter afin de valider ou pas'),
(1052, 3, 103, 1367792714, 106, 'un GRAND merci à TOUS pour vos encouragement'),
(1053, 3, 103, 1367793494, 11, 'fait pas le focu'),
(1055, 6, 50, 1367794178, 11, 'danse avec les loup on la deja mit en image XD\r\nvous pouvez virer le message blanc vue que je ne sais pas supprmer un message XD'),
(1058, 3, 103, 1367828437, 1, '[quote=Aulyro1996]fait pas le focu[/quote]\r\n\r\nTu devrait le faire de temps en temps quand à toi !'),
(1059, 6, 50, 1367831414, 5, 'trop simple Shuya c''est "battle royal" :p\r\n\r\n[URL=http://www.casimages.com/img.php?i=1305061112216025511158311.jpg][img]http://nsm08.casimages.com/img/2013/05/06//1305061112216025511158311.jpg[/img][/URL]'),
(1060, 6, 102, 1367831477, 5, 'Euuuhh ouaip je croie ^^'),
(1061, 6, 80, 1367831538, 5, 'benco pas la peine de repondre il est partie ^^ d''ailleur je ferme se poste'),
(1062, 6, 50, 1367832203, 1, 'Le Rite !\r\n\r\n[IMG]http://pkcine.com/wp-content/uploads/2012/11/Ted-Mark-Wahlberg-banni%C3%A8re1.jpg[/IMG]'),
(1063, 7, 107, 1367832844, 1, 'Les filles, j''aurais besoin de savoir quelles sont les fonctionnalitées qu''il vous reste en tête pour le site ...\r\n\r\nJ''ai 10 jours intansifs en terme de dev que je vais mettre un peu à profit des APAX, seulement, je n''ai que peu d''idées. La plus importantes si je puis dire, sera l''appartition d''une version mobile dédiée au forum uniquement. \r\n\r\nDétail du projet sous peu ;)\r\n\r\n(PS: Si un dev PHP-OO/SQL/jQuery as un peu de temps pour me filler un coup de patte ;) )'),
(1064, 7, 107, 1367834001, 5, '- es qu''il serait possible que les personne s''inscrivant sur le site recoivent un mail lors de l''inscription et lors de l''activation de leur compte?\r\n\r\n- peut etre voir pourquoi le lecteur video ne fonction pas ^^ ou metre un lien comme pour FB vers la chaine youtube Apax ;)'),
(1065, 7, 107, 1367834443, 1, '[quote=Dartoch]- es qu''il serait possible que les personne s''inscrivant sur le site recoivent un mail lors de l''inscription et lors de l''activation de leur compte?\r\n\r\n- peut etre voir pourquoi le lecteur video ne fonction pas ^^ ou metre un lien comme pour FB vers la chaine youtube Apax ;)[/quote]\r\n\r\nLe mail devrait être ok non ?\r\n\r\nEt pour le lecteur, je note :)'),
(1066, 7, 107, 1367837777, 5, '[quote=ben_ftwc]\r\nLe mail devrait être ok non ?\r\n[/quote]\r\n\r\nIl me semble que non'),
(1067, 3, 103, 1367840225, 106, 'je t'' ajoute ce soir Appax_Reaper vers 19h 20'),
(1068, 3, 103, 1367840677, 8, 't a l''air deja d''avoir une qualité c''est que tu es assez precis '),
(1069, 3, 103, 1367840697, 106, 'je t''ajoute ce soir Apax_Reaper'),
(1070, 3, 103, 1367840788, 106, 'j''ai une heure de conduite\r\n'),
(1071, 7, 107, 1367841885, 1, 'Je confirme que si :\r\n\r\n[quote]Bonjour aatest!\r\n\r\nVotre inscription à APAX (www.meute-apax.fr/site) a été prise en compte. Informations sur votre compte:\r\n\r\nPseudo: ******\r\nMot de passe: ******\r\n\r\nPour compléter votre inscription vous devez activer votre compte en cliquant sur le lien suivant:\r\nhttp://www.meute-apax.fr/site/index.php?site=register&key=*****************************\r\n\r\nMerci pour votre inscription\r\nAPAX - www.meute-apax.fr/site[/quote]'),
(1072, 6, 50, 1367842840, 93, 'ted :p\r\n\r\n[IMG]http://image.noelshack.com/fichiers/2013/19/1367842817-jacky.jpg[/IMG]'),
(1073, 7, 107, 1367843581, 1, 'Je viens de te créer un accès sur mon BugTracker toch, tu as du recevoir un mail à adr********ier@hotmail.fr avec un lien pour valider ton compte. \r\n\r\nAprès, tu va sur Manage Profile, tu modif et tu te met le site en français. Une fois OK, je veux bien que tu fasse un tour dans la team afin de récuperer tout les bugs ou modifs que les membres te parlent. ça va me permettre de pouvoir bosser efficacement :)\r\n\r\nTel moi si tu comprends pas =)'),
(1074, 3, 76, 1367843699, 37, 'bonne chance pour tn recrutement shuya!!! ajoute toutoun80740 sur psn'),
(1075, 8, 108, 1367845480, 1, 'Salutation la meute.\r\n\r\nJe m''en viens vous faire part d''une demande sérieuse pour la team.\r\n\r\nJe compte faire une bonne grosse mise à jours de batard sur le site/forum (yeah). Mais pour ce faire, j''aurais besoin d''une petite équipe pouvant se promenner sur le site et me remonter les éventuels bugs présents,\r\nd''en découvrir de nouveaux en essayant toutes les options possibles. Mais également de faire le tour parmis les membres / visiteurs du site afin de savoir ce qu''il peut manquer comme fonctionnalitée (envoyer des mails pour les évents, prévoir un calendrier plus propre, ajouter une version mobile du site ...).\r\n\r\nBien évidement, pour faciliter la tâche de tous, les participants, je vous formerais à l''utilisation d''un Bug Trackeur, qui sert en gros à afficher le bug, voir où en est la correction d''un bug ou à faire un suivit de projet.\r\n\r\nJe mettrais en place sur le site un peu plus tard, pour "forcer" certains loups à venir sur le site, un système de Trophés qui permettra de "classifier" plus ou moins les membres présents, les absents du site, ou ceux qui ne participent que trop. Bref, ils seront inutiles, donc obligatoirs ;)\r\n\r\nEventuellement, des trophés custom partirons pour les braves loups lors de certains évenements (ex: 1 an de la meute, T12 contre les 2099, ou autres ...).\r\n\r\nSi vous êtes interessés pour faire partie de l''aventure sus-nommée "Site APAX : V2", je vous laisse libre de vous faire connaître. \r\n\r\nMerci d''avance à ceux qui souhaitent participer, désolé aux autres pour vous avoir pourris votre site avec cette grosse barre verte :)'),
(1076, 3, 103, 1367855523, 11, 'C''est a dire Ben ??? --"'),
(1077, 3, 103, 1367855615, 1, ':p'),
(1078, 8, 108, 1367855638, 11, 'Je veut bien etre un des testeur (voil je fait le petit focu ^^)mais en temps que noob je risque de te poser quelque question XD'),
(1079, 8, 108, 1367855821, 1, 'Franchement, le but premiers de l''opération est justement de simplifier la vie des membres, donc au contraire. J''aurais presque tendance à dire, que vous soyez expert ou non, venez. Plus de monde testera le site, moins de bugs survivrons à cette V2 ;)\r\n\r\nPS : Premiers bugs corrigés =)\r\n\r\n[IMG]http://image.noelshack.com/fichiers/2013/19/1367855741-capture.png[/IMG]'),
(1080, 8, 108, 1367856716, 1, '[URL=http://www.meute-apax.fr/Mantis.pdf]Tutoriel d''utilisation de Mantis[/URL]'),
(1081, 8, 108, 1367861171, 2, 'bon courage !!!! amuse toi bien'),
(1082, 3, 103, 1367861509, 81, 'Aulyro <3'),
(1083, 12, 56, 1367861600, 81, 'Bonne chance pour ton recrutement :) '),
(1084, 8, 108, 1367861646, 81, 'Ben écoute moi aussi je suis partant si tu as besoin d''aide ! :) '),
(1085, 7, 107, 1367861865, 5, 'c''est bon j''y est accé ma caille ;) et c''est simple d''utilisation, donc sa devrai le faire :p'),
(1086, 8, 108, 1367861960, 5, 'Moi on ma pas laissé le choix ^^ donc bien sur que j''en suis :p'),
(1087, 6, 50, 1367867807, 11, 'je connait pas l''image mais la précédente était trop facile'),
(1088, 8, 108, 1367869231, 1, '[quote=CiiDzZ-HD]Ben écoute moi aussi je suis partant si tu as besoin d''aide ! :) [/quote]\r\n\r\n\r\nCheck tes mails, compte crée > si*******35@hotmail.fr'),
(1089, 6, 102, 1367877278, 10, 'NOn mais serieux t''es un scetch toi aussi , ya des mecs chelou mais toi t''es la cerise sur le guetto !! Tu viens ya 2 jr pr limite me supplier de te reprendre , alors que messieur ca fait 8 mois qu''il est pas venue , et la tu mots sort le terme structuré !! Tu sais si j''avais pas le role de meneur et du coup une image de joueur fairplay , y''aurais bien des tetes que je voudrais destrecturer !! Bonne continuation quand meme !! LOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOL'),
(1090, 7, 107, 1367878551, 10, 'Je trouve ca trés interressant comme arrivée dans un site !! \r\nTu crois que ca serais possible :) a titre imfortatif biensur :) \r\nhttp://2099.teamplay.fr/'),
(1091, 7, 107, 1367879728, 10, 'Ac une vidéo en plein milieu qui présenterais tte la Meute !! Is possible ?'),
(1092, 7, 107, 1367881370, 11, 'Et pour un reglement ???'),
(1093, 7, 100, 1367907669, 2, 'ssa sera pas le premier ni le dernier !'),
(1094, 8, 108, 1367911653, 89, 'Moi je t aurai bien aidai met je suis pas trop for en informatique met je peu aider si tu veux si t ok dit moi comment je peu t aider aaaaooouuuuu'),
(1095, 12, 56, 1367912221, 89, 'yo j ai jouer avec et sa il joue bien je suis pour qu il devienne un loup psy a toi de voir'),
(1096, 8, 108, 1367914441, 1, 'Je te crée un compte, et comme dis dans le topic initial, que tu ais ou non des compétences en informatique, bien au contraire, ton test ne sera que plus probant.\r\n\r\nDu coup, je te crée ton compte MANTIS, tu as un PDF un peu plus haut qui montre comment fonctionne le logiciel.\r\n\r\nMessieurs (Auly, Dartoch & Ciddz), vous pouvez attaquer vos premiers retours ;)'),
(1097, 12, 56, 1367914581, 1, 'Franchement, +1 pour s''te partie de folie hier !'),
(1098, 8, 108, 1367929731, 15, 'Si t''as besoin d''un coup de main pour du dev ou de l''intégration, je suis dispo.\r\nTu utilises un framework / cms, ou tu le fais from scratch ?'),
(1099, 8, 108, 1367931672, 1, 'Owi <3\r\n\r\nC''est du webspell originaire, mais devenu maison à la vue de toutes mes modifs de core \\o/\r\nJ''aurais effectivement besoin de tes talents en inté, et également besoin d''un graphiste. Tweezy si tu lit ça ;)\r\n\r\nOn en reparles demain sur Skype ? J''ai toute ma machine qui passe de Debian à Ubuntu 12.2.4-LTS ce soir =D'),
(1100, 8, 108, 1367931701, 1, 'Check tes mails ita'),
(1101, 8, 108, 1367932488, 1, 'L''équipe V2 se grandis !\r\n\r\n[IMG]http://image.noelshack.com/fichiers/2013/19/1367932456-capture.png[/IMG]'),
(1102, 3, 103, 1367950399, 25, 'Je vais pas me rajouter dans la bataille de testeur quand a moi je te souhaite la bienvenue et bonne chance :)'),
(1103, 6, 102, 1367950641, 25, 'Je vien de lire le ptit sketch c''est assez fou :O mdr'),
(1104, 8, 108, 1367950726, 25, 'C''est une grande aventure dans laquelle tu te lances :) bonne chance ^^'),
(1105, 7, 100, 1367950821, 25, 'Personellement on ne pas eu!j''ai eu une demande de recrutement de plusieurs team alors que j''étais déjà dans la team ;O'),
(1106, 3, 103, 1367951406, 8, 'aucune bataille entre recruteur je pensais simplement que deux avis serait plus cool, j ai pas envie qu''il y est un mec qu''on recrute et qui se barre au bout d''une semaine c''est un peu chiant et une perte de temps en plus de ca je veux que les nouveau soit des loups mange du loups et chie du loup, d''ailleurs flav peux tu me donner tes disponibilité histoire qu''on se fixe quelques parties'),
(1107, 12, 56, 1367951575, 8, 'up pour toi ajoute moi qu''on se fasse quelques parties et pour essayer de te faire rentrer au plus vite dans la meute'),
(1108, 3, 103, 1367952618, 1, '[quote=Apax_Reaper]aucune bataille entre recruteur je pensais simplement que deux avis serait plus cool, j ai pas envie qu''il y est un mec qu''on recrute et qui se barre au bout d''une semaine c''est un peu chiant et une perte de temps en plus de ca je veux que les nouveau soit des loups mange du loups et chie du loup, d''ailleurs flav peux tu me donner tes disponibilité histoire qu''on se fixe quelques parties[/quote]\r\n\r\nJ''aime l''esprit !'),
(1109, 3, 103, 1368024787, 106, 'Demain  la journee    ce soir  peut etre vendredi ce week end'),
(1110, 12, 56, 1368034506, 89, 'bonne nouvel hasmatique rajoute moi sur battlelog que je te la di merci'),
(1111, 3, 103, 1368035899, 8, 'ok b demain on se capte sur le psn alors'),
(1112, 3, 76, 1368085648, 1, 'Petit up :)'),
(1113, 6, 109, 1368090445, 11, 'Salut a tous alors voila en ce moment [COLOR=#0000FF]je me suis fixé un petit objectif réunir toutes les étoile de service sur toutes les armes avoir toutes les médailles et bien sur le plus important les Plaques [/COLOR]en tout il y a [COLOR=#32CD32]512 plaques et j''en ai 308[/COLOR].\r\nAlors voila pour ce qui veulent bien m''aider a atteindre un des objectif je les remercie en avance mais ce [COLOR=#FF0000]dont je vais galérer le plus se sont les plaque et vue que je n''est pas assez de temps de jeu pour certaine plaque du style avoir 100 étoile de service sur un véhicule ou en médecin[/COLOR] enfin bref et en retour je suis prêt a me laisser prendre un couteau pour certaine plaque que vous n''avez pas voila tout ^^ sur ceux l''on se retrouve sur le champs de batailler.^^'),
(1114, 6, 3, 1368091539, 11, 'Lu sa serait possible de faire comme sur l''ancien site que l''on c''est fait pirater. Je m''explique, mettre en privèe les nom de Psn associè a un nom si possible avec les Id Skype.\r\nExemple:\r\nPsn:Aulyro.1996            Prenom:Romain                  Id Skype:aulyro.1996'),
(1115, 3, 76, 1368093265, 89, 'bonne chance pour ton recrutement '),
(1116, 1, 110, 1368093569, 11, 'Salut a tous je me présente.\r\nMa vie dans le monde réel.\r\nJe m’appelle Romain connu sous le Pseudo Aulyro. Je suis né le 07 Mai 1996, j''habite dans le Tarn-Et-Garonne dans le 82.\r\nFan de jeux vidéos depuis mais 3 ans ayant commencé par les mythique jeux sur console portable du style Mario ou bien encore Pokémon, j''ai suivit l’évolution des jeux ainsi que les console j''ai découvert les Fps a partir de la Ps2 d''ou mon immense coup de cœur pour se genre de jeux depuis je suis inséparable. Je joue a toutes sortes de jeux tout comme les plate forme de jeux. Je suis aussi fan de film d''action fantastique,fan de musique je pratique de la Guitare électrique et avant du Piano fan de sport aussi a mon palmarès Judo champion régional junior handball champion départemental et voila les seul sport que j''ai pratiqué et étant fier d''avoir eu de bonne équipe me permettant d''accéder a ces championnat. J''ai arrêter le sport suite a quelque accident grave.\r\n\r\nMa vie dans les jeux vidéos\r\nJe suis un joueur de console sony, Avant d’être sur Bf3 j’étais sur la Licence Call Of Duty comme la plus part, a mes début j’étais seul jusqu’à un moment ou j''ai entendu parler des compétition qui se passé sur les console et de la tout c''est vite enchainé j''ai rejoint en premier temps une Team du Nom de Tag WTF j''ai tenu 7 mois avec du fait de sa dissolution puis après j''ai rejoint la Team Gtsd et c''est la que j''ai réellement connu les compétition de Décerto nous avion participé a tout les tournois qui se passé et dont nous avion la possibilité de nous qualifié mais bon cela na pas duré et la Team fut dissoute pendant une période de 1 mois j''ai jouè seul encore une fois et la quelle que cousin et des amis on eu la Ps3 et j''ai crée la Aofd dont nous avions gagné une bonne dizaine de Lan sur les Call Of Duty et nous nous somme développé sur d''autres jeux du style Assassin Red dead redemption etc. Puis un jeux est sorti Bf3 je mit suis mit a jouai trop compliqué a mes début je les stoppé puis en 2012 je mit suis remit a la recherche d''une Team en même Temps je suis tombé sur 3 mecs assez fort qui porté comme Tag clan Apax j''ai demandé a rejoindre la Apax le 5 mai 2012 et depuis jy suis rentré étant niveau 13  avec un ratio de 0,30 et me voila actuellement pigeon 41 avec un ratio de 1,20.\r\nSur ce jeu magnifique je me suis fait des amis exceptionnel telle que les Apax et certain membres de Team très connu du Style InFamouS,FoG etc ^^\r\nVoila sur ceux l''on se retrouve sur le champs de bataille fait gaffe j''ai l''air innocent mais sur le champs de bataille je te bouffe ;) '),
(1117, 6, 109, 1368098705, 89, 'salut moi je dispose des place 100 soutien et QBB 95  et etoile 50 en char et bien autre encore di moi se que tu veut je te les donner volontairement aaaoouuuu '),
(1118, 6, 109, 1368099813, 11, 'Merci Slimmorle en gros celle dont je vais avoir du mal c''est les etoiles de classe je veut ben te prendre celle du char et du soutien et toi les quelle y te manque'),
(1119, 6, 109, 1368115282, 1, '''va être sympa cette ''Only plaques rares'' ;)\r\n\r\nPar contre, comment fait-on pour connaître son nombre de plaques ?'),
(1120, 6, 109, 1368118583, 11, 'c''est très simple ben une ligne  y a 7 plaque puis tu conte le nombre de colone et tu multiplie le tout et sa te donne le nombres de plaque mais pour cela il faut au par avant cliquer sur le filtre débloqué ^^\r\n'),
(1121, 3, 103, 1368118603, 106, ' Dacor J'' atend ton invitation pour les test '),
(1122, 6, 3, 1368119587, 5, 'tout est fesable ;)\r\n'),
(1123, 3, 103, 1368122984, 8, 'j ai pratiquement pas jouer aujourd hui escuse moi mais ce weekend t inquiete c''est bon'),
(1124, 3, 103, 1368124140, 89, 'dsl flav met en se moment je suis beaucoup occuper reaper merci de occuper de sont recrutement merci mon louppp lol aaaoouuu'),
(1125, 12, 56, 1368124407, 89, 'accepter bienvenue dans la meute aaaaaooouuuuuuuuuuuuuuuuuuuuuuuuuuuuuuu'),
(1126, 12, 56, 1368125595, 8, 'GG '),
(1127, 3, 103, 1368125740, 8, 'oui t inquiete slim je m''y atele ce weekend aujourd''hui j ai pas pu jouer'),
(1128, 12, 56, 1368126924, 5, 'bienvenu parmi nous !!!!! ;)\r\n'),
(1129, 6, 3, 1368132761, 11, 'Il y a une fail dans le Site au point de vue Tchat je vous en dit plus en message Privèe sur Fb pour pas qu''il y est certain ... qui s''amuse a faire ceci.\r\nC''est Eraa qui me la fait remarquer.'),
(1130, 12, 56, 1368144991, 37, 'Bienvenu dans la meute couzin!!!'),
(1131, 6, 111, 1368175167, 89, 'pour ou contres les loupp si on a plus de pour on participe a se tournoi merci aaooouuu\r\n\r\n\r\nhttp://battlelog.battlefield.com/bf3/fr/platoon/2832655391981919090/'),
(1132, 10, 112, 1368176535, 89, '                               [SIZE=4]entrainement dimanche 12 mai a 14 H ruée en escouade 4 vs 4[/SIZE][COLOR=#00008B][/COLOR]\r\n\r\nSalut les loup j organise dimanche un entrainement ruee en escouade il me faudrait 7 apax qui souhaite entraîner en ruée en escouade\r\n\r\n\r\n[SIZE=4]pourquoi j organise cette entrainement:[/SIZE]\r\ncar je trouve que  c est un très bon mode de jeux pour apprendre a jouer en équipe et pour pouvoir faire des match dans se mode de jeux et surement un tournoi étranger\r\n\r\n[SIZE=4]Qui sera admi a entrainement:[/SIZE]\r\nTous les apax qui souhaite faire de la ruée en escouades les premier qui meterons sur le site qui sont la seront prioriter a entrainement et au match de se mode de jeux \r\n\r\n[SIZE=4]Détail de entrainement[/SIZE]\r\nMAP : metro /teherand /bazar/ karg\r\nREGLE: Régle promod \r\nSERV : serv apax configurer en priver avec les parametres promod\r\nDurée : entre 2H A 2H30\r\n___________________________________\r\nJe donnerai les strats a apporter je jour même \r\nun grand merci a vous loup aaaaooouuuu '),
(1133, 10, 112, 1368176635, 43, 'Moi je veux bien essayer, c''est un mode de jeu que je n''ai jamais tenté.'),
(1134, 6, 3, 1368177455, 89, 'Je propose si quelque peu le faire de trouver les règle promod pour ruée en escouade 4 vs 4 pour 12 aussi en gros tout les réglé officiel et aussi préparer une rubrique tournoi svp pour les tournoi que on va trouver et que je créer merci ps le site et top j aime bien venir dessus aaaooouuuu    merci les loup de faire se que vous faite pour la meute'),
(1135, 12, 56, 1368182967, 75, 'merci a tous !!!\r\n\r\nAOUUUUU !!!'),
(1136, 12, 56, 1368185197, 10, 'Bienvenue  a toi , y''a plus qu''à nous montrer qu''on ne sait pas tromper :)\r\nRRRRRRRRrrrrr AOUUUUUUUUUU'),
(1137, 3, 84, 1368185949, 89, 'avé gaulois bonne chance pour ton recrutement aaaaooouuuuu'),
(1138, 3, 83, 1368186027, 89, 'bonne chance pour ton recrutement '),
(1139, 12, 56, 1368187196, 81, 'Bienvenue dans la meute :) '),
(1140, 3, 84, 1368187216, 100, 'Merci Mec ça fait pléz'),
(1141, 3, 84, 1368187286, 81, 'J''ai eu l''occasion  de faire des bonnes partie avec toi hier pour moi +1 :) '),
(1142, 3, 103, 1368187398, 81, 'Bonne chance pour ton recrutement :) '),
(1143, 6, 102, 1368194075, 34, 'bye bye meme si je te connait pas et vu ce que manue a dit je préféré pas te connaitre et j''en resterai la :)'),
(1144, 6, 101, 1368194233, 34, 'toujours present pour du full Assaut *__*'),
(1145, 3, 84, 1368196125, 100, 'Pareil pour moi  c etait Une bonne soirée +100 MDRRRR'),
(1146, 6, 101, 1368198708, 11, 'Alors sa m''interesse seulement je n''arrive pas a jouer full assaut mais ce qui est de grosse partie j''ai l''habitude ( je ne joue que sur des grosse partie sur grand bazzard et metro c''est les seuls map).'),
(1147, 10, 112, 1368199968, 88, 'vas y je me propose , il n''y a pas de problème pour moi.'),
(1148, 3, 103, 1368203220, 106, 'merci les gars a ce week end \r\n'),
(1149, 7, 113, 1368203710, 34, 'Yop la meute voila je voulais vous proposer un projet pas pour maintenant bien sur ne vous emporter pas (surtout toi ben xD ) donc voila je trouver sa cool de voir les LAN avec les team et leur sublime maillot et je me suis renseigner et j''ai trouver le site ou les maillot sont créer et donc je voulais savoir si plu-tard sa vous dirais d''avoir chacun un maillot ApaX a son nom ? :)\r\n\r\nle lien du site [URL][COLOR=#32CD32]http://www.ouikos.com/[/COLOR][/URL]'),
(1150, 7, 113, 1368206990, 89, 'sisi moi j aimerai bien sa peu le faire bonne idee moi je suis pour aaaooouuu'),
(1151, 7, 113, 1368214079, 5, '^^ je t''est déjà devancé, j''avais lancer l''idée sur l''ancien site, j''avais meme fait des models ;)'),
(1152, 7, 113, 1368214321, 25, 'Bonne idée :)'),
(1153, 6, 111, 1368269989, 1, 'Pour : 1\r\nContre : 0\r\n\r\n\r\n(Récuperez mon message pour poster, et faites +1 dans votre réponse, ça fera gagner du temps ^^)'),
(1154, 6, 111, 1368288599, 5, 'J''ai déjà discuté avec le fondateur de ce tournoi (pas facile en italien ^^) donc un +1 vu que c''est du squad rush ;)\r\n\r\n\r\nPour : 2\r\nContre : 0\r\n\r\n\r\n');
INSERT INTO `forum_posts` (`postID`, `boardID`, `topicID`, `date`, `poster`, `message`) VALUES
(1155, 6, 114, 1368296299, 11, 'Alors voila j''aimerait connaitre les objectifs de chaque Apax pour mieux se connaitre entre nous est s''aider entre nous.^^\r\nAlors voici les miens.\r\nObjectif: \r\nAtteindre le pigeon 100. 41/100 \r\nObtenir 5 étoiles de service avec chaque armes. 0/5 \r\nObtenir 100 étoiles de service avec tous les véhicules. 0/100 \r\nObtenir 100 étoiles de service avec toutes les classes. 4/100 \r\nAvoir toutes les plaques (tournois déduites) débloquè non prises: 312/515. '),
(1156, 6, 114, 1368297221, 89, 'moi 5 etoile avec tout les arme\r\n100 etoile en char 58/100\r\nprendre le plus de plaque posible: 4475 plaque prise\r\naugmenter mon ratio\r\naussi avan bf4 avoir fini met 3 tournoi 3  leur nom gfb tournament: fini ligue gfb 1/G : en cour\r\napax tuom tournation qui va bientot commencer\r\naaaooooouuuuuuuuuuuuu'),
(1157, 6, 114, 1368299601, 11, 'Pas mal ^^'),
(1158, 6, 114, 1368310922, 5, 'Pour ma part mon objectif n''est pas vraiment au niveau des stat, c''est plus faire en sorte que les loups est le meilleur templay possible pour que dès qu''on arivera sur bf4, on cartone tous dès le début ;)\r\n\r\nps: si je peut remonter mon ration a 1.5 a serai pas mal aussi (1.3 actuel sans aucune rès)'),
(1159, 6, 114, 1368317710, 81, 'Pour moi : \r\nColonel 100 \r\n100 étoiles de services  M16 \r\n100 étoiles de services classe assaut\r\n300 kill au défibrillateur \r\nEt garder un ratio au dessus de 2.50 :) \r\n'),
(1160, 6, 101, 1368317790, 81, 'Pour moi ce serait du genre métro ou bazar avec 1750 en gros des parties de 1h30 a jouer a fond tout le long. '),
(1161, 6, 114, 1368327736, 11, 'Merci Dartoch et moi aussi aucun rebbot depuis que j''ai Bf3 et Ciid 300 kill au défibri bon courage ^^'),
(1162, 6, 114, 1368347197, 2, 'pour moi c''est d''atteindre les 10000 kills a l''hélico d''attaque j''en suis a plus de 9000\r\npas de reset non plus depuis que j''ai bf3 '),
(1163, 7, 115, 1368351581, 5, 'Salut a tous, d''ici peut de temps, une réunion au sein du staff va avoir lieu, je voudrai donc savoir si vous avez des questions, suggestion ou autre don vous voudriez que l''on aborde?\r\n\r\nPour l''instant, voici les thème qui serons abordé:\r\n[COLOR=#B22222]\r\n- Avec l’arrivé de bf4 sur ps4, comment se passera l''évolution de la meute avec se nouveau support? \r\n- est-ce que nous voulons réellement faire de l''eSport ou du fun?\r\n- Définir les règles définitive sur le serveur\r\n- Définir une charte pour la Meute qui devra être approuver et signé par toute personne souhaitant nous rejoindre\r\n-\r\n-\r\n[/COLOR]\r\n\r\nmerci de répondre assez rapidement que l''on puisse organiser cette réunion le plus rapidement possible pour le bien être et l''évolution sereine de la meute ;)'),
(1164, 6, 114, 1368351717, 11, 'Bin jouè Gp pour tes 9000 Kill a l''helico ^^'),
(1165, 7, 113, 1368367172, 10, 'Écoute pourquoi pas :) et le porter pendant le salon Battlefield 4 a Paris ou pas mal d''équipe vont se rencontrer !!\r\n'),
(1166, 7, 113, 1368372056, 5, '[quote=WAZA-PsY-4-x]Écoute pourquoi pas :) et le porter pendant le salon Battlefield 4 a Paris ou pas mal d''équipe vont se rencontrer !!\r\n[/quote]\r\n\r\n[SIZE=5][COLOR=#FF0000]+1[/COLOR][/SIZE]\r\n\r\ndailleur quelqun a les date et le lieu ?\r\n'),
(1167, 7, 113, 1368372307, 11, 'Il est quand ce salon ???'),
(1168, 7, 115, 1368376968, 1, 'C''est qui le staff et, il faut parler exactement du devenir de la team, cad  est-ce que nous voulons réellement faire de l''eSport ou du fun. Les 2 sont différents. Fun , on prends des partenaires style bf18, 2099 ou ce genre et on ne fait que du trolol, ou on monte une squad pour de la compet.\r\n\r\nSi également vous pourriez entre vous parler des éventuels bug qu''on puisse les corriger, et voir pour avoir le café par manu en costume de pinpon looool'),
(1169, 7, 113, 1368377245, 1, 'à Paris ? Je pense que ce sera l''espace Champerret.\r\nPour les dates, je dirais juste avant la sortie, et pour un impact majeur, pendant les vacances scolaires de la zone, soit, entre fin aout et début octobre :)'),
(1170, 6, 114, 1368377285, 1, 'Avoir un KD ratio de 1 ? :D (Allez vous faire foutre !)'),
(1171, 12, 56, 1368377330, 1, 'Bienvenue chez nous, je ferme le sujet ;)'),
(1172, 10, 112, 1368377411, 1, 'Y''a des partants pour refaire l''entrainement vers 19h30 ? :-)'),
(1173, 6, 114, 1368378407, 11, '??? pk allez vous faire foutre ???'),
(1174, 7, 117, 1368382513, 89, '                         [COLOR=#00008B]Compte Rendu De Entrainement Dimanche 12 MAI 2013\r\n\r\n                                 mode : ruée en escouades \r\n[/COLOR][SIZE=4][/SIZE]\r\nles loup present\r\n\r\nequipe 1\r\n\r\ndartoch \r\nalex\r\nslimmorle\r\ncidzz\r\n\r\nequipe 2\r\n reaper\r\n wait\r\nlaurent\r\nita\r\n\r\nbilan : Malgré le gros problème du serveur sa c est très bien passer nous avons pu nous entraîner sur metro et teherand nous avons passer un bon moment dsl met pas grand chose a dire \r\ncontent avoir organiser se petit entrainement merci d avoir était present'),
(1175, 7, 115, 1368382672, 8, 'ecoute je pense que ca peut se passer cette semaine faudrait qu''on se skype il y a le troll qui vient chez moi cette semaine donc il aura le temps d''y reflechir entre deux gueules de bois'),
(1176, 7, 113, 1368382775, 8, 'ou pour le porter a la CDFBF4 ps4'),
(1177, 6, 101, 1368383022, 8, 'moi je suis chaud aussi'),
(1178, 6, 3, 1368383661, 89, 'up je met ou aussi les détail des tournoi qui vons  commençais svp merci'),
(1179, 7, 113, 1368387748, 5, 'la quoi reap?'),
(1180, 7, 115, 1368388196, 5, 'Pour le staff j''ai pris en compte le leadeur, co-leadeur et recruteur mais on peut aussi voir pour prendre d''autre personne comme webmaster pour les sujet concernant les problème technique\r\n\r\nPour cette semaine on va essayé de faire le tout pour après faut juste que les membre poste leurs demande très rapidement donc parlé en a tout les membres que vous croisé ;) plus vite les sujets serons là et plus vite on pourra se calé sa :p'),
(1181, 6, 3, 1368388358, 5, 'créé un nouveau sujet dans "préparation de match" ;)'),
(1182, 6, 114, 1368388427, 5, 'Pars qu’il sait que je vais le chambré !!!! lol'),
(1183, 6, 114, 1368388987, 11, 'a ok XD'),
(1184, 7, 113, 1368390499, 8, 'coupe de france bf4'),
(1185, 7, 115, 1368390781, 8, 'on se check de toute maniere'),
(1186, 7, 113, 1368432823, 5, '^^ ok\r\n\r\noui pourquoi pas et meme en t''en que participent... :p'),
(1187, 7, 115, 1368432900, 5, 'pas de soucis, je serai co cette aprem si vous êtes là ;)'),
(1188, 6, 3, 1368437682, 1, 'Pour le moment, nous n''avons pas suffisament de matchs prévus pour le tournois qui nécéssite un nouveau forum. Si la gestion des préparatifs match && TN sur un seul forum vous gène, j''en ajouterais un ;)'),
(1189, 7, 115, 1368444053, 89, 'Moi je peut être la si vous avez besoins après au niveau des question j y réfléchie bcp'),
(1190, 10, 118, 1368445036, 89, '                                           [COLOR=#00008B]Torneo MV CRSQ Ruée 4 Vs 4        \r\n                                              Tournoi de Rush Squad[/COLOR]\r\n 0rganisé par le clan Milites Victoriae [MV]. Seuls les chefs de section peuvent s''appliquer à ce peloton si elles souhaitent participer au tournoi.\r\n \r\nLes cartes seront un choix de Téhéran ou Opération Métro.\r\n\r\n Les règles sont les suivantes: NO pompe,\r\n                                No M320 (seulement fumée permis),  \r\n                                No mortier\r\n                                NO claymore (M18)\r\n                                No de C4\r\n\r\n\r\nPDW c''est seulement autorisés pour la reconstruction. Pour régler l''heure / date du match, contactez le leader adversaire respectif. Le préréglage est: Normal. Pour toute information complémentaire concernant le contact du tournoi: SaTa_Lukas-Noob ou Seablackbird7\r\nLien du tournoi :http://battlelog.battlefield.com/bf3/fr/platoon/2832655391981919090/'),
(1191, 10, 119, 1368445733, 89, '                                          [SIZE=4]Tournoi APAX TUOM tour nation \r\n                                                 12 vs 12 CONQUETE[/SIZE][COLOR=#00008B][/COLOR]\r\n-------------------------------------------------------------- \r\nLes créateurs : \r\n\r\nMembre de la team apax ( slimmorle) \r\nLeadeur de la team tuom (TUOMDeadMemorieS) \r\n\r\n______________________________________ \r\nLien du site avec les résultats : http://apaxtuomtournation.xooit.fr/index.php \r\n\r\n_____________________________________ \r\n\r\nDescription du tournoi : \r\nLa team apax et la team tuom organise son tournoi ouvert à tous les team que se soi français ou de n’importe quel pays qui peuvent réunir 8 A 12 soldat il se déroulera sous forme de forme de poule les deux premier de chaque poule seront qualifier pour les quart de final demi et final map aller-retour et se jouera au nombre de ticket tous les résultat sont sur le forum merci a tous \r\n________________________________ \r\n\r\n\r\nDetail tournoi \r\nMode : normal \r\nMap : Tous maps autorisé \r\nNombre de joueur par équipe : T8 ou T12 \r\n\r\nRègle tournoi \r\nNo c4 infanterie et sur drapeau \r\n1 pompe par équipe \r\n------------------------------------------------------------- \r\nComment nous rejoindre le tournoi : \r\nRien de plus simple Contacter sur battlelog les créateurs du tournoi slimmorle ou TUOMDeadMemorieS qui Veron pour vous accepter \r\nDès que vous êtes accepté communiquer le nom de votre team le tag et les personne à contacter pour les matchs \r\n----------------------------------------------------------------- \r\nQuand commence le tournoi ? \r\nLe tournoi commencerons quand les créateurs du tournoi aurons trouvez les 14 Teams \r\n------------------------------------------------------ \r\nEn cas ou vous ne pouvez pas continuer le tournoi \r\nPrévenez les créateurs que vous ne pouvez continuer et le mieux si vous arrêter le tournoi trouvez nous une team pour vous remplacer \r\n------------------------------------------------------ \r\n\r\nPour valider votre match: \r\n\r\n1ere solution : \r\nSoit vous faites le match en privé et en cas de désaccord sur la finalité du match il y aura discutions avec les créateurs du tournoi et les deux leaders de chaque team. \r\n\r\n2éme solution : \r\nVous bloquez votre serveur 9 vs 9 et les 2 créateurs du tournoi sera dans le match et restera dans le déploiement et surveillera le match. \r\n\r\n3éme solution: \r\nVous faites le match et il me faut absolument les rapports de combat. \r\nA vous de vous arranger \r\nEn cas de non-respect des règles, la team qui n’aura pas respecté les règles sera déclarée comme une défaite chose que j’espère n’arrivera pas '),
(1192, 1, 70, 1368453691, 88, 'jguigi'),
(1193, 7, 117, 1368454655, 88, 'cool cette entraimement merci slimmorle'),
(1194, 1, 71, 1368455012, 88, 'bienvenue slim '),
(1195, 6, 3, 1368477017, 11, 'Ok que pensez vous d''un reglement pour eviter les cas comme Kev car d''après mes souvenir c''est pas le 1er cas comme ceci et j''ai toujour mon réglement en attentes ??? je pensse que celui-ci vous plairat.'),
(1196, 10, 118, 1368477310, 11, 'Qui sont les participant de la Apax.'),
(1197, 10, 119, 1368477401, 11, 'Qui est inscrit parmit les Apax.'),
(1198, 10, 119, 1368521428, 89, 'il y a pas équipe précise de loup que seul qui veulent aaaaaoouuuu je précise que notre premier match sera le premier week end du moi de juin le tournoi début le vendredi 31 mai je précise que se n est pas la date de notre premier match '),
(1199, 10, 120, 1368523008, 89, '                                            Entrainement Dimanche 19 Mai A 15H\r\n                                                     conquete \r\n__________________________________________________________________________________\r\n\r\nPréparation pour le tournoi apax tuom tour nation\r\n\r\nmap : bazar et karg\r\nrègle : 1 pompe par équipe \r\n        no C4 drapeau et infanterie\r\n        durée 2H\r\n\r\nDétail :a 15H je mettrais 2 chef escouade je leur donnerai tout les info vous gérerai votre escouade vous seul mettrai voix équipe et donnerai tout les info a votre escouade et au deux autre  chef escouades\r\n\r\nJ aimerai avoir le plus apaxxx possible afin que on puisse bien se préparer a se tournoi que j ai créer\r\n\r\ndite moi si vous êtes présent sa sera plus simple pour organisation merci les loups'),
(1200, 7, 115, 1368538177, 5, 'toujour pas d''autre question? sa va pas etre une réunion mais une pause café...lol Manu aporte la gnole !!!!! ;)'),
(1201, 1, 70, 1368546549, 81, 'Tes dispo dans la semaine pour soirée FIFA et tacos bourritos avec Pedro et Pipo ? '),
(1202, 10, 119, 1368548608, 11, 'je suis partant'),
(1203, 7, 115, 1368553091, 89, 'moi j en ai une sa fait peu de tem que suis la met j aimerai bien avoir ma place entemp que co leadeur si possible  esque sa serai possible que quelqun fait une video de presentation avec tout les apax ?\r\napres je  n est aucune aucune question\r\n'),
(1204, 3, 103, 1368553223, 89, 'je trouve que il faut accepter donc dite moi les apax si je lui dit si il et accepter parmi les louppp merci '),
(1205, 3, 84, 1368553313, 89, 'je suis pour qu il devien a un loup dite moi quoi que je fait mon role de recrutement merci les loup'),
(1206, 3, 84, 1368558002, 5, 'Pour moi c''est oui ;)'),
(1207, 3, 103, 1368558138, 5, '+1 pour ma part mais j''atend quand meme l''avi de reaper vu qu''il devait s''occupé de sont test'),
(1208, 10, 119, 1368558211, 5, 'partant aussi bien sur ;)'),
(1209, 8, 121, 1368559323, 1, 'Salut les filles, suite au problème remonté par dartoch :\r\n\r\n- Y''a pas de mails qui partent du site\r\n- Le code de validation à l''inscription (captcha) est invisible\r\n\r\nje m''en viens vous dire que c''est tout bon réparé.\r\n\r\nMerci à Aulyro pour son aide :)'),
(1210, 8, 121, 1368559587, 11, 'Y a pas de quoi ma biche du moment ou je vous fait plaisir XD'),
(1211, 8, 121, 1368559804, 1, '[quote=Aulyro1996]Y a pas de quoi ma biche du moment ou je vous fait plaisir XD[/quote]\r\n\r\n[IMG]http://funny-pictures-blog.com/wp-content/uploads/2012/06/Funny-GIF-LOLCat.gif[/IMG]'),
(1212, 8, 121, 1368559992, 11, 'C''est quoi cette Vidèo XD elle me fait bien rire ^^'),
(1213, 3, 103, 1368564509, 4, 'attends Dartoch je n''ai pas encore joué avec lui j''aimerais voir ce qu''il vaut quand meme. Merci. PSN : Pignouf30'),
(1214, 10, 120, 1368564696, 4, 'c''est de quel type l''entrainement ? 5vs5 ; 8vs8 ; 12vs12 ; SquadRush ?'),
(1215, 1, 21, 1368565606, 11, '44 menbres nous sommes ^^ une grande famille je vous aime tous mes loulou XD'),
(1216, 10, 120, 1368606991, 89, 'juste je v deja voir qui sera la le mieu serai de entrainer en t12 les map seron bazar et seine car on ma demander que on entraine sur des map que on a jamais fait\r\n'),
(1217, 8, 121, 1368612771, 5, 'bien joué les mecs, je vais passé l''info au membre qui voulais s''inscrire de suite ;)'),
(1218, 3, 103, 1368613571, 8, 'ecoute pour moi j ai fait de bonnes parties avec lui, pour moi il a un bon esprit , un bon teamplay, le sens du sacrifice parfois donc moi je me prononce sur un oui, apres pignouf vas y teste le aussi et si tu es pris, ne fais pas comme certain a partir apres le premier probleme parce que tout n''est pas parfait dans une equipe et surtout dans une de 40 joueurs, le tag implique également un total respect envers tous les autres loups bien entendu '),
(1219, 3, 84, 1368613709, 8, 'pour moi aussi c''est bon de bonne partie avec lui et genant quand il etait en face donc up pour lui'),
(1220, 7, 115, 1368615252, 10, 'Pr ta réponse Slim , etant donné que tu es recruteur ac Reaper , et que ta venue est assez récentes , je préfère avoir un peu de temps vis a vis des autres APAX , je sais que tu fais déjà énormément pr la Meute ( tactique , entraînement , tournoi ...) et je t''en remercie , mais ne tkt pas dans un avenir proche , on en reparlera en tête a tête !! \r\nPour ta vidéo , c une bonne idée ... Je pourrais m''en charger !!'),
(1221, 3, 103, 1368615657, 106, 'je t''ajoute aujour d''hui pignouf a cet aprem\r\n'),
(1222, 7, 115, 1368618046, 5, '[quote=WAZA-PsY-4-x]\r\nPour ta vidéo , c une bonne idée ... Je pourrais m''en charger !![/quote]\r\n\r\nEncore plus de video !!! on en a jamais assez !!!!! ;)'),
(1223, 10, 120, 1368684960, 4, 'Oui jamais fait en T12 mais le 5vs5 on connait déjà bien sur bazard et traversée de la seine'),
(1224, 12, 59, 1368693520, 5, 'Départ de la meute au bou de 15 jour sans explication et sans prévenir...\r\n\r\n[COLOR=#FF0000][SIZE=5]Recrutement Annuler[/SIZE] [/COLOR]'),
(1225, 6, 122, 1368719442, 81, 'Salut tout le monde vous avez sûrement du voir mon changement de section sur le battlelog , et que soit disant je part sans prévenir c''est vrai dans un certain sens car ça c''est fait hier quand j''ai repris battlefield c''est grave a des amies in game qui m''ont redonner le goût de jouer et avec qui je suis rentrer dans ma première team avec le temps chacun est un peut parti de son côté d''où le fait que j''ai rechercher une team. Il y a 2 jours ils sont revenu vers moi pour me proposer de rentrer dans une team avec et de rejouer ensemble. Au début je savais pas trop quoi faire car déjà envers vous je trouvais pas ça juste vu que vous l''avez fait confiance pour me prendre dans la team puis au final j''ai décider de les rejoindre hier quand j''ai opérer les changement battlelog il était environ 3h du mat d''où le fait que j''ai prévenu personne personne était en ligne quand je me suis connecter. En tout cas voilà encore désoler même si je ne suis rester que 15 jours j''ai vraiment apprécier jouez avec vous la APAX est une bonne team changer rien et même si je part c''est pas une raison pour arrêter de jouez ensemble !  Après c''est vous qui voyez à bientôt tout le monde. '),
(1226, 6, 122, 1368721615, 5, 'les mecs si vous voulez commenter faite vous plaisir, pour ma part je n''en est aucune envi, j''ai déjà réglé se sujet pour ma part...'),
(1227, 6, 122, 1368726241, 11, 'Ciid sache que je respect ton choix je suis neutre. Mais je pense que certain apax vont te dire ceci certes 3 du mat est personne de co mais tu aurait pu nous laisser un message. Voila tout mais comme tu la dit maintenant que je tes en ami sa m’empêchera pas de jouer avec toi.\r\nSur ceux je te souhaite bonne continuation et a la prochaine sur le champs de bataille.\r\n'),
(1228, 7, 115, 1368726878, 5, 'des idées on été rajouter ;)'),
(1229, 10, 118, 1368728999, 5, 'Alors pour le tournoi MV (4vs4) on peut le commencé dès que près vu qu''il n''y a pas beaucoup de team adverse et donc pas de poule, il nous suffit donc de prendre contact avec les autre team pour organiser les matchs (on fait tout le boulot en gros lol) je vous demande donc se qui serait partant !!!!! ;)\r\n\r\njoueurs:\r\n- Psy\r\n- Pignouf\r\n-\r\n-\r\nremplacent:\r\n-\r\n-'),
(1230, 10, 118, 1368802612, 11, 'Moi présent le mercredi après midi et les week end sauf exception sa fait un bye que j''ai pas fait de match.'),
(1231, 7, 113, 1368806620, 21, 'OK pour moi'),
(1232, 7, 117, 1368807211, 21, 'Encore merci Slim ...'),
(1233, 10, 119, 1368807512, 21, 'Me too ...'),
(1234, 3, 84, 1368841237, 100, 'Merci a vous Mais je trouve que Le recrutement laisse a desirer  vs etes plus de 40 membres et peu de personnes s y interesse! Enfin bref'),
(1235, 3, 84, 1368854094, 2, 'tu a pas mal de oui donc pas de soucis pour ton recrutement '),
(1236, 3, 84, 1368874765, 5, '[quote=x-lE-GAULOIS]Merci a vous Mais je trouve que Le recrutement laisse a desirer  vs etes plus de 40 membres et peu de personnes s y interesse! Enfin bref[/quote]\r\n\r\nC''est tout simplement parsqu''il y a des personne qui sont chargé des recrutements, les autre membre peuvent donné leur avis mais se ne sont que quelque membre qui choisisse '),
(1237, 3, 84, 1368876767, 11, 'j''accepte cette new recrut si il me ramene du rhum des femmes et dla biere mon dieu ^^\r\n'),
(1238, 3, 103, 1368884058, 106, 'vous inquiété pas j''suis pas du genre à partir au moindre problème '),
(1239, 1, 71, 1368884488, 106, 'moi aussi je sur kiff le catch et surtout la fédération WWE\r\n'),
(1240, 3, 84, 1368894698, 5, 'c''est pas "mon dieu" mais "non de dieu" :p'),
(1241, 3, 84, 1368901338, 11, 'petite faute XD'),
(1242, 1, 71, 1368901933, 89, 'oui moi je kiff aussi de plus que maintenant il y a les evenement sur ab1 sa dechire tout. je regarde depuis que je suis petit a époque de la WWE avec hulk hogan hbk undertakeur et tous les  ancien mon prefere undertakeur celui que je déteste le plus cm punk et brock   '),
(1243, 3, 84, 1368908580, 5, 'Bon plus sérieusement ^^ \r\n\r\nLe Gaulois, cela fait déjà 15 jour que tu a postulé donc il est temps de te donné une réponse\r\nAu vu des ressenti des membres a ton égard et de se j''ai vu aussi je te convie donc a ajouté le Tag Apax devant ton pseudo ;)\r\nBienvenu parmi nous !!!! et j’espère que j''ai raison de faire de toi le premier membre que j''accepte dans la meute... je te fait confiance ;)\r\n\r\nEncore bienvenu a toi mon grand ^^'),
(1244, 3, 84, 1368923727, 100, 'Enfin lol merci et je ne te decevrais pas crixus est fier de faire partir de la Maison des APAX'),
(1246, 8, 124, 1368963775, 25, 'Je tien à vous faire part de mon départ de la team apax. Raison pas trop de temps de jeu = pas d''entrainementpour jouer les match, et je n''ai pas su connaitre plus les joeurs trop nombreux peut-être. Enfin bref j''espere partir sans blabla j''ai apprécier c''est quelques mois et bonne continuation a tous :)\r\n\r\nCordialement Atonium...'),
(1247, 7, 113, 1368964570, 5, 'pour vous donnez une petite idée de se qu''ils sont capable de faire allez sur se lien\r\nhttp://www.ouikos.com/fr/content/16-historique-des-creations\r\n\r\nFranchement la qualité est vraiment top, par contre il créé tout eux même, se qui veut dire qu''il y a un minimum a commandé pour qu''ils accepte la production, donc il va nous falloir une liste des personnes intéressé ainsi qu''un logo définitif si vous ne voulez pas garder l''actuel\r\n\r\nA vous de voir, pour ma part je suis partant ;)'),
(1248, 8, 124, 1368983725, 5, 'Bonne chance pour la suite'),
(1249, 10, 120, 1368986605, 89, 'ANNULER DSL '),
(1250, 10, 120, 1368987193, 5, 'Pas grave mon grand ;)'),
(1251, 8, 124, 1368989891, 2, 'pas de soucis au suivant !'),
(1252, 8, 124, 1368990469, 10, 'LOOOL Gp :) \r\nPas de soucie , et bonne continuation ..\r\n\r\n\r\nPs : je ne pense pas que nous sommes trop de joueurs , ( 6 a 7 connectés en même temps , la plus part du temps ) , j''anticipe juste la nouvelle arrivée de console ou de jeux comme GTA qui vont je pense faire beaucoup de mal a pas mal de Team .. Alors que les APAX resteront toujours fière est debout dût au 38 APAX que ns sommes aujourd''hui ;) '),
(1253, 6, 122, 1368991520, 1, '[quote]déjà envers vous je trouvais pas ça juste vu que vous l''avez fait confiance pour me prendre dans la team[/quote]\r\n\r\nMais juste au début en fait c''est ça ?');

-- --------------------------------------------------------

--
-- Structure de la table `forum_ranks`
--

CREATE TABLE IF NOT EXISTS `forum_ranks` (
  `rankID` int(11) NOT NULL AUTO_INCREMENT,
  `rank` varchar(255) NOT NULL DEFAULT '',
  `pic` varchar(255) NOT NULL DEFAULT '',
  `postmin` int(11) NOT NULL DEFAULT '0',
  `postmax` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`rankID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Contenu de la table `forum_ranks`
--

INSERT INTO `forum_ranks` (`rankID`, `rank`, `pic`, `postmin`, `postmax`) VALUES
(1, 'Rank 1', 'rank1.gif', 0, 9),
(2, 'Rank 2', 'rank2.gif', 10, 24),
(3, 'Rank 3', 'rank3.gif', 25, 49),
(4, 'Rank 4', 'rank4.gif', 50, 199),
(5, 'Rank 5', 'rank5.gif', 200, 399),
(6, 'Rank 6', 'rank6.gif', 400, 2147483647),
(7, 'Administrator', 'admin.gif', 0, 0),
(8, 'Moderator', 'moderator.gif', 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `forum_topics`
--

CREATE TABLE IF NOT EXISTS `forum_topics` (
  `topicID` int(11) NOT NULL AUTO_INCREMENT,
  `boardID` int(11) NOT NULL DEFAULT '0',
  `icon` varchar(255) NOT NULL DEFAULT '',
  `readgrps` text NOT NULL,
  `writegrps` text NOT NULL,
  `userID` int(11) NOT NULL DEFAULT '0',
  `date` int(14) NOT NULL DEFAULT '0',
  `topic` varchar(255) NOT NULL DEFAULT '',
  `lastdate` int(14) NOT NULL DEFAULT '0',
  `lastposter` int(11) NOT NULL DEFAULT '0',
  `lastpostID` int(11) NOT NULL DEFAULT '0',
  `replys` int(11) NOT NULL DEFAULT '0',
  `views` int(11) NOT NULL DEFAULT '0',
  `closed` int(1) NOT NULL DEFAULT '0',
  `moveID` int(11) NOT NULL DEFAULT '0',
  `sticky` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`topicID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=125 ;

--
-- Contenu de la table `forum_topics`
--

INSERT INTO `forum_topics` (`topicID`, `boardID`, `icon`, `readgrps`, `writegrps`, `userID`, `date`, `topic`, `lastdate`, `lastposter`, `lastpostID`, `replys`, `views`, `closed`, `moveID`, `sticky`) VALUES
(1, 1, '', '', '', 1, 1361456101, 'Premier sujet sur le fofo', 1363990541, 16, 340, 10, 470, 0, 0, 0),
(2, 7, '', '2', '2', 5, 1361738164, 'Création des escouades', 1364207252, 1, 372, 46, 476, 0, 0, 0),
(3, 6, '', '', 'user', 2, 1361822034, 'vos idées pour le forum ', 1368477017, 11, 1195, 16, 402, 0, 0, 0),
(4, 6, 'ausrufezeichen.gif', '', 'user', 1, 1361982669, 'Match Minimeute ce soir ?', 1363346766, 1, 179, 7, 213, 0, 0, 0),
(5, 3, 'ausrufezeichen.gif', '', 'user', 5, 1361998633, '"Demande type"', 1361998633, 5, 0, 0, 136, 1, 0, 1),
(6, 5, '', '', 'user', 4, 1362121351, 'Demande des sFG', 1362329166, 1, 43, 3, 74, 1, 0, 0),
(7, 12, '', 'user', 'user', 16, 1362324853, 'recrutement', 1363976943, 1, 332, 31, 892, 1, 0, 0),
(8, 6, '', '', 'user', 1, 1362329055, 'Crysis 3', 1363001421, 1, 133, 4, 125, 0, 0, 0),
(9, 7, 'lampe.gif', '2', '2', 1, 1362339651, 'Modifications qu''il reste à apporter au site', 1363898370, 1, 300, 19, 120, 0, 0, 0),
(11, 5, '', '', 'user', 11, 1362486696, 'Tournoi Match TSG', 1362610509, 19, 94, 8, 216, 0, 0, 0),
(12, 6, '', '', 'user', 16, 1362514464, 'chasseur de trophé', 1363724905, 4, 219, 11, 490, 0, 0, 0),
(13, 5, '', '', 'user', 11, 1362527323, 'Demande des EcLypsia eSport.', 1362564246, 1, 82, 1, 98, 0, 0, 0),
(14, 7, '', '2', '2', 20, 1362605839, '2099 vs Apax', 1366445961, 1, 693, 60, 1441, 0, 0, 0),
(15, 7, 'ausrufezeichen.gif', '2', '2', 1, 1362648187, '[A LIRE URGENT] Disponibilités pour le 12v12 (contre les 2099)', 1365869794, 2, 574, 53, 563, 0, 0, 0),
(16, 6, '', '', 'user', 1, 1363001472, 'Tekken 6', 1363258002, 1, 171, 10, 400, 0, 0, 0),
(17, 7, 'ausrufezeichen.gif', '2', '2', 1, 1363080623, 'Le site need you', 1363853892, 1, 255, 18, 138, 0, 0, 0),
(18, 12, 'pfeil.gif', 'user', 'user', 25, 1363103847, 'Demande de recrutement AtOnIuM_GhAuX', 1363816050, 10, 249, 13, 441, 1, 0, 0),
(19, 6, 'ausrufezeichen.gif', '', 'user', 1, 1363266458, 'Nouvelle fonctionnalité du site', 1363266458, 1, 0, 0, 84, 0, 0, 0),
(20, 5, '0', '', 'user', 22, 1363437015, 'Demande de la Tear of skyline XIII pour entrainement en T4-T5(plus T4)', 1363461030, 22, 190, 3, 135, 0, 0, 0),
(21, 1, '0', '', '', 5, 1363516623, 'Composition de la meute', 1368565606, 11, 1215, 1, 344, 0, 0, 1),
(22, 6, '', '', 'user', 27, 1363540631, 'Salut la Meute', 1363548237, 1, 204, 3, 124, 0, 0, 0),
(23, 7, '', '2', '2', 5, 1363544297, 'nouveau serveur', 1363866613, 2, 264, 24, 145, 0, 0, 0),
(24, 6, '0', '', 'user', 27, 1363720433, '[Service] Signature ', 1363869395, 10, 278, 14, 548, 0, 0, 0),
(25, 6, 'smile.gif', '', 'user', 10, 1363864714, 'Nouvelle Vidéo !? ', 1366446585, 1, 695, 23, 699, 0, 0, 0),
(27, 6, 'ausrufezeichen.gif', '', 'user', 10, 1363896835, 'Binôme de CHOC !! A remplir OBLIGATOIREMENT ', 1366446159, 1, 694, 30, 1305, 0, 0, 0),
(28, 8, '', '', 'user', 2, 1363936760, 'battlefield 4', 1365443957, 5, 524, 5, 150, 0, 0, 0),
(29, 9, '', '', 'user', 1, 1363978001, 'Préparation de la soirée anniversaire Avril 2013', 1366229748, 8, 636, 10, 395, 0, 0, 0),
(30, 12, 'ausrufezeichen.gif', 'user', 'user', 37, 1364148252, 'Recrutement: ', 1364797494, 5, 469, 11, 320, 1, 0, 0),
(31, 6, 'text.gif', '', 'user', 34, 1364241215, 'Serveur APAX', 1364289220, 1, 387, 1, 93, 0, 0, 0),
(32, 10, 'ausrufezeichen.gif', '2', '2', 10, 1364244229, 'Préparation Conquête tempête de Feu ', 1365843277, 8, 572, 32, 316, 0, 0, 0),
(33, 10, 'ausrufezeichen.gif', '2', '2', 10, 1364244435, 'Préparation Rue Pic de damavand ', 1365791183, 5, 558, 37, 332, 0, 0, 0),
(34, 7, '', '2', '2', 1, 1364298333, 'La meute sur google (Statistiques = chiant)', 1367099240, 10, 808, 17, 117, 0, 0, 0),
(35, 8, '', '', '', 1, 1364298333, 'La meute sur google (Statistiques = chiant)', 1364298496, 0, 0, 0, 0, 0, 34, 0),
(36, 1, '', '', '', 34, 1364483646, 'Presentation des nouveau Membres', 1364483646, 34, 0, 0, 61, 0, 0, 0),
(37, 12, '0', 'user', 'user', 43, 1364546637, 'Demande de Recrutement Simrreuhh', 1366719356, 1, 730, 17, 317, 1, 0, 0),
(38, 12, '', 'user', 'user', 21, 1364567799, 'Demande de recrutement', 1366719457, 1, 732, 20, 362, 1, 0, 0),
(39, 11, 'ausrufezeichen.gif', 'user', 'user', 5, 1364923443, 'Envi d''un partenariat ? suivez les indications', 1364923443, 5, 0, 0, 29, 1, 0, 0),
(40, 11, '', 'user', 'user', 50, 1364999632, 'Bonjours la meute APAX ! ', 1367181268, 5, 833, 13, 121, 0, 0, 0),
(41, 6, '0', '', 'user', 1, 1365075219, 'Vieilles Créa', 1365080395, 1, 510, 1, 70, 0, 0, 0),
(42, 6, '', '', 'user', 5, 1365357532, 'création d''une intro pour vidéo', 1365925365, 27, 576, 7, 117, 0, 0, 0),
(43, 7, 'text.gif', '2', '2', 34, 1365524492, 'Tournoi ou Match ', 1365586170, 1, 535, 2, 25, 0, 0, 0),
(44, 7, 'ausrufezeichen.gif', '2', '2', 27, 1365623622, 'Projet intro ', 1365683006, 27, 547, 2, 33, 0, 0, 0),
(45, 8, 'ausrufezeichen.gif', '', 'user', 26, 1365796012, 'AU REVOIR', 1365796012, 26, 0, 0, 54, 0, 0, 0),
(46, 10, 'ausrufezeichen.gif', '2', '2', 5, 1365888754, 'Escouade final ', 1365888754, 5, 0, 0, 14, 0, 0, 0),
(47, 6, '0', '', 'user', 16, 1366010392, 'RDV en 2014', 1366058492, 16, 599, 10, 181, 0, 0, 0),
(48, 7, '', '2', '2', 1, 1366022266, 'Un tri dans la meute ?', 1366445588, 1, 691, 15, 101, 0, 0, 0),
(49, 6, 'frage.gif', '', 'user', 21, 1366107182, 'Battlefeed', 1366191354, 5, 614, 4, 49, 0, 0, 0),
(50, 6, '', '', 'user', 5, 1366135142, 'Partant pour un ptit jeux? lol', 1367867807, 11, 1087, 80, 1783, 0, 0, 0),
(51, 7, '', '2', '2', 5, 1366219239, 'Serveur', 1366282514, 34, 644, 3, 24, 0, 0, 0),
(52, 3, '', '', 'user', 73, 1366279228, 'demande d''integrements de "nono770firedan9', 1366743322, 10, 743, 8, 177, 0, 0, 0),
(53, 7, 'biggrin.gif', '2', '2', 5, 1366304806, 'Serveur Apax, le retour !!!!!', 1366445764, 1, 692, 6, 33, 0, 0, 0),
(54, 5, '', '', '', 20, 1362605839, '2099 vs Apax', 1366445961, 0, 0, 0, 0, 0, 14, 0),
(55, 6, '', '', 'user', 1, 1366448261, 'ENORME BLAGUE !', 1366456058, 16, 703, 1, 39, 0, 0, 0),
(56, 12, '0', 'user', 'user', 75, 1366541274, 'demande recrutement hasma_tik', 1368377330, 1, 1171, 25, 443, 1, 0, 0),
(57, 7, '0', '2', '2', 5, 1366648241, 'Les journées spécial des apax arrive !!!!', 1366890902, 5, 751, 7, 54, 0, 0, 0),
(59, 12, 'thumb_down.gif', 'user', 'user', 81, 1366904176, 'Demande de recrutement sicilianno35 => ANNULER ', 1368693520, 5, 1224, 17, 213, 1, 0, 0),
(60, 3, '0', '', 'user', 83, 1366975100, 'Candidature " Créative Dragon "', 1367001164, 37, 773, 5, 104, 0, 0, 0),
(61, 12, '', 'user', 'user', 86, 1367063330, 'Demande de recrutement de hugo ', 1367222920, 86, 843, 7, 84, 1, 0, 0),
(62, 12, '', 'user', 'user', 89, 1367067221, 'nouvel recrut slimmorle', 1367221041, 2, 838, 4, 49, 1, 0, 0),
(63, 12, '', 'user', 'user', 87, 1367069612, 'candidature tarty51 :)', 1367220668, 2, 836, 7, 83, 1, 0, 0),
(64, 3, '', '', '', 21, 1364567799, 'Demande de recrutement', 1366719457, 0, 0, 0, 0, 0, 38, 0),
(65, 3, '', '', '', 43, 1364546637, 'Demande de Recrutement Simrreuhh', 1366719356, 0, 0, 0, 0, 0, 37, 0),
(67, 12, 'thumb_down.gif', 'user', 'user', 91, 1367096990, ' kevcha  => RECRUTEMENT ANNULER ', 1367738430, 5, 1019, 10, 117, 1, 0, 0),
(70, 1, '', '', '', 88, 1367138093, 'présentation Alexx93190', 1368546549, 81, 1201, 2, 50, 0, 0, 0),
(71, 1, '', '', '', 89, 1367143305, 'loup slimmorle', 1368901933, 89, 1242, 7, 124, 0, 0, 0),
(72, 7, '', '2', '2', 1, 1367162422, 'entrainement du 28-04', 1367239982, 25, 849, 8, 56, 0, 0, 0),
(73, 1, 'pfeil.gif', '', '', 10, 1367172298, 'membres de la meute', 1367172298, 10, 0, 0, 34, 0, 0, 0),
(74, 3, '', '', 'user', 92, 1367172979, 'wolf16000', 1367175273, 92, 829, 2, 62, 0, 0, 0),
(75, 1, '', '', '', 81, 1367178321, 'Présentation CiiDzZ_HD-', 1367178321, 81, 0, 0, 28, 0, 0, 0),
(76, 3, 'biggrin.gif', '', 'user', 93, 1367181806, 'demande de recrutement Shuya', 1368093265, 89, 1115, 14, 290, 0, 0, 0),
(77, 3, '0', '', 'user', 94, 1367231549, '[Recrutement] weapons345', 1367746146, 89, 1021, 9, 197, 0, 0, 0),
(78, 10, '', '2', '2', 89, 1367260829, 'preparation conquete bazar ( avoir)', 1367451866, 10, 880, 3, 30, 0, 0, 0),
(79, 7, 'text.gif', '2', '2', 34, 1367350336, 'Spécialisation', 1367748250, 25, 1023, 3, 33, 0, 0, 0),
(80, 6, 'thumb_up.gif', '', 'user', 95, 1367411667, 'xTyFlo retour d''un ancien ? ', 1367831538, 5, 1061, 11, 136, 1, 0, 0),
(81, 5, '', '', 'user', 98, 1367462609, 'EndLine.NoName T12', 1367679393, 1, 1015, 12, 302, 0, 0, 0),
(82, 4, '0', '', 'user', 84, 1367481057, 'Présentation team [FLAM]', 1367673611, 102, 1005, 4, 113, 0, 0, 0),
(83, 3, '0', '', 'user', 99, 1367488238, 'Candidature Eiiwox', 1368186027, 89, 1138, 4, 89, 0, 0, 0),
(84, 3, 'mad.gif', '', 'user', 100, 1367490354, 'LE GAULOIS POSTULE DANS LE cLAN', 1368923727, 100, 1244, 22, 341, 0, 0, 0),
(85, 6, '', '', 'user', 89, 1367491635, 'Y A T IL DES PILOTE OU AIMERIEZ VOUS LE DEVENIR', 1367675438, 4, 1006, 11, 228, 0, 0, 0),
(86, 3, '0', '', 'user', 101, 1367494055, 'candidature C83Zinio', 1367580543, 1, 927, 6, 101, 0, 0, 0),
(87, 12, '', 'user', 'user', 97, 1367494964, 'Candidature de Yosusu44 (ancien GFB)', 1367580841, 1, 931, 4, 56, 1, 0, 0),
(88, 6, 'ausrufezeichen.gif', '', 'user', 10, 1367528355, 'nouvelle video de WAZA-PsY-4-x', 1367569763, 5, 914, 1, 24, 0, 0, 0),
(91, 10, '0', '2', '2', 10, 1367572233, 'INSCRIPTION MATCH 8 MAI 2099 ', 1367620399, 34, 987, 13, 120, 0, 0, 0),
(92, 7, '', '2', '2', 1, 1367580643, 'Une branche "Junior" ?', 1367679105, 1, 1011, 6, 42, 0, 0, 0),
(93, 6, '', '', 'user', 89, 1367580906, 'entrainement dimanche 5 mai a 15H', 1367686941, 5, 1017, 8, 98, 0, 0, 0),
(94, 5, '', '', 'user', 84, 1367581385, 'Demande de match vs [FLAM]', 1367679271, 84, 1013, 16, 309, 0, 0, 0),
(95, 10, '', '2', '2', 89, 1367590517, 'Strategie metro conquete T8 T12', 1367620155, 91, 986, 4, 57, 0, 0, 0),
(96, 10, '', '2', '2', 89, 1367592916, 'Strategie metro conquete T8 T12', 1367592916, 89, 0, 0, 11, 0, 0, 0),
(97, 10, 'ausrufezeichen.gif', '2', '2', 10, 1367606725, 'Stratégie Kharg T12 a apprendre ', 1367659798, 89, 996, 2, 33, 0, 0, 0),
(98, 6, '', '', 'user', 95, 1367659295, 'Double IG ? ', 1367679335, 95, 1014, 3, 38, 0, 0, 0),
(99, 6, 'biggrin.gif', '', 'user', 89, 1367663283, 'tournoi apax tuom tournation', 1367663283, 89, 0, 0, 17, 0, 0, 0),
(100, 7, '', '2', '2', 5, 1367738255, 'Kevcha', 1367950821, 25, 1105, 5, 44, 0, 0, 0),
(101, 6, '', '', 'user', 81, 1367751972, 'Escouade', 1368383022, 8, 1177, 5, 56, 0, 0, 0),
(102, 6, '', '', 'user', 95, 1367758775, 'Mon départ , xTyFlo', 1368194075, 34, 1143, 7, 101, 1, 0, 0),
(103, 3, '', '', 'user', 106, 1367762289, 'demande de recrutement flav1996', 1368884058, 106, 1238, 34, 639, 0, 0, 0),
(104, 7, '', '2', '2', 89, 1367765829, 'compte rendu de entrainement dimanche 05/05/2013', 1367783172, 1, 1044, 2, 19, 0, 0, 0),
(107, 7, '', '2', '2', 1, 1367832844, 'Petite maj du site sous peu', 1367881370, 11, 1092, 9, 51, 0, 0, 0),
(108, 8, '', '', 'user', 1, 1367845480, 'Site APAX : V2 ... La grosse opération', 1367950726, 25, 1104, 14, 289, 0, 0, 0),
(109, 6, 'ausrufezeichen.gif', '', 'user', 11, 1368090445, 'Plaque et Etoile de service ...', 1368118583, 11, 1120, 4, 39, 0, 0, 0),
(110, 1, '0', '', '', 11, 1368093569, 'Présentation Aulyro', 1368093569, 11, 0, 0, 33, 0, 0, 0),
(111, 6, 'ausrufezeichen.gif', '', 'user', 89, 1368175167, 'pour ou contre ', 1368288599, 5, 1154, 2, 31, 0, 0, 0),
(112, 10, '', '2', '2', 89, 1368176535, 'entrainement dimanche 12 mai', 1368377411, 1, 1172, 3, 28, 0, 0, 0),
(113, 7, 'text.gif', '2', '2', 34, 1368203710, 'Projet de maillot perso', 1368964570, 5, 1247, 13, 80, 0, 0, 0),
(114, 6, 'ausrufezeichen.gif', '', 'user', 11, 1368296299, 'Vos objectif sur Bf3', 1368388987, 11, 1183, 11, 156, 0, 0, 0),
(115, 7, 'ausrufezeichen.gif', '2', '2', 5, 1368351581, 'Réunion du staff d''ici peut', 1368726878, 5, 1228, 11, 88, 0, 0, 0),
(116, 3, '', '', '', 75, 1366541274, 'demande recrutement hasma_tik', 1368377330, 0, 0, 0, 0, 0, 56, 0),
(117, 7, '', '2', '2', 89, 1368382513, 'compte rendu de entrainement dimanche 12 MAI 2013', 1368807211, 21, 1232, 2, 26, 0, 0, 0),
(118, 10, 'ausrufezeichen.gif', '2', '2', 89, 1368445036, 'Torneo MV CRSQ ruée 4 vs 4 (detail)', 1368802612, 11, 1230, 3, 29, 0, 0, 0),
(119, 10, '', '2', '2', 89, 1368445733, ' tournoi Apax tuom tour nation (detail)', 1368807512, 21, 1233, 5, 30, 0, 0, 0),
(120, 10, 'biggrin.gif', '2', '2', 89, 1368523008, 'entrainement dimanche 19 mai a 15H', 1368987193, 5, 1250, 5, 32, 0, 0, 0),
(121, 8, '', '', 'user', 1, 1368559323, '[Site ApaX] Retour des mails et des "captcha"', 1368612771, 5, 1217, 4, 44, 0, 0, 0),
(122, 6, '', '', 'user', 81, 1368719442, 'Mon départ. ', 1368991520, 1, 1253, 3, 32, 0, 0, 0),
(124, 8, '', '', 'user', 25, 1368963775, 'DEPART', 1368990469, 10, 1252, 3, 16, 0, 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `gallery`
--

CREATE TABLE IF NOT EXISTS `gallery` (
  `galleryID` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `date` int(14) NOT NULL,
  `groupID` int(11) NOT NULL,
  PRIMARY KEY (`galleryID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `gallery`
--

INSERT INTO `gallery` (`galleryID`, `userID`, `name`, `date`, `groupID`) VALUES
(1, 0, 'Captures FB', 1361455707, 1),
(2, 43, 'Créas Graphiste', 1364556222, 0),
(3, 11, 'Fun', 1368027770, 0);

-- --------------------------------------------------------

--
-- Structure de la table `gallery_groups`
--

CREATE TABLE IF NOT EXISTS `gallery_groups` (
  `groupID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `sort` int(11) NOT NULL,
  PRIMARY KEY (`groupID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `gallery_groups`
--

INSERT INTO `gallery_groups` (`groupID`, `name`, `sort`) VALUES
(1, 'Screens', 1);

-- --------------------------------------------------------

--
-- Structure de la table `gallery_pictures`
--

CREATE TABLE IF NOT EXISTS `gallery_pictures` (
  `picID` int(11) NOT NULL AUTO_INCREMENT,
  `galleryID` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `comment` text NOT NULL,
  `views` int(11) NOT NULL DEFAULT '0',
  `comments` int(1) NOT NULL DEFAULT '1',
  `votes` int(11) NOT NULL,
  `points` int(11) NOT NULL,
  `rating` int(11) NOT NULL,
  PRIMARY KEY (`picID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Contenu de la table `gallery_pictures`
--

INSERT INTO `gallery_pictures` (`picID`, `galleryID`, `name`, `comment`, `views`, `comments`, `votes`, `points`, `rating`) VALUES
(1, 1, 'BF3 screen', '', 50, 2, 1, 8, 8),
(2, 2, 'Taavail Perso 1', '', 27, 2, 0, 0, 0),
(3, 2, 'P''Art 2.jpg', '', 23, 2, 0, 0, 0),
(4, 2, 'Opera.jpg', '', 21, 2, 0, 0, 0),
(5, 2, 'Affiche-au-temps-de-femmes.jpg', '', 54, 2, 0, 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `games`
--

CREATE TABLE IF NOT EXISTS `games` (
  `gameID` int(3) NOT NULL AUTO_INCREMENT,
  `tag` varchar(5) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`gameID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 PACK_KEYS=0 AUTO_INCREMENT=61 ;

--
-- Contenu de la table `games`
--

INSERT INTO `games` (`gameID`, `tag`, `name`) VALUES
(7, 'bf', 'Battlefield 1942'),
(10, 'b21', 'Battlefield 2142'),
(11, 'bf2', 'Battlefield 2'),
(12, 'bfv', 'Battlefield Vietnam'),
(60, 'BF3', 'Battlefield 3');

-- --------------------------------------------------------

--
-- Structure de la table `guestbook`
--

CREATE TABLE IF NOT EXISTS `guestbook` (
  `gbID` int(11) NOT NULL AUTO_INCREMENT,
  `date` int(14) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `hp` varchar(255) NOT NULL DEFAULT '',
  `icq` varchar(255) NOT NULL DEFAULT '',
  `ip` varchar(255) NOT NULL DEFAULT '',
  `comment` text NOT NULL,
  `admincomment` text NOT NULL,
  PRIMARY KEY (`gbID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `guestbook`
--


-- --------------------------------------------------------

--
-- Structure de la table `history`
--

CREATE TABLE IF NOT EXISTS `history` (
  `history` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `history`
--


-- --------------------------------------------------------

--
-- Structure de la table `imprint`
--

CREATE TABLE IF NOT EXISTS `imprint` (
  `imprintID` int(11) NOT NULL AUTO_INCREMENT,
  `imprint` text NOT NULL,
  PRIMARY KEY (`imprintID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `imprint`
--


-- --------------------------------------------------------

--
-- Structure de la table `links`
--

CREATE TABLE IF NOT EXISTS `links` (
  `linkID` int(11) NOT NULL AUTO_INCREMENT,
  `linkcatID` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `url` varchar(255) NOT NULL DEFAULT '',
  `info` varchar(255) NOT NULL DEFAULT '',
  `banner` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`linkID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `links`
--

INSERT INTO `links` (`linkID`, `linkcatID`, `name`, `url`, `info`, `banner`) VALUES
(1, 1, 'webSPELL.org', 'http://www.webspell.org', 'webspell.org: Webdesign und Webdevelopment', '1.gif');

-- --------------------------------------------------------

--
-- Structure de la table `links_categorys`
--

CREATE TABLE IF NOT EXISTS `links_categorys` (
  `linkcatID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`linkcatID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `links_categorys`
--

INSERT INTO `links_categorys` (`linkcatID`, `name`) VALUES
(1, 'Webdesign');

-- --------------------------------------------------------

--
-- Structure de la table `linkus`
--

CREATE TABLE IF NOT EXISTS `linkus` (
  `bannerID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `file` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`bannerID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `linkus`
--


-- --------------------------------------------------------

--
-- Structure de la table `lock`
--

CREATE TABLE IF NOT EXISTS `lock` (
  `time` int(11) NOT NULL,
  `reason` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `lock`
--


-- --------------------------------------------------------

--
-- Structure de la table `messenger`
--

CREATE TABLE IF NOT EXISTS `messenger` (
  `messageID` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL DEFAULT '0',
  `date` int(14) NOT NULL DEFAULT '0',
  `fromuser` int(11) NOT NULL DEFAULT '0',
  `touser` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `message` text NOT NULL,
  `viewed` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`messageID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=142 ;

--
-- Contenu de la table `messenger`
--

INSERT INTO `messenger` (`messageID`, `userID`, `date`, `fromuser`, `touser`, `title`, `message`, `viewed`) VALUES
(1, 5, 1361806509, 5, 1, '', 'Salut mon grand, es que tu peut récup la baniere de l''ancien forum pour le metre sur le new site, jessaye de voir pour en refair un mais l''inspiration peine a venir... je rame un peut... ;)', 0),
(2, 1, 1361806509, 5, 1, '', 'Salut mon grand, es que tu peut récup la baniere de l''ancien forum pour le metre sur le new site, jessaye de voir pour en refair un mais l''inspiration peine a venir... je rame un peut... ;)', 1),
(3, 1, 1361809893, 1, 5, 'Re[1]: ', '[QUOTE=Dartoch]Salut mon grand, es que tu peut récup la baniere de l''ancien forum pour le metre sur le new site, jessaye de voir pour en refair un mais l''inspiration peine a venir... je rame un peut... ;)[/QUOTE]\r\n\r\nDe toi à moi, elle est un peu laide par rapport au nouveau site. Du coup, je serait plus partant pour la refaire en plus zoli ^^', 0),
(4, 5, 1361809893, 1, 5, 'Re[1]: ', '[QUOTE=Dartoch]Salut mon grand, es que tu peut récup la baniere de l''ancien forum pour le metre sur le new site, jessaye de voir pour en refair un mais l''inspiration peine a venir... je rame un peut... ;)[/QUOTE]\r\n\r\nDe toi à moi, elle est un peu laide par rapport au nouveau site. Du coup, je serait plus partant pour la refaire en plus zoli ^^', 1),
(5, 5, 1361825682, 5, 1, 'Re[2]: ', '[QUOTE=benftwc][QUOTE=Dartoch]Salut mon grand, es que tu peut récup la baniere de l''ancien forum pour le metre sur le new site, jessaye de voir pour en refair un mais l''inspiration peine a venir... je rame un peut... ;)[/QUOTE]\r\n\r\nDe toi à moi, elle est un peu laide par rapport au nouveau site. Du coup, je serait plus partant pour la refaire en plus zoli ^^[/QUOTE]\r\n\r\nJe vais en refair une, déja essayé de refaire celle la et si sa va pas changer completement ;)', 0),
(6, 1, 1361825682, 5, 1, 'Re[2]: ', '[QUOTE=benftwc][QUOTE=Dartoch]Salut mon grand, es que tu peut récup la baniere de l''ancien forum pour le metre sur le new site, jessaye de voir pour en refair un mais l''inspiration peine a venir... je rame un peut... ;)[/QUOTE]\r\n\r\nDe toi à moi, elle est un peu laide par rapport au nouveau site. Du coup, je serait plus partant pour la refaire en plus zoli ^^[/QUOTE]\r\n\r\nJe vais en refair une, déja essayé de refaire celle la et si sa va pas changer completement ;)', 1),
(7, 1, 1361872826, 1, 5, 'Re[3]: ', '[QUOTE=Dartoch][QUOTE=benftwc][QUOTE=Dartoch]Salut mon grand, es que tu peut récup la baniere de l''ancien forum pour le metre sur le new site, jessaye de voir pour en refair un mais l''inspiration peine a venir... je rame un peut... ;)[/QUOTE]\r\n\r\nDe toi à moi, elle est un peu laide par rapport au nouveau site. Du coup, je serait plus partant pour la refaire en plus zoli ^^[/QUOTE]\r\n\r\nJe vais en refair une, déja essayé de refaire celle la et si sa va pas changer completement ;)[/QUOTE]\r\n\r\n\r\nJte préviens, si tu me force à réinstall windobe pour photoshop, je monte à Vierzon et j''te fume ! ^^', 0),
(8, 5, 1361872826, 1, 5, 'Re[3]: ', '[QUOTE=Dartoch][QUOTE=benftwc][QUOTE=Dartoch]Salut mon grand, es que tu peut récup la baniere de l''ancien forum pour le metre sur le new site, jessaye de voir pour en refair un mais l''inspiration peine a venir... je rame un peut... ;)[/QUOTE]\r\n\r\nDe toi à moi, elle est un peu laide par rapport au nouveau site. Du coup, je serait plus partant pour la refaire en plus zoli ^^[/QUOTE]\r\n\r\nJe vais en refair une, déja essayé de refaire celle la et si sa va pas changer completement ;)[/QUOTE]\r\n\r\n\r\nJte préviens, si tu me force à réinstall windobe pour photoshop, je monte à Vierzon et j''te fume ! ^^', 1),
(9, 1, 1362044012, 1, 5, 'Edition des messages', 'C''est toi qui à édité mon post forum ?', 0),
(10, 5, 1362044012, 1, 5, 'Edition des messages', 'C''est toi qui à édité mon post forum ?', 1),
(11, 5, 1362048117, 5, 1, 'Re[1]: Edition des messages', '[QUOTE=benftwc]C''est toi qui à édité mon post forum ?[/QUOTE]\r\n\r\nnon, jai toucher a aucun poste ma caille', 0),
(12, 1, 1362048117, 5, 1, 'Re[1]: Edition des messages', '[QUOTE=benftwc]C''est toi qui à édité mon post forum ?[/QUOTE]\r\n\r\nnon, jai toucher a aucun poste ma caille', 1),
(13, 1, 1362049487, 1, 5, 'Re[2]: Edition des messages', '[QUOTE=Dartoch][QUOTE=benftwc]C''est toi qui à édité mon post forum ?[/QUOTE]\r\n\r\nnon, jai toucher a aucun poste ma caille[/QUOTE]\r\n\r\n\r\nOkay :)', 0),
(14, 5, 1362049487, 1, 5, 'Re[2]: Edition des messages', '[QUOTE=Dartoch][QUOTE=benftwc]C''est toi qui à édité mon post forum ?[/QUOTE]\r\n\r\nnon, jai toucher a aucun poste ma caille[/QUOTE]\r\n\r\n\r\nOkay :)', 1),
(15, 5, 1362304336, 5, 1, 'Re[4]: ', 'Salut grand, je vien de finir un fond pour le site avec ban integré, dit moi se que tu en pense et tu peut le metre sur le site en meme temp ^^\r\nBise ma caille\r\n\r\nPS: il se trouve dans la partie fichier a DL du site ;)', 0),
(16, 1, 1362304336, 5, 1, 'Re[4]: ', 'Salut grand, je vien de finir un fond pour le site avec ban integré, dit moi se que tu en pense et tu peut le metre sur le site en meme temp ^^\r\nBise ma caille\r\n\r\nPS: il se trouve dans la partie fichier a DL du site ;)', 1),
(17, 1, 1362676164, 1, 1, 'new upcoming war in calendar!', 'new upcoming clanwar on 17.03.2013:[br][br]Opponent: [flag]fr[/flag] [url=http://battlelog.battlefield.com/bf3/fr/platoon/2832655240994418586/]2099[/url] [br]League: [url=http://battlelog.battlefield.com/bf3/fr/]Entrainement[/url] [br]Info: Si vous pouviez aller valider votre inscription au T12 au plus vite.\r\n\r\n\r\n// Pour ceux qui ont déjà reçu l''invitation, veuillez m''excuser, il s''agit d''un test des neswletters membre :D\r\n\r\nBenftwc[br][br][url=index.php?site=calendar]link to calendar[/url]', 1),
(18, 3, 1362676164, 1, 3, 'new upcoming war in calendar!', 'new upcoming clanwar on 17.03.2013:[br][br]Opponent: [flag]fr[/flag] [url=http://battlelog.battlefield.com/bf3/fr/platoon/2832655240994418586/]2099[/url] [br]League: [url=http://battlelog.battlefield.com/bf3/fr/]Entrainement[/url] [br]Info: Si vous pouviez aller valider votre inscription au T12 au plus vite.\r\n\r\n\r\n// Pour ceux qui ont déjà reçu l''invitation, veuillez m''excuser, il s''agit d''un test des neswletters membre :D\r\n\r\nBenftwc[br][br][url=index.php?site=calendar]link to calendar[/url]', 1),
(19, 4, 1362676164, 1, 4, 'new upcoming war in calendar!', 'new upcoming clanwar on 17.03.2013:[br][br]Opponent: [flag]fr[/flag] [url=http://battlelog.battlefield.com/bf3/fr/platoon/2832655240994418586/]2099[/url] [br]League: [url=http://battlelog.battlefield.com/bf3/fr/]Entrainement[/url] [br]Info: Si vous pouviez aller valider votre inscription au T12 au plus vite.\r\n\r\n\r\n// Pour ceux qui ont déjà reçu l''invitation, veuillez m''excuser, il s''agit d''un test des neswletters membre :D\r\n\r\nBenftwc[br][br][url=index.php?site=calendar]link to calendar[/url]', 1),
(21, 5, 1362676164, 1, 5, 'new upcoming war in calendar!', 'new upcoming clanwar on 17.03.2013:[br][br]Opponent: [flag]fr[/flag] [url=http://battlelog.battlefield.com/bf3/fr/platoon/2832655240994418586/]2099[/url] [br]League: [url=http://battlelog.battlefield.com/bf3/fr/]Entrainement[/url] [br]Info: Si vous pouviez aller valider votre inscription au T12 au plus vite.\r\n\r\n\r\n// Pour ceux qui ont déjà reçu l''invitation, veuillez m''excuser, il s''agit d''un test des neswletters membre :D\r\n\r\nBenftwc[br][br][url=index.php?site=calendar]link to calendar[/url]', 1),
(22, 10, 1362676164, 1, 10, 'new upcoming war in calendar!', 'new upcoming clanwar on 17.03.2013:[br][br]Opponent: [flag]fr[/flag] [url=http://battlelog.battlefield.com/bf3/fr/platoon/2832655240994418586/]2099[/url] [br]League: [url=http://battlelog.battlefield.com/bf3/fr/]Entrainement[/url] [br]Info: Si vous pouviez aller valider votre inscription au T12 au plus vite.\r\n\r\n\r\n// Pour ceux qui ont déjà reçu l''invitation, veuillez m''excuser, il s''agit d''un test des neswletters membre :D\r\n\r\nBenftwc[br][br][url=index.php?site=calendar]link to calendar[/url]', 1),
(24, 19, 1362676164, 1, 19, 'new upcoming war in calendar!', 'new upcoming clanwar on 17.03.2013:[br][br]Opponent: [flag]fr[/flag] [url=http://battlelog.battlefield.com/bf3/fr/platoon/2832655240994418586/]2099[/url] [br]League: [url=http://battlelog.battlefield.com/bf3/fr/]Entrainement[/url] [br]Info: Si vous pouviez aller valider votre inscription au T12 au plus vite.\r\n\r\n\r\n// Pour ceux qui ont déjà reçu l''invitation, veuillez m''excuser, il s''agit d''un test des neswletters membre :D\r\n\r\nBenftwc[br][br][url=index.php?site=calendar]link to calendar[/url]', 1),
(26, 11, 1362676164, 1, 11, 'new upcoming war in calendar!', 'new upcoming clanwar on 17.03.2013:[br][br]Opponent: [flag]fr[/flag] [url=http://battlelog.battlefield.com/bf3/fr/platoon/2832655240994418586/]2099[/url] [br]League: [url=http://battlelog.battlefield.com/bf3/fr/]Entrainement[/url] [br]Info: Si vous pouviez aller valider votre inscription au T12 au plus vite.\r\n\r\n\r\n// Pour ceux qui ont déjà reçu l''invitation, veuillez m''excuser, il s''agit d''un test des neswletters membre :D\r\n\r\nBenftwc[br][br][url=index.php?site=calendar]link to calendar[/url]', 1),
(27, 8, 1362676164, 1, 8, 'new upcoming war in calendar!', 'new upcoming clanwar on 17.03.2013:[br][br]Opponent: [flag]fr[/flag] [url=http://battlelog.battlefield.com/bf3/fr/platoon/2832655240994418586/]2099[/url] [br]League: [url=http://battlelog.battlefield.com/bf3/fr/]Entrainement[/url] [br]Info: Si vous pouviez aller valider votre inscription au T12 au plus vite.\r\n\r\n\r\n// Pour ceux qui ont déjà reçu l''invitation, veuillez m''excuser, il s''agit d''un test des neswletters membre :D\r\n\r\nBenftwc[br][br][url=index.php?site=calendar]link to calendar[/url]', 1),
(29, 17, 1362676165, 1, 17, 'new upcoming war in calendar!', 'new upcoming clanwar on 17.03.2013:[br][br]Opponent: [flag]fr[/flag] [url=http://battlelog.battlefield.com/bf3/fr/platoon/2832655240994418586/]2099[/url] [br]League: [url=http://battlelog.battlefield.com/bf3/fr/]Entrainement[/url] [br]Info: Si vous pouviez aller valider votre inscription au T12 au plus vite.\r\n\r\n\r\n// Pour ceux qui ont déjà reçu l''invitation, veuillez m''excuser, il s''agit d''un test des neswletters membre :D\r\n\r\nBenftwc[br][br][url=index.php?site=calendar]link to calendar[/url]', 1),
(30, 9, 1362676165, 1, 9, 'new upcoming war in calendar!', 'new upcoming clanwar on 17.03.2013:[br][br]Opponent: [flag]fr[/flag] [url=http://battlelog.battlefield.com/bf3/fr/platoon/2832655240994418586/]2099[/url] [br]League: [url=http://battlelog.battlefield.com/bf3/fr/]Entrainement[/url] [br]Info: Si vous pouviez aller valider votre inscription au T12 au plus vite.\r\n\r\n\r\n// Pour ceux qui ont déjà reçu l''invitation, veuillez m''excuser, il s''agit d''un test des neswletters membre :D\r\n\r\nBenftwc[br][br][url=index.php?site=calendar]link to calendar[/url]', 1),
(31, 12, 1362676165, 1, 12, 'new upcoming war in calendar!', 'new upcoming clanwar on 17.03.2013:[br][br]Opponent: [flag]fr[/flag] [url=http://battlelog.battlefield.com/bf3/fr/platoon/2832655240994418586/]2099[/url] [br]League: [url=http://battlelog.battlefield.com/bf3/fr/]Entrainement[/url] [br]Info: Si vous pouviez aller valider votre inscription au T12 au plus vite.\r\n\r\n\r\n// Pour ceux qui ont déjà reçu l''invitation, veuillez m''excuser, il s''agit d''un test des neswletters membre :D\r\n\r\nBenftwc[br][br][url=index.php?site=calendar]link to calendar[/url]', 1),
(32, 1, 1362927404, 1, 2, 'Coupes BF3 PS3 ESL', 'http://www.consoles.net/fr/bf3-ps3/cdf13/cup22/\r\n\r\nhttp://www.consoles.net/fr/staticpage/show/1932/', 1),
(34, 5, 1363548591, 5, 1, 'yop', 'salut ma caille, sa va t''y? \r\nbon je vien de poster ma premiere news !!!! lol\r\nj''ai essayé de la metre dans les baniere defilante mais impossible de metre une image...\r\nPar contre commen tu fait pour metre une video dans le lecteur? parsque moi je ny voi rien, quand je clique dessus sa ne marche pas...\r\n\r\nbisous bisous :p\r\n', 0),
(35, 1, 1363548591, 5, 1, 'yop', 'salut ma caille, sa va t''y? \r\nbon je vien de poster ma premiere news !!!! lol\r\nj''ai essayé de la metre dans les baniere defilante mais impossible de metre une image...\r\nPar contre commen tu fait pour metre une video dans le lecteur? parsque moi je ny voi rien, quand je clique dessus sa ne marche pas...\r\n\r\nbisous bisous :p\r\n', 1),
(36, 1, 1363550013, 1, 5, 'Re[1]: yop', '[QUOTE=Dartoch]salut ma caille, sa va t''y? \r\nbon je vien de poster ma premiere news !!!! lol\r\nj''ai essayé de la metre dans les baniere defilante mais impossible de metre une image...\r\nPar contre commen tu fait pour metre une video dans le lecteur? parsque moi je ny voi rien, quand je clique dessus sa ne marche pas...\r\n\r\nbisous bisous :p\r\n[/QUOTE]\r\n\r\nLa vidéo est buggée, faudrait que je me repenche dessus. Néanmoins, je sais pas si tu as vu, mais en haut, tu as Administration, et Admin. Thème, dans ce derniers, t''aura quelques options sympatoches ;)', 0),
(37, 5, 1363550013, 1, 5, 'Re[1]: yop', '[QUOTE=Dartoch]salut ma caille, sa va t''y? \r\nbon je vien de poster ma premiere news !!!! lol\r\nj''ai essayé de la metre dans les baniere defilante mais impossible de metre une image...\r\nPar contre commen tu fait pour metre une video dans le lecteur? parsque moi je ny voi rien, quand je clique dessus sa ne marche pas...\r\n\r\nbisous bisous :p\r\n[/QUOTE]\r\n\r\nLa vidéo est buggée, faudrait que je me repenche dessus. Néanmoins, je sais pas si tu as vu, mais en haut, tu as Administration, et Admin. Thème, dans ce derniers, t''aura quelques options sympatoches ;)', 1),
(38, 34, 1364240372, 34, 28, 'recrutement', 'tu joue se soir ? :)', 0),
(40, 34, 1364240413, 34, 4, 'Recrutement', 'tu joue se soir ? :)', 0),
(41, 4, 1364240414, 34, 4, 'Recrutement', 'tu joue se soir ? :)', 1),
(42, 34, 1364240452, 34, 2, 'Recrutement', 'tu joue se soir ? ', 0),
(44, 2, 1364295216, 2, 34, 'Re[1]: Recrutement', '[QUOTE=TweeZy]tu joue se soir ? [/QUOTE]\r\n\r\nje joue principalement l''apres midi ', 0),
(45, 34, 1364295216, 2, 34, 'Re[1]: Recrutement', '[QUOTE=TweeZy]tu joue se soir ? [/QUOTE]\r\n\r\nje joue principalement l''apres midi ', 1),
(47, 4, 1364771133, 21, 4, 'Demande de recrutement', 'Salut Julien,\r\nTu apparais comme recruteur dans la présentation de la meute.\r\nJe t''envoi donc ma candidature.\r\nEn espérant faire bientôt parti de la meute. \r\nA+\r\nLaurent', 1),
(48, 5, 1364799646, 5, 1, '(Aucun objet)', '[COLOR=#FFA500][SIZE=5]Bonjour a tous et joyeuses Pâques tous le monde !!!\r\n\r\nJe vous rappel que pour le mois d''avril nous avons deux Evénements de prévu :[/SIZE]\r\n[/COLOR]\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\Soirée des 1 an de la meute le samedi 13 au soir sur notre serveur //////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n [COLOR=#4169E1]bataille only C4 sur métro ( j''veux que ça pète dans tout les sens ) \r\n Course robot nem sur frontière Caspienne \r\n Photo de Famille sur Tour Ziba[/COLOR]\r\n\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ Match 12 VS 12 contre la team 2099 Dimanche 14 a 15h ///////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n[COLOR=#0000FF] Ruée / Pic de damavand \r\n Conquête / Tempête de feu \r\n\r\nLes membres présent sont:\r\n\r\n Dartoch\r\n Psy-4\r\n Gp\r\n Gyom\r\n Bmdc\r\n Jujube\r\n Ben\r\n Mika\r\n Pignouf\r\n Lebetz\r\n\r\nIl nous manque encore 2 membres pour finalisé les escouades et les strat donc pour ceux qui sont intéressé, veuillé vous rendre sur la partie du site concerné\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n[/COLOR]\r\n[COLOR=#FF0000]En espérant que vous recevrez tous ce message je vous souhaite encore une joyeuses Pâques, une bonne journée et plein de victoire sur le champ de bataille au nom de la meute !!!!!!!!!\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/COLOR]', 0),
(49, 1, 1364799646, 5, 1, '(Aucun objet)', '[COLOR=#FFA500][SIZE=5]Bonjour a tous et joyeuses Pâques tous le monde !!!\r\n\r\nJe vous rappel que pour le mois d''avril nous avons deux Evénements de prévu :[/SIZE]\r\n[/COLOR]\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\Soirée des 1 an de la meute le samedi 13 au soir sur notre serveur //////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n [COLOR=#4169E1]bataille only C4 sur métro ( j''veux que ça pète dans tout les sens ) \r\n Course robot nem sur frontière Caspienne \r\n Photo de Famille sur Tour Ziba[/COLOR]\r\n\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ Match 12 VS 12 contre la team 2099 Dimanche 14 a 15h ///////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n[COLOR=#0000FF] Ruée / Pic de damavand \r\n Conquête / Tempête de feu \r\n\r\nLes membres présent sont:\r\n\r\n Dartoch\r\n Psy-4\r\n Gp\r\n Gyom\r\n Bmdc\r\n Jujube\r\n Ben\r\n Mika\r\n Pignouf\r\n Lebetz\r\n\r\nIl nous manque encore 2 membres pour finalisé les escouades et les strat donc pour ceux qui sont intéressé, veuillé vous rendre sur la partie du site concerné\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n[/COLOR]\r\n[COLOR=#FF0000]En espérant que vous recevrez tous ce message je vous souhaite encore une joyeuses Pâques, une bonne journée et plein de victoire sur le champ de bataille au nom de la meute !!!!!!!!!\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/COLOR]', 1),
(50, 5, 1364799646, 5, 2, '(Aucun objet)', '[COLOR=#FFA500][SIZE=5]Bonjour a tous et joyeuses Pâques tous le monde !!!\r\n\r\nJe vous rappel que pour le mois d''avril nous avons deux Evénements de prévu :[/SIZE]\r\n[/COLOR]\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\Soirée des 1 an de la meute le samedi 13 au soir sur notre serveur //////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n [COLOR=#4169E1]bataille only C4 sur métro ( j''veux que ça pète dans tout les sens ) \r\n Course robot nem sur frontière Caspienne \r\n Photo de Famille sur Tour Ziba[/COLOR]\r\n\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ Match 12 VS 12 contre la team 2099 Dimanche 14 a 15h ///////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n[COLOR=#0000FF] Ruée / Pic de damavand \r\n Conquête / Tempête de feu \r\n\r\nLes membres présent sont:\r\n\r\n Dartoch\r\n Psy-4\r\n Gp\r\n Gyom\r\n Bmdc\r\n Jujube\r\n Ben\r\n Mika\r\n Pignouf\r\n Lebetz\r\n\r\nIl nous manque encore 2 membres pour finalisé les escouades et les strat donc pour ceux qui sont intéressé, veuillé vous rendre sur la partie du site concerné\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n[/COLOR]\r\n[COLOR=#FF0000]En espérant que vous recevrez tous ce message je vous souhaite encore une joyeuses Pâques, une bonne journée et plein de victoire sur le champ de bataille au nom de la meute !!!!!!!!!\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/COLOR]', 0),
(52, 5, 1364799646, 5, 3, '(Aucun objet)', '[COLOR=#FFA500][SIZE=5]Bonjour a tous et joyeuses Pâques tous le monde !!!\r\n\r\nJe vous rappel que pour le mois d''avril nous avons deux Evénements de prévu :[/SIZE]\r\n[/COLOR]\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\Soirée des 1 an de la meute le samedi 13 au soir sur notre serveur //////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n [COLOR=#4169E1]bataille only C4 sur métro ( j''veux que ça pète dans tout les sens ) \r\n Course robot nem sur frontière Caspienne \r\n Photo de Famille sur Tour Ziba[/COLOR]\r\n\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ Match 12 VS 12 contre la team 2099 Dimanche 14 a 15h ///////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n[COLOR=#0000FF] Ruée / Pic de damavand \r\n Conquête / Tempête de feu \r\n\r\nLes membres présent sont:\r\n\r\n Dartoch\r\n Psy-4\r\n Gp\r\n Gyom\r\n Bmdc\r\n Jujube\r\n Ben\r\n Mika\r\n Pignouf\r\n Lebetz\r\n\r\nIl nous manque encore 2 membres pour finalisé les escouades et les strat donc pour ceux qui sont intéressé, veuillé vous rendre sur la partie du site concerné\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n[/COLOR]\r\n[COLOR=#FF0000]En espérant que vous recevrez tous ce message je vous souhaite encore une joyeuses Pâques, une bonne journée et plein de victoire sur le champ de bataille au nom de la meute !!!!!!!!!\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/COLOR]', 0),
(53, 3, 1364799646, 5, 3, '(Aucun objet)', '[COLOR=#FFA500][SIZE=5]Bonjour a tous et joyeuses Pâques tous le monde !!!\r\n\r\nJe vous rappel que pour le mois d''avril nous avons deux Evénements de prévu :[/SIZE]\r\n[/COLOR]\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\Soirée des 1 an de la meute le samedi 13 au soir sur notre serveur //////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n [COLOR=#4169E1]bataille only C4 sur métro ( j''veux que ça pète dans tout les sens ) \r\n Course robot nem sur frontière Caspienne \r\n Photo de Famille sur Tour Ziba[/COLOR]\r\n\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ Match 12 VS 12 contre la team 2099 Dimanche 14 a 15h ///////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n[COLOR=#0000FF] Ruée / Pic de damavand \r\n Conquête / Tempête de feu \r\n\r\nLes membres présent sont:\r\n\r\n Dartoch\r\n Psy-4\r\n Gp\r\n Gyom\r\n Bmdc\r\n Jujube\r\n Ben\r\n Mika\r\n Pignouf\r\n Lebetz\r\n\r\nIl nous manque encore 2 membres pour finalisé les escouades et les strat donc pour ceux qui sont intéressé, veuillé vous rendre sur la partie du site concerné\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n[/COLOR]\r\n[COLOR=#FF0000]En espérant que vous recevrez tous ce message je vous souhaite encore une joyeuses Pâques, une bonne journée et plein de victoire sur le champ de bataille au nom de la meute !!!!!!!!!\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/COLOR]', 1),
(54, 5, 1364799646, 5, 4, '(Aucun objet)', '[COLOR=#FFA500][SIZE=5]Bonjour a tous et joyeuses Pâques tous le monde !!!\r\n\r\nJe vous rappel que pour le mois d''avril nous avons deux Evénements de prévu :[/SIZE]\r\n[/COLOR]\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\Soirée des 1 an de la meute le samedi 13 au soir sur notre serveur //////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n [COLOR=#4169E1]bataille only C4 sur métro ( j''veux que ça pète dans tout les sens ) \r\n Course robot nem sur frontière Caspienne \r\n Photo de Famille sur Tour Ziba[/COLOR]\r\n\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ Match 12 VS 12 contre la team 2099 Dimanche 14 a 15h ///////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n[COLOR=#0000FF] Ruée / Pic de damavand \r\n Conquête / Tempête de feu \r\n\r\nLes membres présent sont:\r\n\r\n Dartoch\r\n Psy-4\r\n Gp\r\n Gyom\r\n Bmdc\r\n Jujube\r\n Ben\r\n Mika\r\n Pignouf\r\n Lebetz\r\n\r\nIl nous manque encore 2 membres pour finalisé les escouades et les strat donc pour ceux qui sont intéressé, veuillé vous rendre sur la partie du site concerné\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n[/COLOR]\r\n[COLOR=#FF0000]En espérant que vous recevrez tous ce message je vous souhaite encore une joyeuses Pâques, une bonne journée et plein de victoire sur le champ de bataille au nom de la meute !!!!!!!!!\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/COLOR]', 0),
(55, 4, 1364799646, 5, 4, '(Aucun objet)', '[COLOR=#FFA500][SIZE=5]Bonjour a tous et joyeuses Pâques tous le monde !!!\r\n\r\nJe vous rappel que pour le mois d''avril nous avons deux Evénements de prévu :[/SIZE]\r\n[/COLOR]\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\Soirée des 1 an de la meute le samedi 13 au soir sur notre serveur //////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n [COLOR=#4169E1]bataille only C4 sur métro ( j''veux que ça pète dans tout les sens ) \r\n Course robot nem sur frontière Caspienne \r\n Photo de Famille sur Tour Ziba[/COLOR]\r\n\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ Match 12 VS 12 contre la team 2099 Dimanche 14 a 15h ///////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n[COLOR=#0000FF] Ruée / Pic de damavand \r\n Conquête / Tempête de feu \r\n\r\nLes membres présent sont:\r\n\r\n Dartoch\r\n Psy-4\r\n Gp\r\n Gyom\r\n Bmdc\r\n Jujube\r\n Ben\r\n Mika\r\n Pignouf\r\n Lebetz\r\n\r\nIl nous manque encore 2 membres pour finalisé les escouades et les strat donc pour ceux qui sont intéressé, veuillé vous rendre sur la partie du site concerné\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n[/COLOR]\r\n[COLOR=#FF0000]En espérant que vous recevrez tous ce message je vous souhaite encore une joyeuses Pâques, une bonne journée et plein de victoire sur le champ de bataille au nom de la meute !!!!!!!!!\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/COLOR]', 1),
(56, 5, 1364799646, 5, 5, '(Aucun objet)', '[COLOR=#FFA500][SIZE=5]Bonjour a tous et joyeuses Pâques tous le monde !!!\r\n\r\nJe vous rappel que pour le mois d''avril nous avons deux Evénements de prévu :[/SIZE]\r\n[/COLOR]\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\Soirée des 1 an de la meute le samedi 13 au soir sur notre serveur //////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n [COLOR=#4169E1]bataille only C4 sur métro ( j''veux que ça pète dans tout les sens ) \r\n Course robot nem sur frontière Caspienne \r\n Photo de Famille sur Tour Ziba[/COLOR]\r\n\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ Match 12 VS 12 contre la team 2099 Dimanche 14 a 15h ///////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n[COLOR=#0000FF] Ruée / Pic de damavand \r\n Conquête / Tempête de feu \r\n\r\nLes membres présent sont:\r\n\r\n Dartoch\r\n Psy-4\r\n Gp\r\n Gyom\r\n Bmdc\r\n Jujube\r\n Ben\r\n Mika\r\n Pignouf\r\n Lebetz\r\n\r\nIl nous manque encore 2 membres pour finalisé les escouades et les strat donc pour ceux qui sont intéressé, veuillé vous rendre sur la partie du site concerné\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n[/COLOR]\r\n[COLOR=#FF0000]En espérant que vous recevrez tous ce message je vous souhaite encore une joyeuses Pâques, une bonne journée et plein de victoire sur le champ de bataille au nom de la meute !!!!!!!!!\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/COLOR]', 1),
(57, 5, 1364799646, 5, 6, '(Aucun objet)', '[COLOR=#FFA500][SIZE=5]Bonjour a tous et joyeuses Pâques tous le monde !!!\r\n\r\nJe vous rappel que pour le mois d''avril nous avons deux Evénements de prévu :[/SIZE]\r\n[/COLOR]\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\Soirée des 1 an de la meute le samedi 13 au soir sur notre serveur //////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n [COLOR=#4169E1]bataille only C4 sur métro ( j''veux que ça pète dans tout les sens ) \r\n Course robot nem sur frontière Caspienne \r\n Photo de Famille sur Tour Ziba[/COLOR]\r\n\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ Match 12 VS 12 contre la team 2099 Dimanche 14 a 15h ///////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n[COLOR=#0000FF] Ruée / Pic de damavand \r\n Conquête / Tempête de feu \r\n\r\nLes membres présent sont:\r\n\r\n Dartoch\r\n Psy-4\r\n Gp\r\n Gyom\r\n Bmdc\r\n Jujube\r\n Ben\r\n Mika\r\n Pignouf\r\n Lebetz\r\n\r\nIl nous manque encore 2 membres pour finalisé les escouades et les strat donc pour ceux qui sont intéressé, veuillé vous rendre sur la partie du site concerné\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n[/COLOR]\r\n[COLOR=#FF0000]En espérant que vous recevrez tous ce message je vous souhaite encore une joyeuses Pâques, une bonne journée et plein de victoire sur le champ de bataille au nom de la meute !!!!!!!!!\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/COLOR]', 0),
(58, 6, 1364799646, 5, 6, '(Aucun objet)', '[COLOR=#FFA500][SIZE=5]Bonjour a tous et joyeuses Pâques tous le monde !!!\r\n\r\nJe vous rappel que pour le mois d''avril nous avons deux Evénements de prévu :[/SIZE]\r\n[/COLOR]\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\Soirée des 1 an de la meute le samedi 13 au soir sur notre serveur //////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n [COLOR=#4169E1]bataille only C4 sur métro ( j''veux que ça pète dans tout les sens ) \r\n Course robot nem sur frontière Caspienne \r\n Photo de Famille sur Tour Ziba[/COLOR]\r\n\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ Match 12 VS 12 contre la team 2099 Dimanche 14 a 15h ///////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n[COLOR=#0000FF] Ruée / Pic de damavand \r\n Conquête / Tempête de feu \r\n\r\nLes membres présent sont:\r\n\r\n Dartoch\r\n Psy-4\r\n Gp\r\n Gyom\r\n Bmdc\r\n Jujube\r\n Ben\r\n Mika\r\n Pignouf\r\n Lebetz\r\n\r\nIl nous manque encore 2 membres pour finalisé les escouades et les strat donc pour ceux qui sont intéressé, veuillé vous rendre sur la partie du site concerné\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n[/COLOR]\r\n[COLOR=#FF0000]En espérant que vous recevrez tous ce message je vous souhaite encore une joyeuses Pâques, une bonne journée et plein de victoire sur le champ de bataille au nom de la meute !!!!!!!!!\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/COLOR]', 1),
(59, 5, 1364799646, 5, 8, '(Aucun objet)', '[COLOR=#FFA500][SIZE=5]Bonjour a tous et joyeuses Pâques tous le monde !!!\r\n\r\nJe vous rappel que pour le mois d''avril nous avons deux Evénements de prévu :[/SIZE]\r\n[/COLOR]\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\Soirée des 1 an de la meute le samedi 13 au soir sur notre serveur //////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n [COLOR=#4169E1]bataille only C4 sur métro ( j''veux que ça pète dans tout les sens ) \r\n Course robot nem sur frontière Caspienne \r\n Photo de Famille sur Tour Ziba[/COLOR]\r\n\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ Match 12 VS 12 contre la team 2099 Dimanche 14 a 15h ///////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n[COLOR=#0000FF] Ruée / Pic de damavand \r\n Conquête / Tempête de feu \r\n\r\nLes membres présent sont:\r\n\r\n Dartoch\r\n Psy-4\r\n Gp\r\n Gyom\r\n Bmdc\r\n Jujube\r\n Ben\r\n Mika\r\n Pignouf\r\n Lebetz\r\n\r\nIl nous manque encore 2 membres pour finalisé les escouades et les strat donc pour ceux qui sont intéressé, veuillé vous rendre sur la partie du site concerné\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n[/COLOR]\r\n[COLOR=#FF0000]En espérant que vous recevrez tous ce message je vous souhaite encore une joyeuses Pâques, une bonne journée et plein de victoire sur le champ de bataille au nom de la meute !!!!!!!!!\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/COLOR]', 0),
(60, 8, 1364799646, 5, 8, '(Aucun objet)', '[COLOR=#FFA500][SIZE=5]Bonjour a tous et joyeuses Pâques tous le monde !!!\r\n\r\nJe vous rappel que pour le mois d''avril nous avons deux Evénements de prévu :[/SIZE]\r\n[/COLOR]\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\Soirée des 1 an de la meute le samedi 13 au soir sur notre serveur //////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n [COLOR=#4169E1]bataille only C4 sur métro ( j''veux que ça pète dans tout les sens ) \r\n Course robot nem sur frontière Caspienne \r\n Photo de Famille sur Tour Ziba[/COLOR]\r\n\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ Match 12 VS 12 contre la team 2099 Dimanche 14 a 15h ///////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n[COLOR=#0000FF] Ruée / Pic de damavand \r\n Conquête / Tempête de feu \r\n\r\nLes membres présent sont:\r\n\r\n Dartoch\r\n Psy-4\r\n Gp\r\n Gyom\r\n Bmdc\r\n Jujube\r\n Ben\r\n Mika\r\n Pignouf\r\n Lebetz\r\n\r\nIl nous manque encore 2 membres pour finalisé les escouades et les strat donc pour ceux qui sont intéressé, veuillé vous rendre sur la partie du site concerné\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n[/COLOR]\r\n[COLOR=#FF0000]En espérant que vous recevrez tous ce message je vous souhaite encore une joyeuses Pâques, une bonne journée et plein de victoire sur le champ de bataille au nom de la meute !!!!!!!!!\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/COLOR]', 1),
(61, 5, 1364799646, 5, 9, '(Aucun objet)', '[COLOR=#FFA500][SIZE=5]Bonjour a tous et joyeuses Pâques tous le monde !!!\r\n\r\nJe vous rappel que pour le mois d''avril nous avons deux Evénements de prévu :[/SIZE]\r\n[/COLOR]\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\Soirée des 1 an de la meute le samedi 13 au soir sur notre serveur //////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n [COLOR=#4169E1]bataille only C4 sur métro ( j''veux que ça pète dans tout les sens ) \r\n Course robot nem sur frontière Caspienne \r\n Photo de Famille sur Tour Ziba[/COLOR]\r\n\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ Match 12 VS 12 contre la team 2099 Dimanche 14 a 15h ///////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n[COLOR=#0000FF] Ruée / Pic de damavand \r\n Conquête / Tempête de feu \r\n\r\nLes membres présent sont:\r\n\r\n Dartoch\r\n Psy-4\r\n Gp\r\n Gyom\r\n Bmdc\r\n Jujube\r\n Ben\r\n Mika\r\n Pignouf\r\n Lebetz\r\n\r\nIl nous manque encore 2 membres pour finalisé les escouades et les strat donc pour ceux qui sont intéressé, veuillé vous rendre sur la partie du site concerné\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n[/COLOR]\r\n[COLOR=#FF0000]En espérant que vous recevrez tous ce message je vous souhaite encore une joyeuses Pâques, une bonne journée et plein de victoire sur le champ de bataille au nom de la meute !!!!!!!!!\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/COLOR]', 0),
(62, 9, 1364799646, 5, 9, '(Aucun objet)', '[COLOR=#FFA500][SIZE=5]Bonjour a tous et joyeuses Pâques tous le monde !!!\r\n\r\nJe vous rappel que pour le mois d''avril nous avons deux Evénements de prévu :[/SIZE]\r\n[/COLOR]\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\Soirée des 1 an de la meute le samedi 13 au soir sur notre serveur //////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n [COLOR=#4169E1]bataille only C4 sur métro ( j''veux que ça pète dans tout les sens ) \r\n Course robot nem sur frontière Caspienne \r\n Photo de Famille sur Tour Ziba[/COLOR]\r\n\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ Match 12 VS 12 contre la team 2099 Dimanche 14 a 15h ///////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n[COLOR=#0000FF] Ruée / Pic de damavand \r\n Conquête / Tempête de feu \r\n\r\nLes membres présent sont:\r\n\r\n Dartoch\r\n Psy-4\r\n Gp\r\n Gyom\r\n Bmdc\r\n Jujube\r\n Ben\r\n Mika\r\n Pignouf\r\n Lebetz\r\n\r\nIl nous manque encore 2 membres pour finalisé les escouades et les strat donc pour ceux qui sont intéressé, veuillé vous rendre sur la partie du site concerné\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n[/COLOR]\r\n[COLOR=#FF0000]En espérant que vous recevrez tous ce message je vous souhaite encore une joyeuses Pâques, une bonne journée et plein de victoire sur le champ de bataille au nom de la meute !!!!!!!!!\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/COLOR]', 1),
(63, 5, 1364799646, 5, 10, '(Aucun objet)', '[COLOR=#FFA500][SIZE=5]Bonjour a tous et joyeuses Pâques tous le monde !!!\r\n\r\nJe vous rappel que pour le mois d''avril nous avons deux Evénements de prévu :[/SIZE]\r\n[/COLOR]\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\Soirée des 1 an de la meute le samedi 13 au soir sur notre serveur //////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n [COLOR=#4169E1]bataille only C4 sur métro ( j''veux que ça pète dans tout les sens ) \r\n Course robot nem sur frontière Caspienne \r\n Photo de Famille sur Tour Ziba[/COLOR]\r\n\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ Match 12 VS 12 contre la team 2099 Dimanche 14 a 15h ///////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n[COLOR=#0000FF] Ruée / Pic de damavand \r\n Conquête / Tempête de feu \r\n\r\nLes membres présent sont:\r\n\r\n Dartoch\r\n Psy-4\r\n Gp\r\n Gyom\r\n Bmdc\r\n Jujube\r\n Ben\r\n Mika\r\n Pignouf\r\n Lebetz\r\n\r\nIl nous manque encore 2 membres pour finalisé les escouades et les strat donc pour ceux qui sont intéressé, veuillé vous rendre sur la partie du site concerné\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n[/COLOR]\r\n[COLOR=#FF0000]En espérant que vous recevrez tous ce message je vous souhaite encore une joyeuses Pâques, une bonne journée et plein de victoire sur le champ de bataille au nom de la meute !!!!!!!!!\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/COLOR]', 0),
(64, 10, 1364799646, 5, 10, '(Aucun objet)', '[COLOR=#FFA500][SIZE=5]Bonjour a tous et joyeuses Pâques tous le monde !!!\r\n\r\nJe vous rappel que pour le mois d''avril nous avons deux Evénements de prévu :[/SIZE]\r\n[/COLOR]\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\Soirée des 1 an de la meute le samedi 13 au soir sur notre serveur //////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n [COLOR=#4169E1]bataille only C4 sur métro ( j''veux que ça pète dans tout les sens ) \r\n Course robot nem sur frontière Caspienne \r\n Photo de Famille sur Tour Ziba[/COLOR]\r\n\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ Match 12 VS 12 contre la team 2099 Dimanche 14 a 15h ///////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n[COLOR=#0000FF] Ruée / Pic de damavand \r\n Conquête / Tempête de feu \r\n\r\nLes membres présent sont:\r\n\r\n Dartoch\r\n Psy-4\r\n Gp\r\n Gyom\r\n Bmdc\r\n Jujube\r\n Ben\r\n Mika\r\n Pignouf\r\n Lebetz\r\n\r\nIl nous manque encore 2 membres pour finalisé les escouades et les strat donc pour ceux qui sont intéressé, veuillé vous rendre sur la partie du site concerné\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n[/COLOR]\r\n[COLOR=#FF0000]En espérant que vous recevrez tous ce message je vous souhaite encore une joyeuses Pâques, une bonne journée et plein de victoire sur le champ de bataille au nom de la meute !!!!!!!!!\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/COLOR]', 1);
INSERT INTO `messenger` (`messageID`, `userID`, `date`, `fromuser`, `touser`, `title`, `message`, `viewed`) VALUES
(65, 5, 1364799646, 5, 11, '(Aucun objet)', '[COLOR=#FFA500][SIZE=5]Bonjour a tous et joyeuses Pâques tous le monde !!!\r\n\r\nJe vous rappel que pour le mois d''avril nous avons deux Evénements de prévu :[/SIZE]\r\n[/COLOR]\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\Soirée des 1 an de la meute le samedi 13 au soir sur notre serveur //////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n [COLOR=#4169E1]bataille only C4 sur métro ( j''veux que ça pète dans tout les sens ) \r\n Course robot nem sur frontière Caspienne \r\n Photo de Famille sur Tour Ziba[/COLOR]\r\n\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ Match 12 VS 12 contre la team 2099 Dimanche 14 a 15h ///////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n[COLOR=#0000FF] Ruée / Pic de damavand \r\n Conquête / Tempête de feu \r\n\r\nLes membres présent sont:\r\n\r\n Dartoch\r\n Psy-4\r\n Gp\r\n Gyom\r\n Bmdc\r\n Jujube\r\n Ben\r\n Mika\r\n Pignouf\r\n Lebetz\r\n\r\nIl nous manque encore 2 membres pour finalisé les escouades et les strat donc pour ceux qui sont intéressé, veuillé vous rendre sur la partie du site concerné\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n[/COLOR]\r\n[COLOR=#FF0000]En espérant que vous recevrez tous ce message je vous souhaite encore une joyeuses Pâques, une bonne journée et plein de victoire sur le champ de bataille au nom de la meute !!!!!!!!!\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/COLOR]', 0),
(66, 11, 1364799646, 5, 11, '(Aucun objet)', '[COLOR=#FFA500][SIZE=5]Bonjour a tous et joyeuses Pâques tous le monde !!!\r\n\r\nJe vous rappel que pour le mois d''avril nous avons deux Evénements de prévu :[/SIZE]\r\n[/COLOR]\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\Soirée des 1 an de la meute le samedi 13 au soir sur notre serveur //////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n [COLOR=#4169E1]bataille only C4 sur métro ( j''veux que ça pète dans tout les sens ) \r\n Course robot nem sur frontière Caspienne \r\n Photo de Famille sur Tour Ziba[/COLOR]\r\n\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ Match 12 VS 12 contre la team 2099 Dimanche 14 a 15h ///////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n[COLOR=#0000FF] Ruée / Pic de damavand \r\n Conquête / Tempête de feu \r\n\r\nLes membres présent sont:\r\n\r\n Dartoch\r\n Psy-4\r\n Gp\r\n Gyom\r\n Bmdc\r\n Jujube\r\n Ben\r\n Mika\r\n Pignouf\r\n Lebetz\r\n\r\nIl nous manque encore 2 membres pour finalisé les escouades et les strat donc pour ceux qui sont intéressé, veuillé vous rendre sur la partie du site concerné\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n[/COLOR]\r\n[COLOR=#FF0000]En espérant que vous recevrez tous ce message je vous souhaite encore une joyeuses Pâques, une bonne journée et plein de victoire sur le champ de bataille au nom de la meute !!!!!!!!!\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/COLOR]', 1),
(67, 5, 1364799646, 5, 12, '(Aucun objet)', '[COLOR=#FFA500][SIZE=5]Bonjour a tous et joyeuses Pâques tous le monde !!!\r\n\r\nJe vous rappel que pour le mois d''avril nous avons deux Evénements de prévu :[/SIZE]\r\n[/COLOR]\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\Soirée des 1 an de la meute le samedi 13 au soir sur notre serveur //////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n [COLOR=#4169E1]bataille only C4 sur métro ( j''veux que ça pète dans tout les sens ) \r\n Course robot nem sur frontière Caspienne \r\n Photo de Famille sur Tour Ziba[/COLOR]\r\n\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ Match 12 VS 12 contre la team 2099 Dimanche 14 a 15h ///////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n[COLOR=#0000FF] Ruée / Pic de damavand \r\n Conquête / Tempête de feu \r\n\r\nLes membres présent sont:\r\n\r\n Dartoch\r\n Psy-4\r\n Gp\r\n Gyom\r\n Bmdc\r\n Jujube\r\n Ben\r\n Mika\r\n Pignouf\r\n Lebetz\r\n\r\nIl nous manque encore 2 membres pour finalisé les escouades et les strat donc pour ceux qui sont intéressé, veuillé vous rendre sur la partie du site concerné\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n[/COLOR]\r\n[COLOR=#FF0000]En espérant que vous recevrez tous ce message je vous souhaite encore une joyeuses Pâques, une bonne journée et plein de victoire sur le champ de bataille au nom de la meute !!!!!!!!!\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/COLOR]', 0),
(68, 12, 1364799646, 5, 12, '(Aucun objet)', '[COLOR=#FFA500][SIZE=5]Bonjour a tous et joyeuses Pâques tous le monde !!!\r\n\r\nJe vous rappel que pour le mois d''avril nous avons deux Evénements de prévu :[/SIZE]\r\n[/COLOR]\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\Soirée des 1 an de la meute le samedi 13 au soir sur notre serveur //////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n [COLOR=#4169E1]bataille only C4 sur métro ( j''veux que ça pète dans tout les sens ) \r\n Course robot nem sur frontière Caspienne \r\n Photo de Famille sur Tour Ziba[/COLOR]\r\n\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ Match 12 VS 12 contre la team 2099 Dimanche 14 a 15h ///////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n[COLOR=#0000FF] Ruée / Pic de damavand \r\n Conquête / Tempête de feu \r\n\r\nLes membres présent sont:\r\n\r\n Dartoch\r\n Psy-4\r\n Gp\r\n Gyom\r\n Bmdc\r\n Jujube\r\n Ben\r\n Mika\r\n Pignouf\r\n Lebetz\r\n\r\nIl nous manque encore 2 membres pour finalisé les escouades et les strat donc pour ceux qui sont intéressé, veuillé vous rendre sur la partie du site concerné\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n[/COLOR]\r\n[COLOR=#FF0000]En espérant que vous recevrez tous ce message je vous souhaite encore une joyeuses Pâques, une bonne journée et plein de victoire sur le champ de bataille au nom de la meute !!!!!!!!!\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/COLOR]', 1),
(69, 5, 1364799646, 5, 14, '(Aucun objet)', '[COLOR=#FFA500][SIZE=5]Bonjour a tous et joyeuses Pâques tous le monde !!!\r\n\r\nJe vous rappel que pour le mois d''avril nous avons deux Evénements de prévu :[/SIZE]\r\n[/COLOR]\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\Soirée des 1 an de la meute le samedi 13 au soir sur notre serveur //////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n [COLOR=#4169E1]bataille only C4 sur métro ( j''veux que ça pète dans tout les sens ) \r\n Course robot nem sur frontière Caspienne \r\n Photo de Famille sur Tour Ziba[/COLOR]\r\n\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ Match 12 VS 12 contre la team 2099 Dimanche 14 a 15h ///////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n[COLOR=#0000FF] Ruée / Pic de damavand \r\n Conquête / Tempête de feu \r\n\r\nLes membres présent sont:\r\n\r\n Dartoch\r\n Psy-4\r\n Gp\r\n Gyom\r\n Bmdc\r\n Jujube\r\n Ben\r\n Mika\r\n Pignouf\r\n Lebetz\r\n\r\nIl nous manque encore 2 membres pour finalisé les escouades et les strat donc pour ceux qui sont intéressé, veuillé vous rendre sur la partie du site concerné\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n[/COLOR]\r\n[COLOR=#FF0000]En espérant que vous recevrez tous ce message je vous souhaite encore une joyeuses Pâques, une bonne journée et plein de victoire sur le champ de bataille au nom de la meute !!!!!!!!!\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/COLOR]', 0),
(70, 14, 1364799646, 5, 14, '(Aucun objet)', '[COLOR=#FFA500][SIZE=5]Bonjour a tous et joyeuses Pâques tous le monde !!!\r\n\r\nJe vous rappel que pour le mois d''avril nous avons deux Evénements de prévu :[/SIZE]\r\n[/COLOR]\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\Soirée des 1 an de la meute le samedi 13 au soir sur notre serveur //////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n [COLOR=#4169E1]bataille only C4 sur métro ( j''veux que ça pète dans tout les sens ) \r\n Course robot nem sur frontière Caspienne \r\n Photo de Famille sur Tour Ziba[/COLOR]\r\n\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ Match 12 VS 12 contre la team 2099 Dimanche 14 a 15h ///////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n[COLOR=#0000FF] Ruée / Pic de damavand \r\n Conquête / Tempête de feu \r\n\r\nLes membres présent sont:\r\n\r\n Dartoch\r\n Psy-4\r\n Gp\r\n Gyom\r\n Bmdc\r\n Jujube\r\n Ben\r\n Mika\r\n Pignouf\r\n Lebetz\r\n\r\nIl nous manque encore 2 membres pour finalisé les escouades et les strat donc pour ceux qui sont intéressé, veuillé vous rendre sur la partie du site concerné\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n[/COLOR]\r\n[COLOR=#FF0000]En espérant que vous recevrez tous ce message je vous souhaite encore une joyeuses Pâques, une bonne journée et plein de victoire sur le champ de bataille au nom de la meute !!!!!!!!!\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/COLOR]', 1),
(71, 5, 1364799646, 5, 15, '(Aucun objet)', '[COLOR=#FFA500][SIZE=5]Bonjour a tous et joyeuses Pâques tous le monde !!!\r\n\r\nJe vous rappel que pour le mois d''avril nous avons deux Evénements de prévu :[/SIZE]\r\n[/COLOR]\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\Soirée des 1 an de la meute le samedi 13 au soir sur notre serveur //////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n [COLOR=#4169E1]bataille only C4 sur métro ( j''veux que ça pète dans tout les sens ) \r\n Course robot nem sur frontière Caspienne \r\n Photo de Famille sur Tour Ziba[/COLOR]\r\n\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ Match 12 VS 12 contre la team 2099 Dimanche 14 a 15h ///////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n[COLOR=#0000FF] Ruée / Pic de damavand \r\n Conquête / Tempête de feu \r\n\r\nLes membres présent sont:\r\n\r\n Dartoch\r\n Psy-4\r\n Gp\r\n Gyom\r\n Bmdc\r\n Jujube\r\n Ben\r\n Mika\r\n Pignouf\r\n Lebetz\r\n\r\nIl nous manque encore 2 membres pour finalisé les escouades et les strat donc pour ceux qui sont intéressé, veuillé vous rendre sur la partie du site concerné\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n[/COLOR]\r\n[COLOR=#FF0000]En espérant que vous recevrez tous ce message je vous souhaite encore une joyeuses Pâques, une bonne journée et plein de victoire sur le champ de bataille au nom de la meute !!!!!!!!!\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/COLOR]', 0),
(73, 5, 1364799646, 5, 16, '(Aucun objet)', '[COLOR=#FFA500][SIZE=5]Bonjour a tous et joyeuses Pâques tous le monde !!!\r\n\r\nJe vous rappel que pour le mois d''avril nous avons deux Evénements de prévu :[/SIZE]\r\n[/COLOR]\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\Soirée des 1 an de la meute le samedi 13 au soir sur notre serveur //////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n [COLOR=#4169E1]bataille only C4 sur métro ( j''veux que ça pète dans tout les sens ) \r\n Course robot nem sur frontière Caspienne \r\n Photo de Famille sur Tour Ziba[/COLOR]\r\n\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ Match 12 VS 12 contre la team 2099 Dimanche 14 a 15h ///////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n[COLOR=#0000FF] Ruée / Pic de damavand \r\n Conquête / Tempête de feu \r\n\r\nLes membres présent sont:\r\n\r\n Dartoch\r\n Psy-4\r\n Gp\r\n Gyom\r\n Bmdc\r\n Jujube\r\n Ben\r\n Mika\r\n Pignouf\r\n Lebetz\r\n\r\nIl nous manque encore 2 membres pour finalisé les escouades et les strat donc pour ceux qui sont intéressé, veuillé vous rendre sur la partie du site concerné\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n[/COLOR]\r\n[COLOR=#FF0000]En espérant que vous recevrez tous ce message je vous souhaite encore une joyeuses Pâques, une bonne journée et plein de victoire sur le champ de bataille au nom de la meute !!!!!!!!!\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/COLOR]', 0),
(74, 16, 1364799646, 5, 16, '(Aucun objet)', '[COLOR=#FFA500][SIZE=5]Bonjour a tous et joyeuses Pâques tous le monde !!!\r\n\r\nJe vous rappel que pour le mois d''avril nous avons deux Evénements de prévu :[/SIZE]\r\n[/COLOR]\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\Soirée des 1 an de la meute le samedi 13 au soir sur notre serveur //////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n [COLOR=#4169E1]bataille only C4 sur métro ( j''veux que ça pète dans tout les sens ) \r\n Course robot nem sur frontière Caspienne \r\n Photo de Famille sur Tour Ziba[/COLOR]\r\n\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ Match 12 VS 12 contre la team 2099 Dimanche 14 a 15h ///////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n[COLOR=#0000FF] Ruée / Pic de damavand \r\n Conquête / Tempête de feu \r\n\r\nLes membres présent sont:\r\n\r\n Dartoch\r\n Psy-4\r\n Gp\r\n Gyom\r\n Bmdc\r\n Jujube\r\n Ben\r\n Mika\r\n Pignouf\r\n Lebetz\r\n\r\nIl nous manque encore 2 membres pour finalisé les escouades et les strat donc pour ceux qui sont intéressé, veuillé vous rendre sur la partie du site concerné\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n[/COLOR]\r\n[COLOR=#FF0000]En espérant que vous recevrez tous ce message je vous souhaite encore une joyeuses Pâques, une bonne journée et plein de victoire sur le champ de bataille au nom de la meute !!!!!!!!!\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/COLOR]', 1),
(75, 5, 1364799646, 5, 17, '(Aucun objet)', '[COLOR=#FFA500][SIZE=5]Bonjour a tous et joyeuses Pâques tous le monde !!!\r\n\r\nJe vous rappel que pour le mois d''avril nous avons deux Evénements de prévu :[/SIZE]\r\n[/COLOR]\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\Soirée des 1 an de la meute le samedi 13 au soir sur notre serveur //////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n [COLOR=#4169E1]bataille only C4 sur métro ( j''veux que ça pète dans tout les sens ) \r\n Course robot nem sur frontière Caspienne \r\n Photo de Famille sur Tour Ziba[/COLOR]\r\n\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ Match 12 VS 12 contre la team 2099 Dimanche 14 a 15h ///////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n[COLOR=#0000FF] Ruée / Pic de damavand \r\n Conquête / Tempête de feu \r\n\r\nLes membres présent sont:\r\n\r\n Dartoch\r\n Psy-4\r\n Gp\r\n Gyom\r\n Bmdc\r\n Jujube\r\n Ben\r\n Mika\r\n Pignouf\r\n Lebetz\r\n\r\nIl nous manque encore 2 membres pour finalisé les escouades et les strat donc pour ceux qui sont intéressé, veuillé vous rendre sur la partie du site concerné\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n[/COLOR]\r\n[COLOR=#FF0000]En espérant que vous recevrez tous ce message je vous souhaite encore une joyeuses Pâques, une bonne journée et plein de victoire sur le champ de bataille au nom de la meute !!!!!!!!!\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/COLOR]', 0),
(76, 17, 1364799646, 5, 17, '(Aucun objet)', '[COLOR=#FFA500][SIZE=5]Bonjour a tous et joyeuses Pâques tous le monde !!!\r\n\r\nJe vous rappel que pour le mois d''avril nous avons deux Evénements de prévu :[/SIZE]\r\n[/COLOR]\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\Soirée des 1 an de la meute le samedi 13 au soir sur notre serveur //////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n [COLOR=#4169E1]bataille only C4 sur métro ( j''veux que ça pète dans tout les sens ) \r\n Course robot nem sur frontière Caspienne \r\n Photo de Famille sur Tour Ziba[/COLOR]\r\n\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ Match 12 VS 12 contre la team 2099 Dimanche 14 a 15h ///////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n[COLOR=#0000FF] Ruée / Pic de damavand \r\n Conquête / Tempête de feu \r\n\r\nLes membres présent sont:\r\n\r\n Dartoch\r\n Psy-4\r\n Gp\r\n Gyom\r\n Bmdc\r\n Jujube\r\n Ben\r\n Mika\r\n Pignouf\r\n Lebetz\r\n\r\nIl nous manque encore 2 membres pour finalisé les escouades et les strat donc pour ceux qui sont intéressé, veuillé vous rendre sur la partie du site concerné\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n[/COLOR]\r\n[COLOR=#FF0000]En espérant que vous recevrez tous ce message je vous souhaite encore une joyeuses Pâques, une bonne journée et plein de victoire sur le champ de bataille au nom de la meute !!!!!!!!!\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/COLOR]', 0),
(77, 5, 1364799646, 5, 18, '(Aucun objet)', '[COLOR=#FFA500][SIZE=5]Bonjour a tous et joyeuses Pâques tous le monde !!!\r\n\r\nJe vous rappel que pour le mois d''avril nous avons deux Evénements de prévu :[/SIZE]\r\n[/COLOR]\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\Soirée des 1 an de la meute le samedi 13 au soir sur notre serveur //////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n [COLOR=#4169E1]bataille only C4 sur métro ( j''veux que ça pète dans tout les sens ) \r\n Course robot nem sur frontière Caspienne \r\n Photo de Famille sur Tour Ziba[/COLOR]\r\n\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ Match 12 VS 12 contre la team 2099 Dimanche 14 a 15h ///////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n[COLOR=#0000FF] Ruée / Pic de damavand \r\n Conquête / Tempête de feu \r\n\r\nLes membres présent sont:\r\n\r\n Dartoch\r\n Psy-4\r\n Gp\r\n Gyom\r\n Bmdc\r\n Jujube\r\n Ben\r\n Mika\r\n Pignouf\r\n Lebetz\r\n\r\nIl nous manque encore 2 membres pour finalisé les escouades et les strat donc pour ceux qui sont intéressé, veuillé vous rendre sur la partie du site concerné\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n[/COLOR]\r\n[COLOR=#FF0000]En espérant que vous recevrez tous ce message je vous souhaite encore une joyeuses Pâques, une bonne journée et plein de victoire sur le champ de bataille au nom de la meute !!!!!!!!!\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/COLOR]', 0),
(78, 18, 1364799647, 5, 18, '(Aucun objet)', '[COLOR=#FFA500][SIZE=5]Bonjour a tous et joyeuses Pâques tous le monde !!!\r\n\r\nJe vous rappel que pour le mois d''avril nous avons deux Evénements de prévu :[/SIZE]\r\n[/COLOR]\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\Soirée des 1 an de la meute le samedi 13 au soir sur notre serveur //////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n [COLOR=#4169E1]bataille only C4 sur métro ( j''veux que ça pète dans tout les sens ) \r\n Course robot nem sur frontière Caspienne \r\n Photo de Famille sur Tour Ziba[/COLOR]\r\n\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ Match 12 VS 12 contre la team 2099 Dimanche 14 a 15h ///////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n[COLOR=#0000FF] Ruée / Pic de damavand \r\n Conquête / Tempête de feu \r\n\r\nLes membres présent sont:\r\n\r\n Dartoch\r\n Psy-4\r\n Gp\r\n Gyom\r\n Bmdc\r\n Jujube\r\n Ben\r\n Mika\r\n Pignouf\r\n Lebetz\r\n\r\nIl nous manque encore 2 membres pour finalisé les escouades et les strat donc pour ceux qui sont intéressé, veuillé vous rendre sur la partie du site concerné\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n[/COLOR]\r\n[COLOR=#FF0000]En espérant que vous recevrez tous ce message je vous souhaite encore une joyeuses Pâques, une bonne journée et plein de victoire sur le champ de bataille au nom de la meute !!!!!!!!!\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/COLOR]', 1),
(79, 5, 1364799647, 5, 19, '(Aucun objet)', '[COLOR=#FFA500][SIZE=5]Bonjour a tous et joyeuses Pâques tous le monde !!!\r\n\r\nJe vous rappel que pour le mois d''avril nous avons deux Evénements de prévu :[/SIZE]\r\n[/COLOR]\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\Soirée des 1 an de la meute le samedi 13 au soir sur notre serveur //////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n [COLOR=#4169E1]bataille only C4 sur métro ( j''veux que ça pète dans tout les sens ) \r\n Course robot nem sur frontière Caspienne \r\n Photo de Famille sur Tour Ziba[/COLOR]\r\n\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ Match 12 VS 12 contre la team 2099 Dimanche 14 a 15h ///////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n[COLOR=#0000FF] Ruée / Pic de damavand \r\n Conquête / Tempête de feu \r\n\r\nLes membres présent sont:\r\n\r\n Dartoch\r\n Psy-4\r\n Gp\r\n Gyom\r\n Bmdc\r\n Jujube\r\n Ben\r\n Mika\r\n Pignouf\r\n Lebetz\r\n\r\nIl nous manque encore 2 membres pour finalisé les escouades et les strat donc pour ceux qui sont intéressé, veuillé vous rendre sur la partie du site concerné\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n[/COLOR]\r\n[COLOR=#FF0000]En espérant que vous recevrez tous ce message je vous souhaite encore une joyeuses Pâques, une bonne journée et plein de victoire sur le champ de bataille au nom de la meute !!!!!!!!!\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/COLOR]', 1),
(80, 19, 1364799647, 5, 19, '(Aucun objet)', '[COLOR=#FFA500][SIZE=5]Bonjour a tous et joyeuses Pâques tous le monde !!!\r\n\r\nJe vous rappel que pour le mois d''avril nous avons deux Evénements de prévu :[/SIZE]\r\n[/COLOR]\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\Soirée des 1 an de la meute le samedi 13 au soir sur notre serveur //////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n [COLOR=#4169E1]bataille only C4 sur métro ( j''veux que ça pète dans tout les sens ) \r\n Course robot nem sur frontière Caspienne \r\n Photo de Famille sur Tour Ziba[/COLOR]\r\n\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ Match 12 VS 12 contre la team 2099 Dimanche 14 a 15h ///////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n[COLOR=#0000FF] Ruée / Pic de damavand \r\n Conquête / Tempête de feu \r\n\r\nLes membres présent sont:\r\n\r\n Dartoch\r\n Psy-4\r\n Gp\r\n Gyom\r\n Bmdc\r\n Jujube\r\n Ben\r\n Mika\r\n Pignouf\r\n Lebetz\r\n\r\nIl nous manque encore 2 membres pour finalisé les escouades et les strat donc pour ceux qui sont intéressé, veuillé vous rendre sur la partie du site concerné\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n[/COLOR]\r\n[COLOR=#FF0000]En espérant que vous recevrez tous ce message je vous souhaite encore une joyeuses Pâques, une bonne journée et plein de victoire sur le champ de bataille au nom de la meute !!!!!!!!!\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/COLOR]', 0),
(81, 5, 1364799647, 5, 22, '(Aucun objet)', '[COLOR=#FFA500][SIZE=5]Bonjour a tous et joyeuses Pâques tous le monde !!!\r\n\r\nJe vous rappel que pour le mois d''avril nous avons deux Evénements de prévu :[/SIZE]\r\n[/COLOR]\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\Soirée des 1 an de la meute le samedi 13 au soir sur notre serveur //////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n [COLOR=#4169E1]bataille only C4 sur métro ( j''veux que ça pète dans tout les sens ) \r\n Course robot nem sur frontière Caspienne \r\n Photo de Famille sur Tour Ziba[/COLOR]\r\n\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ Match 12 VS 12 contre la team 2099 Dimanche 14 a 15h ///////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n[COLOR=#0000FF] Ruée / Pic de damavand \r\n Conquête / Tempête de feu \r\n\r\nLes membres présent sont:\r\n\r\n Dartoch\r\n Psy-4\r\n Gp\r\n Gyom\r\n Bmdc\r\n Jujube\r\n Ben\r\n Mika\r\n Pignouf\r\n Lebetz\r\n\r\nIl nous manque encore 2 membres pour finalisé les escouades et les strat donc pour ceux qui sont intéressé, veuillé vous rendre sur la partie du site concerné\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n[/COLOR]\r\n[COLOR=#FF0000]En espérant que vous recevrez tous ce message je vous souhaite encore une joyeuses Pâques, une bonne journée et plein de victoire sur le champ de bataille au nom de la meute !!!!!!!!!\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/COLOR]', 0),
(82, 22, 1364799647, 5, 22, '(Aucun objet)', '[COLOR=#FFA500][SIZE=5]Bonjour a tous et joyeuses Pâques tous le monde !!!\r\n\r\nJe vous rappel que pour le mois d''avril nous avons deux Evénements de prévu :[/SIZE]\r\n[/COLOR]\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\Soirée des 1 an de la meute le samedi 13 au soir sur notre serveur //////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n [COLOR=#4169E1]bataille only C4 sur métro ( j''veux que ça pète dans tout les sens ) \r\n Course robot nem sur frontière Caspienne \r\n Photo de Famille sur Tour Ziba[/COLOR]\r\n\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ Match 12 VS 12 contre la team 2099 Dimanche 14 a 15h ///////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n[COLOR=#0000FF] Ruée / Pic de damavand \r\n Conquête / Tempête de feu \r\n\r\nLes membres présent sont:\r\n\r\n Dartoch\r\n Psy-4\r\n Gp\r\n Gyom\r\n Bmdc\r\n Jujube\r\n Ben\r\n Mika\r\n Pignouf\r\n Lebetz\r\n\r\nIl nous manque encore 2 membres pour finalisé les escouades et les strat donc pour ceux qui sont intéressé, veuillé vous rendre sur la partie du site concerné\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n[/COLOR]\r\n[COLOR=#FF0000]En espérant que vous recevrez tous ce message je vous souhaite encore une joyeuses Pâques, une bonne journée et plein de victoire sur le champ de bataille au nom de la meute !!!!!!!!!\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/COLOR]', 1),
(83, 5, 1364799647, 5, 24, '(Aucun objet)', '[COLOR=#FFA500][SIZE=5]Bonjour a tous et joyeuses Pâques tous le monde !!!\r\n\r\nJe vous rappel que pour le mois d''avril nous avons deux Evénements de prévu :[/SIZE]\r\n[/COLOR]\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\Soirée des 1 an de la meute le samedi 13 au soir sur notre serveur //////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n [COLOR=#4169E1]bataille only C4 sur métro ( j''veux que ça pète dans tout les sens ) \r\n Course robot nem sur frontière Caspienne \r\n Photo de Famille sur Tour Ziba[/COLOR]\r\n\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ Match 12 VS 12 contre la team 2099 Dimanche 14 a 15h ///////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n[COLOR=#0000FF] Ruée / Pic de damavand \r\n Conquête / Tempête de feu \r\n\r\nLes membres présent sont:\r\n\r\n Dartoch\r\n Psy-4\r\n Gp\r\n Gyom\r\n Bmdc\r\n Jujube\r\n Ben\r\n Mika\r\n Pignouf\r\n Lebetz\r\n\r\nIl nous manque encore 2 membres pour finalisé les escouades et les strat donc pour ceux qui sont intéressé, veuillé vous rendre sur la partie du site concerné\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n[/COLOR]\r\n[COLOR=#FF0000]En espérant que vous recevrez tous ce message je vous souhaite encore une joyeuses Pâques, une bonne journée et plein de victoire sur le champ de bataille au nom de la meute !!!!!!!!!\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/COLOR]', 0),
(84, 24, 1364799647, 5, 24, '(Aucun objet)', '[COLOR=#FFA500][SIZE=5]Bonjour a tous et joyeuses Pâques tous le monde !!!\r\n\r\nJe vous rappel que pour le mois d''avril nous avons deux Evénements de prévu :[/SIZE]\r\n[/COLOR]\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\Soirée des 1 an de la meute le samedi 13 au soir sur notre serveur //////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n [COLOR=#4169E1]bataille only C4 sur métro ( j''veux que ça pète dans tout les sens ) \r\n Course robot nem sur frontière Caspienne \r\n Photo de Famille sur Tour Ziba[/COLOR]\r\n\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ Match 12 VS 12 contre la team 2099 Dimanche 14 a 15h ///////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n[COLOR=#0000FF] Ruée / Pic de damavand \r\n Conquête / Tempête de feu \r\n\r\nLes membres présent sont:\r\n\r\n Dartoch\r\n Psy-4\r\n Gp\r\n Gyom\r\n Bmdc\r\n Jujube\r\n Ben\r\n Mika\r\n Pignouf\r\n Lebetz\r\n\r\nIl nous manque encore 2 membres pour finalisé les escouades et les strat donc pour ceux qui sont intéressé, veuillé vous rendre sur la partie du site concerné\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n[/COLOR]\r\n[COLOR=#FF0000]En espérant que vous recevrez tous ce message je vous souhaite encore une joyeuses Pâques, une bonne journée et plein de victoire sur le champ de bataille au nom de la meute !!!!!!!!!\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/COLOR]', 0),
(85, 5, 1364799647, 5, 25, '(Aucun objet)', '[COLOR=#FFA500][SIZE=5]Bonjour a tous et joyeuses Pâques tous le monde !!!\r\n\r\nJe vous rappel que pour le mois d''avril nous avons deux Evénements de prévu :[/SIZE]\r\n[/COLOR]\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\Soirée des 1 an de la meute le samedi 13 au soir sur notre serveur //////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n [COLOR=#4169E1]bataille only C4 sur métro ( j''veux que ça pète dans tout les sens ) \r\n Course robot nem sur frontière Caspienne \r\n Photo de Famille sur Tour Ziba[/COLOR]\r\n\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ Match 12 VS 12 contre la team 2099 Dimanche 14 a 15h ///////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n[COLOR=#0000FF] Ruée / Pic de damavand \r\n Conquête / Tempête de feu \r\n\r\nLes membres présent sont:\r\n\r\n Dartoch\r\n Psy-4\r\n Gp\r\n Gyom\r\n Bmdc\r\n Jujube\r\n Ben\r\n Mika\r\n Pignouf\r\n Lebetz\r\n\r\nIl nous manque encore 2 membres pour finalisé les escouades et les strat donc pour ceux qui sont intéressé, veuillé vous rendre sur la partie du site concerné\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n[/COLOR]\r\n[COLOR=#FF0000]En espérant que vous recevrez tous ce message je vous souhaite encore une joyeuses Pâques, une bonne journée et plein de victoire sur le champ de bataille au nom de la meute !!!!!!!!!\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/COLOR]', 0),
(86, 25, 1364799647, 5, 25, '(Aucun objet)', '[COLOR=#FFA500][SIZE=5]Bonjour a tous et joyeuses Pâques tous le monde !!!\r\n\r\nJe vous rappel que pour le mois d''avril nous avons deux Evénements de prévu :[/SIZE]\r\n[/COLOR]\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\Soirée des 1 an de la meute le samedi 13 au soir sur notre serveur //////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n [COLOR=#4169E1]bataille only C4 sur métro ( j''veux que ça pète dans tout les sens ) \r\n Course robot nem sur frontière Caspienne \r\n Photo de Famille sur Tour Ziba[/COLOR]\r\n\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ Match 12 VS 12 contre la team 2099 Dimanche 14 a 15h ///////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n[COLOR=#0000FF] Ruée / Pic de damavand \r\n Conquête / Tempête de feu \r\n\r\nLes membres présent sont:\r\n\r\n Dartoch\r\n Psy-4\r\n Gp\r\n Gyom\r\n Bmdc\r\n Jujube\r\n Ben\r\n Mika\r\n Pignouf\r\n Lebetz\r\n\r\nIl nous manque encore 2 membres pour finalisé les escouades et les strat donc pour ceux qui sont intéressé, veuillé vous rendre sur la partie du site concerné\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n[/COLOR]\r\n[COLOR=#FF0000]En espérant que vous recevrez tous ce message je vous souhaite encore une joyeuses Pâques, une bonne journée et plein de victoire sur le champ de bataille au nom de la meute !!!!!!!!!\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/COLOR]', 1),
(87, 5, 1364799647, 5, 26, '(Aucun objet)', '[COLOR=#FFA500][SIZE=5]Bonjour a tous et joyeuses Pâques tous le monde !!!\r\n\r\nJe vous rappel que pour le mois d''avril nous avons deux Evénements de prévu :[/SIZE]\r\n[/COLOR]\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\Soirée des 1 an de la meute le samedi 13 au soir sur notre serveur //////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n [COLOR=#4169E1]bataille only C4 sur métro ( j''veux que ça pète dans tout les sens ) \r\n Course robot nem sur frontière Caspienne \r\n Photo de Famille sur Tour Ziba[/COLOR]\r\n\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ Match 12 VS 12 contre la team 2099 Dimanche 14 a 15h ///////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n[COLOR=#0000FF] Ruée / Pic de damavand \r\n Conquête / Tempête de feu \r\n\r\nLes membres présent sont:\r\n\r\n Dartoch\r\n Psy-4\r\n Gp\r\n Gyom\r\n Bmdc\r\n Jujube\r\n Ben\r\n Mika\r\n Pignouf\r\n Lebetz\r\n\r\nIl nous manque encore 2 membres pour finalisé les escouades et les strat donc pour ceux qui sont intéressé, veuillé vous rendre sur la partie du site concerné\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n[/COLOR]\r\n[COLOR=#FF0000]En espérant que vous recevrez tous ce message je vous souhaite encore une joyeuses Pâques, une bonne journée et plein de victoire sur le champ de bataille au nom de la meute !!!!!!!!!\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/COLOR]', 0),
(88, 26, 1364799647, 5, 26, '(Aucun objet)', '[COLOR=#FFA500][SIZE=5]Bonjour a tous et joyeuses Pâques tous le monde !!!\r\n\r\nJe vous rappel que pour le mois d''avril nous avons deux Evénements de prévu :[/SIZE]\r\n[/COLOR]\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\Soirée des 1 an de la meute le samedi 13 au soir sur notre serveur //////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n [COLOR=#4169E1]bataille only C4 sur métro ( j''veux que ça pète dans tout les sens ) \r\n Course robot nem sur frontière Caspienne \r\n Photo de Famille sur Tour Ziba[/COLOR]\r\n\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ Match 12 VS 12 contre la team 2099 Dimanche 14 a 15h ///////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n[COLOR=#0000FF] Ruée / Pic de damavand \r\n Conquête / Tempête de feu \r\n\r\nLes membres présent sont:\r\n\r\n Dartoch\r\n Psy-4\r\n Gp\r\n Gyom\r\n Bmdc\r\n Jujube\r\n Ben\r\n Mika\r\n Pignouf\r\n Lebetz\r\n\r\nIl nous manque encore 2 membres pour finalisé les escouades et les strat donc pour ceux qui sont intéressé, veuillé vous rendre sur la partie du site concerné\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n[/COLOR]\r\n[COLOR=#FF0000]En espérant que vous recevrez tous ce message je vous souhaite encore une joyeuses Pâques, une bonne journée et plein de victoire sur le champ de bataille au nom de la meute !!!!!!!!!\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/COLOR]', 1);
INSERT INTO `messenger` (`messageID`, `userID`, `date`, `fromuser`, `touser`, `title`, `message`, `viewed`) VALUES
(89, 5, 1364799647, 5, 27, '(Aucun objet)', '[COLOR=#FFA500][SIZE=5]Bonjour a tous et joyeuses Pâques tous le monde !!!\r\n\r\nJe vous rappel que pour le mois d''avril nous avons deux Evénements de prévu :[/SIZE]\r\n[/COLOR]\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\Soirée des 1 an de la meute le samedi 13 au soir sur notre serveur //////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n [COLOR=#4169E1]bataille only C4 sur métro ( j''veux que ça pète dans tout les sens ) \r\n Course robot nem sur frontière Caspienne \r\n Photo de Famille sur Tour Ziba[/COLOR]\r\n\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ Match 12 VS 12 contre la team 2099 Dimanche 14 a 15h ///////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n[COLOR=#0000FF] Ruée / Pic de damavand \r\n Conquête / Tempête de feu \r\n\r\nLes membres présent sont:\r\n\r\n Dartoch\r\n Psy-4\r\n Gp\r\n Gyom\r\n Bmdc\r\n Jujube\r\n Ben\r\n Mika\r\n Pignouf\r\n Lebetz\r\n\r\nIl nous manque encore 2 membres pour finalisé les escouades et les strat donc pour ceux qui sont intéressé, veuillé vous rendre sur la partie du site concerné\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n[/COLOR]\r\n[COLOR=#FF0000]En espérant que vous recevrez tous ce message je vous souhaite encore une joyeuses Pâques, une bonne journée et plein de victoire sur le champ de bataille au nom de la meute !!!!!!!!!\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/COLOR]', 0),
(90, 27, 1364799647, 5, 27, '(Aucun objet)', '[COLOR=#FFA500][SIZE=5]Bonjour a tous et joyeuses Pâques tous le monde !!!\r\n\r\nJe vous rappel que pour le mois d''avril nous avons deux Evénements de prévu :[/SIZE]\r\n[/COLOR]\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\Soirée des 1 an de la meute le samedi 13 au soir sur notre serveur //////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n [COLOR=#4169E1]bataille only C4 sur métro ( j''veux que ça pète dans tout les sens ) \r\n Course robot nem sur frontière Caspienne \r\n Photo de Famille sur Tour Ziba[/COLOR]\r\n\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ Match 12 VS 12 contre la team 2099 Dimanche 14 a 15h ///////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n[COLOR=#0000FF] Ruée / Pic de damavand \r\n Conquête / Tempête de feu \r\n\r\nLes membres présent sont:\r\n\r\n Dartoch\r\n Psy-4\r\n Gp\r\n Gyom\r\n Bmdc\r\n Jujube\r\n Ben\r\n Mika\r\n Pignouf\r\n Lebetz\r\n\r\nIl nous manque encore 2 membres pour finalisé les escouades et les strat donc pour ceux qui sont intéressé, veuillé vous rendre sur la partie du site concerné\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n[/COLOR]\r\n[COLOR=#FF0000]En espérant que vous recevrez tous ce message je vous souhaite encore une joyeuses Pâques, une bonne journée et plein de victoire sur le champ de bataille au nom de la meute !!!!!!!!!\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/COLOR]', 1),
(91, 5, 1364799647, 5, 28, '(Aucun objet)', '[COLOR=#FFA500][SIZE=5]Bonjour a tous et joyeuses Pâques tous le monde !!!\r\n\r\nJe vous rappel que pour le mois d''avril nous avons deux Evénements de prévu :[/SIZE]\r\n[/COLOR]\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\Soirée des 1 an de la meute le samedi 13 au soir sur notre serveur //////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n [COLOR=#4169E1]bataille only C4 sur métro ( j''veux que ça pète dans tout les sens ) \r\n Course robot nem sur frontière Caspienne \r\n Photo de Famille sur Tour Ziba[/COLOR]\r\n\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ Match 12 VS 12 contre la team 2099 Dimanche 14 a 15h ///////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n[COLOR=#0000FF] Ruée / Pic de damavand \r\n Conquête / Tempête de feu \r\n\r\nLes membres présent sont:\r\n\r\n Dartoch\r\n Psy-4\r\n Gp\r\n Gyom\r\n Bmdc\r\n Jujube\r\n Ben\r\n Mika\r\n Pignouf\r\n Lebetz\r\n\r\nIl nous manque encore 2 membres pour finalisé les escouades et les strat donc pour ceux qui sont intéressé, veuillé vous rendre sur la partie du site concerné\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n[/COLOR]\r\n[COLOR=#FF0000]En espérant que vous recevrez tous ce message je vous souhaite encore une joyeuses Pâques, une bonne journée et plein de victoire sur le champ de bataille au nom de la meute !!!!!!!!!\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/COLOR]', 0),
(92, 28, 1364799647, 5, 28, '(Aucun objet)', '[COLOR=#FFA500][SIZE=5]Bonjour a tous et joyeuses Pâques tous le monde !!!\r\n\r\nJe vous rappel que pour le mois d''avril nous avons deux Evénements de prévu :[/SIZE]\r\n[/COLOR]\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\Soirée des 1 an de la meute le samedi 13 au soir sur notre serveur //////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n [COLOR=#4169E1]bataille only C4 sur métro ( j''veux que ça pète dans tout les sens ) \r\n Course robot nem sur frontière Caspienne \r\n Photo de Famille sur Tour Ziba[/COLOR]\r\n\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ Match 12 VS 12 contre la team 2099 Dimanche 14 a 15h ///////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n[COLOR=#0000FF] Ruée / Pic de damavand \r\n Conquête / Tempête de feu \r\n\r\nLes membres présent sont:\r\n\r\n Dartoch\r\n Psy-4\r\n Gp\r\n Gyom\r\n Bmdc\r\n Jujube\r\n Ben\r\n Mika\r\n Pignouf\r\n Lebetz\r\n\r\nIl nous manque encore 2 membres pour finalisé les escouades et les strat donc pour ceux qui sont intéressé, veuillé vous rendre sur la partie du site concerné\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n[/COLOR]\r\n[COLOR=#FF0000]En espérant que vous recevrez tous ce message je vous souhaite encore une joyeuses Pâques, une bonne journée et plein de victoire sur le champ de bataille au nom de la meute !!!!!!!!!\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/COLOR]', 1),
(93, 5, 1364799647, 5, 29, '(Aucun objet)', '[COLOR=#FFA500][SIZE=5]Bonjour a tous et joyeuses Pâques tous le monde !!!\r\n\r\nJe vous rappel que pour le mois d''avril nous avons deux Evénements de prévu :[/SIZE]\r\n[/COLOR]\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\Soirée des 1 an de la meute le samedi 13 au soir sur notre serveur //////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n [COLOR=#4169E1]bataille only C4 sur métro ( j''veux que ça pète dans tout les sens ) \r\n Course robot nem sur frontière Caspienne \r\n Photo de Famille sur Tour Ziba[/COLOR]\r\n\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ Match 12 VS 12 contre la team 2099 Dimanche 14 a 15h ///////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n[COLOR=#0000FF] Ruée / Pic de damavand \r\n Conquête / Tempête de feu \r\n\r\nLes membres présent sont:\r\n\r\n Dartoch\r\n Psy-4\r\n Gp\r\n Gyom\r\n Bmdc\r\n Jujube\r\n Ben\r\n Mika\r\n Pignouf\r\n Lebetz\r\n\r\nIl nous manque encore 2 membres pour finalisé les escouades et les strat donc pour ceux qui sont intéressé, veuillé vous rendre sur la partie du site concerné\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n[/COLOR]\r\n[COLOR=#FF0000]En espérant que vous recevrez tous ce message je vous souhaite encore une joyeuses Pâques, une bonne journée et plein de victoire sur le champ de bataille au nom de la meute !!!!!!!!!\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/COLOR]', 0),
(94, 29, 1364799647, 5, 29, '(Aucun objet)', '[COLOR=#FFA500][SIZE=5]Bonjour a tous et joyeuses Pâques tous le monde !!!\r\n\r\nJe vous rappel que pour le mois d''avril nous avons deux Evénements de prévu :[/SIZE]\r\n[/COLOR]\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\Soirée des 1 an de la meute le samedi 13 au soir sur notre serveur //////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n [COLOR=#4169E1]bataille only C4 sur métro ( j''veux que ça pète dans tout les sens ) \r\n Course robot nem sur frontière Caspienne \r\n Photo de Famille sur Tour Ziba[/COLOR]\r\n\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ Match 12 VS 12 contre la team 2099 Dimanche 14 a 15h ///////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n[COLOR=#0000FF] Ruée / Pic de damavand \r\n Conquête / Tempête de feu \r\n\r\nLes membres présent sont:\r\n\r\n Dartoch\r\n Psy-4\r\n Gp\r\n Gyom\r\n Bmdc\r\n Jujube\r\n Ben\r\n Mika\r\n Pignouf\r\n Lebetz\r\n\r\nIl nous manque encore 2 membres pour finalisé les escouades et les strat donc pour ceux qui sont intéressé, veuillé vous rendre sur la partie du site concerné\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n[/COLOR]\r\n[COLOR=#FF0000]En espérant que vous recevrez tous ce message je vous souhaite encore une joyeuses Pâques, une bonne journée et plein de victoire sur le champ de bataille au nom de la meute !!!!!!!!!\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/COLOR]', 0),
(95, 5, 1364799647, 5, 30, '(Aucun objet)', '[COLOR=#FFA500][SIZE=5]Bonjour a tous et joyeuses Pâques tous le monde !!!\r\n\r\nJe vous rappel que pour le mois d''avril nous avons deux Evénements de prévu :[/SIZE]\r\n[/COLOR]\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\Soirée des 1 an de la meute le samedi 13 au soir sur notre serveur //////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n [COLOR=#4169E1]bataille only C4 sur métro ( j''veux que ça pète dans tout les sens ) \r\n Course robot nem sur frontière Caspienne \r\n Photo de Famille sur Tour Ziba[/COLOR]\r\n\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ Match 12 VS 12 contre la team 2099 Dimanche 14 a 15h ///////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n[COLOR=#0000FF] Ruée / Pic de damavand \r\n Conquête / Tempête de feu \r\n\r\nLes membres présent sont:\r\n\r\n Dartoch\r\n Psy-4\r\n Gp\r\n Gyom\r\n Bmdc\r\n Jujube\r\n Ben\r\n Mika\r\n Pignouf\r\n Lebetz\r\n\r\nIl nous manque encore 2 membres pour finalisé les escouades et les strat donc pour ceux qui sont intéressé, veuillé vous rendre sur la partie du site concerné\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n[/COLOR]\r\n[COLOR=#FF0000]En espérant que vous recevrez tous ce message je vous souhaite encore une joyeuses Pâques, une bonne journée et plein de victoire sur le champ de bataille au nom de la meute !!!!!!!!!\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/COLOR]', 0),
(96, 30, 1364799647, 5, 30, '(Aucun objet)', '[COLOR=#FFA500][SIZE=5]Bonjour a tous et joyeuses Pâques tous le monde !!!\r\n\r\nJe vous rappel que pour le mois d''avril nous avons deux Evénements de prévu :[/SIZE]\r\n[/COLOR]\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\Soirée des 1 an de la meute le samedi 13 au soir sur notre serveur //////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n [COLOR=#4169E1]bataille only C4 sur métro ( j''veux que ça pète dans tout les sens ) \r\n Course robot nem sur frontière Caspienne \r\n Photo de Famille sur Tour Ziba[/COLOR]\r\n\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ Match 12 VS 12 contre la team 2099 Dimanche 14 a 15h ///////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n[COLOR=#0000FF] Ruée / Pic de damavand \r\n Conquête / Tempête de feu \r\n\r\nLes membres présent sont:\r\n\r\n Dartoch\r\n Psy-4\r\n Gp\r\n Gyom\r\n Bmdc\r\n Jujube\r\n Ben\r\n Mika\r\n Pignouf\r\n Lebetz\r\n\r\nIl nous manque encore 2 membres pour finalisé les escouades et les strat donc pour ceux qui sont intéressé, veuillé vous rendre sur la partie du site concerné\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n[/COLOR]\r\n[COLOR=#FF0000]En espérant que vous recevrez tous ce message je vous souhaite encore une joyeuses Pâques, une bonne journée et plein de victoire sur le champ de bataille au nom de la meute !!!!!!!!!\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/COLOR]', 1),
(97, 5, 1364799647, 5, 31, '(Aucun objet)', '[COLOR=#FFA500][SIZE=5]Bonjour a tous et joyeuses Pâques tous le monde !!!\r\n\r\nJe vous rappel que pour le mois d''avril nous avons deux Evénements de prévu :[/SIZE]\r\n[/COLOR]\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\Soirée des 1 an de la meute le samedi 13 au soir sur notre serveur //////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n [COLOR=#4169E1]bataille only C4 sur métro ( j''veux que ça pète dans tout les sens ) \r\n Course robot nem sur frontière Caspienne \r\n Photo de Famille sur Tour Ziba[/COLOR]\r\n\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ Match 12 VS 12 contre la team 2099 Dimanche 14 a 15h ///////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n[COLOR=#0000FF] Ruée / Pic de damavand \r\n Conquête / Tempête de feu \r\n\r\nLes membres présent sont:\r\n\r\n Dartoch\r\n Psy-4\r\n Gp\r\n Gyom\r\n Bmdc\r\n Jujube\r\n Ben\r\n Mika\r\n Pignouf\r\n Lebetz\r\n\r\nIl nous manque encore 2 membres pour finalisé les escouades et les strat donc pour ceux qui sont intéressé, veuillé vous rendre sur la partie du site concerné\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n[/COLOR]\r\n[COLOR=#FF0000]En espérant que vous recevrez tous ce message je vous souhaite encore une joyeuses Pâques, une bonne journée et plein de victoire sur le champ de bataille au nom de la meute !!!!!!!!!\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/COLOR]', 0),
(98, 31, 1364799647, 5, 31, '(Aucun objet)', '[COLOR=#FFA500][SIZE=5]Bonjour a tous et joyeuses Pâques tous le monde !!!\r\n\r\nJe vous rappel que pour le mois d''avril nous avons deux Evénements de prévu :[/SIZE]\r\n[/COLOR]\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\Soirée des 1 an de la meute le samedi 13 au soir sur notre serveur //////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n [COLOR=#4169E1]bataille only C4 sur métro ( j''veux que ça pète dans tout les sens ) \r\n Course robot nem sur frontière Caspienne \r\n Photo de Famille sur Tour Ziba[/COLOR]\r\n\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ Match 12 VS 12 contre la team 2099 Dimanche 14 a 15h ///////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n[COLOR=#0000FF] Ruée / Pic de damavand \r\n Conquête / Tempête de feu \r\n\r\nLes membres présent sont:\r\n\r\n Dartoch\r\n Psy-4\r\n Gp\r\n Gyom\r\n Bmdc\r\n Jujube\r\n Ben\r\n Mika\r\n Pignouf\r\n Lebetz\r\n\r\nIl nous manque encore 2 membres pour finalisé les escouades et les strat donc pour ceux qui sont intéressé, veuillé vous rendre sur la partie du site concerné\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n[/COLOR]\r\n[COLOR=#FF0000]En espérant que vous recevrez tous ce message je vous souhaite encore une joyeuses Pâques, une bonne journée et plein de victoire sur le champ de bataille au nom de la meute !!!!!!!!!\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/COLOR]', 0),
(99, 5, 1364799647, 5, 32, '(Aucun objet)', '[COLOR=#FFA500][SIZE=5]Bonjour a tous et joyeuses Pâques tous le monde !!!\r\n\r\nJe vous rappel que pour le mois d''avril nous avons deux Evénements de prévu :[/SIZE]\r\n[/COLOR]\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\Soirée des 1 an de la meute le samedi 13 au soir sur notre serveur //////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n [COLOR=#4169E1]bataille only C4 sur métro ( j''veux que ça pète dans tout les sens ) \r\n Course robot nem sur frontière Caspienne \r\n Photo de Famille sur Tour Ziba[/COLOR]\r\n\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ Match 12 VS 12 contre la team 2099 Dimanche 14 a 15h ///////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n[COLOR=#0000FF] Ruée / Pic de damavand \r\n Conquête / Tempête de feu \r\n\r\nLes membres présent sont:\r\n\r\n Dartoch\r\n Psy-4\r\n Gp\r\n Gyom\r\n Bmdc\r\n Jujube\r\n Ben\r\n Mika\r\n Pignouf\r\n Lebetz\r\n\r\nIl nous manque encore 2 membres pour finalisé les escouades et les strat donc pour ceux qui sont intéressé, veuillé vous rendre sur la partie du site concerné\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n[/COLOR]\r\n[COLOR=#FF0000]En espérant que vous recevrez tous ce message je vous souhaite encore une joyeuses Pâques, une bonne journée et plein de victoire sur le champ de bataille au nom de la meute !!!!!!!!!\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/COLOR]', 0),
(100, 32, 1364799647, 5, 32, '(Aucun objet)', '[COLOR=#FFA500][SIZE=5]Bonjour a tous et joyeuses Pâques tous le monde !!!\r\n\r\nJe vous rappel que pour le mois d''avril nous avons deux Evénements de prévu :[/SIZE]\r\n[/COLOR]\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\Soirée des 1 an de la meute le samedi 13 au soir sur notre serveur //////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n [COLOR=#4169E1]bataille only C4 sur métro ( j''veux que ça pète dans tout les sens ) \r\n Course robot nem sur frontière Caspienne \r\n Photo de Famille sur Tour Ziba[/COLOR]\r\n\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ Match 12 VS 12 contre la team 2099 Dimanche 14 a 15h ///////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n[COLOR=#0000FF] Ruée / Pic de damavand \r\n Conquête / Tempête de feu \r\n\r\nLes membres présent sont:\r\n\r\n Dartoch\r\n Psy-4\r\n Gp\r\n Gyom\r\n Bmdc\r\n Jujube\r\n Ben\r\n Mika\r\n Pignouf\r\n Lebetz\r\n\r\nIl nous manque encore 2 membres pour finalisé les escouades et les strat donc pour ceux qui sont intéressé, veuillé vous rendre sur la partie du site concerné\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n[/COLOR]\r\n[COLOR=#FF0000]En espérant que vous recevrez tous ce message je vous souhaite encore une joyeuses Pâques, une bonne journée et plein de victoire sur le champ de bataille au nom de la meute !!!!!!!!!\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/COLOR]', 0),
(101, 5, 1364799647, 5, 34, '(Aucun objet)', '[COLOR=#FFA500][SIZE=5]Bonjour a tous et joyeuses Pâques tous le monde !!!\r\n\r\nJe vous rappel que pour le mois d''avril nous avons deux Evénements de prévu :[/SIZE]\r\n[/COLOR]\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\Soirée des 1 an de la meute le samedi 13 au soir sur notre serveur //////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n [COLOR=#4169E1]bataille only C4 sur métro ( j''veux que ça pète dans tout les sens ) \r\n Course robot nem sur frontière Caspienne \r\n Photo de Famille sur Tour Ziba[/COLOR]\r\n\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ Match 12 VS 12 contre la team 2099 Dimanche 14 a 15h ///////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n[COLOR=#0000FF] Ruée / Pic de damavand \r\n Conquête / Tempête de feu \r\n\r\nLes membres présent sont:\r\n\r\n Dartoch\r\n Psy-4\r\n Gp\r\n Gyom\r\n Bmdc\r\n Jujube\r\n Ben\r\n Mika\r\n Pignouf\r\n Lebetz\r\n\r\nIl nous manque encore 2 membres pour finalisé les escouades et les strat donc pour ceux qui sont intéressé, veuillé vous rendre sur la partie du site concerné\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n[/COLOR]\r\n[COLOR=#FF0000]En espérant que vous recevrez tous ce message je vous souhaite encore une joyeuses Pâques, une bonne journée et plein de victoire sur le champ de bataille au nom de la meute !!!!!!!!!\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/COLOR]', 0),
(102, 34, 1364799647, 5, 34, '(Aucun objet)', '[COLOR=#FFA500][SIZE=5]Bonjour a tous et joyeuses Pâques tous le monde !!!\r\n\r\nJe vous rappel que pour le mois d''avril nous avons deux Evénements de prévu :[/SIZE]\r\n[/COLOR]\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\Soirée des 1 an de la meute le samedi 13 au soir sur notre serveur //////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n [COLOR=#4169E1]bataille only C4 sur métro ( j''veux que ça pète dans tout les sens ) \r\n Course robot nem sur frontière Caspienne \r\n Photo de Famille sur Tour Ziba[/COLOR]\r\n\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ Match 12 VS 12 contre la team 2099 Dimanche 14 a 15h ///////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n[COLOR=#0000FF] Ruée / Pic de damavand \r\n Conquête / Tempête de feu \r\n\r\nLes membres présent sont:\r\n\r\n Dartoch\r\n Psy-4\r\n Gp\r\n Gyom\r\n Bmdc\r\n Jujube\r\n Ben\r\n Mika\r\n Pignouf\r\n Lebetz\r\n\r\nIl nous manque encore 2 membres pour finalisé les escouades et les strat donc pour ceux qui sont intéressé, veuillé vous rendre sur la partie du site concerné\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n[/COLOR]\r\n[COLOR=#FF0000]En espérant que vous recevrez tous ce message je vous souhaite encore une joyeuses Pâques, une bonne journée et plein de victoire sur le champ de bataille au nom de la meute !!!!!!!!!\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/COLOR]', 1),
(103, 5, 1364799647, 5, 35, '(Aucun objet)', '[COLOR=#FFA500][SIZE=5]Bonjour a tous et joyeuses Pâques tous le monde !!!\r\n\r\nJe vous rappel que pour le mois d''avril nous avons deux Evénements de prévu :[/SIZE]\r\n[/COLOR]\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\Soirée des 1 an de la meute le samedi 13 au soir sur notre serveur //////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n [COLOR=#4169E1]bataille only C4 sur métro ( j''veux que ça pète dans tout les sens ) \r\n Course robot nem sur frontière Caspienne \r\n Photo de Famille sur Tour Ziba[/COLOR]\r\n\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ Match 12 VS 12 contre la team 2099 Dimanche 14 a 15h ///////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n[COLOR=#0000FF] Ruée / Pic de damavand \r\n Conquête / Tempête de feu \r\n\r\nLes membres présent sont:\r\n\r\n Dartoch\r\n Psy-4\r\n Gp\r\n Gyom\r\n Bmdc\r\n Jujube\r\n Ben\r\n Mika\r\n Pignouf\r\n Lebetz\r\n\r\nIl nous manque encore 2 membres pour finalisé les escouades et les strat donc pour ceux qui sont intéressé, veuillé vous rendre sur la partie du site concerné\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n[/COLOR]\r\n[COLOR=#FF0000]En espérant que vous recevrez tous ce message je vous souhaite encore une joyeuses Pâques, une bonne journée et plein de victoire sur le champ de bataille au nom de la meute !!!!!!!!!\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/COLOR]', 0),
(104, 35, 1364799647, 5, 35, '(Aucun objet)', '[COLOR=#FFA500][SIZE=5]Bonjour a tous et joyeuses Pâques tous le monde !!!\r\n\r\nJe vous rappel que pour le mois d''avril nous avons deux Evénements de prévu :[/SIZE]\r\n[/COLOR]\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\Soirée des 1 an de la meute le samedi 13 au soir sur notre serveur //////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n [COLOR=#4169E1]bataille only C4 sur métro ( j''veux que ça pète dans tout les sens ) \r\n Course robot nem sur frontière Caspienne \r\n Photo de Famille sur Tour Ziba[/COLOR]\r\n\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ Match 12 VS 12 contre la team 2099 Dimanche 14 a 15h ///////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n[COLOR=#0000FF] Ruée / Pic de damavand \r\n Conquête / Tempête de feu \r\n\r\nLes membres présent sont:\r\n\r\n Dartoch\r\n Psy-4\r\n Gp\r\n Gyom\r\n Bmdc\r\n Jujube\r\n Ben\r\n Mika\r\n Pignouf\r\n Lebetz\r\n\r\nIl nous manque encore 2 membres pour finalisé les escouades et les strat donc pour ceux qui sont intéressé, veuillé vous rendre sur la partie du site concerné\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n[/COLOR]\r\n[COLOR=#FF0000]En espérant que vous recevrez tous ce message je vous souhaite encore une joyeuses Pâques, une bonne journée et plein de victoire sur le champ de bataille au nom de la meute !!!!!!!!!\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/COLOR]', 1),
(105, 5, 1364799647, 5, 36, '(Aucun objet)', '[COLOR=#FFA500][SIZE=5]Bonjour a tous et joyeuses Pâques tous le monde !!!\r\n\r\nJe vous rappel que pour le mois d''avril nous avons deux Evénements de prévu :[/SIZE]\r\n[/COLOR]\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\Soirée des 1 an de la meute le samedi 13 au soir sur notre serveur //////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n [COLOR=#4169E1]bataille only C4 sur métro ( j''veux que ça pète dans tout les sens ) \r\n Course robot nem sur frontière Caspienne \r\n Photo de Famille sur Tour Ziba[/COLOR]\r\n\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ Match 12 VS 12 contre la team 2099 Dimanche 14 a 15h ///////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n[COLOR=#0000FF] Ruée / Pic de damavand \r\n Conquête / Tempête de feu \r\n\r\nLes membres présent sont:\r\n\r\n Dartoch\r\n Psy-4\r\n Gp\r\n Gyom\r\n Bmdc\r\n Jujube\r\n Ben\r\n Mika\r\n Pignouf\r\n Lebetz\r\n\r\nIl nous manque encore 2 membres pour finalisé les escouades et les strat donc pour ceux qui sont intéressé, veuillé vous rendre sur la partie du site concerné\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n[/COLOR]\r\n[COLOR=#FF0000]En espérant que vous recevrez tous ce message je vous souhaite encore une joyeuses Pâques, une bonne journée et plein de victoire sur le champ de bataille au nom de la meute !!!!!!!!!\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/COLOR]', 0),
(106, 36, 1364799647, 5, 36, '(Aucun objet)', '[COLOR=#FFA500][SIZE=5]Bonjour a tous et joyeuses Pâques tous le monde !!!\r\n\r\nJe vous rappel que pour le mois d''avril nous avons deux Evénements de prévu :[/SIZE]\r\n[/COLOR]\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\Soirée des 1 an de la meute le samedi 13 au soir sur notre serveur //////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n [COLOR=#4169E1]bataille only C4 sur métro ( j''veux que ça pète dans tout les sens ) \r\n Course robot nem sur frontière Caspienne \r\n Photo de Famille sur Tour Ziba[/COLOR]\r\n\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ Match 12 VS 12 contre la team 2099 Dimanche 14 a 15h ///////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n[COLOR=#0000FF] Ruée / Pic de damavand \r\n Conquête / Tempête de feu \r\n\r\nLes membres présent sont:\r\n\r\n Dartoch\r\n Psy-4\r\n Gp\r\n Gyom\r\n Bmdc\r\n Jujube\r\n Ben\r\n Mika\r\n Pignouf\r\n Lebetz\r\n\r\nIl nous manque encore 2 membres pour finalisé les escouades et les strat donc pour ceux qui sont intéressé, veuillé vous rendre sur la partie du site concerné\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n[/COLOR]\r\n[COLOR=#FF0000]En espérant que vous recevrez tous ce message je vous souhaite encore une joyeuses Pâques, une bonne journée et plein de victoire sur le champ de bataille au nom de la meute !!!!!!!!!\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/COLOR]', 0),
(107, 5, 1364799647, 5, 37, '(Aucun objet)', '[COLOR=#FFA500][SIZE=5]Bonjour a tous et joyeuses Pâques tous le monde !!!\r\n\r\nJe vous rappel que pour le mois d''avril nous avons deux Evénements de prévu :[/SIZE]\r\n[/COLOR]\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\Soirée des 1 an de la meute le samedi 13 au soir sur notre serveur //////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n [COLOR=#4169E1]bataille only C4 sur métro ( j''veux que ça pète dans tout les sens ) \r\n Course robot nem sur frontière Caspienne \r\n Photo de Famille sur Tour Ziba[/COLOR]\r\n\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ Match 12 VS 12 contre la team 2099 Dimanche 14 a 15h ///////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n[COLOR=#0000FF] Ruée / Pic de damavand \r\n Conquête / Tempête de feu \r\n\r\nLes membres présent sont:\r\n\r\n Dartoch\r\n Psy-4\r\n Gp\r\n Gyom\r\n Bmdc\r\n Jujube\r\n Ben\r\n Mika\r\n Pignouf\r\n Lebetz\r\n\r\nIl nous manque encore 2 membres pour finalisé les escouades et les strat donc pour ceux qui sont intéressé, veuillé vous rendre sur la partie du site concerné\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n[/COLOR]\r\n[COLOR=#FF0000]En espérant que vous recevrez tous ce message je vous souhaite encore une joyeuses Pâques, une bonne journée et plein de victoire sur le champ de bataille au nom de la meute !!!!!!!!!\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/COLOR]', 0),
(108, 37, 1364799647, 5, 37, '(Aucun objet)', '[COLOR=#FFA500][SIZE=5]Bonjour a tous et joyeuses Pâques tous le monde !!!\r\n\r\nJe vous rappel que pour le mois d''avril nous avons deux Evénements de prévu :[/SIZE]\r\n[/COLOR]\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\Soirée des 1 an de la meute le samedi 13 au soir sur notre serveur //////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n [COLOR=#4169E1]bataille only C4 sur métro ( j''veux que ça pète dans tout les sens ) \r\n Course robot nem sur frontière Caspienne \r\n Photo de Famille sur Tour Ziba[/COLOR]\r\n\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ Match 12 VS 12 contre la team 2099 Dimanche 14 a 15h ///////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n[COLOR=#0000FF] Ruée / Pic de damavand \r\n Conquête / Tempête de feu \r\n\r\nLes membres présent sont:\r\n\r\n Dartoch\r\n Psy-4\r\n Gp\r\n Gyom\r\n Bmdc\r\n Jujube\r\n Ben\r\n Mika\r\n Pignouf\r\n Lebetz\r\n\r\nIl nous manque encore 2 membres pour finalisé les escouades et les strat donc pour ceux qui sont intéressé, veuillé vous rendre sur la partie du site concerné\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n[/COLOR]\r\n[COLOR=#FF0000]En espérant que vous recevrez tous ce message je vous souhaite encore une joyeuses Pâques, une bonne journée et plein de victoire sur le champ de bataille au nom de la meute !!!!!!!!!\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/COLOR]', 1),
(109, 5, 1364896963, 5, 34, 'match 12vs12', 'Salut grand, je vien de voir ton mess pour le match\r\nPour ma part ya pas de soucis ;) par contre poste ta demande sur la partie du forum réserver a la meute (c''est une partie masqué pour ceux qui ne font pas partie des apax) je te met le lien si dessous ;)\r\n\r\nhttp://meute-apax.fr/site/index.php?site=forum_topic&topic=15&page=4#.UVqstifyqZI\r\n\r\n', 0),
(110, 34, 1364896963, 5, 34, 'match 12vs12', 'Salut grand, je vien de voir ton mess pour le match\r\nPour ma part ya pas de soucis ;) par contre poste ta demande sur la partie du forum réserver a la meute (c''est une partie masqué pour ceux qui ne font pas partie des apax) je te met le lien si dessous ;)\r\n\r\nhttp://meute-apax.fr/site/index.php?site=forum_topic&topic=15&page=4#.UVqstifyqZI\r\n\r\n', 1),
(111, 1, 1364914696, 1, 1, 'Bisou', 'Ceux qui souhaitent avoir un accès en "temps réels" de ses statistiques, demandez le moi [URL=http://www.meute-apax.fr/site/index.php?site=messenger&action=touser&touser=1]par message privé[/URL] en pensant à bien ajouter votre adresse mail :)Ceux qui souhaitent avoir un accès en "temps réels" de ses statistiques, demandez le moi [URL=http://www.meute-apax.fr/site/index.php?site=messenger&action=touser&touser=1]par message privé[/URL] en pensant à bien ajouter votre adresse mail :)Ceux qui souhaitent avoir un accès en "temps réels" de ses statistiques, demandez le moi [URL=http://www.meute-apax.fr/site/index.php?site=messenger&action=touser&touser=1]par message privé[/URL] en pensant à bien ajouter votre adresse mail :)Ceux qui souhaitent avoir un accès en "temps réels" de ses statistiques, demandez le moi [URL=http://www.meute-apax.fr/site/index.php?site=messenger&action=touser&touser=1]par message privé[/URL] en pensant à bien ajouter votre adresse mail :)Ceux qui souhaitent avoir un accès en "temps réels" de ses statistiques, demandez le moi [URL=http://www.meute-apax.fr/site/index.php?site=messenger&action=touser&touser=1]par message privé[/URL] en pensant à bien ajouter votre adresse mail :)Ceux qui souhaitent avoir un accès en "temps réels" de ses statistiques, demandez le moi [URL=http://www.meute-apax.fr/site/index.php?site=messenger&action=touser&touser=1]par message privé[/URL] en pensant à bien ajouter votre adresse mail :)Ceux qui souhaitent avoir un accès en "temps réels" de ses statistiques, demandez le moi [URL=http://www.meute-apax.fr/site/index.php?site=messenger&action=touser&touser=1]par message privé[/URL] en pensant à bien ajouter votre adresse mail :)', 1),
(112, 1, 1364928488, 1, 5, 'Tweezee ...', 'Entre nous, toche, j''ai du mal, mais genre BEAUCOUP de mal avec tweezee... Ce mec me sors par les yeux, me harcèle sur PSN et me gave IG (je suis de black ops, mais j''ai les touches de tatatatatatat TA GUEULE !!!) tu vois le genre ?\r\n\r\nEnfin bref. Je veux bien faire des efforts, mais je risque de le mute pendant le T12, ce qui est certes immature, mais j''ai pas envie de le tailler 4H durant ;)\r\n\r\nJe te dis ça uniquement à titre informatif, je suis pas non plus du genre à lui gâcher la vie, mais que si ça part en cacahuète, hésite pas à me kick le temps que je me calme ;)\r\n\r\n[SIZE=5][COLOR=#87CEEB]B[/COLOR][COLOR=#FFA500]I[/COLOR][COLOR=#FF4500]Z[/COLOR][COLOR=#FF0000]O[/COLOR][COLOR=#32CD32]U[/COLOR][COLOR=#800080]S[/COLOR][/SIZE]', 1),
(113, 5, 1364928488, 1, 5, 'Tweezee ...', 'Entre nous, toche, j''ai du mal, mais genre BEAUCOUP de mal avec tweezee... Ce mec me sors par les yeux, me harcèle sur PSN et me gave IG (je suis de black ops, mais j''ai les touches de tatatatatatat TA GUEULE !!!) tu vois le genre ?\r\n\r\nEnfin bref. Je veux bien faire des efforts, mais je risque de le mute pendant le T12, ce qui est certes immature, mais j''ai pas envie de le tailler 4H durant ;)\r\n\r\nJe te dis ça uniquement à titre informatif, je suis pas non plus du genre à lui gâcher la vie, mais que si ça part en cacahuète, hésite pas à me kick le temps que je me calme ;)\r\n\r\n[SIZE=5][COLOR=#87CEEB]B[/COLOR][COLOR=#FFA500]I[/COLOR][COLOR=#FF4500]Z[/COLOR][COLOR=#FF0000]O[/COLOR][COLOR=#32CD32]U[/COLOR][COLOR=#800080]S[/COLOR][/SIZE]', 1),
(114, 5, 1364989502, 5, 22, 'match 12vs12', 'Salut misteur, commen va?\r\nJe vien de voir ton post et en effet il nous manque 1 joueur ^^\r\nje te laisse marqué un poste sur le lien si desous pour dire que tu sera présent pour que je puisse validé ta présence ;)\r\n\r\nhttp://meute-apax.fr/site/index.php?site=forum_topic&topic=15&page=5#.UVwWMifyqZI', 0),
(115, 22, 1364989502, 5, 22, 'match 12vs12', 'Salut misteur, commen va?\r\nJe vien de voir ton post et en effet il nous manque 1 joueur ^^\r\nje te laisse marqué un poste sur le lien si desous pour dire que tu sera présent pour que je puisse validé ta présence ;)\r\n\r\nhttp://meute-apax.fr/site/index.php?site=forum_topic&topic=15&page=5#.UVwWMifyqZI', 1),
(116, 5, 1365013989, 5, 1, 'Re[1]: Tweezee ...', 'Salut benco, j''ai vu aussi qu''il est un bou fougueu ^^ mais bon il est jeune ;)\r\nDe toute facon pour le match il n''y aura pas le droit de parlé a tout vas, sinon a 12... on va plus s''entendre :p\r\n\r\nPar contre, j''ai essayé de metre le match contre les 2099 sur la page principale du site (tu sait avec le logo) mais impossible d''y arivé.... tu pourrai pas le faire quand tu aurra 5min?\r\nje te fournirai toute les info qu''il te faut se tu est ok ;)\r\n\r\nMerci d''avance ma caille, bisous bisous ;)', 0),
(117, 1, 1365013989, 5, 1, 'Re[1]: Tweezee ...', 'Salut benco, j''ai vu aussi qu''il est un bou fougueu ^^ mais bon il est jeune ;)\r\nDe toute facon pour le match il n''y aura pas le droit de parlé a tout vas, sinon a 12... on va plus s''entendre :p\r\n\r\nPar contre, j''ai essayé de metre le match contre les 2099 sur la page principale du site (tu sait avec le logo) mais impossible d''y arivé.... tu pourrai pas le faire quand tu aurra 5min?\r\nje te fournirai toute les info qu''il te faut se tu est ok ;)\r\n\r\nMerci d''avance ma caille, bisous bisous ;)', 1);
INSERT INTO `messenger` (`messageID`, `userID`, `date`, `fromuser`, `touser`, `title`, `message`, `viewed`) VALUES
(118, 35, 1365267170, 35, 5, 'Re[1]: (Aucun objet)', '[QUOTE=Dartoch][COLOR=#FFA500][SIZE=5]Bonjour a tous et joyeuses Pâques tous le monde !!!\r\n\r\nJe vous rappel que pour le mois d''avril nous avons deux Evénements de prévu :[/SIZE]\r\n[/COLOR]\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\Soirée des 1 an de la meute le samedi 13 au soir sur notre serveur //////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n [COLOR=#4169E1]bataille only C4 sur métro ( j''veux que ça pète dans tout les sens ) \r\n Course robot nem sur frontière Caspienne \r\n Photo de Famille sur Tour Ziba[/COLOR]\r\n\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ Match 12 VS 12 contre la team 2099 Dimanche 14 a 15h ///////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n[COLOR=#0000FF] Ruée / Pic de damavand \r\n Conquête / Tempête de feu \r\n\r\nLes membres présent sont:\r\n\r\n Dartoch\r\n Psy-4\r\n Gp\r\n Gyom\r\n Bmdc\r\n Jujube\r\n Ben\r\n Mika\r\n Pignouf\r\n Lebetz\r\n\r\nIl nous manque encore 2 membres pour finalisé les escouades et les strat donc pour ceux qui sont intéressé, veuillé vous rendre sur la partie du site concerné\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n[/COLOR]\r\n[COLOR=#FF0000]En espérant que vous recevrez tous ce message je vous souhaite encore une joyeuses Pâques, une bonne journée et plein de victoire sur le champ de bataille au nom de la meute !!!!!!!!!\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/COLOR][/QUOTE]\r\n\r\n\r\n\r\n\r\n\r\n\r\nsaloute! compte moi présent pour le 12 vs 12 ;-)', 0),
(119, 5, 1365267170, 35, 5, 'Re[1]: (Aucun objet)', '[QUOTE=Dartoch][COLOR=#FFA500][SIZE=5]Bonjour a tous et joyeuses Pâques tous le monde !!!\r\n\r\nJe vous rappel que pour le mois d''avril nous avons deux Evénements de prévu :[/SIZE]\r\n[/COLOR]\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\Soirée des 1 an de la meute le samedi 13 au soir sur notre serveur //////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n [COLOR=#4169E1]bataille only C4 sur métro ( j''veux que ça pète dans tout les sens ) \r\n Course robot nem sur frontière Caspienne \r\n Photo de Famille sur Tour Ziba[/COLOR]\r\n\r\n[COLOR=#FF0000][SIZE=4]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ Match 12 VS 12 contre la team 2099 Dimanche 14 a 15h ///////////////////\r\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/SIZE]\r\n[/COLOR]\r\n[COLOR=#0000FF] Ruée / Pic de damavand \r\n Conquête / Tempête de feu \r\n\r\nLes membres présent sont:\r\n\r\n Dartoch\r\n Psy-4\r\n Gp\r\n Gyom\r\n Bmdc\r\n Jujube\r\n Ben\r\n Mika\r\n Pignouf\r\n Lebetz\r\n\r\nIl nous manque encore 2 membres pour finalisé les escouades et les strat donc pour ceux qui sont intéressé, veuillé vous rendre sur la partie du site concerné\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////\r\n[/COLOR]\r\n[COLOR=#FF0000]En espérant que vous recevrez tous ce message je vous souhaite encore une joyeuses Pâques, une bonne journée et plein de victoire sur le champ de bataille au nom de la meute !!!!!!!!!\r\n[/COLOR]\r\n[COLOR=#FF0000]\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////////////[/COLOR][/QUOTE]\r\n\r\n\r\n\r\n\r\n\r\n\r\nsaloute! compte moi présent pour le 12 vs 12 ;-)', 1),
(120, 5, 1365317897, 5, 35, 'Re[2]: (Aucun objet)', '[QUOTE=Ankoleloup]\r\nsaloute! compte moi présent pour le 12 vs 12 ;-)[/QUOTE]\r\n\r\nSalut Anko, commen va?\r\nJ''ai bien pris en note que tu serait présent ;)\r\nPar contre tu est sur d’être là? car vu que juju fais son enterrement de vie de garçon la veille, je l''aurait bien mis en remplacent et toi en titulaire, comme sa il pourrai profité de sa soirée et nous on sera sur qu''il y est tout le monde, tu en pense quoi?\r\n\r\nBonne journée ma caille et peut être a tout a l''heure ;)', 0),
(121, 35, 1365317897, 5, 35, 'Re[2]: (Aucun objet)', '[QUOTE=Ankoleloup]\r\nsaloute! compte moi présent pour le 12 vs 12 ;-)[/QUOTE]\r\n\r\nSalut Anko, commen va?\r\nJ''ai bien pris en note que tu serait présent ;)\r\nPar contre tu est sur d’être là? car vu que juju fais son enterrement de vie de garçon la veille, je l''aurait bien mis en remplacent et toi en titulaire, comme sa il pourrai profité de sa soirée et nous on sera sur qu''il y est tout le monde, tu en pense quoi?\r\n\r\nBonne journée ma caille et peut être a tout a l''heure ;)', 0),
(122, 34, 1365447743, 34, 5, '', 'tien si tu veux me parler :) 06 25 45 88 21 ou mon fcb perso Alexis Londat (TweeZy) ou skype frelonyeti :)', 0),
(123, 5, 1365447743, 34, 5, '', 'tien si tu veux me parler :) 06 25 45 88 21 ou mon fcb perso Alexis Londat (TweeZy) ou skype frelonyeti :)', 1),
(124, 27, 1365504661, 27, 5, 'Re[1]: (Aucun objet)', '[ALIGN=center][B]Dartoch j''ai le prototipe de l''intro il sera ameliorér et il y a avec Background logo\r\n\r\nVoilà le heyken sera aussi enlever http://www.mediafire.com/?c8eox4cscvqs6j2 dit moi ce que tu en pense ^^\r\n[/B][/ALIGN]', 1),
(125, 5, 1365504661, 27, 5, 'Re[1]: (Aucun objet)', '[ALIGN=center][B]Dartoch j''ai le prototipe de l''intro il sera ameliorér et il y a avec Background logo\r\n\r\nVoilà le heyken sera aussi enlever http://www.mediafire.com/?c8eox4cscvqs6j2 dit moi ce que tu en pense ^^\r\n[/B][/ALIGN]', 1),
(126, 5, 1365505128, 5, 27, 'Re[2]: (Aucun objet)', '[QUOTE=NoSkill4Ever][ALIGN=center][B]Dartoch j''ai le prototipe de l''intro il sera ameliorér et il y a avec Background logo\r\n\r\nVoilà le heyken sera aussi enlever http://www.mediafire.com/?c8eox4cscvqs6j2 dit moi ce que tu en pense ^^\r\n[/B][/ALIGN][/QUOTE]\r\n\r\nC''est toi qui la fait? si c''est le cas tu peut faire quoi comme modif dessus? par exemple, a la place du carré metre le logo de la meute ^^ \r\n', 0),
(127, 27, 1365505128, 5, 27, 'Re[2]: (Aucun objet)', '[QUOTE=NoSkill4Ever][ALIGN=center][B]Dartoch j''ai le prototipe de l''intro il sera ameliorér et il y a avec Background logo\r\n\r\nVoilà le heyken sera aussi enlever http://www.mediafire.com/?c8eox4cscvqs6j2 dit moi ce que tu en pense ^^\r\n[/B][/ALIGN][/QUOTE]\r\n\r\nC''est toi qui la fait? si c''est le cas tu peut faire quoi comme modif dessus? par exemple, a la place du carré metre le logo de la meute ^^ \r\n', 1),
(128, 27, 1365505629, 27, 5, 'Re[3]: (Aucun objet)', '[QUOTE=Dartoch][QUOTE=NoSkill4Ever][ALIGN=center][B]Dartoch j''ai le prototipe de l''intro il sera ameliorér et il y a avec Background logo\r\n\r\nVoilà le heyken sera aussi enlever http://www.mediafire.com/?c8eox4cscvqs6j2 dit moi ce que tu en pense ^^\r\n[/B][/ALIGN][/QUOTE]\r\n\r\nC''est toi qui la fait? si c''est le cas tu peut faire quoi comme modif dessus? par exemple, a la place du carré metre le logo de la meute ^^ \r\n[/QUOTE]\r\nC''est pas moi mais un amis envoie le Logo je lui demande  ', 1),
(129, 5, 1365505629, 27, 5, 'Re[3]: (Aucun objet)', '[QUOTE=Dartoch][QUOTE=NoSkill4Ever][ALIGN=center][B]Dartoch j''ai le prototipe de l''intro il sera ameliorér et il y a avec Background logo\r\n\r\nVoilà le heyken sera aussi enlever http://www.mediafire.com/?c8eox4cscvqs6j2 dit moi ce que tu en pense ^^\r\n[/B][/ALIGN][/QUOTE]\r\n\r\nC''est toi qui la fait? si c''est le cas tu peut faire quoi comme modif dessus? par exemple, a la place du carré metre le logo de la meute ^^ \r\n[/QUOTE]\r\nC''est pas moi mais un amis envoie le Logo je lui demande  ', 1),
(130, 27, 1365529338, 27, 5, 'Re[3]: (Aucun objet)', 'Envoie le Logo je voie et c''est un pote qui le fait ^^\r\n', 0),
(131, 5, 1365529338, 27, 5, 'Re[3]: (Aucun objet)', 'Envoie le Logo je voie et c''est un pote qui le fait ^^\r\n', 1),
(132, 5, 1365530140, 5, 27, 'Re[4]: (Aucun objet)', '[QUOTE=NoSkill4Ever]Envoie le Logo je voie et c''est un pote qui le fait ^^\r\n[/QUOTE]\r\n\r\n[URL=http://www.casimages.com/img.php?i=1301141103466025510759849.png][img]http://nsm08.casimages.com/img/2013/01/14//1301141103466025510759849.png[/img][/URL]', 1),
(133, 27, 1365530140, 5, 27, 'Re[4]: (Aucun objet)', '[QUOTE=NoSkill4Ever]Envoie le Logo je voie et c''est un pote qui le fait ^^\r\n[/QUOTE]\r\n\r\n[URL=http://www.casimages.com/img.php?i=1301141103466025510759849.png][img]http://nsm08.casimages.com/img/2013/01/14//1301141103466025510759849.png[/img][/URL]', 1),
(134, 27, 1365623162, 27, 5, 'Re[5]: (Aucun objet)', 'http://www.mediafire.com/?3qvlhixx5kkvctv Rezgarde un peut sa ^^\r\n', 1),
(135, 5, 1365623162, 27, 5, 'Re[5]: (Aucun objet)', 'http://www.mediafire.com/?3qvlhixx5kkvctv Rezgarde un peut sa ^^\r\n', 1),
(136, 27, 1365623370, 27, 5, 'Intro', 'http://www.mediafire.com/?3qvlhixx5kkvctv\r\nTu en pense ?N', 1),
(137, 5, 1365623370, 27, 5, 'Intro', 'http://www.mediafire.com/?3qvlhixx5kkvctv\r\nTu en pense ?N', 1),
(138, 5, 1365667346, 5, 27, 'Re[1]: Intro', '[QUOTE=NoSkill4Ever]http://www.mediafire.com/?3qvlhixx5kkvctv\r\nTu en pense ?N[/QUOTE]\r\n\r\nPas mal, poste la sur le forum pour voir se qu''en pense les autre ;)', 0),
(139, 27, 1365667346, 5, 27, 'Re[1]: Intro', '[QUOTE=NoSkill4Ever]http://www.mediafire.com/?3qvlhixx5kkvctv\r\nTu en pense ?N[/QUOTE]\r\n\r\nPas mal, poste la sur le forum pour voir se qu''en pense les autre ;)', 1),
(140, 5, 1365868021, 5, 34, '', 'tu peut utilisé toute les arme que tu veut pour le match mon grand (sauf pompe) par contre regarde bien les classe que tu aura, car tu ne poura avoir que les classe marqué sur le site ;)', 0),
(141, 34, 1365868021, 5, 34, '', 'tu peut utilisé toute les arme que tu veut pour le match mon grand (sauf pompe) par contre regarde bien les classe que tu aura, car tu ne poura avoir que les classe marqué sur le site ;)', 1);

-- --------------------------------------------------------

--
-- Structure de la table `movie_categories`
--

CREATE TABLE IF NOT EXISTS `movie_categories` (
  `movcatID` int(11) NOT NULL AUTO_INCREMENT,
  `movcatname` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`movcatID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `movie_categories`
--

INSERT INTO `movie_categories` (`movcatID`, `movcatname`) VALUES
(1, 'Fragmovies');

-- --------------------------------------------------------

--
-- Structure de la table `movies`
--

CREATE TABLE IF NOT EXISTS `movies` (
  `movID` int(11) NOT NULL AUTO_INCREMENT,
  `movheadline` varchar(255) DEFAULT NULL,
  `movscreenshot` varchar(255) DEFAULT NULL,
  `movdescription` varchar(255) DEFAULT NULL,
  `movfile` varchar(255) DEFAULT NULL,
  `date` int(11) DEFAULT NULL,
  `hits` int(11) DEFAULT '0',
  `movcatID` int(11) DEFAULT NULL,
  `embed` text,
  `uploader` int(11) DEFAULT NULL,
  `activated` int(11) DEFAULT '0',
  `votes` int(11) DEFAULT '0',
  `points` int(11) DEFAULT '0',
  `comments` int(11) DEFAULT '2',
  `rating` int(11) DEFAULT '0',
  PRIMARY KEY (`movID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `movies`
--

INSERT INTO `movies` (`movID`, `movheadline`, `movscreenshot`, `movdescription`, `movfile`, `date`, `hits`, `movcatID`, `embed`, `uploader`, `activated`, `votes`, `points`, `comments`, `rating`) VALUES
(1, 'Triumphant Return | A Battlefield 3 Montage by Patwwa', NULL, 'As the title says I''m back. Hope you guys enjoyed my montage. There will be more! Like and share; I worked hard on this one.', 'http://www.youtube.com/watch?v=FOvKYAe5n2k', 1361455301, 0, 1, '<iframe width="560" height="315" src="http://www.youtube.com/embed/FOvKYAe5n2k" frameborder="0" allowfullscreen></iframe>', 1, 2, 0, 0, 2, 0);

-- --------------------------------------------------------

--
-- Structure de la table `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `newsID` int(11) NOT NULL AUTO_INCREMENT,
  `date` int(14) NOT NULL DEFAULT '0',
  `rubric` int(11) NOT NULL DEFAULT '0',
  `screens` text NOT NULL,
  `poster` int(11) NOT NULL DEFAULT '0',
  `link1` varchar(255) NOT NULL DEFAULT '',
  `url1` varchar(255) NOT NULL DEFAULT '',
  `window1` int(11) NOT NULL DEFAULT '0',
  `link2` varchar(255) NOT NULL DEFAULT '',
  `url2` varchar(255) NOT NULL DEFAULT '',
  `window2` int(11) NOT NULL DEFAULT '0',
  `link3` varchar(255) NOT NULL DEFAULT '',
  `url3` varchar(255) NOT NULL DEFAULT '',
  `window3` int(11) NOT NULL DEFAULT '0',
  `link4` varchar(255) NOT NULL DEFAULT '',
  `url4` varchar(255) NOT NULL DEFAULT '',
  `window4` int(11) NOT NULL DEFAULT '0',
  `saved` int(1) NOT NULL DEFAULT '1',
  `published` int(11) NOT NULL DEFAULT '0',
  `comments` int(1) NOT NULL DEFAULT '0',
  `cwID` int(11) NOT NULL DEFAULT '0',
  `intern` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`newsID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Contenu de la table `news`
--

INSERT INTO `news` (`newsID`, `date`, `rubric`, `screens`, `poster`, `link1`, `url1`, `window1`, `link2`, `url2`, `window2`, `link3`, `url3`, `window3`, `link4`, `url4`, `window4`, `saved`, `published`, `comments`, `cwID`, `intern`) VALUES
(1, 1361455802, 1, '', 1, 'La vidéo', 'http://bit.ly/YdqJGE', 1, '', 'http://', 1, '', 'http://', 1, '', 'http://', 1, 1, 1, 2, 0, 0),
(2, 1361456369, 1, '', 1, 'KSS', 'http://www.google.fr/', 1, 'Tournois RdP', 'http://', 1, '', 'http://', 1, '', 'http://', 1, 1, 1, 2, 1, 0),
(4, 1362044879, 1, '|4_1362045002.jpg', 1, '', 'http://', 1, '', 'http://', 1, '', 'http://', 1, '', 'http://', 1, 1, 1, 2, 0, 0),
(6, 1363547247, 1, '|6_1363547484.jpg', 5, 'bf-france.com', 'http://www.bf-france.com/actualites/battlefield-4-un-teaser-video-cette-semaine-9254.html', 1, '', 'http://', 1, '', 'http://', 1, '', 'http://', 1, 1, 1, 1, 0, 0),
(8, 1366998256, 1, '', 5, '', 'http://', 1, '', 'http://', 1, '', 'http://', 1, '', 'http://', 1, 1, 1, 2, 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `news_contents`
--

CREATE TABLE IF NOT EXISTS `news_contents` (
  `newsID` int(11) NOT NULL,
  `language` varchar(2) NOT NULL,
  `headline` varchar(255) NOT NULL,
  `content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `news_contents`
--

INSERT INTO `news_contents` (`newsID`, `language`, `headline`, `content`) VALUES
(1, 'fr', 'Ma première news !', 'Découvrez une vidéo sublime réalisée par mashed8 ! Qu''en pensez-vous ?\nVoir la vidéo : http://bit.ly/YdqJGE'),
(2, 'fr', 'War APAX Squad 1 vs. Killer Special Squad won', 'Match contre [flag]fr[/flag] [url=http://www.google.fr/][b]KSS / Killer Special Squad[/b][/url] On 21.02.2013\n		\nLigue: Tournois RdP\nRésultat: [color=#00CC00][b]4:0[/b][/color]\n[TOGGLE=Results (extended)]<table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td width="30%">bazar</td><td>[color=#00CC00][b]2[/b][/color] : [color=#DD0000][b]0[/b][/color]</td></tr></table><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td width="30%">seine</td><td>[color=#00CC00][b]2[/b][/color] : [color=#DD0000][b]0[/b][/color]</td></tr></table>[/TOGGLE]\nAPAX Team: \nKSS Team: \n\n[TOGGLE=Report]Victoire par forfait au 2ème round[/TOGGLE]\n<a href="index.php?site=clanwars_details&amp;cwID=1">Détails du match</a>'),
(4, 'fr', 'Organisation des futurs matchs', 'Tous nos futurs matchs a l''avenir auront un leader obligatoire : \n\n- WAZA-PsY-4-x \n-  gyom-88 \n- Pignouf30 \n- Yuri \n- waza-gp_one \n\nAucun match ne pourra se faire sans l''un des 5 joueurs .. Évidement il sera responsable de l''entraînement ( tactique , équipe .. ) en amont .\n\nPetit aparté , je ne veux pas que vous me prouviez votre talent de gamer, mais plus votre dévouement a la meute , aujourd''hui je n''est pas besoin d''excellent joueurs , j''ai juste besoin de joueurs de confiance ;) \n\nGood Night mes loups\n\n[ALIGN=center][img]images/news-pics/4_1362045002.jpg[/img][/ALIGN] '),
(6, 'fr', 'Battlefield 4 : un teaser vidéo cette semaine ?', 'Selon une rumeur rapportée par le site Battlefield 4 central, EA devrait révéler un teaser vidéo de Battlefield 4 cette semaine. Ce dernier serait réservé aux membres du service Battlefield Premium et accessible via le Battlelog. Toujours d’après la rumeur, les joueurs de Battlefield 3 qui regarderont cette vidéo obtiendront une plaque dog tag spéciale à utiliser dans le jeu sur laquelle est écrit "I was there" ("J’y étais"). Comme toujours avec les rumeurs, l’information est à prendre au conditionnel en attendant une éventuelle confirmation de la part de l’éditeur ou une mise en ligne de la dite vidéo. Une bonne nouvelle tout de même pour ceux qui attendaient des nouveautés sur ce prochain volet.\n\n\n[img]images/news-pics/6_1363547484.jpg[/img] \n\nCela étant dit, il convient de noter qu’en parallèle à l’événement de San Francisco du 26 mars prochain, EA tiendra le même jour un événement similaire à Stockholm en Suède (la même invitation a été envoyée aux médias européens). Le FPS très attendu devrait donc faire parler de lui de manière officielle très prochainement !'),
(8, 'fr', 'Calendrier des "ApaxEvent"', 'Suite au succès qu''a eu la première ApaxEvent, les prochaine sont donc lancé !!!!!\nJe vous donne donc la liste des dates des prochaines ainsi que les thèmes \n\n01/05 : "Only SV-98"\n08/05 : "Spécial Plaque rare"\n15/05 : "Only Pompe & C4"\n22/05 : "Spécial VIP"\n29/05 : "Only Smaw/Rpg"\n\nLa liste des maps ainsi que les règles seront indiqué au fur et a mesure donc venez régulièrement vous renseignez sur notre site meute-apax.fr (section News)\n\nLa liste pour les mois suivant seront donné par la suite, avec des thèmes proposé par les membres\n\nBonne journée a tous et bon jeux !!!\n\nApax_Dartoch');

-- --------------------------------------------------------

--
-- Structure de la table `news_languages`
--

CREATE TABLE IF NOT EXISTS `news_languages` (
  `langID` int(11) NOT NULL AUTO_INCREMENT,
  `language` varchar(255) NOT NULL DEFAULT '',
  `lang` char(2) NOT NULL DEFAULT '',
  `alt` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`langID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

--
-- Contenu de la table `news_languages`
--

INSERT INTO `news_languages` (`langID`, `language`, `lang`, `alt`) VALUES
(1, 'danish', 'dk', 'danish'),
(2, 'dutch', 'nl', 'dutch'),
(3, 'english', 'uk', 'english'),
(4, 'finnish', 'fi', 'finnish'),
(5, 'french', 'fr', 'french'),
(6, 'german', 'de', 'german'),
(7, 'hungarian', 'hu', 'hungarian'),
(8, 'italian', 'it', 'italian'),
(9, 'norwegian', 'no', 'norwegian'),
(10, 'spanish', 'es', 'spanish'),
(11, 'swedish', 'se', 'swedish'),
(12, 'czech', 'cz', 'czech'),
(13, 'croatian', 'hr', 'croatian'),
(14, 'lithuanian', 'lt', 'lithuanian'),
(15, 'polish', 'pl', 'polish'),
(16, 'portugese', 'pt', 'portugese'),
(17, 'slovak', 'sk', 'slovak');

-- --------------------------------------------------------

--
-- Structure de la table `news_rubrics`
--

CREATE TABLE IF NOT EXISTS `news_rubrics` (
  `rubricID` int(11) NOT NULL AUTO_INCREMENT,
  `rubric` varchar(255) NOT NULL DEFAULT '',
  `pic` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`rubricID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `news_rubrics`
--

INSERT INTO `news_rubrics` (`rubricID`, `rubric`, `pic`) VALUES
(1, 'News APAX', '1.png');

-- --------------------------------------------------------

--
-- Structure de la table `newsletter`
--

CREATE TABLE IF NOT EXISTS `newsletter` (
  `email` varchar(255) NOT NULL DEFAULT '',
  `pass` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `newsletter`
--


-- --------------------------------------------------------

--
-- Structure de la table `partners`
--

CREATE TABLE IF NOT EXISTS `partners` (
  `partnerID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `url` varchar(255) NOT NULL DEFAULT '',
  `banner` varchar(255) NOT NULL DEFAULT '',
  `date` int(14) NOT NULL DEFAULT '0',
  `sort` int(11) NOT NULL DEFAULT '0',
  `displayed` varchar(255) NOT NULL DEFAULT '1',
  `hits` int(11) DEFAULT '0',
  PRIMARY KEY (`partnerID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 PACK_KEYS=0 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `partners`
--

INSERT INTO `partners` (`partnerID`, `name`, `url`, `banner`, `date`, `sort`, `displayed`, `hits`) VALUES
(1, 'webSPELL 4', 'http://www.webspell.org', '1.gif', 1361454924, 1, '1', 42),
(2, 'Battlefield.com', 'http://battlefield.com/fr/battlefield-4', '2.png', 1364025336, 1, '1', 40);

-- --------------------------------------------------------

--
-- Structure de la table `poll`
--

CREATE TABLE IF NOT EXISTS `poll` (
  `pollID` int(10) NOT NULL AUTO_INCREMENT,
  `aktiv` int(1) NOT NULL DEFAULT '0',
  `laufzeit` bigint(20) NOT NULL DEFAULT '0',
  `titel` varchar(255) NOT NULL DEFAULT '',
  `o1` varchar(255) NOT NULL DEFAULT '',
  `o2` varchar(255) NOT NULL DEFAULT '',
  `o3` varchar(255) NOT NULL DEFAULT '',
  `o4` varchar(255) NOT NULL DEFAULT '',
  `o5` varchar(255) NOT NULL DEFAULT '',
  `o6` varchar(255) NOT NULL DEFAULT '',
  `o7` varchar(255) NOT NULL DEFAULT '',
  `o8` varchar(255) NOT NULL DEFAULT '',
  `o9` varchar(255) NOT NULL DEFAULT '',
  `o10` varchar(255) NOT NULL DEFAULT '',
  `comments` int(1) NOT NULL DEFAULT '0',
  `hosts` text NOT NULL,
  `intern` int(1) NOT NULL DEFAULT '0',
  `userIDs` text NOT NULL,
  PRIMARY KEY (`pollID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `poll`
--

INSERT INTO `poll` (`pollID`, `aktiv`, `laufzeit`, `titel`, `o1`, `o2`, `o3`, `o4`, `o5`, `o6`, `o7`, `o8`, `o9`, `o10`, `comments`, `hosts`, `intern`, `userIDs`) VALUES
(1, 1, 1364767200, 'On organiserais pas un p''tit tournosi pour féter les 1 ans de la meute ?', 'Oui, pourquoi pas.', 'Non, la fleme', 'Boarf, m''en fous', '', '', '', '', '', '', '', 2, '#109.23.236.23##78.193.218.193##88.120.181.37##87.231.217.178##86.69.79.50#', 0, '5;1;24;18;34'),
(2, 0, 1366970400, 'Quel thème voudriez vous avoir pour les prochaines journée a thème de la meute', 'only SV-98', 'Only 44 Magnum', 'Only défibr', 'Santé max', 'VIP', 'Only smaw/Rpg', 'Autres (indic en commentaire)', '', '', '', 1, '#109.23.236.23##92.137.231.136##92.146.90.184##88.167.3.244#', 0, '5;71;11;1');

-- --------------------------------------------------------

--
-- Structure de la table `poll_votes`
--

CREATE TABLE IF NOT EXISTS `poll_votes` (
  `pollID` int(10) NOT NULL DEFAULT '0',
  `o1` int(11) NOT NULL DEFAULT '0',
  `o2` int(11) NOT NULL DEFAULT '0',
  `o3` int(11) NOT NULL DEFAULT '0',
  `o4` int(11) NOT NULL DEFAULT '0',
  `o5` int(11) NOT NULL DEFAULT '0',
  `o6` int(11) NOT NULL DEFAULT '0',
  `o7` int(11) NOT NULL DEFAULT '0',
  `o8` int(11) NOT NULL DEFAULT '0',
  `o9` int(11) NOT NULL DEFAULT '0',
  `o10` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`pollID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `poll_votes`
--

INSERT INTO `poll_votes` (`pollID`, `o1`, `o2`, `o3`, `o4`, `o5`, `o6`, `o7`, `o8`, `o9`, `o10`) VALUES
(1, 2, 1, 2, 0, 0, 0, 0, 0, 0, 0),
(2, 1, 0, 1, 0, 1, 1, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `scrolltext`
--

CREATE TABLE IF NOT EXISTS `scrolltext` (
  `text` longtext NOT NULL,
  `delay` int(11) NOT NULL DEFAULT '100',
  `direction` varchar(255) NOT NULL DEFAULT '',
  `color` varchar(7) NOT NULL DEFAULT '#000000'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `scrolltext`
--

INSERT INTO `scrolltext` (`text`, `delay`, `direction`, `color`) VALUES
('Salut & bienvenue dans la team APAX sur PS3 , une meute rempli d''hommes , de femmes , d''anciens & de jeunes , délirant et sérieux quand les balles sifflent , alors rejoins nous vite ..', 2, 'left', '');

-- --------------------------------------------------------

--
-- Structure de la table `servers`
--

CREATE TABLE IF NOT EXISTS `servers` (
  `serverID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `ip` varchar(255) NOT NULL DEFAULT '',
  `game` char(3) NOT NULL DEFAULT '',
  `info` text NOT NULL,
  `sort` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`serverID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `servers`
--


-- --------------------------------------------------------

--
-- Structure de la table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `settingID` int(11) NOT NULL AUTO_INCREMENT,
  `hpurl` varchar(255) NOT NULL DEFAULT '',
  `clanname` varchar(255) NOT NULL DEFAULT '',
  `clantag` varchar(255) NOT NULL DEFAULT '',
  `adminname` varchar(255) NOT NULL DEFAULT '',
  `adminemail` varchar(255) NOT NULL DEFAULT '',
  `news` int(11) NOT NULL DEFAULT '0',
  `newsarchiv` int(11) NOT NULL DEFAULT '0',
  `headlines` int(11) NOT NULL DEFAULT '0',
  `headlineschars` int(11) NOT NULL DEFAULT '0',
  `topnewschars` int(11) NOT NULL DEFAULT '0',
  `articles` int(11) NOT NULL DEFAULT '0',
  `latestarticles` int(11) NOT NULL DEFAULT '0',
  `articleschars` int(11) NOT NULL DEFAULT '0',
  `clanwars` int(11) NOT NULL DEFAULT '0',
  `results` int(11) NOT NULL DEFAULT '0',
  `upcoming` int(11) NOT NULL DEFAULT '0',
  `shoutbox` int(11) NOT NULL DEFAULT '0',
  `sball` int(11) NOT NULL DEFAULT '0',
  `sbrefresh` int(11) NOT NULL DEFAULT '0',
  `topics` int(11) NOT NULL DEFAULT '0',
  `posts` int(11) NOT NULL DEFAULT '0',
  `latesttopics` int(11) NOT NULL DEFAULT '0',
  `latesttopicchars` int(11) NOT NULL DEFAULT '0',
  `awards` int(11) NOT NULL DEFAULT '0',
  `demos` int(11) NOT NULL DEFAULT '0',
  `guestbook` int(11) NOT NULL DEFAULT '0',
  `feedback` int(11) NOT NULL DEFAULT '0',
  `messages` int(11) NOT NULL DEFAULT '0',
  `users` int(11) NOT NULL DEFAULT '0',
  `profilelast` int(11) NOT NULL DEFAULT '0',
  `topnewsID` int(11) NOT NULL DEFAULT '0',
  `sessionduration` int(3) NOT NULL DEFAULT '0',
  `closed` int(1) NOT NULL DEFAULT '0',
  `gb_info` int(1) NOT NULL DEFAULT '1',
  `imprint` int(1) NOT NULL DEFAULT '0',
  `picsize_l` int(11) NOT NULL DEFAULT '450',
  `picsize_h` int(11) NOT NULL DEFAULT '500',
  `pictures` int(11) NOT NULL DEFAULT '12',
  `publicadmin` int(1) NOT NULL DEFAULT '1',
  `thumbwidth` int(11) NOT NULL DEFAULT '130',
  `usergalleries` int(1) NOT NULL DEFAULT '1',
  `maxusergalleries` int(11) NOT NULL DEFAULT '1048576',
  `default_language` varchar(2) NOT NULL DEFAULT 'fr',
  `insertlinks` int(1) NOT NULL DEFAULT '1',
  `search_min_len` int(3) NOT NULL DEFAULT '3',
  `max_wrong_pw` int(2) NOT NULL DEFAULT '10',
  `captcha_math` int(1) NOT NULL DEFAULT '2',
  `captcha_bgcol` varchar(7) NOT NULL DEFAULT '#FFFFFF',
  `captcha_fontcol` varchar(7) NOT NULL DEFAULT '#000000',
  `captcha_type` int(1) NOT NULL DEFAULT '2',
  `captcha_noise` int(3) NOT NULL DEFAULT '100',
  `captcha_linenoise` int(2) NOT NULL DEFAULT '10',
  `autoresize` int(1) NOT NULL DEFAULT '2',
  `bancheck` int(13) NOT NULL,
  PRIMARY KEY (`settingID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `settings`
--

INSERT INTO `settings` (`settingID`, `hpurl`, `clanname`, `clantag`, `adminname`, `adminemail`, `news`, `newsarchiv`, `headlines`, `headlineschars`, `topnewschars`, `articles`, `latestarticles`, `articleschars`, `clanwars`, `results`, `upcoming`, `shoutbox`, `sball`, `sbrefresh`, `topics`, `posts`, `latesttopics`, `latesttopicchars`, `awards`, `demos`, `guestbook`, `feedback`, `messages`, `users`, `profilelast`, `topnewsID`, `sessionduration`, `closed`, `gb_info`, `imprint`, `picsize_l`, `picsize_h`, `pictures`, `publicadmin`, `thumbwidth`, `usergalleries`, `maxusergalleries`, `default_language`, `insertlinks`, `search_min_len`, `max_wrong_pw`, `captcha_math`, `captcha_bgcol`, `captcha_fontcol`, `captcha_type`, `captcha_noise`, `captcha_linenoise`, `autoresize`, `bancheck`) VALUES
(1, 'www.meute-apax.fr/', 'ApaX', 'APAX', 'Benftwc', 'benftwc@gmail.com', 10, 20, 10, 22, 200, 20, 5, 20, 20, 5, 5, 5, 30, 60, 20, 10, 10, 18, 20, 20, 20, 20, 20, 60, 10, 6, 0, 0, 1, 0, 450, 500, 12, 1, 130, 1, 8388608, 'fr', 1, 3, 10, 2, '#FFFFFF', '#000000', 2, 100, 10, 2, 1368992890);

-- --------------------------------------------------------

--
-- Structure de la table `shoutbox`
--

CREATE TABLE IF NOT EXISTS `shoutbox` (
  `shoutID` int(11) NOT NULL AUTO_INCREMENT,
  `date` int(14) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `message` varchar(255) NOT NULL DEFAULT '',
  `ip` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`shoutID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `shoutbox`
--

INSERT INTO `shoutbox` (`shoutID`, `date`, `name`, `message`, `ip`) VALUES
(1, 1361784333, 'benftwc', 'Je test le chat =)', '78.193.218.193'),
(2, 1364132401, 'ben_ftwc', 'héhé', '88.167.3.244'),
(3, 1364156657, 'WAZA-PsY-4-x', 'Yep', '90.84.144.226');

-- --------------------------------------------------------

--
-- Structure de la table `smileys`
--

CREATE TABLE IF NOT EXISTS `smileys` (
  `smileyID` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `alt` varchar(255) NOT NULL DEFAULT '',
  `pattern` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`smileyID`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Contenu de la table `smileys`
--

INSERT INTO `smileys` (`smileyID`, `name`, `alt`, `pattern`) VALUES
(1, 'biggrin.gif', 'amused', ':D'),
(2, 'confused.gif', 'confused', '?('),
(3, 'crying.gif', 'sad', ';('),
(4, 'pleased.gif', 'pleased', ':]'),
(5, 'happy.gif', 'happy', '=)'),
(6, 'smile.gif', 'smiling', ':)'),
(7, 'wink.gif', 'wink', ';)'),
(8, 'frown.gif', 'unhappy', ':('),
(9, 'tongue.gif', 'tongue', ':p'),
(10, 'tongue2.gif', 'funny', ';p'),
(11, 'redface.gif', 'tired', ':O'),
(12, 'cool.gif', 'cool', '8)'),
(13, 'eek.gif', 'shocked', '8o'),
(14, 'evil.gif', 'devilish', ':evil:'),
(15, 'mad.gif', 'angry', 'X('),
(16, 'crazy.gif', 'crazy', '^^');

-- --------------------------------------------------------

--
-- Structure de la table `sponsors`
--

CREATE TABLE IF NOT EXISTS `sponsors` (
  `sponsorID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `url` varchar(255) NOT NULL DEFAULT '',
  `info` text NOT NULL,
  `banner` varchar(255) NOT NULL DEFAULT '',
  `sort` int(11) NOT NULL DEFAULT '1',
  `banner_small` varchar(255) NOT NULL DEFAULT '',
  `displayed` varchar(255) NOT NULL DEFAULT '1',
  `mainsponsor` varchar(255) NOT NULL DEFAULT '0',
  `hits` int(11) DEFAULT '0',
  `date` int(14) NOT NULL DEFAULT '0',
  PRIMARY KEY (`sponsorID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `sponsors`
--


-- --------------------------------------------------------

--
-- Structure de la table `squads`
--

CREATE TABLE IF NOT EXISTS `squads` (
  `squadID` int(11) NOT NULL AUTO_INCREMENT,
  `gamesquad` int(11) NOT NULL DEFAULT '1',
  `games` text NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `icon` varchar(255) NOT NULL DEFAULT '',
  `icon_small` varchar(255) NOT NULL DEFAULT '',
  `info` text NOT NULL,
  `sort` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`squadID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `squads`
--

INSERT INTO `squads` (`squadID`, `gamesquad`, `games`, `name`, `icon`, `icon_small`, `info`, `sort`) VALUES
(1, 1, 'Battlefield 3', 'MEUTE APAX ', '1.jpg', '1_small.jpg', '[flag]fr[/flag]', 1),
(2, 1, 'Battlefield 3', 'Ancien Apax', '2.jpg', '2_small.jpg', '', 1),
(3, 1, 'Battlefield 3', 'Meute Apax (T12) ', '', '', 'La meute pour le T12', 1),
(4, 1, 'Battlefield 3', 'Staff Apax', '', '', '', 1);

-- --------------------------------------------------------

--
-- Structure de la table `squads_members`
--

CREATE TABLE IF NOT EXISTS `squads_members` (
  `sqmID` int(11) NOT NULL AUTO_INCREMENT,
  `squadID` int(11) NOT NULL DEFAULT '0',
  `userID` int(11) NOT NULL DEFAULT '0',
  `position` varchar(255) NOT NULL DEFAULT '',
  `activity` int(1) NOT NULL DEFAULT '0',
  `sort` int(11) NOT NULL DEFAULT '0',
  `joinmember` int(1) NOT NULL DEFAULT '0',
  `warmember` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`sqmID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=85 ;

--
-- Contenu de la table `squads_members`
--

INSERT INTO `squads_members` (`sqmID`, `squadID`, `userID`, `position`, `activity`, `sort`, `joinmember`, `warmember`) VALUES
(1, 1, 1, 'Ingénieur', 1, 1, 1, 1),
(2, 1, 3, 'Assault', 1, 1, 0, 0),
(3, 1, 4, 'Assault', 1, 1, 1, 1),
(4, 1, 2, 'Soutient', 1, 1, 1, 1),
(5, 1, 5, 'Eclaireur', 1, 1, 0, 0),
(6, 1, 10, '', 1, 1, 0, 0),
(14, 1, 18, '', 1, 1, 0, 0),
(15, 1, 11, '', 1, 1, 0, 0),
(16, 1, 8, '', 1, 1, 0, 0),
(17, 1, 15, '', 1, 1, 0, 0),
(18, 1, 17, '', 1, 1, 0, 0),
(19, 1, 9, '', 1, 1, 0, 0),
(20, 1, 12, '', 1, 1, 0, 0),
(21, 1, 24, '', 1, 1, 0, 0),
(22, 1, 14, '', 1, 1, 0, 0),
(23, 1, 22, '', 1, 1, 0, 0),
(24, 1, 6, '', 1, 1, 0, 0),
(27, 1, 27, '', 1, 1, 0, 0),
(28, 1, 28, '', 1, 1, 0, 0),
(30, 1, 29, '', 1, 1, 0, 0),
(31, 1, 16, '', 1, 1, 0, 0),
(32, 1, 31, '', 1, 1, 0, 0),
(33, 1, 36, '', 1, 1, 0, 0),
(34, 1, 35, '', 1, 1, 0, 0),
(35, 2, 19, '', 1, 1, 0, 0),
(36, 2, 32, '', 1, 1, 0, 0),
(37, 1, 34, '', 1, 1, 0, 0),
(38, 1, 37, '', 1, 1, 0, 0),
(39, 1, 30, '', 1, 1, 0, 0),
(40, 1, 46, '', 1, 1, 0, 0),
(41, 3, 5, '', 1, 1, 0, 0),
(42, 3, 10, '', 1, 1, 0, 0),
(43, 3, 2, '', 1, 1, 0, 0),
(44, 3, 8, '', 1, 1, 0, 0),
(45, 3, 28, '', 1, 1, 0, 0),
(46, 3, 3, '', 1, 1, 0, 0),
(47, 3, 1, '', 1, 1, 0, 0),
(48, 3, 16, '', 1, 1, 0, 0),
(49, 3, 4, '', 1, 1, 0, 0),
(50, 3, 6, '', 1, 1, 0, 0),
(51, 3, 34, '', 1, 1, 0, 0),
(52, 3, 22, '', 1, 1, 0, 0),
(53, 3, 11, '', 1, 1, 0, 0),
(54, 3, 27, '', 1, 1, 0, 0),
(55, 3, 30, '', 1, 1, 0, 0),
(56, 2, 26, '', 1, 1, 0, 0),
(58, 1, 21, '', 1, 1, 0, 0),
(59, 1, 43, '', 1, 1, 0, 0),
(61, 1, 89, '', 1, 1, 0, 0),
(62, 1, 86, '', 1, 1, 0, 0),
(63, 1, 88, '', 1, 1, 0, 0),
(64, 1, 87, '', 1, 1, 0, 0),
(67, 2, 95, '', 1, 1, 0, 0),
(68, 3, 18, '', 1, 1, 0, 0),
(69, 3, 9, '', 1, 1, 0, 0),
(70, 3, 87, '', 1, 1, 0, 0),
(71, 3, 89, '', 1, 1, 0, 0),
(72, 3, 14, '', 1, 1, 0, 0),
(73, 3, 33, '', 1, 1, 0, 0),
(74, 1, 75, '', 1, 1, 0, 0),
(75, 4, 5, 'Co-Leadeur', 1, 1, 0, 0),
(76, 4, 35, '', 1, 1, 0, 0),
(77, 4, 8, '', 1, 1, 0, 0),
(78, 4, 28, '', 1, 1, 0, 0),
(79, 4, 4, '', 1, 1, 0, 0),
(80, 4, 89, '', 1, 1, 0, 0),
(81, 4, 2, '', 1, 1, 0, 0),
(82, 4, 10, '', 1, 1, 0, 0),
(83, 1, 100, '', 1, 1, 0, 0),
(84, 2, 25, '', 1, 1, 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `static`
--

CREATE TABLE IF NOT EXISTS `static` (
  `staticID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `accesslevel` int(1) NOT NULL,
  `content` text NOT NULL,
  PRIMARY KEY (`staticID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `static`
--

INSERT INTO `static` (`staticID`, `name`, `accesslevel`, `content`) VALUES
(1, 'Musique', 0, '<iframe width="100%" height="450" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=http%3A%2F%2Fapi.soundcloud.com%2Fplaylists%2F4005287"></iframe>'),
(2, 'Tournois #1 : Torneo MV CRSQ', 0, '[URL=http://battlelog.battlefield.com/bf3/fr/platoon/2832655391981919090/]Lien du tournois[/URL]\r\n\r\n<table  cellspacing="5" cellpadding="5">\r\n  <tr>\r\n    <td>#</td>\r\n    <td>Numéro</td>\r\n    <td>Équipe</td>\r\n    <td>Joueurs</td>\r\n    <td>Points</td>\r\n  </tr>\r\n  <tr>\r\n    <td>0.</td>\r\n    <td>1.</td>\r\n    <td>Milites Victoriae ™</td>\r\n    <td>SaTaLaBaM - Lukasr98 - Matteo1613 - HeLicoPter__LOL - xSuperZ97 - priMal__Luna</td>\r\n    <td>0.</td>\r\n  </tr>\r\n  <tr>\r\n    <td>0.</td>\r\n    <td>2.</td>\r\n    <td>™ BuL</td>\r\n    <td>bul_legend - bul_grieco - bul_giovi - bul_dog - bul_apex - bul_fenigma</td>\r\n    <td>0.</td>\r\n  </tr>\r\n  <tr>\r\n    <td>0.</td>\r\n    <td>3.</td>\r\n    <td>Blood of Glory</td>\r\n    <td>ATABOT--KSA - waldhail - AR7L-X - A-SNIPR-SA - Zz_misho0o_zZ - Hmoody07KSA - ksa_JoKeR_07_ - meernach</td>\r\n    <td>0.</td>\r\n  </tr>\r\n  <tr>\r\n    <td>0.</td>\r\n    <td>4.</td>\r\n    <td>Blood Seekers</td>\r\n    <td>lavezzi8690-peppesammy-Naplesmania-priMal_Kratos</td>\r\n    <td>0.</td>\r\n  </tr>\r\n  <tr>\r\n    <td>0.</td>\r\n    <td>5.</td>\r\n    <td>ApaX</td>\r\n    <td></td>\r\n    <td>0.</td>\r\n  </tr>\r\n</table>'),
(3, 'Tournois APAX', 0, '[URL=http://www.meute-apax.fr/index.php?site=static&staticID=2]Tournois #1 : Torneo MV CRSQ[/URL]');

-- --------------------------------------------------------

--
-- Structure de la table `styles`
--

CREATE TABLE IF NOT EXISTS `styles` (
  `styleID` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `bgpage` varchar(255) NOT NULL DEFAULT '',
  `border` varchar(255) NOT NULL DEFAULT '',
  `bghead` varchar(255) NOT NULL DEFAULT '',
  `bgcat` varchar(255) NOT NULL DEFAULT '',
  `bg1` varchar(255) NOT NULL DEFAULT '',
  `bg2` varchar(255) NOT NULL DEFAULT '',
  `bg3` varchar(255) NOT NULL DEFAULT '',
  `bg4` varchar(255) NOT NULL DEFAULT '',
  `win` varchar(255) NOT NULL DEFAULT '',
  `loose` varchar(255) NOT NULL DEFAULT '',
  `draw` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`styleID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `styles`
--

INSERT INTO `styles` (`styleID`, `title`, `bgpage`, `border`, `bghead`, `bgcat`, `bg1`, `bg2`, `bg3`, `bg4`, `win`, `loose`, `draw`) VALUES
(1, 'APAX', '#232323', '#161616', '#232323', '#161616', '#161616', '#202020', '#161616', '#202020', '#00CC00', '#DD0000', '#FF6600');

-- --------------------------------------------------------

--
-- Structure de la table `tchat`
--

CREATE TABLE IF NOT EXISTS `tchat` (
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `pseudo` text NOT NULL,
  `message` text NOT NULL,
  `ip` varchar(32) NOT NULL,
  PRIMARY KEY (`time`,`pseudo`(5))
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `tchat`
--

INSERT INTO `tchat` (`time`, `pseudo`, `message`, `ip`) VALUES
('2013-03-24 21:35:48', 'Ben', 'Voila <img src="smilies/icon_e_wink.gif" alt="icon_e_wink.gif"/>', '88.167.3.244'),
('2013-03-24 21:35:56', 'Ben', 'Bon, sur ce, je t''attends betzouille !', '88.167.3.244'),
('2013-03-24 21:40:53', 'Ben', 'T''es pas encore tout prop'' betzouille ?', '88.167.3.244'),
('2013-03-24 21:49:09', 'Apax_LeLe', 'je me co', '77.193.29.229'),
('2013-03-24 22:10:40', 'PSYKATRE', 'Allez j''me m''arrache moi , sport fini o dodo <img src="smilies/icon_e_wink.gif" alt="icon_e_wink.gif"/>', '90.84.144.39'),
('2013-03-25 09:20:19', 'Ben', 'Salut la meute !^', '78.193.218.193'),
('2013-03-25 09:55:56', 'Dartoch', 'Salut !!!!!!!!!!', '109.23.236.23'),
('2013-03-25 11:25:31', 'Ben', 'A partir de quand le serveur passe en priv&eacute; ?', '78.193.218.193'),
('2013-03-25 11:28:40', 'Ben', 'Par contre, faudrait reprendre les images de manu et en faire des news sur le site, si quelqu''un se sent motiv&eacute;, je c&egrave;de ma place (trop de taf aujd pour m''occuper de &ccedil;a :/)', '78.193.218.193'),
('2013-03-25 13:50:46', 'TweeZy', 'coucou la meute <img src="smilies/icon_e_smile.gif" alt="icon_e_smile.gif"/>', '86.215.184.170'),
('2013-03-25 14:34:28', 'Ben', 'Coucou TweeZy <img src="smilies/icon_e_wink.gif" alt="icon_e_wink.gif"/>', '78.193.218.193'),
('2013-03-25 14:34:30', 'Ben', 'Whassup ?', '78.193.218.193'),
('2013-03-25 20:38:09', 'TweeZy', 'Qui joue se soir ? <img src="smilies/icon_e_smile.gif" alt="icon_e_smile.gif"/>', '86.215.184.170'),
('2013-03-25 20:41:31', 'TweeZy', 'Il y a qui de co ? <img src="smilies/icon_e_smile.gif" alt="icon_e_smile.gif"/>', '86.215.184.170'),
('2013-03-25 21:02:02', 'PsYkATre du T-90', 'Yep', '90.84.144.10'),
('2013-03-25 21:02:29', 'PsYkATre du T-90', 'Pas trop de Monde connect&eacute; soir ce ^^', '90.84.144.10'),
('2013-03-25 21:11:11', 'TweeZy', 'Se soir je suis sur le serveur kiss a tous <img src="smilies/icon_e_biggrin.gif" alt="icon_e_biggrin.gif"/>', '86.215.184.170'),
('2013-03-26 10:13:11', 'Ben', 'Ah que koukou <img src="smilies/icon_e_biggrin.gif" alt="icon_e_biggrin.gif"/>', '78.193.218.193'),
('2013-03-26 10:19:33', 'Ben', 'Manu, Ben et Sniper, c''est un peu comme p&acirc;t&eacute; et ketchup, ou comme twix gauche et twix droit tu vois ? ^^', '78.193.218.193'),
('2013-03-26 12:50:16', 'PSYKATRE de la rime ^^', 'Loool c r&eacute;par&eacute; ', '90.84.144.220'),
('2013-03-26 14:02:16', 'Ben', 'Merci <img src="smilies/icon_razz.gif" alt="icon_razz.gif"/>', '78.193.218.193'),
('2013-03-26 18:58:00', 'TweeZy', 'bonsoir tous le monde <img src="smilies/icon_e_biggrin.gif" alt="icon_e_biggrin.gif"/>', '86.215.184.170'),
('2013-03-26 19:33:35', 'Dartoch ', 'Salut tweezy, comment va?', '109.23.236.23'),
('2013-03-26 20:04:08', 'TweeZy', 'tranquille et toi ? <img src="smilies/icon_e_smile.gif" alt="icon_e_smile.gif"/>', '86.215.184.170'),
('2013-03-26 21:03:53', 'TweeZy', 'se soir je suis sur bf a ce qui veulent ajouter moi ou laisser un message a TweeZy_D-Ace', '86.215.184.170'),
('2013-03-27 10:49:29', 'Ben', 'Lebetz, j''t''avais dis que j''allais win mon 1v1, j''ai mis 2-0 mec ! <img src="smilies/icon_e_wink.gif" alt="icon_e_wink.gif"/>', '78.193.218.193'),
('2013-03-27 10:53:37', 'Ben', 'http://youtu.be/ZD9fWUUeL00', '78.193.218.193'),
('2013-03-27 13:02:02', 'Dartoch', 'Allez go le champ de bataille dans 30 min, qui m''aime me suive!!!! ^^', '109.23.236.23'),
('2013-03-27 14:29:06', 'TweeZy', 'coucou la meute', '88.186.76.188'),
('2013-03-27 16:02:24', 'Ben', 'Coucou TweeZy =)', '78.193.218.193'),
('2013-03-27 16:21:42', 'ApaX_LeLe ', 'ta femme aucune experience dans bf compare a toi <img src="smilies/icon_razz.gif" alt="icon_razz.gif"/> ', '79.80.155.83'),
('2013-03-27 16:45:03', 'Ben', 'Qui parle de BF ? <img src="smilies/icon_e_smile.gif" alt="icon_e_smile.gif"/>', '78.193.218.193'),
('2013-03-27 19:10:44', 'TweeZy', 'Re tous le monde <img src="smilies/icon_e_biggrin.gif" alt="icon_e_biggrin.gif"/>', '86.215.184.170'),
('2013-03-28 13:25:14', 'Ben', 'Yep la meute <img src="smilies/icon_razz.gif" alt="icon_razz.gif"/>', '78.193.218.193'),
('2013-03-28 15:37:22', 'TweeZy', 'Yop tous la meute <img src="smilies/icon_e_biggrin.gif" alt="icon_e_biggrin.gif"/>', '90.7.25.103'),
('2013-03-29 09:26:36', 'PsY 4 DE TA mere', 'bon a ce soir les lououps <img src="smilies/icon_e_smile.gif" alt="icon_e_smile.gif"/> si je peux sinon mardi je reviens !! tchao', '90.20.243.65'),
('2013-03-29 09:56:23', 'Dartoch', 'Bon courage ma caille et envoi une photo du r&eacute;sultat <img src="smilies/icon_e_wink.gif" alt="icon_e_wink.gif"/>', '109.23.236.23'),
('2013-03-29 15:11:48', 'Ben', 'J''ai presque de quoi vous pondre 1 mois de stats !', '78.193.218.193'),
('2013-03-29 15:11:51', 'Ben', 'Quelle chance hein ? <img src="smilies/icon_e_wink.gif" alt="icon_e_wink.gif"/>', '78.193.218.193'),
('2013-03-29 15:40:49', 'Le Loup Blanc de La APAX', 'yo la meute <img src="smilies/icon_razz.gif" alt="icon_razz.gif"/>', '90.7.25.103'),
('2013-03-30 14:37:04', 'LaPsYkOsE', 'Coucou', '87.231.217.178'),
('2013-03-30 16:09:25', 'toutoun80740', 'Salut la meute!', '31.35.254.101'),
('2013-04-01 13:18:17', 'sami favel', 'salut tous le monde', '92.133.237.251'),
('2013-04-01 13:44:41', 'dartoch', 'salut salut!!!!!! vous avez recu mon mess?', '109.23.236.23'),
('2013-04-01 23:20:36', 'Pignouf30', 'oui comme quoi fallait se pr&eacute;parer pour les 1 an et le match', '77.192.87.248'),
('2013-04-02 00:41:37', 'toutoun80740', 'Oui message bien re&ccedil;u!', '31.35.254.101'),
('2013-04-02 12:03:48', 'dartoch', 'Tweezy, tu a un message <img src="smilies/icon_e_wink.gif" alt="icon_e_wink.gif"/>regarde au dessus de la barre de menu', '109.23.236.23'),
('2013-04-02 12:13:00', 'TweeZy (le loup blanc)', 'Yo(play) la meute <img src="smilies/icon_e_biggrin.gif" alt="icon_e_biggrin.gif"/>', '83.204.23.132'),
('2013-04-03 00:10:11', 'Aulyro', 'Ouuuuuuuuuuuuuah les mec le choque que j''ai eu quand j''ai vue ce chat en plein milieu d&eacute;ja que je gal&eacute;re avec ce site a me retrouver la vous m''avez encore pomm&eacute; ^^', '92.149.213.242'),
('2013-04-03 12:37:02', 'Ben', 'C''est pas moi qui ai demand&eacute; le chat hein, je ne suis que les mains de Manu <img src="smilies/icon_e_biggrin.gif" alt="icon_e_biggrin.gif"/>', '78.193.218.193'),
('2013-04-03 12:37:13', 'Ben', 'Enfin, pour ce qui touche le site, je pr&eacute;cise <img src="smilies/icon_e_biggrin.gif" alt="icon_e_biggrin.gif"/>', '78.193.218.193'),
('2013-04-03 13:02:59', 'Mister_reveur', 'Salut la meute comment &ccedil;a va ?', '178.193.215.103'),
('2013-04-03 13:06:24', 'NoSkill4Ever', 'Salut Salut ', '78.233.163.100'),
('2013-04-03 14:11:07', 'Ben', 'Salut !', '78.193.218.193'),
('2013-04-03 14:15:43', 'Ben', '&lt;a href=&quot;http://www.meute-apax.fr/site/index.php?site=squads&amp;action=show&amp;squadID=3&quot;&gt;La &quot;Big meute&quot; pour le T12 &lt;/a&gt;', '78.193.218.193'),
('2013-04-03 18:00:37', 'Aulyro', 'Salut a tous', '92.149.213.242'),
('2013-04-05 20:30:34', 'Atonium', 'y''a du monde ? <img src="smilies/icon_e_smile.gif" alt="icon_e_smile.gif"/>', '188.224.6.94'),
('2013-04-05 20:30:53', 'Atonium', 'Aussinon vnez ce soir sur bf3 ', '188.224.6.94'),
('2013-04-05 20:52:38', 'TweeZy', 'Yo(play) se soir moi je serais sur BF3 <img src="smilies/icon_e_biggrin.gif" alt="icon_e_biggrin.gif"/> ', '83.204.23.132'),
('2013-04-06 10:05:45', 'dartoch', 'Allez j''ai encore vir&eacute; 6 bot de piratage se matin, comme quoi le site est bien prot&eacute;g&eacute; <img src="smilies/icon_e_wink.gif" alt="icon_e_wink.gif"/>', '109.23.236.23'),
('2013-04-06 23:26:12', 'Ryukio', 'Yo les filles pour Dimanche pro je suis pris ???', '89.2.158.250'),
('2013-04-07 08:59:33', 'Dartoch', 'Salut Ryu, pour le match on est au complet mais je peut te metre en remplacent <img src="smilies/icon_e_wink.gif" alt="icon_e_wink.gif"/>', '109.23.236.23'),
('2013-04-07 09:01:14', 'Dartoch', 'part contre je voit que l''on arive a regroup&eacute;  15 membres pour un match, sa fait plaisir <img src="smilies/icon_e_wink.gif" alt="icon_e_wink.gif"/> on va pouvoir reprendre les match si il y a autant de monde de motiv&eacute; <img src="smilies/icon_e_wink.gif" alt="icon_e_wink.gif"/>', '109.23.236.23'),
('2013-04-07 13:47:42', 'Ben', 'Le match est quand exactement ?!?', '88.167.3.244'),
('2013-04-07 14:10:46', 'dartoch', ' dimanche 14/04 a 15h donc a mon avis faudra se donn&eacute; rdv a 14h30 max pour etre sur que toout le monde est la et pr&eacute;venir les remplacent si ya besoin', '109.23.236.23'),
('2013-04-08 11:36:53', 'Ryukio', 'Ouais fout moi en rempla&ccedil;ant ! <img src="smilies/icon_e_biggrin.gif" alt="icon_e_biggrin.gif"/>', '89.2.158.250'),
('2013-04-08 23:38:43', 'toutoun80740', 'Salut la Meute! Pour Samedi (les 1 an de la meute) c''est vers quel heure??? car je suis dispo jusque 20h Samedi', '31.35.254.101'),
('2013-04-09 11:20:24', 'dartoch', 'aucune id&eacute;e toutoun, mais manu devrait nous donn&eacute; une r&eacute;ponse rapidement <img src="smilies/icon_e_wink.gif" alt="icon_e_wink.gif"/>', '109.23.236.23'),
('2013-04-09 16:15:09', 'Aulyro', 'Salut a tous comment allez vous??? Alors voila une team de Snip la Mr gaming ma propos&eacute; de rentr&eacute; dedans mais sans quitt&eacute; la Team celle ci reunit plusieur joueur d''une multitude de Team afin de reunir les meilleur snip et faire des match 100 leur regle ne pa affich&eacute; les tag de meute (Apax) pendant les match) mais aussitot que un match et fait  je remet le tag Aapax doije accepter ou pas leur proposition ???', '90.11.167.232'),
('2013-04-09 20:54:19', 'Dartoch', 'Seul les leadeur peuvent rep a cette question donc voi cela directement avec eux, pour ma part je n''est qu''une team et c''est la Meute !!!', '109.23.236.23'),
('2013-04-09 21:36:03', 'Aulyro', 'Moi aussi sur Bf3 j''ai la Apax sur COD j''ai ma Team (la Aofd) mais la pour moi je consid&eacute;re cette organisation pas comme une team vue que c''est une multitude de joueurs de diff&eacute;rentes Team qui sont bon au snip (d''ou mon 1800 et des poussi&eacute;re ^^) et au d&eacute;but je ne savait pas qui c''&eacute;tait mes le mec a qui je lui est call&eacute; la balle entre les deux yeux s''av&eacute;re etre le chef de cette organisation Ptdr)', '90.11.167.232'),
('2013-04-09 21:37:09', 'Aulyro', 'D''ailleur la multitude de joueur pr&eacute;sent dans cette organistion sont surtout des joueur de comp&eacute;tition anglais --&quot; ', '90.11.167.232'),
('2013-04-09 22:11:22', 'Apax_LeLe ', 'Il regroupe les meilleurs sniper ou les plus nuls?', '79.80.173.102'),
('2013-04-09 22:11:58', 'Apax_LeLe ', 'Si faut juste mettre une balle dans un mec , autant mettre toute la communaute de BF3 ', '79.80.173.102'),
('2013-04-09 22:15:57', 'ApaX_LeLe ', 'Sinon TouToun s''est vers 20h ^^', '79.80.173.102'),
('2013-04-09 22:16:28', 'Aulyro', 'Alors toi mon pauvres je te jure tu va voir pr&eacute;pare ton snip', '90.11.167.232'),
('2013-04-09 22:21:12', 'ApaX_LeLe ', 'Ton meilleur sniper n''a que 690kills et c''est le L96 et pour le headshot a 1800m c''es pas complique .', '79.80.173.102'),
('2013-04-09 22:21:29', 'ApaX_LeLe ', 'Suffit juste de faire du snip v snip sur une carte d''armored kill', '79.80.173.102'),
('2013-04-09 23:22:36', 'Aulyro', 'Lol c''est pas sur armored kill que je l''ai fait c''es sur op&eacute;ration temp&eacute;te de feu du spone U.S.A a Russe en via un hedshoot du pilote de l''helico de transport et je suis que a 690 au L96 car je rush avec ^^', '90.11.167.232'),
('2013-04-10 11:30:12', 'Ben', 'Salut les filles, excusez moi d''&ecirc;tre absent en ce moment, je changes de taf, et &eacute;crire une lettre de dem, s''long m''voyez <img src="smilies/icon_razz.gif" alt="icon_razz.gif"/>', '78.193.218.193'),
('2013-04-10 11:30:24', 'Ben', 'En tout cas, pr&eacute;parez vous &agrave; me revoir tr&egrave;s vite sur les champs de tirs <img src="smilies/icon_e_wink.gif" alt="icon_e_wink.gif"/>', '78.193.218.193'),
('2013-04-10 12:42:38', 'ApaX_LeLe', 'C''est encore pire alors si tu fais ce petit score avce du rush ..', '79.80.173.102'),
('2013-04-10 13:50:26', 'Ben', 'Han, j''balance pas, mais je crois que lele te taille un bon gros costar <img src="smilies/icon_razz.gif" alt="icon_razz.gif"/>', '78.193.218.193'),
('2013-04-10 13:50:37', 'Ben', 'T''auras pas froid s''t''hiver au moins x)', '78.193.218.193'),
('2013-04-10 21:25:07', 'Aulyro', '^^', '90.11.167.232'),
('2013-04-11 20:02:31', 'dartoch', 'Une petite modif a &eacute;t&eacute; faite sur les escouade sur tempete de feu mais rien de m&eacute;chan ^^ donc retener bien avec qui vous allez etre et se que vous avez a faire <img src="smilies/icon_e_wink.gif" alt="icon_e_wink.gif"/>', '109.23.236.23'),
('2013-04-11 20:48:25', 'TweeZy', 'Yo(play) la meute <img src="smilies/icon_razz.gif" alt="icon_razz.gif"/>', '83.198.225.56'),
('2013-04-12 18:32:15', 'dartoch', 'RDV a 14h30 (max 14h45) dimanche sur notre serveur pour le match, que l''on est le temps de m&egrave;tre les escouades en place\nPens&eacute; a :\n-v&eacute;rifi&eacute; vos atout par escouade\n-v&eacute;rifi&eacute; votre tenu (noir sur temp&ecirc;te sa se voie de loin... )\n-v&eacute;rifi&eacute; vos &eacute;quipements pour les v&eacute;hicules par &eacute;quipage (2 scanners ne serve a rien...)\n- et bien sur v&eacute;rifi&eacute; vos strat, si vous avez des question n''h&eacute;sit&eacute; pas', '109.23.236.23'),
('2013-04-13 19:10:59', 'Noskill4Ever', 'C''est a quelle heure ce soir ? (anniv)', '78.233.163.100'),
('2013-04-15 13:23:28', 'toutoun80740', 'tr&egrave;s bonne question, je n ai aucune nouvelle pour la soir&eacute;e anniv!!!', '31.35.254.101'),
('2013-04-15 19:21:38', 'dartoch', 'on devait la faire samedi dernier mais on a pr&eacute;f&eacute;r&eacute; s''entrain&eacute; pour le match, et on a bien fait ^^', '109.23.236.23'),
('2013-04-17 18:02:26', 'TweeZy', 'Yo la meute <img src="smilies/icon_e_biggrin.gif" alt="icon_e_biggrin.gif"/>', '90.34.55.9'),
('2013-04-18 01:07:42', 'nono770firedan', 'bonjour tous le monde moi c''est nono770firedan j''ai 14 ans je joue a battlefield 3 depuis maintenant 3 semaines j''ai un niveau moyen ou bon j''ai 1.00 de ratio E/M et je suis a la recherche d''une team pour jouer et d&eacute;lirer merci ', '176.180.92.96'),
('2013-04-18 10:42:17', 'Dartoch', 'Salut nono, je te laisse post&eacute; ta demande dans la partie appropri&eacute; dans le forum, merci d''avance', '109.23.236.23'),
('2013-04-18 11:44:53', 'nono770firedan', 'ok merci ^^', '176.180.92.96'),
('2013-04-18 12:53:22', 'TweeZy', 'coucou nono bienvenue a toi et juste nous ici on regarde pas le ratio mais le team play <img src="smilies/icon_e_smile.gif" alt="icon_e_smile.gif"/>', '90.34.55.9'),
('2013-04-18 13:28:56', 'Ben', 'Salout&eacute;. Toch, t''es encore loiiiiiiins derri&egrave;re moi au classement !', '78.193.218.193'),
('2013-04-18 18:46:45', 'dartoch', 'Va chier benco !!!! lol', '109.23.236.23'),
('2013-04-18 19:58:21', 'dartoch', 'une ptite video que je vien de regard&eacute; et qui est pas mal du tout ^^   http://youtu.be/5pVLdFeqelY', '109.23.236.23'),
('2013-04-18 23:55:45', 'Toutoun80740', 'salut la meute, sa va?', '31.35.254.101'),
('2013-04-18 23:56:13', 'Toutoun80740', 'Et bienvenu nono! ', '31.35.254.101'),
('2013-04-19 18:14:51', 'TweeZy', 'Yo la meute <img src="smilies/icon_e_biggrin.gif" alt="icon_e_biggrin.gif"/>', '90.34.55.9'),
('2013-04-19 19:36:03', 'nono770firedan', 'salit la meute ^^ c''etait pour vous demander je suis pris ou pas ?? ', '176.180.92.96'),
('2013-04-20 10:01:41', 'Dartoch', 'salut nono, il faut &ecirc;tre patien pour ton recrutement, on ne prend pas de nouveau loup comme sa, cela peut prendre entre 2 semaine a 1 mois, on selectionne pas mal (on doit etre a une 30ene de pris sur plus de 300 candidatures)', '109.23.236.23'),
('2013-04-20 12:10:12', 'Nono770firedan', 'A oki dsl ( 30/300 O_O )', '176.180.92.96'),
('2013-04-21 20:02:13', 'TweeZy', 'yo et encore j''ai un bon joueur a presenter mais faut juste le motiv&eacute; <img src="smilies/icon_e_biggrin.gif" alt="icon_e_biggrin.gif"/>', '90.34.55.9'),
('2013-04-22 10:01:36', 'Ryukio', 'Bon ? Et quand est-ce qu''on recrute notre petit Lawlaw ? ', '92.137.217.175'),
('2013-04-22 10:29:15', 'Dartoch', 'Va faloir que notre ptit Manu se penche sur les recrutes <img src="smilies/icon_e_wink.gif" alt="icon_e_wink.gif"/>', '109.23.236.23'),
('2013-04-22 10:31:36', 'toutoun80740', 'Oui je pense car il y a du monde a l''entrer de la taniere!!! <img src="smilies/icon_e_wink.gif" alt="icon_e_wink.gif"/>', '31.35.254.101'),
('2013-04-25 10:05:48', 'Dartoch', 'Salut la meute !!!!!!!!', '109.23.236.23'),
('2013-04-25 13:47:50', 'PsYkaTre', 'yeppp', '83.154.164.2'),
('2013-04-25 13:54:35', 'toutoun80740', 'je pense me mettre en mode geek cette aprem!!!', '84.6.215.39'),
('2013-04-26 12:49:06', 'pSykatre le fou du zizi', 'ya pas de soucie je serais co <img src="smilies/icon_e_smile.gif" alt="icon_e_smile.gif"/>', '83.154.164.2'),
('2013-04-26 13:00:00', 'creative dragon', 'salut a la meute !', '84.102.143.76'),
('2013-04-26 14:06:57', 'Dartoch', 'Salut  salut !!!!!', '109.23.236.23'),
('2013-04-26 20:37:27', 'toutoun80740', 'Salut bienvenu!!!', '31.35.254.101'),
('2013-04-27 15:07:01', 'lf', 'nul', '92.140.245.123'),
('2013-04-27 23:35:30', 'Tarty', 'Saluuuut', '92.90.16.94'),
('2013-04-27 23:35:57', 'Tarty', 'Il y a quelqu''un', '92.90.16.94'),
('2013-04-27 23:44:16', 'Dartoch', 'Oui, ya du monde!!! <img src="smilies/icon_e_wink.gif" alt="icon_e_wink.gif"/>', '109.23.236.23'),
('2013-04-27 23:53:00', 'psykatre le foufou du zguegue', 'saluttttttt tarty', '86.196.238.54'),
('2013-04-28 11:36:52', 'slimmorle', 'big up les loups je profite pour dire aussi pour dire que j ai plusieur strat donc avi a ce qui ocupe des strat de me contacter merci aouuuuuuuuuuuuuuuuuuuuu', '83.155.252.229'),
('2013-04-28 11:39:28', 'slimmorle', 'salut a tous sa va les loups  j en profite pour dire que j ai mi en place des strat donc je demande a ceu qui ocupe des strat de me contact ps quan je rentre dans une team j aime mi m etre a fond haouuuuuu lol', '83.155.252.229'),
('2013-04-28 13:11:31', 'WAZA-PsY-4-x', 'Yes slim , faut qu''on en parle , la partie preparation match sur le forum est visible que par les APAX ', '86.196.238.54'),
('2013-04-28 15:25:26', 'Niroleta06', 'je suis sur votre serveur <img src="smilies/icon_e_smile.gif" alt="icon_e_smile.gif"/>', '86.203.233.155'),
('2013-04-28 15:25:30', 'Niroleta06', 'simpa le site les gars', '86.203.233.155'),
('2013-04-28 15:45:00', 'Niroleta06', 'Simpa les gars de kick des joueurs alors qu''on respect les regles super l''esprit...', '86.203.233.155'),
('2013-04-28 16:12:49', 'wolf16000', 'Je vien et de valide mes sa mdit encore que se pas valide?', '62.147.188.216'),
('2013-04-28 18:22:31', 'PsY', 'Salut niroleta , tte mes excuses mais on a d&ucirc;t exclure pour entra&icirc;nement de la meute ... Y''a pas de souci tu peux revenir quand tu veux <img src="smilies/icon_e_smile.gif" alt="icon_e_smile.gif"/>', '86.196.238.54'),
('2013-04-28 18:22:46', 'PsY', 'Je m''occupe de sa wolf', '86.196.238.54'),
('2013-04-28 20:17:35', 'wolf16000', 'Jai poster ma candidature et mrc spy', '62.147.188.216'),
('2013-04-29 13:34:24', 'Ben', 'Concernant les bugs du mail &agrave; l''inscription, je suis dessus actuellement. Devrait &ecirc;tre corrig&eacute; dans l''aprem', '78.193.218.193'),
('2013-04-29 14:26:33', 'weapons345', 'Ok thx <img src="smilies/icon_e_smile.gif" alt="icon_e_smile.gif"/>', '82.225.143.59'),
('2013-04-29 19:41:13', 'Dartoch', 'AaaaaaOooooooUuuuuuuuuuuuuuuuuuu!!!!!!!!!!!!!', '109.23.236.23'),
('2013-04-30 23:01:57', 'colonel-zboubi', 'salut bande de gens j''ai vu votre meute sur le serveur battlefield3 et honnetement sa a pas l''air mal du tout donc a vous de voir si vous voulez de moi<img src="smilies/icon_e_smile.gif" alt="icon_e_smile.gif"/> ', '83.198.220.8'),
('2013-04-30 23:55:09', 'dartoch', 'les recrute ne se fond pas ici mais dans le forum, partie recrutement, merci', '109.23.236.23'),
('2013-05-01 14:27:39', 'xTyFlo', 'hei <img src="smilies/icon_e_biggrin.gif" alt="icon_e_biggrin.gif"/>DD', '88.219.88.61'),
('2013-05-01 14:29:29', 'dartoch', 'yop tyflo <img src="smilies/icon_e_wink.gif" alt="icon_e_wink.gif"/>', '109.23.236.23'),
('2013-05-01 14:35:18', 'xTyFlo', 'jai post&eacute; un topic pour mon retour', '88.219.88.61'),
('2013-05-01 14:35:38', 'xTyFlo', 'pour savoir si j''ai encore ma place chez vous ou pas ? x) ', '88.219.88.61'),
('2013-05-02 13:49:37', 'Eiiwox', 'Quelle l''adresse du serveur ?', '83.199.103.197'),
('2013-05-02 18:26:39', 'Dartoch', 'Il te suffit de tap&eacute;: Meute Apax dans la recherche de serveur, il n''y en a qu''un seul et unique, tu peut pas nous loup&eacute; <img src="smilies/icon_e_wink.gif" alt="icon_e_wink.gif"/> ', '109.23.236.23'),
('2013-05-03 11:40:26', 'Ryukio', 'Je trouve pas o&ugrave; m''inscrir pour le match.. :''(', '89.2.158.250'),
('2013-05-03 11:40:35', 'Ryukio', 'm''inscrire*', '89.2.158.250'),
('2013-05-03 12:46:54', 'Bl4ck_Skyread', 'salut <img src="smilies/icon_e_wink.gif" alt="icon_e_wink.gif"/>', '86.204.73.221'),
('2013-05-03 12:48:13', 'Bl4ck_Skyread', '*a', '86.204.73.221'),
('2013-05-03 12:48:27', 'Bl4ck_Skyread', '? APAX ?', '86.204.73.221'),
('2013-05-03 14:53:30', 'tarty51', 'yo ', '31.38.51.150'),
('2013-05-03 17:03:11', 'choub', 'hey salut &agrave; vous alors, y a t il 4 Apax dispo pour un pti fun ce soir??? <img src="smilies/icon_e_smile.gif" alt="icon_e_smile.gif"/>', '80.11.20.240'),
('2013-05-03 18:50:11', 'blackskyread', 'moi <img src="smilies/icon_e_smile.gif" alt="icon_e_smile.gif"/>', '86.204.73.221'),
('2013-05-03 18:50:34', 'Bl4ck_Skyread', '??', '86.204.73.221'),
('2013-05-03 18:51:28', 'Bl4ck_Skyread', 'redi moi 06.33.69.99.67', '86.204.73.221'),
('2013-05-03 19:10:37', 'Pignouf30', 'Salut Choub, j''aimerai savoir quelle est votre map d&eacute;finitive pour que je puisse pr&eacute;parer le serveur', '77.201.71.195'),
('2013-05-03 21:17:15', 'Bl4ck_Skyread', 'cc', '86.204.73.221'),
('2013-05-04 18:11:48', 'Ryukio', 'Moi j''suis dispo pour ce soir !', '89.2.158.250'),
('2013-05-04 18:12:10', 'Ryukio', 'Au fait ? Quelqu''un peux me pr&ecirc;ter les codes pour le premium ? :3', '89.2.158.250'),
('2013-05-05 14:02:28', 'slimmorle', 'yo qui s aurai me dire c est koi les regle pour mercredi ', '83.155.252.229'),
('2013-05-05 14:11:06', 'slimmorle', 'c est bon j ai trouver aaaoooouuuu', '83.155.252.229'),
('2013-05-05 22:56:08', 'Eraa', 'Chalute , moi c''est Eraa , on m''a demand&eacute; de parler de vous sur ma chaine youtube alors je venais voir c''&eacute;tait quoi votre site^^', '86.196.18.245'),
('2013-05-06 11:03:47', 'Dartoch', 'Salut Eraa et bienvenu sur notre site <img src="smilies/icon_e_wink.gif" alt="icon_e_wink.gif"/>', '109.23.236.23'),
('2013-05-06 11:23:58', 'Ben', 'Salut Eraa. Bienvenu dans la tani&egrave;re <img src="smilies/icon_e_wink.gif" alt="icon_e_wink.gif"/>', '78.193.218.193'),
('2013-05-06 13:39:46', 'slimmorle', 'salut era on se concerte tkt on te tien aucourant merci ', '83.155.252.229'),
('2013-05-06 14:41:37', 'toutoun80740', 'salut eraa bienvenu sur notre site!!!', '31.35.254.101'),
('2013-05-06 19:19:20', 'Aulyro', 'Salut a tous alors voila j''ai plus de 350 plaque alors celui qui veut une plaque qu''il me pr&eacute;vien.', '92.149.68.24'),
('2013-05-06 19:19:48', 'Aulyro', 'Ps:il me reste 20 armes et j''ai 1 &eacute;toile de service avec toutes les armes XD', '92.149.68.24'),
('2013-05-06 21:37:51', 'Dartoch', 'Pour les plaque tu n''aura qua metre ta plus rare mercredi, c''est journ&eacute;e &quot;plaque rare&quot; sur notre serveur^^', '109.23.236.23'),
('2013-05-06 22:27:35', 'Aulyro', 'je ne sais pas quelle est ma plaque la plus rare pourrait tu m''aider sachant que je vais en avoir encore 3 ou 4', '92.149.68.24'),
('2013-05-06 22:51:24', 'Aulyro', 'Alors voila j''ai un autre partenaire mais celui-ci c''est un tr&egrave;s gros seriez vous ok de se faire sponsoriser par ... pour cela il me faudrat quelle que info que seul les haut plac&eacute; peuvent me donner.', '92.149.68.24'),
('2013-05-07 09:31:10', 'slimmorle', 'moi j en veu bien tout les plaque 100 medecin etc*', '83.155.252.229'),
('2013-05-07 10:11:29', 'Ben', 'Auly, par qui ?!?', '78.193.218.193'),
('2013-05-07 14:37:39', 'Ryukio ', 'Ola Eraa !', '89.2.158.250'),
('2013-05-08 18:48:56', 'Dartoch', 'Nous revoilous !!!!!!!!', '109.23.236.23'),
('2013-05-08 18:52:24', 'Dartoch', 'Joyeux anniversaire Rako !!!!!!!!!', '109.23.236.23'),
('2013-05-09 10:51:46', 'Aulyro', 'Bientot la plaque ami Dice XD ^^', '92.149.68.24'),
('2013-05-09 11:51:37', 'Ben', 'Auly, cad ?!?', '88.167.3.244'),
('2013-05-09 12:00:02', 'Aulyro', 'De quoi', '92.149.68.24'),
('2013-05-09 18:00:14', 'Ben', 'C''est quoi la plaque Ami Dice ?', '88.167.3.244'),
('2013-05-09 18:54:12', 'Aulyro', 'C''est une plaque que seul qui ont aider a la cr&eacute;ation du jeu peuvent l''avoir ^^', '92.149.68.24'),
('2013-05-09 18:54:30', 'Aulyro', 'maid je vais la prendre mais je pourrait pas la mettre snif snif', '92.149.68.24'),
('2013-05-09 22:28:55', 'Aulyro', 'Alors voila eraa ne sachant pas quoi dire nous a fait vite fait une petite pub pour nous mais pour ce qui ai d''une pub complete il faudrat voir sa avec War sur ceux je vous laisse contempler la vid&eacute;o de Eraa abonnez vous likez et laissez des commentaire https://www.youtube.com/watch?v=VGZSd_AEBqQ', '92.149.68.24'),
('2013-05-09 22:39:15', 'eraa', 'Oui Merci, abonnez vous likez , lachez un com'' ''j''vois avec mon acolyte pour faire un truc avec vous sur bf3 d&egrave;s que possible', '90.20.64.195'),
('2013-05-09 22:40:17', 'Aulyro', '^^', '92.149.68.24'),
('2013-05-10 10:38:15', 'slimmorle', 'ben je c est ou avoir la plaque je connais le serveur aaaoouuu', '83.155.252.229'),
('2013-05-10 21:29:21', 'Dartoch', 'Fait tourn&eacute; slim !!!!!! <img src="smilies/icon_e_wink.gif" alt="icon_e_wink.gif"/>', '109.23.236.23'),
('2013-05-11 21:41:54', 'toutoun80740', 'AOUUUUUUU!!!', '92.131.75.66'),
('2013-05-11 22:04:47', 'Aulyro', 'aouaouaouaou loup malade XD', '86.201.85.245'),
('2013-05-12 02:08:38', 'CiiDzZ_HD-', 'De belle parties ce soir la meute c''est bon &ccedil;a <img src="smilies/icon_e_smile.gif" alt="icon_e_smile.gif"/>', '109.15.213.56'),
('2013-05-12 04:58:57', 'Aulyro', 'oui pas mal jusqu''a la fameuse partie de spwan kill d&eacute;but de soir&egrave; u lourd sur le serveur les InFamouS ', '86.201.85.245'),
('2013-05-12 15:30:59', 'slimmorle', 'http://stalkdice.oscarmike.com/index.php voici le site ou vous trouverez ou si cache les ami de dice pour avoir la plaque le plus simple a attraper et sunnyboy sont serveur et game oasis un truc de se genre  j en profite pour dire que j inscrit les loup a un tournoi 4 vs 4 RU&eacute;e en scouades  bonne chance pour les atraper aaaaaaaaaa', '83.155.252.229'),
('2013-05-13 16:57:47', 'LFA_Piwee', 'Petit Message pour Slimmor machin truc: merci pour nous avoir accept&eacute; pour le tournoi... Le type juge une team sur un fait qu''il AURAIT entendu de quelqu''un ( Et d''ailleurs tu ne nous a jamais expliqu&eacute; pourquoi LFA=probl&egrave;mes). On a jamais fait de match ensemble et tu viens nous dire qu''on est des merdiers ? Dans ce cas f&eacute;licitation pour tes pr&eacute;jug&eacute;s, je connais une &quot;Team&quot; qui fait pareil que toi, elle s''appel le FRONT NATIONAL, tu t''y plairais je pense...', '109.190.68.24'),
('2013-05-13 16:59:06', 'LFA_Piwee', 'Bref sur ce bonne continuation aux autres c''est bien dommage ce tournoi avait l''air bien sympa', '109.190.68.24'),
('2013-05-13 20:22:29', 'LFA_Piwee', 'Ah oui d''accord, t''es l''ancien des GFB, tout s''explique... hate de voir ton fair play IG comme tu le faisais si bien dans ton ancienne team.', '109.190.68.24'),
('2013-05-14 17:51:41', 'CiiDzZ-HD', 'Tu te trompe d''endroit c''est pas le mur des lamentations ici <img src="smilies/icon_e_wink.gif" alt="icon_e_wink.gif"/> ', '78.127.40.34'),
('2013-05-14 18:21:41', 'Aulyro', '???', '92.149.194.49'),
('2013-05-14 19:32:16', 'slimmorle', 'mdr j ai meme pas envi de repondre pufff', '83.155.252.229'),
('2013-05-14 21:19:59', 'Ben', 'Franchement les mecs, arr&ecirc;tez vos conneries, c''est moi qui passe le balais sur les bases de donn&eacute;es apr&egrave;s ... ', '88.167.3.244'),
('2013-05-14 21:31:35', 'Aulyro', 'Ben &lt;3', '92.149.194.49'),
('2013-05-15 11:03:08', 'slimmorle', 'moi jy peu rien je leur avai donner la reputation qu il avai maintenan a se que on ma dit il on un news leadeur il ma di qu il avais changer voila quoi dsl les apax ', '83.155.252.229'),
('2013-05-19 17:40:15', 'Aulyro', 'Mais merde quoi les mecs quesce que vous avez tous a donner vos d&eacute;mission vous rest&egrave; tous entre 1 semaine et 1 mois grand max --&quot; mais bon pas pour autan que l''on jourat pas ensemble', '92.149.194.49'),
('2013-05-16 12:18:43', 'toutoun80740', 'Depuis quand il y a de la pub pr WoW sur le site???? WHAT THE F**CK!!', '31.35.254.101'),
('2013-05-16 20:02:52', 'Aulyro', 'Hey friend here is not the market so no pub in the chat if you please here is to talk about here is the hideout of the pack so pa pub just something fun and demand match or recruitment in short you understood my.', '92.149.194.49'),
('2013-05-17 10:20:10', 'dartoch', 'pas la peine dit r&eacute;pondre les mecs, c''est un bot <img src="smilies/icon_e_wink.gif" alt="icon_e_wink.gif"/>', '109.23.236.23'),
('2013-05-17 11:50:25', 'toutoun80740', 'AOOUUUUUU', '31.35.254.101'),
('2013-05-17 19:02:01', 'Aulyro', 'esce normal que maintenant j''ai une pub a droite de ma fenetre depuis aujourd''hui', '92.149.194.49'),
('2013-05-18 12:06:25', 'x-trem-_flach', 'salut comment on se fait recruter par la meute APAX', '78.129.90.211'),
('2013-05-18 12:56:26', 'Dartoch', 'Salut Flach, alors il faut que tu te rende sur le forum, partie &quot;demande de recrutement&quot;', '109.23.236.23'),
('2013-05-18 12:57:30', 'Dartoch', 'tu vera il y a un poste avec la demande type, il te suffi de faire un copi&eacute;, cr&eacute;&eacute; un poste pour coll&eacute; la demande type et la remplir <img src="smilies/icon_e_wink.gif" alt="icon_e_wink.gif"/> si tu a besoin tu sait ou me trouver <img src="smilies/icon_e_wink.gif" alt="icon_e_wink.gif"/>', '109.23.236.23');

-- --------------------------------------------------------

--
-- Structure de la table `topmatch_settings`
--

CREATE TABLE IF NOT EXISTS `topmatch_settings` (
  `topmatch_setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `logowidth` int(11) NOT NULL DEFAULT '0',
  `logoheight` int(11) NOT NULL DEFAULT '0',
  `logo` varchar(255) NOT NULL DEFAULT '',
  `country` varchar(255) NOT NULL DEFAULT '',
  `team` varchar(255) NOT NULL DEFAULT '',
  `homepage` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`topmatch_setting_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `topmatch_settings`
--

INSERT INTO `topmatch_settings` (`topmatch_setting_id`, `logowidth`, `logoheight`, `logo`, `country`, `team`, `homepage`) VALUES
(1, 0, 0, '', '', '', '');

-- --------------------------------------------------------

--
-- Structure de la table `upcoming`
--

CREATE TABLE IF NOT EXISTS `upcoming` (
  `upID` int(11) NOT NULL AUTO_INCREMENT,
  `date` int(14) NOT NULL DEFAULT '0',
  `type` char(1) NOT NULL DEFAULT '',
  `squad` int(11) NOT NULL DEFAULT '0',
  `opponent` varchar(255) NOT NULL DEFAULT '',
  `opptag` varchar(255) NOT NULL DEFAULT '',
  `opphp` varchar(255) NOT NULL DEFAULT '',
  `oppcountry` char(2) NOT NULL DEFAULT '',
  `maps` varchar(255) NOT NULL DEFAULT '',
  `server` varchar(255) NOT NULL DEFAULT '',
  `league` varchar(255) NOT NULL DEFAULT '',
  `leaguehp` varchar(255) NOT NULL DEFAULT '',
  `warinfo` text NOT NULL,
  `short` varchar(255) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL DEFAULT '',
  `enddate` int(14) NOT NULL DEFAULT '0',
  `country` char(2) NOT NULL DEFAULT '',
  `location` varchar(255) NOT NULL DEFAULT '',
  `locationhp` varchar(255) NOT NULL DEFAULT '',
  `dateinfo` text NOT NULL,
  PRIMARY KEY (`upID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `upcoming`
--

INSERT INTO `upcoming` (`upID`, `date`, `type`, `squad`, `opponent`, `opptag`, `opphp`, `oppcountry`, `maps`, `server`, `league`, `leaguehp`, `warinfo`, `short`, `title`, `enddate`, `country`, `location`, `locationhp`, `dateinfo`) VALUES
(1, 1363527000, 'c', 1, '2099', '2099', 'http://battlelog.battlefield.com/bf3/fr/platoon/2832655240994418586/', 'fr', '', '', 'Entrainement', 'http://battlelog.battlefield.com/bf3/fr/', 'Si vous pouviez aller valider votre inscription au T12 au plus vite.\r\n\r\n\r\n// Pour ceux qui ont déjà reçu l''invitation, veuillez m''excuser, il s''agit d''un test des neswletters membre :D\r\n\r\nBenftwc', '', '', 0, '', '', '', '');

-- --------------------------------------------------------

--
-- Structure de la table `upcoming_announce`
--

CREATE TABLE IF NOT EXISTS `upcoming_announce` (
  `annID` int(11) NOT NULL AUTO_INCREMENT,
  `upID` int(11) NOT NULL DEFAULT '0',
  `userID` int(11) NOT NULL DEFAULT '0',
  `status` char(1) NOT NULL DEFAULT '',
  PRIMARY KEY (`annID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Contenu de la table `upcoming_announce`
--

INSERT INTO `upcoming_announce` (`annID`, `upID`, `userID`, `status`) VALUES
(1, 1, 1, 'y'),
(2, 1, 5, 'y'),
(3, 1, 2, 'y'),
(4, 1, 4, 'y'),
(5, 1, 12, 'y'),
(6, 1, 22, 'y');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `userID` int(11) NOT NULL AUTO_INCREMENT,
  `registerdate` int(14) NOT NULL DEFAULT '0',
  `lastlogin` int(14) NOT NULL DEFAULT '0',
  `username` varchar(255) NOT NULL DEFAULT '',
  `password` varchar(255) NOT NULL DEFAULT '',
  `nickname` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `email_hide` int(1) NOT NULL DEFAULT '1',
  `email_change` varchar(255) NOT NULL,
  `email_activate` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL DEFAULT '',
  `lastname` varchar(255) NOT NULL DEFAULT '',
  `sex` char(1) NOT NULL DEFAULT 'u',
  `country` varchar(255) NOT NULL DEFAULT '',
  `town` varchar(255) NOT NULL DEFAULT '',
  `birthday` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `icq` varchar(255) NOT NULL DEFAULT '',
  `avatar` varchar(255) NOT NULL DEFAULT '',
  `usertext` varchar(255) NOT NULL DEFAULT '',
  `userpic` varchar(255) NOT NULL DEFAULT '',
  `clantag` varchar(255) NOT NULL DEFAULT '',
  `clanname` varchar(255) NOT NULL DEFAULT '',
  `clanhp` varchar(255) NOT NULL DEFAULT '',
  `clanirc` varchar(255) NOT NULL DEFAULT '',
  `clanhistory` varchar(255) NOT NULL DEFAULT '',
  `cpu` varchar(255) NOT NULL DEFAULT '',
  `mainboard` varchar(255) NOT NULL DEFAULT '',
  `ram` varchar(255) NOT NULL DEFAULT '',
  `monitor` varchar(255) NOT NULL DEFAULT '',
  `graphiccard` varchar(255) NOT NULL DEFAULT '',
  `soundcard` varchar(255) NOT NULL DEFAULT '',
  `verbindung` varchar(255) NOT NULL DEFAULT '',
  `keyboard` varchar(255) NOT NULL DEFAULT '',
  `mouse` varchar(255) NOT NULL DEFAULT '',
  `mousepad` varchar(255) NOT NULL DEFAULT '',
  `newsletter` int(1) NOT NULL DEFAULT '1',
  `homepage` varchar(255) NOT NULL,
  `about` text NOT NULL,
  `pmgot` int(11) NOT NULL DEFAULT '0',
  `pmsent` int(11) NOT NULL DEFAULT '0',
  `visits` int(11) NOT NULL DEFAULT '0',
  `banned` varchar(255) DEFAULT NULL,
  `ban_reason` varchar(255) NOT NULL,
  `ip` varchar(255) NOT NULL DEFAULT '',
  `topics` text NOT NULL,
  `articles` text NOT NULL,
  `demos` text NOT NULL,
  `files` text NOT NULL,
  `gallery_pictures` text NOT NULL,
  `mailonpm` int(1) NOT NULL DEFAULT '0',
  `userdescription` text NOT NULL,
  `activated` varchar(255) NOT NULL DEFAULT '1',
  `language` varchar(2) NOT NULL,
  `movies` text NOT NULL,
  PRIMARY KEY (`userID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=112 ;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`userID`, `registerdate`, `lastlogin`, `username`, `password`, `nickname`, `email`, `email_hide`, `email_change`, `email_activate`, `firstname`, `lastname`, `sex`, `country`, `town`, `birthday`, `icq`, `avatar`, `usertext`, `userpic`, `clantag`, `clanname`, `clanhp`, `clanirc`, `clanhistory`, `cpu`, `mainboard`, `ram`, `monitor`, `graphiccard`, `soundcard`, `verbindung`, `keyboard`, `mouse`, `mousepad`, `newsletter`, `homepage`, `about`, `pmgot`, `pmsent`, `visits`, `banned`, `ban_reason`, `ip`, `topics`, `articles`, `demos`, `files`, `gallery_pictures`, `mailonpm`, `userdescription`, `activated`, `language`, `movies`) VALUES
(1, 1361454913, 1368991768, 'benftwc', '7f47c853b4334df83f26221e3296a673', 'ben_ftwc', 'benftwc@gmail.com', 1, '', '', 'Benjamin', '', 'm', 'fr', 'Coulommiers', '1992-07-28 00:00:00', '', '1.jpg', '[URL=http://www.meute-apax.fr/site/index.php?site=static&staticID=1]Ma page perso[/URL]\r\n\r\n[img]http://g.bf3stats.com/ps3/LBF3Sig1/ben_ftwc.png[/img]\r\n[img]http://sphotos-c.ak.fbcdn.net/hphotos-ak-ash4/405685_3028678249099_784078104_n.jpg[/img]', '1.jpg', 'APAX', 'APAX', 'http://apax-team.xooit.fr/', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', '[i]Sire ! Mon père est peut-être unijambiste, mais moi, ma femme n''a pas de moustache ![/i]\r\n\r\n[img]http://g.bf3stats.com/ps3/LBF3Sig1/ben_ftwc.png[/img]\r\n[img]http://sphotos-c.ak.fbcdn.net/hphotos-ak-ash4/405685_3028678249099_784078104_n.jpg[/img]', 9, 8, 22, NULL, '', '88.167.3.244', '|69|59|123|', '1', '', '1:2:3', '1', 1, '', '1', 'fr', ''),
(2, 1361649408, 1368989975, 'gp', '1adcc73dfd26f02f9675d1cf23a957f7', 'waza-gp_one', 'loic1975@free.fr', 1, '', '', 'loic', '', 'm', 'fr', '', '1975-11-27 00:00:00', '', '2.png', 'dans la gueule du loup nous avançons ( 300 ) \r\n                                                                             [url=http://bf3stats.com/stats_ps3/WAZA-gp_one][img]http://g.bf3stats.com/ps3/60o0DXqU/WAZA-gp_one.png[/img][/url]', '2.jpg', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', '', 4, 1, 6, NULL, '', '88.176.242.208', '|8|13|16|4|22|12|24|18|9|26|21|30|41|42|28|25|51|29|38|48|27|37|57|68|69|80|86|88|87|89|96|91|95|97|93|67|104|105|106|107|50|108|100|110|56|111|101|113|112|114|118|70|3|119|121|122|103|71|123|120|124|', '', '', '', '', 0, '', '1', 'fr', ''),
(3, 1361649681, 1368384235, 'julien', 'ee511f249bbeab8b67c129b2b5b681d4', 'jujub3', 'El_parigo40@hotmail.fr', 1, '', '', 'julien', 'felicetti', 'm', 'fr', 'quincy sous senart', '1986-04-08 00:00:00', '', '', '[url=http://bf3stats.com/stats_ps3/jujube-basque][img]http://g.bf3stats.com/ps3/5UogHRcV/jujube-basque.png[/img][/url]', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', '', 2, 0, 0, NULL, '', '82.225.43.248', '|50|57|37|56|38|52|58|59|60|34|61|62|63|67|68|69|70|71|40|72|73|74|75|76|77|78|79|80|81|82|83|84|85|86|87|88|89|90|91|92|93|94|95|96|97|21|98|99|100|101|102|103|104|105|106|107|108|109|3|110|111|112|113|114|117|115|118|119|120|121|122|123|124|', '', '', '', '', 0, '', '1', 'fr', ''),
(4, 1361652771, 1368983795, 'BlackSanka', '9f2f83c9f9b363ad70e6c419cc66fe75', 'Pignouf30', 'blacksanka@hotmail.com', 1, '', '', 'Julien', '', 'm', 'fr', '', '1983-07-14 00:00:00', '', '', 'Si tu m''as dans ton viseur me loupe pas, sinon t''es mort !\r\n\r\n[url=http://bf3stats.com/stats_ps3/Pignouf30][img]http://g.bf3stats.com/ps3/4dW5zwFx/Pignouf30.png[/img][/url]', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', '', 4, 0, 6, NULL, '', '79.84.90.137', '|30|26|68|69|88|80|21|93|67|101|102|56|50|105|106|107|100|108|109|3|110|111|113|114|115|70|71|59|122|117|84|123|120|124|', '', '', '', '', 0, '', '1', 'fr', ''),
(5, 1361655334, 1368987221, 'Dartoch', '88425de2ae84e2c6246aad44a2666dab', 'Dartoch', 'adriendartier@hotmail.fr', 1, '', '', 'adrien', '', 'm', 'fr', 'vierzon', '1985-09-19 00:00:00', '', '5.png', '[url=http://bf3stats.com/server/ps3_165ae39b-6e5c-4735-a238-6312dd94d31d][img]http://g.bf3stats.com/ps3/5XG4KUKP/165ae39b-6e5c-4735-a238-6312dd94d31d.png[/img][/url]', '5.jpg', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', '', 15, 44, 38, NULL, '', '109.23.236.23', '|69|89|105|106|124|122|', '', '', '3', '', 0, '', '1', 'fr', ''),
(6, 1361664616, 1368723862, 'LeBetz', 'b42697cbce5e3cd1dc0873d93d58df30', 'ApaX_LeLe', 'monkeyaffoler@live.fr', 1, '', '', 'Melvin', '', 'u', 'fr', '', '1996-11-16 00:00:00', '', '', '[url=http://bf3stats.com/stats_ps3/ApaX_LeLe][img]http://g.bf3stats.com/ps3/C1sriTLL/ApaX_LeLe.png[/img][/url]', '', 'apaX', 'LeLe', '', '', 'LFP', '', '', '', '', '', '', '', '', '', '', 1, '', '', 1, 0, 3, NULL, '', '79.80.175.182', '|3|5|11|12|8|16|18|19|20|17|9|2|41|28|33|42|45|29|46|40|49|51|38|48|25|27|53|55|37|52|57|59|60|56|34|50|61|62|63|67|68|69|70|71|72|73|74|75|76|77|78|79|80|81|83|84|85|86|87|88|89|90|92|93|94|95|96|97|98|99|100|101|102|104|105|106|107|108|109|110|111|112|113|114|115|117|118|119|120|121|21|122|103|123|124|', '', '', '', '', 0, '', '1', 'fr', ''),
(8, 1361696858, 1368986909, 'Apax_Reaper', '9f05aa4202e4ce8d6a72511dc735cce9', 'Apax_Reaper', 'bmdc64@yahoo.fr', 1, '', '', 'baptiste', '', 'm', 'al', '', '1990-07-16 00:00:00', '', '', 'Que le cul te pèle mon grand', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', '', 2, 0, 13, NULL, '', '89.3.201.170', '|12|8|16|20|24|17|23|9|1|2|31|36|30|28|47|51|27|48|53|25|55|37|38|58|62|69|61|63|88|89|90|86|83|87|98|99|94|85|50|105|106|107|109|3|111|102|70|21|120|115|123|124|122|', '', '', '', '', 0, '', '1', 'fr', ''),
(9, 1361704220, 1366290027, 'lord-beckett59', '0ecc7b6db5d260c8625d17bb235dfda9', 'lord-beckett59', 'chris.adr@hotmail.fr', 1, '', '', 'Christopher', '', 'm', 'fr', 'Douai', '1993-08-22 00:00:00', '', '', 'Blut und Ehre', '', 'APAX', 'APAX Team', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', '', 2, 0, 4, NULL, '', '82.244.206.28', '|3|4|5|6|7|8|9|1|10|11|12|13|14|16|17|18|19|20|21|22|23|24|2|25|26|28|27|29|30|15|31|32|33|34|36|37|38|39|40|41|42|43|44|45|46|47|48|49|50|51|52|53|55|56|57|58|59|60|61|62|63|67|68|69|70|71|72|73|74|75|76|77|78|79|80|81|82|83|84|85|86|87|88|89|90|91|92|93|94|95|96|97|98|99|100|101|102|103|104|105|106|107|108|109|110|111|112|113|114|115|117|118|119|120|121|122|123|124|', '', '', '', '', 0, '', '1', 'fr', ''),
(10, 1361727261, 1368990469, 'WAZA-PsY-4-x', 'fdd6daf75e4342d976538853a5e5ca8b', 'WAZA-PsY-4-x', 'Guguffcg@hotmail.fr', 1, '', '', 'manu', '', 'm', 'fr', 'paris', '1988-07-11 00:00:00', '', '10.png', '[url=http://bf3stats.com/stats_ps3/WAZA-PsY-4-x][img]http://g.bf3stats.com/ps3/RGoIg6GD/WAZA-PsY-4-x.png[/img][/url][url=http://bf3stats.com/stats_ps3/WAZA-PsY-4-x][img]http://g.bf3stats.com/ps3/BWgpBHJ9/WAZA-PsY-4-x.png[/img][/url]', '10.png', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', '', 2, 0, 8, NULL, '', '90.84.144.243', '|3|4|8|16|19|20|22|23|9|28|27|29|31|15|36|30|26|39|41|42|43|44|46|47|48|49|51|53|55|57|37|67|40|50|85|80|87|82|88|86|77|93|92|21|98|99|79|94|81|101|104|76|105|106|102|108|109|110|103|111|56|112|114|113|117|118|119|70|120|121|59|122|115|123|', '', '', '', '', 0, '', '1', 'fr', ''),
(11, 1361733227, 1368989675, 'aulyro', 'a1016bd8c3ab1272635415cbfddaeec4', 'Aulyro1996', 'romain.aulyro@live.fr', 1, '', '', 'Romain', 'Fiche', 'm', 'fr', 'Castelsarrasin', '1996-05-07 00:00:00', '', '', 'Il y a des guerres justes. Il n’y a pas d’armée juste.\r\n\r\n[url=http://bf3stats.com/stats_ps3/aulyro1996][img]http://g.bf3stats.com/ps3/yHb3ZF2b/aulyro1996.png[/img][/url]', '', '', '', '', '', '', 'Intel(R)Core(TM)2Duo CPU T6400@ 2.00GHz 2.00GHZ', '', '300Go', '', 'NVIDIA GeForce9300M GS', 'IDT High Definition Audio CODEC', 'ADSL', 'Standar 101/102-Key or Microsoft Natural PS/2 Keyboard with HP QLB', 'Souris compatible PS/2', '', 1, '', 'Salut a tous moi c''est Romain sinon c''est Aulyro. Ancien joueur compétitif Decerto sur Mw2 et Mw3.\r\nSur ceux l''on se retrouve sur le champs de Bataille.', 2, 0, 11, NULL, '', '92.149.194.49', '|123|124|122|', '', '', '', '', 0, '', '1', 'fr', ''),
(12, 1361837459, 1365101177, 'MrRakoto', 'd98b3a63c621b127b5a6a40e03de3458', 'MrRako', 'malagachy@gmail.com', 1, '', '', 'Shaffer', 'Smith', 'u', 'al', '', '1989-05-08 00:00:00', '', '12.png', '[url=http://bf3stats.com/stats_ps3/MrRakoto][img]http://g.bf3stats.com/ps3/TxO-WK/MrRakoto.png[/img][/url]', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', '', 2, 0, 2, NULL, '', '92.144.71.173', '|3|5|6|7|8|9|1|13|12|15|16|19|14|20|21|22|23|24|18|17|25|26|28|27|29|2|30|31|33|34|36|32|39|40|38|42|43|44|45|37|46|47|48|49|50|51|52|53|55|56|57|58|59|60|61|62|63|67|68|69|70|71|72|73|74|75|76|77|78|79|80|81|82|83|84|85|86|87|88|89|90|91|92|93|94|95|96|97|98|99|100|101|102|103|104|105|106|107|108|109|110|111|112|113|114|115|117|118|119|120|121|122|123|124|', '', '', '', '', 0, '', '1', 'fr', ''),
(14, 1362157266, 1367607918, 'Bl4ck_Skyread', '059bf68f71c80fce55214b411dd2280c', 'Bl4ck_Skyread', 'a.vernerey399@laposte.net', 1, '', '', 'alexis', 'vernerey', 'm', 'fr', 'frasne', '1996-08-18 00:00:00', '', '', '', '', '', '', '', '', 'Utopie ( UTP )', '', '', '', '', '', '', '', '', '', '', 1, '', '', 1, 0, 3, NULL, '', '86.204.73.221', '|2|2|7|8|6|9|1|10|11|12|13|14|15|16|17|18|19|4|20|21|22|23|24|25|26|27|28|29|30|31|32|33|34|36|37|38|39|40|41|42|43|44|45|46|47|48|49|50|51|52|53|55|56|57|58|59|60|61|62|63|67|68|69|70|71|72|73|74|75|76|77|78|79|80|81|82|83|84|85|87|88|89|90|86|92|93|94|96|97|95|91|98|99|100|101|102|103|104|105|106|107|108|109|3|110|111|112|113|114|115|117|118|119|120|121|122|123|124|', '', '', '', '', 0, '', '1', 'fr', ''),
(15, 1362247480, 1367929745, 'ItadakimasTB', 'dd1cf281c9b2227f51e8afcbffcf31b9', 'itadakimasTB', 'makunouchi.ippo.desu@gmail.com', 1, '', '', '', '', 'u', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', '', 2, 0, 4, NULL, '', '78.193.15.53', '|7|7|8|6|11|12|13|18|19|14|20|22|23|24|17|25|9|26|27|1|28|21|30|2|29|15|31|32|33|34|36|37|38|39|40|41|42|43|44|45|46|47|48|49|50|51|52|53|55|56|57|58|59|60|61|62|63|67|68|69|70|71|72|73|74|75|76|77|78|79|80|81|82|83|84|85|86|87|88|89|90|91|92|93|94|95|96|97|98|99|100|101|102|103|104|105|106|107|108|109|3|110|111|112|113|114|115|117|118|119|120|121|122|123|124|', '', '', '', '', 0, '', '1', '', ''),
(16, 1362317684, 1367755677, 'mika', '075d8df74e16cad7965449d7c9e78f8c', 'mika44500', 'boby72@hotmail.fr', 1, '', '', 'mickael', '', 'm', 'fr', '', '1989-03-20 00:00:00', '', '16.jpg', '[url=http://bf3stats.com/stats_ps3/mika44500][img]http://g.bf3stats.com/ps3/z4mBgdFJ/mika44500.png[/img][/url]', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', '', 1, 0, 2, NULL, '', '79.84.31.133', '|52|58|59|60|56|61|62|63|67|68|69|70|71|40|73|74|76|77|72|78|50|79|80|81|82|83|84|86|87|88|89|90|91|92|93|94|95|96|97|21|98|99|100|101|102|103|104|105|106|107|108|109|3|110|111|112|113|114|115|117|118|119|120|121|122|123|124|', '', '', '', '', 0, '', '1', 'fr', ''),
(17, 1362398106, 1363900888, 'Thierry felicetti', '28e70e5585f2d30961cf77ccea8be21d', 'Ittecilef', 'Titilaboule@hotmail.fr', 1, '', '', '', '', 'u', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', '', 2, 0, 7, NULL, '', '78.248.5.64', '|7|1|9|7|10|11|12|13|14|15|8|16|17|18|19|4|20|21|22|23|24|25|26|27|28|29|2|30|31|32|33|34|36|37|38|39|40|41|42|43|44|45|46|47|48|49|50|51|52|53|55|56|57|58|59|60|61|62|63|67|68|69|70|71|72|73|74|75|76|77|78|79|80|81|82|83|84|85|86|87|88|89|90|91|92|93|94|95|96|97|98|99|100|101|102|103|104|105|106|107|108|109|3|110|111|112|113|114|115|117|118|119|120|121|122|123|124|', '', '', '', '', 0, '', '1', '', ''),
(18, 1362510810, 1367608620, 'LaPsYkOsE', '375862c07e016e3c43d92a67a67e6a88', 'PsYkOsE', 'mdhoum@hotmail.fr', 1, '', '', '', '', 'u', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', '', 2, 0, 2, NULL, '', '87.231.217.178', '|12|13|7|12|11|8|16|17|18|19|4|2|20|22|23|24|25|9|26|27|1|28|29|30|31|32|33|34|37|38|39|21|40|41|42|43|44|45|46|47|48|49|50|51|52|53|55|56|57|58|59|60|61|62|63|67|68|69|70|71|72|73|74|75|76|77|78|79|80|81|82|83|84|85|86|87|88|89|90|91|93|94|95|96|97|92|98|99|100|101|102|103|104|105|106|107|108|109|3|110|111|112|113|114|115|117|118|119|120|121|122|123|124|', '', '', '', '', 0, '', '1', '', ''),
(19, 1362518160, 1363530935, 'zoheir', 'c19bca4e225caf41e6042351a60c8690', 'Yuri', 'zoheirdu31@hotmail.fr', 1, '', '', 'Zoheir', '', 'm', 'fr', 'Toulouse', '1990-10-05 00:00:00', '', '', '', '', 'ApaX', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', '', 2, 0, 6, NULL, '', '78.251.94.211', '|12|14|15|8|16|17|18|19|4|2|20|21|22|7|23|24|25|9|26|27|1|28|29|30|31|32|33|34|36|37|38|39|40|41|42|43|44|45|46|47|48|49|50|51|52|53|55|56|57|58|59|60|61|62|63|67|68|69|70|71|72|73|74|75|76|77|78|79|80|81|82|83|84|85|86|87|88|89|90|91|92|93|94|95|96|97|98|99|100|101|102|103|104|105|106|107|108|109|3|110|111|112|113|114|115|117|118|119|120|121|122|123|124|', '', '', '', '', 0, '', '1', 'fr', ''),
(20, 1362605600, 1365803768, 'darkyoyo19-1', 'd8536e9452d04b71a3489f69c704e4bb', 'darkyoyo19-1', 'callen.gea@gmail.com', 1, '', '', '', '', 'u', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', '', 0, 0, 2, NULL, '', '80.119.60.205', '|11|12|15|8|16|17|18|19|4|2|20|21|22|7|23|24|25|9|26|27|1|28|29|30|31|32|33|34|36|37|38|39|40|41|42|43|44|45|14|46|47|48|49|50|51|52|53|55|56|57|58|59|60|61|62|63|67|68|69|70|71|72|73|74|75|76|77|78|79|80|81|82|83|84|85|86|87|88|89|90|91|92|93|94|95|96|97|98|99|100|101|102|103|104|105|106|107|108|109|3|110|111|112|113|114|115|117|118|119|120|121|122|123|124|', '', '', '', '', 0, '', '1', '', ''),
(21, 1362832995, 1368808449, 'lawrent33', 'dcc40695ca7ba935f26f8721d2ebcf22', 'lawrent33', 'lawrent33@yahoo.fr', 1, '', '', 'Laurent', '', 'm', 'fr', '', '1972-02-10 00:00:00', '', '21.jpg', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', '', 0, 1, 3, NULL, '', '77.200.129.178', '|12|15|8|16|17|18|19|4|2|20|22|23|12|9|26|1|30|32|33|39|41|42|43|44|46|48|51|53|25|40|52|37|38|58|60|59|69|67|70|71|73|74|75|63|62|61|77|78|79|80|81|82|83|84|85|86|87|88|89|90|92|93|94|21|98|99|101|102|105|106|107|108|76|3|111|112|120|121|103|123|124|113|122|', '', '', '', '', 0, '', '1', 'fr', ''),
(22, 1362834301, 1365945568, 'mister_reveur', '251cb0896e290645f782ece75dd6eccf', 'Mister_reveur', 'jeanmic96@hotmail.com', 1, '', '', 'Michaël', '', 'm', 'ch', 'Nyon', '1996-11-28 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', '', 2, 0, 2, NULL, '', '178.193.215.103', '|12|8|16|17|18|19|4|2|21|23|7|24|25|9|27|1|28|30|31|34|39|40|41|42|43|44|32|45|37|29|14|46|47|38|48|49|50|51|52|53|55|56|57|58|59|60|61|62|63|67|68|69|70|71|72|73|74|75|76|77|78|79|80|81|82|83|84|85|86|87|88|89|90|91|92|93|94|95|96|97|98|99|100|101|102|103|104|105|106|107|108|109|3|110|111|112|113|114|115|117|118|119|120|121|122|123|124|', '', '', '1:3', '', 0, '', '1', 'fr', ''),
(24, 1363084916, 1364592390, 'typex59', '34a321664be49e31c2368f6f42798a98', 'typex59', 'ludovic.lestrez@hotmail.fr', 1, '', '', '', '', 'u', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', '', 1, 0, 0, NULL, '', '88.120.184.70', '|16|17|19|4|14|20|7|12|23|18|9|26|25|1|28|2|15|32|33|34|36|37|38|30|29|39|21|40|41|42|43|44|45|46|47|48|49|50|51|52|27|53|55|56|57|58|59|60|61|62|63|67|68|69|70|71|72|73|74|75|76|77|78|79|80|81|82|83|84|85|86|87|88|89|90|91|92|93|94|95|96|97|98|99|100|101|102|103|104|105|106|107|108|109|3|110|111|112|113|114|115|117|118|119|120|121|122|123|124|', '', '', '3', '', 0, '', '1', '', ''),
(25, 1363094089, 1368988522, 'AtOnIuM_GhAuX', '7d61b24921af6ac96497a0c7bc809381', 'AtOnIuM_GhAuX', 'jaja395@hotmail.fr', 1, '', '', 'Jaouen', '', 'u', 'al', '', '1997-12-28 00:00:00', '', '25.jpg', '[url=http://www.enjin.com/bf3-signature-generator][img]http://sigs.enjin.com/sig-bf3/1ba0009c8871a7fb.png[/img][/url]', '25.jpg', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', '\r\n', 1, 0, 7, NULL, '', '93.182.199.77', '|16|19|22|12|24|23|17|7|9|31|33|26|15|39|41|42|32|44|38|37|29|47|49|25|51|27|53|55|57|52|58|59|63|67|34|68|69|61|62|70|40|73|74|75|50|78|81|83|85|86|87|88|89|90|96|95|97|94|98|99|21|101|104|105|106|109|76|110|3|111|102|114|115|56|112|118|120|103|121|122|123|124|', '', '', '', '', 0, '', '1', 'fr', ''),
(26, 1363517726, 1366371822, 'Kenouzz', '4ad76946ae9331ae90d6545bb2c997c7', 'PaPa_KeNoUzZ', 'ken10600@hotmail.fr', 0, '', '', 'Steven', 'Transler', 'm', 'fr', 'La Chapelle-Saint-Luc', '1994-01-20 00:00:00', '', '', 'Fait gaffe t''as un point rouge sur le front ... BAM ! ... tu feras gaffe , ton crane est par terre , pense à le ramasser l''ami ! NARMOL [url=http://bf3stats.com/stats_ps3/PaPa_KeNoUzZ][img]http://g.bf3stats.com/ps3/9JTNqQUX/PaPa_KeNoUzZ.png[/img][/url]', '26.jpg', 'APAX', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', '', 1, 0, 3, NULL, '', '78.124.154.38', '|14|22|7|23|24|12|17|25|9|27|28|29|30|26|31|39|21|40|41|38|42|43|33|15|32|37|46|47|48|49|50|51|52|53|55|56|57|58|59|60|34|61|62|63|67|68|69|70|71|72|73|74|75|76|77|78|79|80|81|82|83|84|85|86|87|88|89|90|91|92|93|94|95|96|97|98|99|100|101|102|103|104|105|106|107|108|109|3|110|111|112|113|114|115|117|118|119|120|121|122|123|124|', '', '', '', '', 0, '', '1', 'fr', ''),
(27, 1363540181, 1367927635, 'Noskill4Ever', '50fce7aa1ca1a5f1f84c32252eec1f73', 'NoSkill4Ever', 'noSkill4Ever@hotmail.fr', 0, '', '', 'Tom', '', 'm', 'fr', 'Lyon', '1995-12-31 00:00:00', '', '27.gif', '[Size=4][ALIGN=center]\r\nAvec les loups on apprend à hurler.\r\n[url=http://www.dicocitations.com/citation/loup/1/125.php#75HJw8XRs4hU8yWi.99] [url=http://image.noelshack.com/fichiers/2013/12/1363719541-ra.png][img]http://image.noelshack.com/fichiers/2013/12', '', 'ApaX', 'ApaX', '', '', 'Elle S''appelait Et S''appellera ApaX ', '', '', '', '', '', '', '', '', '', '', 1, '', '', 4, 5, 2, NULL, '', '78.233.163.100', '|12|17|23|7|9|25|1|2|30|26|31|36|34|33|32|46|47|49|29|40|53|27|48|55|37|38|58|50|59|60|61|62|63|67|68|69|70|71|72|73|74|75|76|77|78|79|80|81|82|83|84|85|86|87|88|89|90|91|92|93|94|95|96|97|21|98|99|101|102|103|104|105|106|107|108|100|56|109|3|110|111|112|113|114|115|117|118|119|120|121|122|123|124|', '', '', '2', '', 1, '', '1', 'fr', ''),
(28, 1363546912, 1367680674, 'gyom-88', 'bcc233f87461211d41a67b0406cd4a69', 'gyom-88', 'peronnoguillaume@hotmail.fr', 1, '', '', '', '', 'u', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', '', 2, 0, 2, NULL, '', '83.154.164.2', '|22|23|7|2|24|12|18|17|25|9|14|27|26|1|28|29|21|30|15|31|34|36|37|38|39|40|41|42|43|44|45|47|48|49|50|51|52|53|55|56|57|58|59|60|61|62|63|67|68|69|70|71|72|73|74|75|77|78|79|80|81|82|85|86|84|88|89|90|92|83|87|93|94|96|98|97|99|100|101|102|103|104|76|105|106|107|108|109|3|110|111|112|113|114|115|117|118|119|120|121|122|123|124|', '', '', '', '', 0, '', '1', '', ''),
(29, 1363704271, 1363704566, '777RSC777', '1a23ba111c1b14cfad7ba69fd66126bd', 'RS-C77', 'loanne-gwen@hotmail.be', 1, '', '', '', '', 'u', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', '', 1, 0, 1, NULL, '', '81.244.199.247', '24|12|23|24|18|7|17|2|25|9|26|27|14|1|28|29|21|30|15|31|32|33|34|36|37|38|39|40|41|42|43|44|45|46|47|48|49|50|51|52|53|55|56|57|58|59|60|61|62|63|67|68|69|70|71|72|73|74|75|76|77|78|79|80|81|82|83|84|85|86|87|88|89|90|91|92|93|94|95|96|97|98|99|100|101|102|103|104|105|106|107|108|109|3|110|111|112|113|114|115|117|118|119|120|121|122|123|124|', '', '', '', '', 0, '', '1', '', ''),
(30, 1363778596, 1368389521, 'Ryukio', 'c19959b8eee7dc6f03394f184d27aad3', 'Ryukio', 'p.houise@gmail.com', 0, '', '', 'Pablo', 'Houise', 'm', 'fr', 'Lyon', '1992-07-13 00:00:00', '', '', 'http://g.bf3stats.com/ps3/dNP0ergV/Ryukio.png', '30.jpg', 'APAX UN JOUR', '', '', '', 'APAX TOUJOURS', '', '', '', '', '', '', '', '', '', '', 1, '', 'Le Saiga 12 K et pour moi LA SOLUTION...\r\n\r\n\r\n\r\n\r\nFacebook : Panlo-Chuck Norris Houise\r\nTwitter : Pablohou\r\nSkype : Pablo.houise1', 1, 0, 7, NULL, '', '77.201.31.163', '|24|23|18|7|24|17|2|25|9|26|27|14|1|28|29|30|15|31|32|33|34|36|37|38|39|40|41|42|44|45|46|47|48|49|50|51|52|53|55|56|57|58|59|60|61|62|63|67|68|69|70|71|72|73|74|75|76|77|78|79|80|81|82|83|84|85|86|87|88|89|91|92|93|94|95|96|97|21|98|99|100|101|102|103|104|105|106|107|108|109|3|110|111|112|113|114|117|115|118|119|120|121|122|123|124|', '', '', '', '', 1, '', '1', 'fr', ''),
(31, 1363794908, 1363794908, 'Lucas-74_44', '6043bbab2f9d18e5dd171fae7baeef45', 'Lucas-74_44', 'chavanneb@wanadoo.fr', 1, '', '', '', '', 'u', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', '', 1, 0, 0, NULL, '', '', '24|23|18|7|24|17|2|25|9|26|27|14|1|28|29|21|30|15|31|32|33|34|36|37|38|39|40|41|42|43|44|45|46|47|48|49|50|51|52|53|55|56|57|58|59|60|61|62|63|67|68|69|70|71|72|73|74|75|76|77|78|79|80|81|82|83|84|85|86|87|88|89|90|91|92|93|94|95|96|97|98|99|100|101|102|103|104|105|106|107|108|109|3|110|111|112|113|114|115|117|118|119|120|121|122|123|124|', '', '', '', '', 0, '', '1', '', ''),
(32, 1363797029, 1363799224, 'Cindy', 'c35b3aca7406b3c04b91eb8df4d9512b', 'ZeaCross', 'zeacross@hotmail.fr', 1, '', '', 'Cindy', '', 'f', 'fr', '', '1994-05-28 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', '', 1, 0, 1, NULL, '', '89.170.217.165', '23|18|7|24|23|17|2|25|9|26|27|14|1|28|29|21|30|15|31|32|33|34|36|37|38|39|40|41|42|43|44|45|46|47|48|49|50|51|52|53|55|56|57|58|59|60|61|62|63|67|68|69|70|71|72|73|74|75|76|77|78|79|80|81|82|83|84|85|86|87|88|89|90|91|92|93|94|95|96|97|98|99|100|101|102|103|104|105|106|107|108|109|3|110|111|112|113|114|115|117|118|119|120|121|122|123|124|', '', '', '', '', 0, '', '1', 'fr', ''),
(33, 1363867372, 1363867372, 'TweeZy', 'a2e2f0647a408de45e1047a04d932eca', 'TweeZy_D-Ace', 'swageur10@gmail.com', 1, '', '', '', '', 'u', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', '', 0, 0, 2, 'perm', 'Double compte avec TweeZy', '', '9|9|25|24|26|7|27|2|14|1|28|29|21|30|15|31|32|33|34|36|37|38|39|40|41|42|43|44|45|46|47|48|49|50|51|52|53|55|56|57|58|59|60|61|62|63|67|68|69|70|71|72|73|74|75|76|77|78|79|80|81|82|83|84|85|86|87|88|89|90|91|92|93|94|95|96|97|98|99|100|101|102|103|104|105|106|107|108|109|3|110|111|112|113|114|115|117|118|119|120|121|122|123|124|', '', '', '', '', 0, '', '1', '', ''),
(34, 1363868198, 1368268647, 'TweeZy_D-Ace', '2db88a09437d78c2392dff26d90e9701', 'TweeZy', 'pti.dejeuner@gmail.com', 1, '', '', 'Alexis', '', 'm', 'fr', 'Troyes', '1993-10-16 00:00:00', '06 25 45 88 21', '34.jpg', '[url=http://bf3stats.com/stats_ps3/TweeZy_D-Ace][img]http://g.bf3stats.com/ps3/a7dNle3F/TweeZy_D-Ace.png[/img][/url]\r\n[url=http://bf3stats.com/stats_ps3/TweeZy_D-Ace][img]http://g.bf3stats.com/ps3/ykASDAB1/TweeZy_D-Ace.png[/img][/url]', '34.png', 'APAX', 'La meute APAX', 'http://meute-apax.fr/site/index.php', '', 'Never Die', '', '', '', '', '', '', '', '', '', '', 1, 'www.facebook.com/TweezyOfficiel', 'Joueur Sniper sur Battlefield 3, Black Ops I , Black Ops II , Medal Of Honor Warfighter ;D', 4, 4, 6, NULL, '', '90.18.223.111', '|105|106|101|112|103|111|114|115|113|56|117|3|118|119|70|71|120|84|121|21|59|122|123|124|', '', '', '', '', 0, '', '1', 'fr', ''),
(35, 1363898498, 1365267397, 'Ankoleloup', '0f82f29ab14f9b5ba15c42b58f0e805c', 'Ankoleloup', 'blanco.p@gmx.fr', 1, '', '', '', '', 'u', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', '', 2, 1, 4, NULL, '', '78.234.56.29', '|27|27|2|25|14|7|26|1|28|29|21|30|31|32|33|34|36|37|38|39|40|41|15|42|43|44|45|46|47|48|49|50|51|52|53|55|56|57|58|59|60|61|62|63|67|68|69|70|71|72|73|74|75|76|77|78|79|80|81|82|83|84|85|86|87|88|89|90|91|92|93|94|95|96|97|98|99|100|101|102|103|104|105|106|107|108|109|3|110|111|112|113|114|115|117|118|119|120|121|122|123|124|', '', '', '', '', 0, '', '1', '', ''),
(36, 1363943005, 1363943005, 'ousou78', '1307586a4dea76f97a77e999d307082b', 'ousou78', 'ousou78@live.fr', 1, '', '', '', '', 'u', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', '', 1, 0, 0, NULL, '', '93.10.191.35', '1|14|1|27|25|7|29|28|26|2|21|30|15|31|32|33|34|36|37|38|39|40|41|42|43|44|45|46|47|48|49|50|51|52|53|55|56|57|58|59|60|61|62|63|67|68|69|70|71|72|73|74|75|76|77|78|79|80|81|82|83|84|85|86|87|88|89|90|91|92|93|94|95|96|97|98|99|100|101|102|103|104|105|106|107|108|109|3|110|111|112|113|114|115|117|118|119|120|121|122|123|124|', '', '', '', '', 0, '', '1', '', ''),
(37, 1363970094, 1368948149, 'toutoun80740', 'aaa49567588ed2a026b87aef0fa67e96', 'toutoun80740', 'bibu5689@hotmail.fr', 1, '', '', 'anthony', 'delanchy', 'm', 'fr', 'Saint-Quentin', '1989-06-05 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', '', 1, 0, 7, NULL, '', '93.5.224.233', '|25|27|1|2|26|33|37|39|21|40|41|28|44|32|45|29|46|14|47|48|49|38|51|53|52|55|57|63|62|67|34|68|69|70|71|72|73|74|75|78|50|80|81|83|86|82|88|89|90|91|77|95|96|92|97|98|85|99|79|101|104|105|106|108|107|102|100|103|109|110|112|111|114|118|119|120|121|122|117|123|124|113|', '', '', '', '', 0, '', '1', 'fr', ''),
(38, 1364057788, 1364057788, 'louis', '15de722f94a77fd7b8f4318cfee6f2f7', 'killeurdu73', 'isabelle.cornuty@sfr.fr', 1, '', '', '', '', 'u', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', '', 0, 0, 0, NULL, '', '', '2|21|2|30|14|29|26|15|31|32|33|34|28|36|37|38|39|40|41|42|43|44|45|46|47|48|49|25|50|51|52|27|53|55|56|57|58|59|60|61|62|63|67|68|69|70|71|72|73|74|75|76|77|78|79|80|81|82|83|84|85|86|87|88|89|90|91|92|93|94|95|96|97|98|99|100|101|102|103|104|105|106|107|108|109|3|110|111|112|113|114|115|117|118|119|120|121|122|123|124|', '', '', '', '', 0, '', '1', '', ''),
(42, 1364545430, 1364585174, 'PATRICK', '4b71c142758b80cc915e5ea1e1d04564', 'golf081961', 'patrick.siau@hotmail.com', 1, '', '', '', '', 'u', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', '', 0, 0, 0, NULL, '', '85.27.91.147', '33|37|14|33|38|32|30|26|29|15|34|39|21|40|41|42|28|43|44|45|46|47|48|49|25|50|51|52|27|53|55|56|57|58|59|60|61|62|63|67|68|69|70|71|72|73|74|75|76|77|78|79|80|81|82|83|84|85|86|87|88|89|90|91|92|93|94|95|96|97|98|99|100|101|102|103|104|105|106|107|108|109|3|110|111|112|113|114|115|117|118|119|120|121|122|123|124|', '', '', '', '', 0, '', '1', '', ''),
(43, 1364545731, 1368176766, 'Simrreuhh', '3a1da011b93d574272b4fb27ed14b0b5', 'Simrreuhh', 'simrreuhh@hotmail.fr', 1, '', '', 'Simon', '', 'm', 'fr', 'Bordeaux', '1990-09-19 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', '', 0, 0, 4, NULL, '', '89.80.94.7', '|33|38|30|29|14|15|34|39|41|42|28|43|44|45|46|48|49|25|51|52|27|53|57|50|58|59|60|61|62|63|67|68|69|70|71|72|74|75|76|77|78|79|81|82|83|84|86|87|88|89|92|93|94|91|85|95|80|96|97|98|99|100|101|102|104|105|106|107|108|109|3|110|56|103|112|113|111|114|115|117|118|119|120|121|21|122|123|124|', '', '', '', '', 0, '', '1', 'fr', ''),
(46, 1364814986, 1364814986, 'Favelas(Sami)', '5583413443164b56500def9a533c7c70', 'favelas(sami)', 'samigrosso@yahoo.fr', 1, '', '', '', '', 'u', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', '', 0, 0, 2, NULL, '', '', '38|32|38|37|29|14|15|34|39|21|40|41|42|28|33|43|44|45|46|47|48|49|25|50|51|52|27|53|55|56|57|58|59|60|61|62|63|67|68|69|70|71|72|73|74|75|76|77|78|79|80|81|82|83|84|85|86|87|88|89|90|91|92|93|94|95|96|97|98|99|100|101|102|103|104|105|106|107|108|109|3|110|111|112|113|114|115|117|118|119|120|121|122|123|124|', '', '', '', '', 0, '', '1', '', ''),
(47, 1364821353, 1364821353, 'Llukiox', '23046e47dc79e782acb6e1a5c86e09dd', 'Llukiox', 'lulu50g@hotmail.fr', 1, '', '', '', '', 'u', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', '', 0, 0, 0, NULL, '', '', '32|32|14|15|34|39|38|21|40|41|42|28|33|43|44|45|37|29|46|47|48|49|25|50|51|52|27|53|55|56|57|58|59|60|61|62|63|67|68|69|70|71|72|73|74|75|76|77|78|79|80|81|82|83|84|85|86|87|88|89|90|91|92|93|94|95|96|97|98|99|100|101|102|103|104|105|106|107|108|109|3|110|111|112|113|114|115|117|118|119|120|121|122|123|124|', '', '', '', '', 0, '', '1', '', ''),
(48, 1364890906, 1364890906, 'horaceyysy', '862e34370f8b5ca89af84d946c8a4d16', 'horaceyysy', 'sltsky30@gmail.com', 1, '', '', '', '', 'u', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', '', 0, 0, 0, NULL, '', '', '14|15|34|39|38|21|32|14|40|41|42|28|33|43|44|45|37|29|46|47|48|49|25|50|51|52|27|53|55|56|57|58|59|60|61|62|63|67|68|69|70|71|72|73|74|75|76|77|78|79|80|81|82|83|84|85|86|87|88|89|90|91|92|93|94|95|96|97|98|99|100|101|102|103|104|105|106|107|108|109|3|110|111|112|113|114|115|117|118|119|120|121|122|123|124|', '', '', '', '', 0, '', '1', '', ''),
(49, 1364904920, 1364904920, 'jus9870x97', '775474565a858bd98f9ad885b235e230', 'jus9870x97', 'v89dsdd@126.com', 1, '', '', '', '', 'u', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', '', 0, 0, 1, NULL, '', '', '34|34|15|39|38|21|32|14|40|41|42|28|33|43|44|45|37|29|46|47|48|49|25|50|51|52|27|53|55|56|57|58|59|60|61|62|63|67|68|69|70|71|72|73|74|75|76|77|78|79|80|81|82|83|84|85|86|87|88|89|90|91|92|93|94|95|96|97|98|99|100|101|102|103|104|105|106|107|108|109|3|110|111|112|113|114|115|117|118|119|120|121|122|123|124|', '', '', '', '', 0, '', '1', '', ''),
(50, 1364914960, 1365717271, 'romain', 'a5a196237a1c50de45fc8423a03edef2', 'XxCrAzYyKiLLeRxX', 'romainvaco@hotmail.fr', 1, '', '', '', '', 'u', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', '', 0, 0, 1, NULL, '', '78.228.126.217', '|15|38|15|21|32|14|41|42|28|33|43|44|45|37|29|46|47|48|49|25|40|50|51|52|27|53|55|56|57|58|59|60|34|61|62|63|67|68|69|70|71|72|73|74|75|76|77|78|79|80|81|82|83|84|85|86|87|88|89|90|91|92|93|94|95|96|97|98|99|100|101|102|103|104|105|106|107|108|109|3|110|111|112|113|114|115|117|118|119|120|121|122|123|124|', '', '', '', '', 0, '', '1', '', ''),
(51, 1364954121, 1364954121, 'Nat11', '0192023a7bbd73250516f069df18b500', 'Nat11', 'subsidyerf@gmail.com', 1, '', '', '', '', 'u', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', '', 0, 0, 0, NULL, '', '', '15|32|14|15|40|41|38|42|28|33|43|44|45|37|29|46|47|48|49|25|50|51|52|27|53|55|56|57|58|59|60|34|61|62|63|67|68|69|70|71|72|73|74|75|76|77|78|79|80|81|82|83|84|85|86|87|88|89|90|91|92|93|94|95|96|97|21|98|99|100|101|102|103|104|105|106|107|108|109|3|110|111|112|113|114|115|117|118|119|120|121|122|123|124|', '', '', '', '', 0, '', '1', '', ''),
(61, 1365593525, 1365593525, 'Lady_Caca', '34b6c95e032fdafa7abbbc23351d758f', 'Lady_Caca', 'poupou.jdhe@laposte.net', 1, '', '', '', '', 'u', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', '', 0, 0, 2, NULL, '', '', '15|44|14|15|33|32|42|45|38|37|29|46|47|48|49|25|40|50|51|52|27|53|55|56|57|58|59|60|34|61|62|63|67|68|69|70|71|72|73|74|75|76|77|78|79|80|81|82|83|84|85|86|87|88|89|90|91|92|93|94|95|96|97|21|98|99|100|101|102|103|104|105|106|107|108|109|3|110|111|112|113|114|115|117|118|119|120|121|122|123|124|', '', '', '', '', 0, '', '1', '', ''),
(65, 1365765038, 1365765038, 'quentin', 'a74cd4f28168ec19be34acebef17a61e', 'quuentin247', 'quentin247@live.fr', 1, '', '', '', '', 'u', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', '', 0, 0, 1, NULL, '', '', '33|15|33|32|45|38|37|29|14|46|42|47|48|49|25|40|50|51|52|27|53|55|56|57|58|59|60|34|61|62|63|67|68|69|70|71|72|73|74|75|76|77|78|79|80|81|82|83|84|85|86|87|88|89|90|91|92|93|94|95|96|97|21|98|99|100|101|102|103|104|105|106|107|108|109|3|110|111|112|113|114|115|117|118|119|120|121|122|123|124|', '', '', '', '', 0, '', '1', '', ''),
(68, 1366042928, 1366042928, 'akim67', '2a2c14970219b3ed89d7b6222c90e93f', 'akkim67', 'nassim-67200@live.fr', 1, '', '', '', '', 'u', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', '', 0, 0, 2, NULL, '', '', '47|47|14|48|49|25|40|50|29|38|51|37|52|27|53|55|56|57|58|59|60|34|61|62|63|67|68|69|70|71|72|73|74|75|76|77|78|79|80|81|82|83|84|85|86|87|88|89|90|91|92|93|94|95|96|97|21|98|99|100|101|102|103|104|105|106|107|108|109|3|110|111|112|113|114|115|117|118|119|120|121|122|123|124|', '', '', '', '', 0, '', '1', '', ''),
(71, 1366136055, 1366824106, 'psyko', '64258b62eb7216c088ce2c5b16a2a6a8', 'psyko', 'tapiscrochetdu01@orange.fr', 1, '', '', '', '', 'u', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', '', 0, 0, 1, NULL, '', '92.137.231.136', '|29|25|50|38|51|37|29|48|14|52|27|53|55|56|57|58|59|60|34|61|62|63|67|68|69|70|71|40|72|73|74|75|76|77|78|79|80|81|82|83|84|85|86|87|88|89|90|91|92|93|94|95|96|97|21|98|99|100|101|102|103|104|105|106|107|108|109|3|110|111|112|113|114|115|117|118|119|120|121|122|123|124|', '', '', '', '', 0, '', '1', '', ''),
(73, 1366239913, 1366413063, 'nono770firedan', '35195b927f83b808a12fc0645fa63561', 'nono770firedan', 'styck-du77@hotmail.fr', 1, '', '', '', '', 'u', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', '', 0, 0, 0, NULL, '', '176.180.92.96', '|14|48|50|51|38|40|27|53|37|52|25|55|56|57|58|59|60|34|61|62|63|67|68|69|70|71|72|73|74|75|76|77|78|79|80|81|82|83|84|85|86|87|88|89|90|91|92|93|94|95|96|97|21|98|99|100|101|102|103|104|105|106|107|108|109|3|110|111|112|113|114|115|117|118|119|120|121|122|123|124|', '', '', '', '', 0, '', '1', '', ''),
(75, 1366495191, 1368440532, 'hasma_tik', '96cbf5e894a7d2420f1ca2c706dde3f6', 'hasma_tik', 'hasma801@hotmail.fr', 1, '', '', '', '', 'u', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', '', 0, 0, 1, NULL, '', '92.142.64.218', '|50|37|57|52|58|59|60|34|61|62|63|67|68|69|70|71|40|72|74|75|76|77|78|79|80|81|82|83|84|85|86|87|88|89|90|91|92|93|94|95|96|97|21|98|99|100|101|102|103|104|105|106|107|108|109|3|110|111|112|56|113|114|115|117|118|119|120|121|122|123|124|', '', '', '', '', 0, '', '1', '', ''),
(76, 1366603955, 1366603955, 'Liane7Porter', '56e424366a2bb44727f0a05fa5008f4c', 'Liane7Porter', 'fpat24@pub-mail.com', 1, '', '', '', '', 'u', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', '', 0, 0, 1, NULL, '', '', '37|38|56|50|37|57|52|58|59|60|34|61|62|63|67|68|69|70|71|40|72|73|74|75|76|77|78|79|80|81|82|83|84|85|86|87|88|89|90|91|92|93|94|95|96|97|21|98|99|100|101|102|103|104|105|106|107|108|109|3|110|111|112|113|114|115|117|118|119|120|121|122|123|124|', '', '', '', '', 0, '', '1', '', ''),
(81, 1366900046, 1368727302, 'CiiDzZ-HD', '78ef7bf39099e83864be0de11559116c', 'CiiDzZ-HD', 'sicilianno35@hotmail.fr', 1, '', '', 'Cedric ', '', 'm', 'fr', '', '1992-08-03 00:00:00', '', '81.jpg', 'Ils ont dit que j''étais mort que j''avais péri , je vous répond que je suis fort et que je suis guéri. CiiDzZ\r\n\r\n[url=http://bf3stats.com/stats_ps3/CiiDzZ_HD-][img]http://g.bf3stats.com/ps3/EoGX6sMr/CiiDzZ_HD-.png[/img][/url]', '81.jpg', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', '', 0, 0, 0, NULL, '', '78.124.161.77', '|34|69|62|72|61|71|78|79|86|89|90|91|92|95|96|97|94|81|100|104|76|50|105|106|107|102|108|3|110|112|113|115|56|117|101|114|118|119|120|84|121|21|122|103|123|124|', '', '', '', '', 0, '', '1', 'fr', ''),
(83, 1366973125, 1366982106, 'creative dragon', '0eadf7d77170f6cc11510eddfc397f85', 'creative dragon', 'team.red_dragons@yahoo.fr', 1, '', '', '', '', 'u', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', '', 0, 0, 0, NULL, '', '84.102.143.76', '|59|59|60|56|34|50|61|62|63|67|68|69|70|71|40|72|73|74|75|76|77|78|79|80|81|82|83|84|85|86|87|88|89|90|91|92|93|94|95|96|97|21|98|99|100|101|102|103|104|105|106|107|108|109|3|110|111|112|113|114|115|117|118|119|120|121|122|123|124|', '', '', '', '', 0, '', '1', '', ''),
(84, 1366986252, 1367877295, 'choub', 'eb0e9e1f3b68a7dde4220b55b5583892', 'choub', 'chub_chub_62@hotmail.com', 1, '', '', '', '', 'u', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', '', 0, 0, 0, NULL, '', '78.222.104.201', '|60|59|60|56|34|50|61|62|63|67|68|69|70|71|40|72|73|74|75|76|77|78|79|80|81|83|84|85|86|87|88|89|90|91|92|93|95|96|97|21|98|99|100|101|102|103|104|105|106|107|108|109|3|110|111|112|113|114|115|117|118|119|120|121|122|123|124|', '', '', '', '', 0, '', '1', '', ''),
(86, 1367061734, 1367579982, 'hugo_AK47_95', '010283adcfc3770abfcf6790cec33243', 'hugo_AK47_95', 'hugoboss78360@live.fr', 1, '', '', '', '', 'u', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', '', 0, 0, 1, NULL, '', '109.9.73.17', '|59|50|34|67|68|69|70|40|72|73|74|75|76|71|78|79|77|80|81|82|83|84|85|86|87|88|89|90|91|92|93|94|95|96|97|21|98|99|100|101|102|103|104|56|105|106|107|108|109|3|110|111|112|113|114|115|117|118|119|120|121|122|123|124|', '', '', '', '', 0, '', '1', '', ''),
(87, 1367062895, 1368615625, 'tarty', 'ec581e3197487ae5759793bc8d38b561', 'tarty51', 'jissaye51@hotmail.fr', 1, '', '', 'Jean-christophe', 'Tarty', 'm', 'fr', 'Chalons', '1990-12-03 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', 'Pgm', 0, 0, 0, NULL, '', '31.38.51.150', '|61|59|50|34|62|68|69|70|71|40|72|73|74|75|76|67|77|78|79|80|81|82|83|84|85|87|88|89|90|91|86|92|93|94|95|96|97|21|98|99|100|101|102|103|104|56|105|106|107|108|109|3|110|111|112|113|114|115|117|118|119|120|121|122|123|124|', '', '', '', '', 0, '', '1', 'fr', ''),
(88, 1367064051, 1368746786, 'alexx93190', '31963c2e9de30160c0be8bd0fde816fa', 'alexx93190', 'alexxdu93@hotmail.fr', 1, '', '', 'alexandre', '', 'm', 'fr', '', '1991-06-21 00:00:00', '', '', '[url=http://bf3stats.com/stats_ps3/alexx93190][img]http://g.bf3stats.com/ps3/TlL6s1NA/alexx93190.png[/img][/url]', '', 'APAX', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', '', 0, 0, 0, NULL, '', '83.155.242.10', '|50|34|50|62|63|59|67|69|40|72|73|74|75|76|61|77|78|79|80|81|82|83|84|85|86|87|88|89|90|92|93|94|96|97|21|98|99|101|102|103|104|56|105|106|107|108|109|3|110|111|114|115|112|118|119|120|70|121|122|113|117|71|123|124|', '', '', '', '', 1, '', '1', 'fr', ''),
(89, 1367064502, 1368986605, 'slimmorle', '3f0f1fdda4f894994a95750aba3d25a6', 'slimmorle', 'lemortcyril@hotmail.fr', 1, '', '', 'CYRIL', 'lemort', 'm', 'fr', 'dunkerque', '1986-07-22 00:00:00', '', '89.png', '\r\n[url=http://bf3stats.com/stats_ps3/slimmorle][img]http://g.bf3stats.com/ps3/8KAmD5ol/slimmorle.png[/img][/url]', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', 'bonjour de tous moi je suis cyril alias slimmorle\r\nj ai 26 ans ancien leadeur de la team gfb devenu un loup lol met passion dans la vie sont met enfants, la pèche la musique et les jeux vidéo et particulièrement la play grand fans de bf voila a peu prés se qu il faut savoir sur moi merci a tous', 0, 0, 3, NULL, '', '83.155.252.229', '|63|69|70|73|74|80|87|88|21|99|94|67|79|102|104|100|105|106|107|108|56|101|114|121|118|84|123|124|120|122|', '', '', '', '', 1, '', '1', 'fr', ''),
(90, 1367074681, 1367074681, 'max', '5cb38d6556a06a9ac6f0803a3f9fcff9', 'aventador-67', 'm.rialti@orange.fr', 1, '', '', '', '', 'u', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', '', 0, 0, 0, NULL, '', '', '63|62|63|59|61|67|34|68|69|70|71|40|72|50|73|74|75|76|77|78|79|80|81|82|83|84|85|86|87|88|89|90|91|92|93|94|95|96|97|21|98|99|100|101|102|103|104|56|105|106|107|108|109|3|110|111|112|113|114|115|117|118|119|120|121|122|123|124|', '', '', '', '', 0, '', '1', '', ''),
(91, 1367096332, 1367661495, 'kevin', 'e3d7f45cc5bf10b1eb67f2372d5400a9', 'kevcha0205', 'whyplash@hotmail.fr', 1, '', '', '', '', 'u', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', '', 0, 0, 0, NULL, '', '81.244.112.192', '|68|69|62|63|70|71|73|74|75|80|83|84|86|87|82|81|88|89|90|77|93|94|21|99|92|79|85|98|100|67|101|102|103|104|56|76|50|105|106|107|108|109|3|110|111|112|113|114|115|117|118|119|120|121|59|122|123|124|', '', '', '', '', 0, '', '1', '', ''),
(92, 1367157927, 1367258225, 'jojo', 'cc46f9fbb3073dc8b271704543190553', 'wolf16000', 'dofusjojo@live.fr', 1, '', '', '', '', 'u', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', '', 0, 0, 0, NULL, '', '62.147.188.216', '|72|72|50|73|75|40|76|67|62|61|71|77|78|79|80|81|82|83|84|85|86|87|88|89|90|91|92|93|94|95|96|97|21|98|99|100|101|102|103|104|56|105|106|107|108|109|3|110|111|112|113|114|115|117|118|119|70|120|121|59|122|123|124|', '', '', '', '', 0, '', '1', '', ''),
(93, 1367180119, 1368869909, 'shuya', 'bacf0a7d80521c15544b2ddf02d8bc5c', 'shuya', 'roxaneboudot@gmail.com', 1, '', '', 'roxane', '', 'f', 'fr', '', '1984-03-06 00:00:00', '', '93.jpg', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', '', 0, 0, 2, NULL, '', '88.167.3.244', '|40|72|63|61|71|78|79|82|88|89|90|91|86|92|84|83|87|93|94|95|96|97|77|85|98|99|100|104|101|107|102|108|56|103|76|109|111|112|113|114|115|117|118|119|70|120|121|21|59|123|124|122|', '', '', '', '', 0, '', '1', 'fr', ''),
(94, 1367223618, 1367653586, 'Weapons345', 'ccfb77278908b07ef49af6cbb9e16c6a', 'weapons345', 'M.brogniart75@gmail.com', 1, '', '', 'Maxime', 'Brogniart', 'm', 'al', 'Paris', '1997-07-25 00:00:00', '', '', '"J''ai tellement d''ambition qu''l''avenir me craint"', '94.png', '', '', '', '', '', 'Intel Core i3', '', '4gb ', 'Pc portable ^^', 'Ati Radeon', '', '', '', '', '', 1, '', 'J''habite a Paris, Je suis au lycée, en 2nd Bac Pro :)', 0, 0, 1, NULL, '', '82.225.143.59', '|72|50|78|76|79|80|81|82|83|84|85|86|88|89|90|91|92|87|93|94|95|96|97|21|98|99|100|67|77|101|102|103|104|56|105|106|107|108|109|3|110|111|112|113|114|115|117|118|119|70|71|120|121|59|122|123|124|', '', '', '', '', 0, '', '1', 'fr', ''),
(95, 1367411150, 1367758831, 'xTyFlO', '0aecb24aa3f9efe9e6217c6ab1333953', 'xTyFlO', 'enzioo@hotmail.fr', 1, '', '', '', '', 'u', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', '', 0, 0, 0, NULL, '', '88.219.86.41', '|89|90|103|104|102|56|76|50|100|80|101|105|106|107|108|109|3|110|111|112|84|83|113|114|115|117|118|119|70|71|120|121|21|59|122|123|124|', '', '', '', '', 0, '', '1', '', ''),
(96, 1367422664, 1367763902, 'Galopin', '937d330973e8dd4c6451814ab03eb559', 'Galopin', 'toniocerruti@hotmail.fr', 1, '', '', '', '', 'u', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', '', 0, 0, 0, NULL, '', '88.166.108.213', '|80|80|78|81|82|83|84|85|86|89|90|91|92|77|93|94|95|96|97|21|98|99|79|100|67|101|102|103|104|56|76|50|105|106|107|108|109|3|110|111|112|113|114|115|117|118|119|70|71|120|121|59|122|123|124|', '', '', '', '', 0, '', '1', '', ''),
(97, 1367423120, 1367668086, 'Yosusu44', '882baf28143fb700b388a87ef561a6e5', 'Yosusu44', 'Yosusu44240@live.fr', 1, '', '', '', '', 'u', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', '', 0, 0, 1, NULL, '', '92.135.187.117', '|80|80|78|81|82|85|86|87|84|83|88|89|90|91|92|77|93|94|95|96|97|21|98|99|79|100|67|101|102|103|104|56|76|50|105|106|107|108|109|3|110|111|112|113|114|115|117|118|119|70|71|120|121|59|122|123|124|', '', '', '', '', 0, '', '1', '', ''),
(99, 1367487730, 1367495741, 'Eiiwox', 'a427d297f692a591d63ad3e6203e6283', 'Eiiwox', 'eiiwox.officiel@gmail.com', 1, '', '', '', '', 'u', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', '', 0, 0, 1, NULL, '', '83.199.103.197', '|87|85|80|86|84|82|83|88|81|89|90|91|92|77|93|94|95|96|97|21|98|99|79|100|67|101|102|103|104|56|76|50|105|106|107|108|109|3|110|111|112|113|114|115|117|118|119|70|71|120|121|59|122|123|124|', '', '', '', '', 0, '', '1', '', ''),
(100, 1367488893, 1368978252, 'CRIXUS LE GAULOIS INVAINCU', 'd494c9d3f8c8d12c7afc830121edc370', 'x-lE-GAULOIS', 'Torresromain9@gmail.com', 1, '', '', '', '', 'u', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', '', 0, 0, 1, NULL, '', '88.173.218.29', '|85|86|87|80|82|83|88|81|89|90|91|92|77|93|94|95|96|97|98|99|79|100|67|101|102|104|76|50|105|106|107|108|109|3|110|111|112|113|114|115|117|118|119|70|71|120|121|21|59|122|123|124|', '', '', '', '', 0, '', '1', '', ''),
(101, 1367491926, 1367695303, 'C83Zinio', '3b567b20868227c7e44041cdbee6bee1', 'C83Zinio', 'alexandre.alles83@gmail.com', 1, '', '', '', '', 'u', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', '', 0, 0, 0, NULL, '', '88.138.139.28', '|85|87|84|82|88|81|89|90|91|92|83|77|93|80|94|95|96|97|21|98|99|79|100|67|101|102|103|104|56|76|50|105|106|107|108|109|3|110|111|112|113|114|115|117|118|119|70|71|120|121|59|122|123|124|', '', '', '', '', 0, '', '1', '', ''),
(102, 1367501584, 1367753100, 'ArRuaCeiroOo', 'b6658812bfcd174773274e853f4bd2ce', 'ArRuaCeiroOo', 'rmartin62120@yahoo.fr', 1, '', '', '', '', 'u', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', '', 0, 0, 0, NULL, '', '109.23.42.110', '|85|80|85|87|86|84|83|88|89|90|91|92|77|93|95|96|97|98|99|79|21|100|67|101|102|103|104|56|76|50|105|106|107|108|109|3|110|111|112|113|114|115|117|118|119|70|71|120|121|59|122|123|124|', '', '', '', '', 0, '', '1', '', ''),
(103, 1367516545, 1367516545, 'mattlekillerdu64', 'ec2dab39aec7634f0b07ce8f4eada694', 'mattlekillerdu64', 'ludo-malikdu644@hotmail.fr', 1, '', '', '', '', 'u', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', '', 0, 0, 1, NULL, '', '', '86|84|86|88|81|89|90|91|82|92|83|87|77|93|85|80|94|95|96|97|21|98|99|79|100|67|101|102|103|104|56|76|50|105|106|107|108|109|3|110|111|112|113|114|115|117|118|119|70|71|120|121|59|122|123|124|', '', '', '', '', 0, '', '1', '', ''),
(104, 1367535700, 1367618134, 'Naistlalz', '5d50037ac890d8248c22e5bfd61f66c1', 'NoName_Naist', 'endlineminato@gmail.com', 1, '', '', '', '', 'u', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', '', 0, 0, 3, 'perm', 'Provocation et insultes inutiles ...', '82.67.54.232', '|88|89|90|91|86|82|92|84|83|87|93|85|80|94|95|96|97|77|21|98|99|79|81|100|67|101|102|103|104|56|76|50|105|106|107|108|109|3|110|111|112|113|114|115|117|118|119|70|71|120|121|59|122|123|124|', '', '', '', '', 0, '', '1', '', ''),
(105, 1367655978, 1367655978, 'irxiaow2009', '269db8f54224c924f53577684904f087', 'irxiaow2009', 'nieut47@outlook.com', 1, '', '', '', '', 'u', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', '', 0, 0, 0, NULL, '', '', '21|21|98|94|85|97|99|92|79|82|81|93|100|67|77|101|102|103|104|56|76|50|80|105|106|107|108|109|3|110|111|112|84|83|113|114|115|117|118|119|70|71|120|121|59|122|123|124|', '', '', '', '', 0, '', '1', '', ''),
(106, 1367760647, 1368970708, 'flapastex1996', '3cb285f3f3a9f9346fface982bc6b969', 'flav1996', 'flapastex1996@laposte.net', 1, '', '', '', '', 'u', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', '', 0, 0, 4, NULL, '', '92.146.94.253', '|104|50|100|80|101|105|106|107|108|109|3|111|112|113|114|115|117|118|119|120|121|21|84|71|123|124|122|', '', '', '', '', 0, '', '1', 'fr', ''),
(107, 1367783671, 1367783671, 'favel', '5583413443164b56500def9a533c7c70', 'favel', 'favelsami@yahoo.fr', 1, '', '', '', '', 'u', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', '', 0, 0, 1, NULL, '', '', '103|103|50|105|106|102|80|107|76|108|56|100|109|3|110|111|112|84|83|101|113|114|115|117|118|119|70|71|120|121|21|59|122|123|124|', '', '', '', '', 0, '', '1', '', ''),
(108, 1367787047, 1367855655, 'eraa', '78fbf032115a408a8b954a3f04223201', 'eraa', 'jacebeleren@live.fr', 1, '', '', '', '', 'u', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', '', 0, 0, 1, NULL, '', '86.196.18.245', '|103|50|105|106|102|80|107|76|108|56|100|109|3|110|111|112|84|83|101|113|114|115|117|118|119|70|71|120|121|21|59|122|123|124|', '', '', '', '', 0, '', '1', '', ''),
(109, 1367823362, 1367827484, 'carnation09', '83cc7b9f2e7d2c576c4c7a2d7d2b002a', 'carnation09', 'canmimi552@yahoo.com', 1, '', '', '', '', 'u', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', '', 0, 0, 2, 'perm', 'Bot postant des publicitées sur le site', '175.44.6.188', '105|106|103|50|102|80|107|76|108|56|100|109|3|110|111|112|84|83|101|113|114|115|117|118|119|70|71|120|121|21|59|122|123|124|', '', '', '', '', 0, '', '1', '', '');
INSERT INTO `user` (`userID`, `registerdate`, `lastlogin`, `username`, `password`, `nickname`, `email`, `email_hide`, `email_change`, `email_activate`, `firstname`, `lastname`, `sex`, `country`, `town`, `birthday`, `icq`, `avatar`, `usertext`, `userpic`, `clantag`, `clanname`, `clanhp`, `clanirc`, `clanhistory`, `cpu`, `mainboard`, `ram`, `monitor`, `graphiccard`, `soundcard`, `verbindung`, `keyboard`, `mouse`, `mousepad`, `newsletter`, `homepage`, `about`, `pmgot`, `pmsent`, `visits`, `banned`, `ban_reason`, `ip`, `topics`, `articles`, `demos`, `files`, `gallery_pictures`, `mailonpm`, `userdescription`, `activated`, `language`, `movies`) VALUES
(110, 1368871405, 1368873350, 'x-trem-_flach', 'c15140646a88b827f00b487c839224ff', 'x-trem-_flach', 'aureliendubois@hotmail.com', 1, '', '', '', '', 'u', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', '', 0, 0, 0, NULL, '', '78.129.90.211', '|84|103|71|123|124|113|120|122|', '', '', '', '', 0, '', '1', '', ''),
(111, 1368875822, 1368958791, 'hesfqxc450', 'c3dd66e69777ae33c1d8721ca2391e79', 'hesfqxc450', 'bbilligabeats@hotmail.com', 1, '', '', '', '', 'u', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', '', 0, 0, 0, 'perm', 'bot', '120.43.29.30', '84|103|71|84|123|124|113|120|122|', '', '', '', '', 0, '', '1', '', '');

-- --------------------------------------------------------

--
-- Structure de la table `user_forum_groups`
--

CREATE TABLE IF NOT EXISTS `user_forum_groups` (
  `usfgID` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL DEFAULT '0',
  `1` int(1) NOT NULL,
  `2` int(1) NOT NULL,
  PRIMARY KEY (`usfgID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=45 ;

--
-- Contenu de la table `user_forum_groups`
--

INSERT INTO `user_forum_groups` (`usfgID`, `userID`, `1`, `2`) VALUES
(1, 1, 1, 1),
(2, 2, 0, 1),
(3, 4, 0, 1),
(4, 3, 0, 1),
(5, 5, 0, 1),
(6, 10, 0, 1),
(7, 6, 0, 1),
(8, 11, 0, 1),
(9, 8, 0, 1),
(10, 13, 0, 1),
(11, 9, 0, 1),
(12, 12, 0, 1),
(13, 7, 0, 1),
(14, 15, 0, 1),
(15, 17, 0, 1),
(16, 19, 0, 0),
(17, 18, 0, 1),
(18, 24, 0, 1),
(19, 25, 0, 0),
(20, 26, 0, 1),
(21, 27, 0, 1),
(22, 28, 0, 1),
(23, 32, 0, 0),
(24, 29, 0, 1),
(25, 16, 0, 1),
(26, 31, 0, 1),
(27, 36, 0, 1),
(28, 35, 0, 1),
(29, 34, 0, 1),
(30, 37, 0, 1),
(31, 30, 0, 1),
(32, 46, 0, 1),
(33, 22, 1, 1),
(34, 21, 0, 1),
(35, 43, 0, 1),
(36, 89, 0, 1),
(37, 86, 0, 1),
(38, 88, 0, 1),
(39, 87, 0, 1),
(40, 91, 0, 0),
(41, 14, 0, 1),
(42, 95, 0, 0),
(43, 75, 0, 1),
(44, 100, 0, 1);

-- --------------------------------------------------------

--
-- Structure de la table `user_gbook`
--

CREATE TABLE IF NOT EXISTS `user_gbook` (
  `userID` int(11) NOT NULL DEFAULT '0',
  `gbID` int(11) NOT NULL AUTO_INCREMENT,
  `date` int(14) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `hp` varchar(255) NOT NULL DEFAULT '',
  `icq` varchar(255) NOT NULL DEFAULT '',
  `ip` varchar(255) NOT NULL DEFAULT '',
  `comment` text NOT NULL,
  PRIMARY KEY (`gbID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `user_gbook`
--

INSERT INTO `user_gbook` (`userID`, `gbID`, `date`, `name`, `email`, `hp`, `icq`, `ip`, `comment`) VALUES
(5, 1, 1361810959, 'benftwc', '', '', '', '78.193.218.193', 'Le zboub vaincra !');

-- --------------------------------------------------------

--
-- Structure de la table `user_groups`
--

CREATE TABLE IF NOT EXISTS `user_groups` (
  `usgID` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL DEFAULT '0',
  `news` int(1) NOT NULL DEFAULT '0',
  `news_writer` int(1) NOT NULL,
  `newsletter` int(1) NOT NULL DEFAULT '0',
  `polls` int(1) NOT NULL DEFAULT '0',
  `forum` int(1) NOT NULL DEFAULT '0',
  `moderator` int(1) NOT NULL DEFAULT '0',
  `clanwars` int(1) NOT NULL DEFAULT '0',
  `feedback` int(1) NOT NULL DEFAULT '0',
  `user` int(1) NOT NULL DEFAULT '0',
  `page` int(1) NOT NULL DEFAULT '0',
  `files` int(1) NOT NULL DEFAULT '0',
  `cash` int(1) NOT NULL DEFAULT '0',
  `gallery` int(1) NOT NULL,
  `super` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`usgID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=113 ;

--
-- Contenu de la table `user_groups`
--

INSERT INTO `user_groups` (`usgID`, `userID`, `news`, `news_writer`, `newsletter`, `polls`, `forum`, `moderator`, `clanwars`, `feedback`, `user`, `page`, `files`, `cash`, `gallery`, `super`) VALUES
(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(2, 2, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(4, 4, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(5, 5, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(6, 6, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(9, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(10, 10, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(14, 14, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(15, 15, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(16, 16, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(17, 17, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(18, 18, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(19, 19, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(20, 20, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(21, 11, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(22, 21, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(23, 22, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(25, 24, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(26, 25, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(27, 26, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(28, 27, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(29, 28, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(30, 29, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(31, 30, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(32, 31, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(33, 32, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(34, 33, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(35, 34, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(36, 35, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(37, 36, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(38, 37, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(39, 38, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(43, 42, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(44, 43, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(47, 46, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(48, 47, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(49, 48, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(50, 49, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(51, 50, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(52, 51, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(62, 61, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(66, 65, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(69, 68, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(74, 73, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(76, 75, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(77, 76, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(84, 83, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(85, 84, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(87, 86, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(88, 87, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(89, 88, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(90, 89, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(91, 90, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(93, 92, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(94, 93, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(95, 94, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(97, 96, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(98, 97, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(100, 99, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(101, 100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(102, 101, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(103, 102, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(104, 103, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(105, 104, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(106, 105, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(107, 106, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(108, 107, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(109, 108, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(110, 109, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(111, 110, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(112, 111, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `user_visitors`
--

CREATE TABLE IF NOT EXISTS `user_visitors` (
  `visitID` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL DEFAULT '0',
  `visitor` int(11) NOT NULL DEFAULT '0',
  `date` int(14) NOT NULL DEFAULT '0',
  PRIMARY KEY (`visitID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=155 ;

--
-- Contenu de la table `user_visitors`
--

INSERT INTO `user_visitors` (`visitID`, `userID`, `visitor`, `date`) VALUES
(1, 1, 5, 1361694315),
(2, 6, 5, 1361694395),
(3, 7, 5, 1363517005),
(4, 4, 1, 1361891063),
(5, 8, 5, 1362901494),
(6, 9, 5, 1361706043),
(7, 5, 1, 1364928158),
(8, 10, 5, 1361737104),
(9, 11, 5, 1361782569),
(10, 12, 5, 1361910604),
(11, 13, 1, 1361896488),
(12, 13, 5, 1361982744),
(13, 6, 1, 1362044307),
(14, 10, 1, 1362044427),
(15, 2, 1, 1362044433),
(16, 5, 14, 1362158179),
(17, 14, 5, 1367605500),
(18, 15, 5, 1362249042),
(19, 8, 1, 1363899945),
(20, 5, 8, 1362388220),
(21, 17, 5, 1362423027),
(22, 1, 16, 1366026009),
(23, 7, 12, 1362589138),
(24, 18, 5, 1362599190),
(25, 19, 5, 1362599202),
(26, 19, 11, 1367778622),
(27, 1, 9, 1362672439),
(28, 22, 5, 1364989381),
(29, 9, 1, 1363104933),
(30, 1, 25, 1363279066),
(31, 1, 11, 1367778654),
(32, 11, 1, 1367845053),
(33, 11, 12, 1363391311),
(34, 10, 29, 1363704502),
(35, 29, 1, 1363707004),
(36, 5, 27, 1366107363),
(37, 16, 10, 1363784789),
(38, 1, 27, 1366107387),
(39, 5, 2, 1363867572),
(40, 33, 1, 1363870539),
(41, 17, 1, 1363900836),
(42, 10, 27, 1363950365),
(43, 17, 3, 1364026398),
(44, 5, 37, 1364148381),
(45, 20, 1, 1364199102),
(46, 37, 1, 1364218917),
(47, 20, 4, 1364241469),
(48, 32, 5, 1364462653),
(49, 8, 43, 1364546614),
(50, 1, 43, 1364555522),
(51, 43, 1, 1364559309),
(52, 35, 43, 1364559358),
(53, 1, 8, 1364649020),
(54, 43, 37, 1364656501),
(55, 26, 34, 1364998770),
(56, 4, 21, 1364814417),
(57, 10, 21, 1364770198),
(58, 2, 21, 1366228638),
(59, 5, 21, 1364770234),
(60, 1, 21, 1364770262),
(61, 28, 21, 1364770302),
(62, 35, 21, 1364770329),
(63, 9, 21, 1364770354),
(64, 30, 5, 1364797017),
(65, 34, 5, 1365867938),
(66, 37, 5, 1364797619),
(67, 16, 5, 1364797624),
(68, 25, 5, 1364797628),
(69, 45, 5, 1364798039),
(70, 40, 5, 1364798063),
(71, 23, 5, 1364798094),
(72, 19, 21, 1364816769),
(73, 46, 5, 1364817144),
(74, 25, 8, 1364829582),
(75, 49, 1, 1364914269),
(76, 5, 26, 1364931718),
(77, 5, 11, 1364946393),
(78, 26, 11, 1364946406),
(79, 5, 50, 1365001849),
(80, 1, 34, 1365097364),
(81, 54, 5, 1365235366),
(82, 52, 5, 1365097799),
(83, 5, 30, 1365414060),
(84, 2, 30, 1365414098),
(85, 27, 30, 1365414426),
(86, 5, 34, 1365961347),
(87, 61, 5, 1365613539),
(88, 62, 5, 1365669396),
(89, 61, 27, 1365669642),
(90, 59, 5, 1365754416),
(91, 30, 27, 1366107362),
(92, 21, 27, 1366107294),
(93, 65, 27, 1365940168),
(94, 2, 27, 1365941063),
(95, 2, 22, 1365945562),
(96, 8, 21, 1366240710),
(97, 25, 21, 1366065122),
(98, 68, 5, 1366116881),
(99, 34, 27, 1366296606),
(100, 50, 27, 1366107357),
(101, 11, 27, 1366910799),
(102, 8, 27, 1366107375),
(103, 43, 27, 1366107381),
(104, 37, 27, 1366107384),
(105, 9, 27, 1366107390),
(106, 35, 5, 1368347263),
(107, 69, 5, 1366116871),
(108, 71, 5, 1366136149),
(109, 70, 5, 1366305051),
(110, 72, 5, 1366305063),
(111, 30, 34, 1366211724),
(112, 21, 34, 1366211831),
(113, 27, 21, 1366240717),
(114, 30, 73, 1366393063),
(115, 37, 73, 1366412984),
(116, 75, 37, 1366498556),
(117, 37, 75, 1366540656),
(118, 76, 5, 1366619369),
(119, 89, 88, 1367070394),
(120, 89, 37, 1367072605),
(121, 25, 1, 1367161271),
(122, 4, 81, 1367518215),
(123, 1, 94, 1367235708),
(124, 93, 94, 1367262287),
(125, 100, 28, 1367490740),
(126, 86, 99, 1367494815),
(127, 5, 99, 1367494972),
(128, 97, 101, 1367510844),
(129, 99, 93, 1367524382),
(130, 14, 4, 1367525114),
(131, 19, 104, 1367536327),
(132, 10, 86, 1367579982),
(133, 89, 81, 1367581222),
(134, 103, 87, 1367585659),
(135, 104, 1, 1367679446),
(136, 104, 11, 1368727409),
(137, 93, 11, 1367783296),
(138, 94, 11, 1367783305),
(139, 108, 11, 1367793984),
(140, 109, 1, 1367828312),
(141, 1, 93, 1367867114),
(142, 10, 75, 1367870662),
(143, 15, 1, 1367932247),
(144, 106, 1, 1368085285),
(145, 109, 89, 1368124330),
(146, 1, 89, 1368176886),
(147, 28, 100, 1368196636),
(148, 106, 88, 1368200604),
(149, 5, 89, 1368443918),
(150, 11, 106, 1368710659),
(151, 106, 6, 1368723601),
(152, 107, 11, 1368727437),
(153, 106, 11, 1368830037),
(154, 5, 110, 1368872733);

-- --------------------------------------------------------

--
-- Structure de la table `whoisonline`
--

CREATE TABLE IF NOT EXISTS `whoisonline` (
  `time` int(14) NOT NULL DEFAULT '0',
  `ip` varchar(20) NOT NULL DEFAULT '',
  `userID` int(11) NOT NULL DEFAULT '0',
  `site` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `whoisonline`
--

INSERT INTO `whoisonline` (`time`, `ip`, `userID`, `site`) VALUES
(1368992670, '66.249.75.136', 0, 'forum_topic'),
(1368992871, '66.249.75.217', 0, 'forum_topic');

-- --------------------------------------------------------

--
-- Structure de la table `whowasonline`
--

CREATE TABLE IF NOT EXISTS `whowasonline` (
  `time` int(14) NOT NULL DEFAULT '0',
  `ip` varchar(20) NOT NULL DEFAULT '',
  `userID` int(11) NOT NULL DEFAULT '0',
  `site` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `whowasonline`
--

INSERT INTO `whowasonline` (`time`, `ip`, `userID`, `site`) VALUES
(1368987221, '', 5, 'news_comments'),
(1368989675, '', 11, 'forum_topic'),
(1368948149, '', 37, 'forum_topic'),
(1368986605, '', 89, 'forum_topic'),
(1368970708, '', 106, 'forum_topic'),
(1368978252, '', 100, 'forum'),
(1368986909, '', 8, 'forum'),
(1368990469, '', 10, 'forum_topic'),
(1368991768, '', 1, 'forum_topic'),
(1368958791, '', 111, 'forum'),
(1368988522, '', 25, 'forum_topic'),
(1368983795, '', 4, 'forum'),
(1368989975, '', 2, 'forum_topic');
